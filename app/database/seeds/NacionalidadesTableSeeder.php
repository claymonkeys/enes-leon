<?php

class NacionalidadesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('nacionalidades')->delete();

        Nacionalidad::create(array(
        'id' => '1', 
        'nacionalidad' => 'AFGANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '2', 
        'nacionalidad' => 'ALEMÁN(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '3', 
        'nacionalidad' => 'ÁRABE', 
        ));

        Nacionalidad::create(array(
        'id' => '4', 
        'nacionalidad' => 'ARGENTINO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '5', 
        'nacionalidad' => 'AUSTRALIANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '6', 
        'nacionalidad' => 'BELGA', 
        ));

        Nacionalidad::create(array(
        'id' => '7', 
        'nacionalidad' => 'BOLIVIANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '8', 
        'nacionalidad' => 'BRASILERO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '9', 
        'nacionalidad' => 'CAMBOYANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '10', 
        'nacionalidad' => 'CANADIENSE', 
        ));

        Nacionalidad::create(array(
        'id' => '11', 
        'nacionalidad' => 'CHILENO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '12', 
        'nacionalidad' => 'CHINO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '13', 
        'nacionalidad' => 'COLOMBIANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '14', 
        'nacionalidad' => 'COREANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '15', 
        'nacionalidad' => 'COSTARRICENSE', 
        ));

        Nacionalidad::create(array(
        'id' => '16', 
        'nacionalidad' => 'CUBANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '17', 
        'nacionalidad' => 'DANÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '18', 
        'nacionalidad' => 'ECUATORIANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '19', 
        'nacionalidad' => 'EGIPCIO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '20', 
        'nacionalidad' => 'SALVADOREÑO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '21', 
        'nacionalidad' => 'ESPAÑOL(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '22', 
        'nacionalidad' => 'ESTADOUNIDENSE', 
        ));

        Nacionalidad::create(array(
        'id' => '23', 
        'nacionalidad' => 'ESTONIO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '24', 
        'nacionalidad' => 'ETIOPE', 
        ));

        Nacionalidad::create(array(
        'id' => '25', 
        'nacionalidad' => 'FILIPINO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '26', 
        'nacionalidad' => 'FINLANDÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '27', 
        'nacionalidad' => 'FRANCÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '28', 
        'nacionalidad' => 'GALÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '29', 
        'nacionalidad' => 'GRIEGO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '30', 
        'nacionalidad' => 'GUATEMALTECO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '31', 
        'nacionalidad' => 'HAITIANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '32', 
        'nacionalidad' => 'HOLANDÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '33', 
        'nacionalidad' => 'HONDUREÑO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '34', 
        'nacionalidad' => 'INDONÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '35', 
        'nacionalidad' => 'INGLÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '36', 
        'nacionalidad' => 'IRAQUÍ', 
        ));

        Nacionalidad::create(array(
        'id' => '37', 
        'nacionalidad' => 'IRANÍ', 
        ));

        Nacionalidad::create(array(
        'id' => '38', 
        'nacionalidad' => 'IRLANDÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '39', 
        'nacionalidad' => 'ISRAELÍ', 
        ));

        Nacionalidad::create(array(
        'id' => '40', 
        'nacionalidad' => 'ITALIANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '41', 
        'nacionalidad' => 'JAPONÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '42', 
        'nacionalidad' => 'JORDANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '43', 
        'nacionalidad' => 'LAOSIANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '44', 
        'nacionalidad' => 'LETÓN(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '45', 
        'nacionalidad' => 'LETONÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '46', 
        'nacionalidad' => 'MALAYO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '47', 
        'nacionalidad' => 'MARROQUÍ', 
        ));

        Nacionalidad::create(array(
        'id' => '48', 
        'nacionalidad' => 'MEXICANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '49', 
        'nacionalidad' => 'NICARAGÜENSE', 
        ));

        Nacionalidad::create(array(
        'id' => '50', 
        'nacionalidad' => 'NORUEGO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '51', 
        'nacionalidad' => 'NEOZELANDÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '52', 
        'nacionalidad' => 'PANAMEÑO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '53', 
        'nacionalidad' => 'PARAGUAYO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '54', 
        'nacionalidad' => 'PERUANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '55', 
        'nacionalidad' => 'POLACO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '56', 
        'nacionalidad' => 'PORTUGUÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '57', 
        'nacionalidad' => 'PUERTORRIQUEÑO', 
        ));

        Nacionalidad::create(array(
        'id' => '58', 
        'nacionalidad' => 'DOMINICANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '59', 
        'nacionalidad' => 'RUMANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '60', 
        'nacionalidad' => 'RUSO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '61', 
        'nacionalidad' => 'SUECO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '62', 
        'nacionalidad' => 'SUIZO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '63', 
        'nacionalidad' => 'TAILANDÉS(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '64', 
        'nacionalidad' => 'TAIWANES(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '65', 
        'nacionalidad' => 'TURCO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '66', 
        'nacionalidad' => 'UCRANIANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '67', 
        'nacionalidad' => 'URUGUAYO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '68', 
        'nacionalidad' => 'VENEZOLANO(A)', 
        ));

        Nacionalidad::create(array(
        'id' => '69', 
        'nacionalidad' => 'VIETNAMITA', 
        ));




    }

}