<?php

class ProgramasTableSeeder extends Seeder {

    public function run()
    {
        DB::table('programas')->delete();


        Programa::create(array(
            'id' => '1', 
            'tipo' => 1,
            'programa' => 'DESARROLLO HUMANO',
            'modalidad' => 1,
            'admision' => 2,
            'fecha_inicio' => '2015-01-01',
            'fecha_fin' => '2015-02-01',
            'estatus' => 'ACTIVO',
            ));

        Programa::create(array(
            'id' => '2', 
            'tipo' => 2,
            'programa' => 'PEDIATRIA',
            'modalidad' => 2,
            'admision' => 1,
            'fecha_inicio' => '2015-02-01',
            'fecha_fin' => '2015-03-01',
            'estatus' => 'ACTIVO',
            ));

        Programa::create(array(
            'id' => '3', 
            'tipo' => 3,
            'programa' => 'ELECTRICIDAD Y MAGNETISMO',
            'modalidad' => 2,
            'admision' => 1,
            'fecha_inicio' => '2015-04-01',
            'fecha_fin' => '2015-05-01',
            'estatus' => 'INACTIVO',
            ));

        Programa::create(array(
            'id' => '4', 
            'tipo' => 4,
            'programa' => 'SISTEMAS ABIERTOS',
            'modalidad' => 1,
            'admision' => 2,
            'fecha_inicio' => '2015-05-01',
            'fecha_fin' => '2015-06-01',
            'estatus' => 'ACTIVO',
            ));

        Programa::create(array(
            'id' => '5', 
            'tipo' => 5,
            'programa' => 'TRATADOS DE COMERCIOS INTERNACIONALES',
            'modalidad' => 2,
            'admision' => 1,
            'fecha_inicio' => '2015-06-01',
            'fecha_fin' => '2015-07-01',
            'estatus' => 'INACTIVO',
            ));
        
        Programa::create(array(
            'id' => '6', 
            'tipo' => 6,
            'programa' => 'ESPECIALIDAD EN ADMINISTRACION',
            'modalidad' => 1,
            'admision' => 2,
            'fecha_inicio' => '2015-07-01',
            'fecha_fin' => '2015-08-01',
            'estatus' => 'ACTIVO',
            ));
        
        Programa::create(array(
            'id' => '7', 
            'tipo' => 7,
            'programa' => 'PENSAMIENTO CREATIVO',
            'modalidad' => 2,
            'admision' => 1,
            'fecha_inicio' => '2015-08-01',
            'fecha_fin' => '2015-09-01',
            'estatus' => 'INACTIVO',
            ));

        Programa::create(array(
            'id' => '8', 
            'tipo' => 8,
            'programa' => 'CIRUGIA BUCAL',
            'modalidad' => 2,
            'admision' => 2,
            'fecha_inicio' => '2015-09-01',
            'fecha_fin' => '2015-10-01',
            'estatus' => 'ACTIVO',
            ));

        Programa::create(array(
            'id' => '9', 
            'tipo' => 9,
            'programa' => 'RESTAURACION DE ESTRUCTURAS',
            'modalidad' => 1,
            'admision' => 1,
            'fecha_inicio' => '2015-10-01',
            'fecha_fin' => '2015-11-01',
            'estatus' => 'INACTIVO',
            ));

    }

}