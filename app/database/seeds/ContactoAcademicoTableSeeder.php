<?php

class ContactoAcademicoTableSeeder extends Seeder {

    public function run()
    {
        DB::table('contacto_academicos')->delete();

        ContactoAcademico::create(array(
            'id_maestro' => '1', 
            'calle' => 'PROLONGACION DEL REFUGIO', 
            'num_int' => '23', 
            'num_ext' => '', 
            'colonia' => 'SAN ANTONIO', 
            'ciudad' => '323', 
            'estado' => '11',
            'nacionalidad' => 48,  
            'cp' => '37750', 
            'rfc' => 'DEGE256ALGO', 
            'telefono' => '1526645', 
            'movil' => '0444151097774', 
            'email' => 'azael_dg@hotmail.com',
        	));


        ContactoAcademico::create(array(
            'id_maestro' => '2', 
            'calle' => 'CALZADA DE LA ESTACION', 
            'num_int' => '5', 
            'num_ext' => '', 
            'colonia' => 'CALZADA DE LA PRESA', 
            'ciudad' => '323', 
            'nacionalidad' => 48,  
            'estado' => '11', 
            'cp' => '37700', 
            'rfc' => 'SOEL123ALGO', 
            'telefono' => '1523456', 
            'movil' => '0444151234567', 
            'email' => 'solislf@gmail.com',
            ));
    }

}