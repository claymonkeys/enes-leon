<?php

class TiposTableSeeder extends Seeder {

    public function run()
    {
        DB::table('tipos')->delete();


        Tipo::create(array(
            'id' => '1', 
            'tipo_programa' => 'CURSO',
            ));

        Tipo::create(array(
            'id' => '2', 
            'tipo_programa' => 'TALLER',
            ));

        Tipo::create(array(
            'id' => '3', 
            'tipo_programa' => 'DIPLOMADO',
            ));

        Tipo::create(array(
            'id' => '4', 
            'tipo_programa' => 'MESA CLINICA',
            ));

        Tipo::create(array(
            'id' => '5', 
            'tipo_programa' => 'SEMINARIO',
            ));
        
        Tipo::create(array(
            'id' => '6', 
            'tipo_programa' => 'CONFERENCIA',
            ));
        
        Tipo::create(array(
            'id' => '7', 
            'tipo_programa' => 'CONGRESO',
            ));

        Tipo::create(array(
            'id' => '8', 
            'tipo_programa' => 'SIMPOSIUM',
            ));

        Tipo::create(array(
            'id' => '9', 
            'tipo_programa' => 'OTROS',
            ));

    }

}