<?php

class AspirantesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('aspirantes')->delete();


        Aspirante::create(array(
            'id' => '1', 
            'id_alumno' => 1,
            'id_programa' => 3,
            'estatus' => 'PENDIENTE',
            ));

        Aspirante::create(array(
            'id' => '2', 
            'id_alumno' => 2,
            'id_programa' => 4,
            'estatus' => 'PENDIENTE',
            ));

    }

}