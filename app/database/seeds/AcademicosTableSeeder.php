<?php

class AcademicosTableSeeder extends Seeder {

    public function run()
    {
        DB::table('academicos')->delete();

        Maestro::create(array(
            'id' => '1', 
            'nombre' => 'JUAN', 
            'a_paterno' => 'PERES', 
            'a_materno' => 'GOMEZ', 
            'sexo' => 'MASCULINO', 
            'fecha_nac' => '1980-01-23', 
            'titulo_profesional' => 'INGENIERO EN SISTEMAS',
            'unidad_academica' => 2,  
            'estatus' => 'ACTIVO', 
        	));


        Maestro::create(array(
            'id' => '2', 
            'nombre' => 'MARIA', 
            'a_paterno' => 'ROBLEZ', 
            'a_materno' => 'GONZÀLEZ', 
            'sexo' => 'FEMENINO', 
            'fecha_nac' => '1980-02-01', 
            'titulo_profesional' => 'DOCTORA EN PSIQUIATRIA',
            'unidad_academica' => 1,  
            'estatus' => 'ACTIVO', 
            ));
    }

}