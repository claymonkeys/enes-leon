<?php

class ModalidadesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('modalidad')->delete();

        Modalidad::create(array(
            'id' => '1', 
            'modalidad' => 'PRESENCIAL', 
        	));


        Modalidad::create(array(
            'id' => '2', 
            'modalidad' => 'EN LÍNEA', 
            ));
    }

}