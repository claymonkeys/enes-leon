<?php

class AlumnosTableSeeder extends Seeder {

    public function run()
    {
        DB::table('alumnos')->delete();

        Alumno::create(array(
            'id' => '1',          
        	'nombre' => 'EDGAR AZAEL', 
        	'a_paterno' => 'DEANDA',
            'a_materno' => 'GONZÀLEZ',
            'sexo' => 'MASCULINO',
            'fecha_nac' => '1991-01-25',
            'unidad_academica' => 2,
            'nivel_estudios' => 'LICENCIATURA',
            'perfil_profesional' => 'INGENIERO',
            'termino_estudios' => 'TITULADO',
            'institucion_estudios' => 'ITSES',
        	'facturacion' => 1, 
            'como_se_entero' => 'PAGINA DE INTERNET',
        	));

        Alumno::create(array(
            'id' => '2',          
            'nombre' => 'LUIS FELIPE', 
            'a_paterno' => 'SOLIS',
            'a_materno' => 'ENRIQUEZ',
            'sexo' => 'MASCULINO',
            'fecha_nac' => '1987-01-01',
            'unidad_academica' => 1,
            'nivel_estudios' => 'LICENCIATURA',
            'perfil_profesional' => 'INGENIERO',
            'termino_estudios' => 'TITULADO',
            'institucion_estudios' => 'ITSES',
            'facturacion' => 1, 
            'como_se_entero' => 'FACEBOOK',
            ));
    }
}