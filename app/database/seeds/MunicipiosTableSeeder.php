<?php

class MunicipiosTableSeeder extends Seeder {

    public function run()
    {
        DB::table('municipios')->delete();
Municipio::create(array(
            'id' => '1', 
            'id_estado' =>'1',
            'municipio' => 'AGUASCALIENTES', 
        ));

        Municipio::create(array(
            'id' => '2', 
            'id_estado' =>'1',
            'municipio' => 'ASIENTOS', 
        ));

        Municipio::create(array(
            'id' => '3', 
            'id_estado' =>'1',
            'municipio' => 'CALVILLO', 
        ));

        Municipio::create(array(
            'id' => '4', 
            'id_estado' =>'1',
            'municipio' => 'COSÍO', 
        ));

        Municipio::create(array(
            'id' => '5', 
            'id_estado' =>'1',
            'municipio' => 'EL LLANO', 
        ));

        Municipio::create(array(
            'id' => '6', 
            'id_estado' =>'1',
            'municipio' => 'JESÚS MARÍA', 
        ));

        Municipio::create(array(
            'id' => '7', 
            'id_estado' =>'1',
            'municipio' => 'PABELLÓN DE ARTEAGA', 
        ));

        Municipio::create(array(
            'id' => '8', 
            'id_estado' =>'1',
            'municipio' => 'RINCÓN DE ROMOS', 
        ));

        Municipio::create(array(
            'id' => '9', 
            'id_estado' =>'1',
            'municipio' => 'SAN FRANCISCO DE LOS ROMO', 
        ));

        Municipio::create(array(
            'id' => '10', 
            'id_estado' =>'1',
            'municipio' => 'SAN JOSÉ DE GRACIA', 
        ));

        Municipio::create(array(
            'id' => '11', 
            'id_estado' =>'1',
            'municipio' => 'TEPEZALÁ', 
        ));

        Municipio::create(array(
            'id' => '12', 
            'id_estado' =>'2',
            'municipio' => 'ENSENADA', 
        ));

        Municipio::create(array(
            'id' => '13', 
            'id_estado' =>'2',
            'municipio' => 'MEXICALI', 
        ));

        Municipio::create(array(
            'id' => '14', 
            'id_estado' =>'2',
            'municipio' => 'PLAYAS DE ROSARITO', 
        ));

        Municipio::create(array(
            'id' => '15', 
            'id_estado' =>'2',
            'municipio' => 'TECATE', 
        ));

        Municipio::create(array(
            'id' => '16', 
            'id_estado' =>'2',
            'municipio' => 'TIJUANA', 
        ));

        Municipio::create(array(
            'id' => '17', 
            'id_estado' =>'3',
            'municipio' => 'COMONDÚ', 
        ));

        Municipio::create(array(
            'id' => '18', 
            'id_estado' =>'3',
            'municipio' => 'LA PAZ', 
        ));

        Municipio::create(array(
            'id' => '19', 
            'id_estado' =>'3',
            'municipio' => 'LORETO', 
        ));

        Municipio::create(array(
            'id' => '20', 
            'id_estado' =>'3',
            'municipio' => 'LOS CABOS', 
        ));

        Municipio::create(array(
            'id' => '21', 
            'id_estado' =>'3',
            'municipio' => 'MULEGÉ', 
        ));

        Municipio::create(array(
            'id' => '22', 
            'id_estado' =>'4',
            'municipio' => 'CALAKMUL', 
        ));

        Municipio::create(array(
            'id' => '23', 
            'id_estado' =>'4',
            'municipio' => 'CALKINÍ', 
        ));

        Municipio::create(array(
            'id' => '24', 
            'id_estado' =>'4',
            'municipio' => 'CAMPECHE', 
        ));

        Municipio::create(array(
            'id' => '25', 
            'id_estado' =>'4',
            'municipio' => 'CANDELARIA', 
        ));

        Municipio::create(array(
            'id' => '26', 
            'id_estado' =>'4',
            'municipio' => 'CARMEN', 
        ));

        Municipio::create(array(
            'id' => '27', 
            'id_estado' =>'4',
            'municipio' => 'CHAMPOTÓN', 
        ));

        Municipio::create(array(
            'id' => '28', 
            'id_estado' =>'4',
            'municipio' => 'ESCÁRCEGA', 
        ));

        Municipio::create(array(
            'id' => '29', 
            'id_estado' =>'4',
            'municipio' => 'HECELCHAKÁN', 
        ));

        Municipio::create(array(
            'id' => '30', 
            'id_estado' =>'4',
            'municipio' => 'HOPELCHÉN', 
        ));

        Municipio::create(array(
            'id' => '31', 
            'id_estado' =>'4',
            'municipio' => 'PALIZADA', 
        ));

        Municipio::create(array(
            'id' => '32', 
            'id_estado' =>'4',
            'municipio' => 'TENABO', 
        ));

        Municipio::create(array(
            'id' => '33', 
            'id_estado' =>'5',
            'municipio' => 'ABASOLO', 
        ));

        Municipio::create(array(
            'id' => '34', 
            'id_estado' =>'5',
            'municipio' => 'ACUÑA', 
        ));

        Municipio::create(array(
            'id' => '35', 
            'id_estado' =>'5',
            'municipio' => 'ALLENDE', 
        ));

        Municipio::create(array(
            'id' => '36', 
            'id_estado' =>'5',
            'municipio' => 'ARTEAGA', 
        ));

        Municipio::create(array(
            'id' => '37', 
            'id_estado' =>'5',
            'municipio' => 'CANDELA', 
        ));

        Municipio::create(array(
            'id' => '38', 
            'id_estado' =>'5',
            'municipio' => 'CASTAÑOS', 
        ));

        Municipio::create(array(
            'id' => '39', 
            'id_estado' =>'5',
            'municipio' => 'CUATROCIÉNEGAS', 
        ));

        Municipio::create(array(
            'id' => '40', 
            'id_estado' =>'5',
            'municipio' => 'ESCOBEDO', 
        ));

        Municipio::create(array(
            'id' => '41', 
            'id_estado' =>'5',
            'municipio' => 'FRANCISCO I. MADERO', 
        ));

        Municipio::create(array(
            'id' => '42', 
            'id_estado' =>'5',
            'municipio' => 'FRONTERA', 
        ));

        Municipio::create(array(
            'id' => '43', 
            'id_estado' =>'5',
            'municipio' => 'GENERAL CEPEDA', 
        ));

        Municipio::create(array(
            'id' => '44', 
            'id_estado' =>'5',
            'municipio' => 'GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '45', 
            'id_estado' =>'5',
            'municipio' => 'HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '46', 
            'id_estado' =>'5',
            'municipio' => 'JIMÉNEZ', 
        ));

        Municipio::create(array(
            'id' => '47', 
            'id_estado' =>'5',
            'municipio' => 'JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '48', 
            'id_estado' =>'5',
            'municipio' => 'LAMADRID', 
        ));

        Municipio::create(array(
            'id' => '49', 
            'id_estado' =>'5',
            'municipio' => 'MATAMOROS', 
        ));

        Municipio::create(array(
            'id' => '50', 
            'id_estado' =>'5',
            'municipio' => 'MONCLOVA', 
        ));

        Municipio::create(array(
            'id' => '51', 
            'id_estado' =>'5',
            'municipio' => 'MORELOS', 
        ));

        Municipio::create(array(
            'id' => '52', 
            'id_estado' =>'5',
            'municipio' => 'MÚZQUIZ', 
        ));

        Municipio::create(array(
            'id' => '53', 
            'id_estado' =>'5',
            'municipio' => 'NADADORES', 
        ));

        Municipio::create(array(
            'id' => '54', 
            'id_estado' =>'5',
            'municipio' => 'NAVA', 
        ));

        Municipio::create(array(
            'id' => '55', 
            'id_estado' =>'5',
            'municipio' => 'OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '56', 
            'id_estado' =>'5',
            'municipio' => 'PARRAS', 
        ));

        Municipio::create(array(
            'id' => '57', 
            'id_estado' =>'5',
            'municipio' => 'PIEDRAS NEGRAS', 
        ));

        Municipio::create(array(
            'id' => '58', 
            'id_estado' =>'5',
            'municipio' => 'PROGRESO', 
        ));

        Municipio::create(array(
            'id' => '59', 
            'id_estado' =>'5',
            'municipio' => 'RAMOS ARIZPE', 
        ));

        Municipio::create(array(
            'id' => '60', 
            'id_estado' =>'5',
            'municipio' => 'SABINAS', 
        ));

        Municipio::create(array(
            'id' => '61', 
            'id_estado' =>'5',
            'municipio' => 'SACRAMENTO', 
        ));

        Municipio::create(array(
            'id' => '62', 
            'id_estado' =>'5',
            'municipio' => 'SALTILLO', 
        ));

        Municipio::create(array(
            'id' => '63', 
            'id_estado' =>'5',
            'municipio' => 'SAN BUENAVENTURA', 
        ));

        Municipio::create(array(
            'id' => '64', 
            'id_estado' =>'5',
            'municipio' => 'SAN JUAN DE SABINAS', 
        ));

        Municipio::create(array(
            'id' => '65', 
            'id_estado' =>'5',
            'municipio' => 'SAN PEDRO', 
        ));

        Municipio::create(array(
            'id' => '66', 
            'id_estado' =>'5',
            'municipio' => 'SIERRA MOJADA', 
        ));

        Municipio::create(array(
            'id' => '67', 
            'id_estado' =>'5',
            'municipio' => 'TORREÓN', 
        ));

        Municipio::create(array(
            'id' => '68', 
            'id_estado' =>'5',
            'municipio' => 'VIESCA', 
        ));

        Municipio::create(array(
            'id' => '69', 
            'id_estado' =>'5',
            'municipio' => 'VILLA UNIÓN', 
        ));

        Municipio::create(array(
            'id' => '70', 
            'id_estado' =>'5',
            'municipio' => 'ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '71', 
            'id_estado' =>'6',
            'municipio' => 'ARMERÍA', 
        ));

        Municipio::create(array(
            'id' => '72', 
            'id_estado' =>'6',
            'municipio' => 'COLIMA', 
        ));

        Municipio::create(array(
            'id' => '73', 
            'id_estado' =>'6',
            'municipio' => 'COMALA', 
        ));

        Municipio::create(array(
            'id' => '74', 
            'id_estado' =>'6',
            'municipio' => 'COQUIMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '75', 
            'id_estado' =>'6',
            'municipio' => 'CUAUHTÉMOC', 
        ));

        Municipio::create(array(
            'id' => '76', 
            'id_estado' =>'6',
            'municipio' => 'IXTLAHUACÁN', 
        ));

        Municipio::create(array(
            'id' => '77', 
            'id_estado' =>'6',
            'municipio' => 'MANZANILLO', 
        ));

        Municipio::create(array(
            'id' => '78', 
            'id_estado' =>'6',
            'municipio' => 'MINATITLÁN', 
        ));

        Municipio::create(array(
            'id' => '79', 
            'id_estado' =>'6',
            'municipio' => 'TECOMÁN', 
        ));

        Municipio::create(array(
            'id' => '80', 
            'id_estado' =>'6',
            'municipio' => 'VILLA DE ALVAREZ', 
        ));

        Municipio::create(array(
            'id' => '81', 
            'id_estado' =>'7',
            'municipio' => 'ACACOYAGUA', 
        ));

        Municipio::create(array(
            'id' => '82', 
            'id_estado' =>'7',
            'municipio' => 'ACALA', 
        ));

        Municipio::create(array(
            'id' => '83', 
            'id_estado' =>'7',
            'municipio' => 'ACAPETAHUA', 
        ));

        Municipio::create(array(
            'id' => '84', 
            'id_estado' =>'7',
            'municipio' => 'ALDAMA', 
        ));

        Municipio::create(array(
            'id' => '85', 
            'id_estado' =>'7',
            'municipio' => 'ALTAMIRANO', 
        ));

        Municipio::create(array(
            'id' => '86', 
            'id_estado' =>'7',
            'municipio' => 'AMATÁN', 
        ));

        Municipio::create(array(
            'id' => '87', 
            'id_estado' =>'7',
            'municipio' => 'AMATENANGO DE LA FRONTERA', 
        ));

        Municipio::create(array(
            'id' => '88', 
            'id_estado' =>'7',
            'municipio' => 'AMATENANGO DEL VALLE', 
        ));

        Municipio::create(array(
            'id' => '89', 
            'id_estado' =>'7',
            'municipio' => 'ANGEL ALBINO CORZO', 
        ));

        Municipio::create(array(
            'id' => '90', 
            'id_estado' =>'7',
            'municipio' => 'ARRIAGA', 
        ));

        Municipio::create(array(
            'id' => '91', 
            'id_estado' =>'7',
            'municipio' => 'BEJUCAL DE OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '92', 
            'id_estado' =>'7',
            'municipio' => 'BELLA VISTA', 
        ));

        Municipio::create(array(
            'id' => '93', 
            'id_estado' =>'7',
            'municipio' => 'BENEMÉRITO DE LAS AMÉRICAS', 
        ));

        Municipio::create(array(
            'id' => '94', 
            'id_estado' =>'7',
            'municipio' => 'BERRIOZÁBAL', 
        ));

        Municipio::create(array(
            'id' => '95', 
            'id_estado' =>'7',
            'municipio' => 'BOCHIL', 
        ));

        Municipio::create(array(
            'id' => '96', 
            'id_estado' =>'7',
            'municipio' => 'CACAHOATÁN', 
        ));

        Municipio::create(array(
            'id' => '97', 
            'id_estado' =>'7',
            'municipio' => 'CATAZAJÁ', 
        ));

        Municipio::create(array(
            'id' => '98', 
            'id_estado' =>'7',
            'municipio' => 'CHALCHIHUITÁN', 
        ));

        Municipio::create(array(
            'id' => '99', 
            'id_estado' =>'7',
            'municipio' => 'CHAMULA', 
        ));

        Municipio::create(array(
            'id' => '100', 
            'id_estado' =>'7',
            'municipio' => 'CHANAL', 
        ));

        Municipio::create(array(
            'id' => '101', 
            'id_estado' =>'7',
            'municipio' => 'CHAPULTENANGO', 
        ));

        Municipio::create(array(
            'id' => '102', 
            'id_estado' =>'7',
            'municipio' => 'CHENALHÓ', 
        ));

        Municipio::create(array(
            'id' => '103', 
            'id_estado' =>'7',
            'municipio' => 'CHIAPA DE CORZO', 
        ));

        Municipio::create(array(
            'id' => '104', 
            'id_estado' =>'7',
            'municipio' => 'CHIAPILLA', 
        ));

        Municipio::create(array(
            'id' => '105', 
            'id_estado' =>'7',
            'municipio' => 'CHICOASÉN', 
        ));

        Municipio::create(array(
            'id' => '106', 
            'id_estado' =>'7',
            'municipio' => 'CHICOMUSELO', 
        ));

        Municipio::create(array(
            'id' => '107', 
            'id_estado' =>'7',
            'municipio' => 'CHILÓN', 
        ));

        Municipio::create(array(
            'id' => '108', 
            'id_estado' =>'7',
            'municipio' => 'CINTALAPA', 
        ));

        Municipio::create(array(
            'id' => '109', 
            'id_estado' =>'7',
            'municipio' => 'COAPILLA', 
        ));

        Municipio::create(array(
            'id' => '110', 
            'id_estado' =>'7',
            'municipio' => 'COMITÁN DE DOMÍNGUEZ', 
        ));

        Municipio::create(array(
            'id' => '111', 
            'id_estado' =>'7',
            'municipio' => 'COPAINALÁ', 
        ));

        Municipio::create(array(
            'id' => '112', 
            'id_estado' =>'7',
            'municipio' => 'EL BOSQUE', 
        ));

        Municipio::create(array(
            'id' => '113', 
            'id_estado' =>'7',
            'municipio' => 'EL PORVENIR', 
        ));

        Municipio::create(array(
            'id' => '114', 
            'id_estado' =>'7',
            'municipio' => 'ESCUINTLA', 
        ));

        Municipio::create(array(
            'id' => '115', 
            'id_estado' =>'7',
            'municipio' => 'FRANCISCO LEÓN', 
        ));

        Municipio::create(array(
            'id' => '116', 
            'id_estado' =>'7',
            'municipio' => 'FRONTERA COMALAPA', 
        ));

        Municipio::create(array(
            'id' => '117', 
            'id_estado' =>'7',
            'municipio' => 'FRONTERA HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '118', 
            'id_estado' =>'7',
            'municipio' => 'HUEHUETÁN', 
        ));

        Municipio::create(array(
            'id' => '119', 
            'id_estado' =>'7',
            'municipio' => 'HUITIUPÁN', 
        ));

        Municipio::create(array(
            'id' => '120', 
            'id_estado' =>'7',
            'municipio' => 'HUIXTÁN', 
        ));

        Municipio::create(array(
            'id' => '121', 
            'id_estado' =>'7',
            'municipio' => 'HUIXTLA', 
        ));

        Municipio::create(array(
            'id' => '122', 
            'id_estado' =>'7',
            'municipio' => 'IXHUATÁN', 
        ));

        Municipio::create(array(
            'id' => '123', 
            'id_estado' =>'7',
            'municipio' => 'IXTACOMITÁN', 
        ));

        Municipio::create(array(
            'id' => '124', 
            'id_estado' =>'7',
            'municipio' => 'IXTAPA', 
        ));

        Municipio::create(array(
            'id' => '125', 
            'id_estado' =>'7',
            'municipio' => 'IXTAPANGAJOYA', 
        ));

        Municipio::create(array(
            'id' => '126', 
            'id_estado' =>'7',
            'municipio' => 'JIQUIPILAS', 
        ));

        Municipio::create(array(
            'id' => '127', 
            'id_estado' =>'7',
            'municipio' => 'JITOTOL', 
        ));

        Municipio::create(array(
            'id' => '128', 
            'id_estado' =>'7',
            'municipio' => 'JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '129', 
            'id_estado' =>'7',
            'municipio' => 'LA CONCORDIA', 
        ));

        Municipio::create(array(
            'id' => '130', 
            'id_estado' =>'7',
            'municipio' => 'LA GRANDEZA', 
        ));

        Municipio::create(array(
            'id' => '131', 
            'id_estado' =>'7',
            'municipio' => 'LA INDEPENDENCIA', 
        ));

        Municipio::create(array(
            'id' => '132', 
            'id_estado' =>'7',
            'municipio' => 'LA LIBERTAD', 
        ));

        Municipio::create(array(
            'id' => '133', 
            'id_estado' =>'7',
            'municipio' => 'LA TRINITARIA', 
        ));

        Municipio::create(array(
            'id' => '134', 
            'id_estado' =>'7',
            'municipio' => 'LARRÁINZAR', 
        ));

        Municipio::create(array(
            'id' => '135', 
            'id_estado' =>'7',
            'municipio' => 'LAS MARGARITAS', 
        ));

        Municipio::create(array(
            'id' => '136', 
            'id_estado' =>'7',
            'municipio' => 'LAS ROSAS', 
        ));

        Municipio::create(array(
            'id' => '137', 
            'id_estado' =>'7',
            'municipio' => 'MAPASTEPEC', 
        ));

        Municipio::create(array(
            'id' => '138', 
            'id_estado' =>'7',
            'municipio' => 'MARAVILLA TENEJAPA', 
        ));

        Municipio::create(array(
            'id' => '139', 
            'id_estado' =>'7',
            'municipio' => 'MARQUÉS DE COMILLAS', 
        ));

        Municipio::create(array(
            'id' => '140', 
            'id_estado' =>'7',
            'municipio' => 'MAZAPA DE MADERO', 
        ));

        Municipio::create(array(
            'id' => '141', 
            'id_estado' =>'7',
            'municipio' => 'MAZATÁN', 
        ));

        Municipio::create(array(
            'id' => '142', 
            'id_estado' =>'7',
            'municipio' => 'METAPA', 
        ));

        Municipio::create(array(
            'id' => '143', 
            'id_estado' =>'7',
            'municipio' => 'MITONTIC', 
        ));

        Municipio::create(array(
            'id' => '144', 
            'id_estado' =>'7',
            'municipio' => 'MONTECRISTO DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '145', 
            'id_estado' =>'7',
            'municipio' => 'MOTOZINTLA', 
        ));

        Municipio::create(array(
            'id' => '146', 
            'id_estado' =>'7',
            'municipio' => 'NICOLÁS RUÍZ', 
        ));

        Municipio::create(array(
            'id' => '147', 
            'id_estado' =>'7',
            'municipio' => 'OCOSINGO', 
        ));

        Municipio::create(array(
            'id' => '148', 
            'id_estado' =>'7',
            'municipio' => 'OCOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '149', 
            'id_estado' =>'7',
            'municipio' => 'OCOZOCOAUTLA DE ESPINOSA', 
        ));

        Municipio::create(array(
            'id' => '150', 
            'id_estado' =>'7',
            'municipio' => 'OSTUACÁN', 
        ));

        Municipio::create(array(
            'id' => '151', 
            'id_estado' =>'7',
            'municipio' => 'OSUMACINTA', 
        ));

        Municipio::create(array(
            'id' => '152', 
            'id_estado' =>'7',
            'municipio' => 'OXCHUC', 
        ));

        Municipio::create(array(
            'id' => '153', 
            'id_estado' =>'7',
            'municipio' => 'PALENQUE', 
        ));

        Municipio::create(array(
            'id' => '154', 
            'id_estado' =>'7',
            'municipio' => 'PANTELHÓ', 
        ));

        Municipio::create(array(
            'id' => '155', 
            'id_estado' =>'7',
            'municipio' => 'PANTEPEC', 
        ));

        Municipio::create(array(
            'id' => '156', 
            'id_estado' =>'7',
            'municipio' => 'PICHUCALCO', 
        ));

        Municipio::create(array(
            'id' => '157', 
            'id_estado' =>'7',
            'municipio' => 'PIJIJIAPAN', 
        ));

        Municipio::create(array(
            'id' => '158', 
            'id_estado' =>'7',
            'municipio' => 'PUEBLO NUEVO SOLISTAHUACÁN', 
        ));

        Municipio::create(array(
            'id' => '159', 
            'id_estado' =>'7',
            'municipio' => 'RAYÓN', 
        ));

        Municipio::create(array(
            'id' => '160', 
            'id_estado' =>'7',
            'municipio' => 'REFORMA', 
        ));

        Municipio::create(array(
            'id' => '161', 
            'id_estado' =>'7',
            'municipio' => 'SABANILLA', 
        ));

        Municipio::create(array(
            'id' => '162', 
            'id_estado' =>'7',
            'municipio' => 'SALTO DE AGUA', 
        ));

        Municipio::create(array(
            'id' => '163', 
            'id_estado' =>'7',
            'municipio' => 'SAN ANDRÉS DURAZNAL', 
        ));

        Municipio::create(array(
            'id' => '164', 
            'id_estado' =>'7',
            'municipio' => 'SAN CRISTÓBAL DE LAS CASAS', 
        ));

        Municipio::create(array(
            'id' => '165', 
            'id_estado' =>'7',
            'municipio' => 'SAN FERNANDO', 
        ));

        Municipio::create(array(
            'id' => '166', 
            'id_estado' =>'7',
            'municipio' => 'SAN JUAN CANCUC', 
        ));

        Municipio::create(array(
            'id' => '167', 
            'id_estado' =>'7',
            'municipio' => 'SAN LUCAS', 
        ));

        Municipio::create(array(
            'id' => '168', 
            'id_estado' =>'7',
            'municipio' => 'SANTIAGO EL PINAR', 
        ));

        Municipio::create(array(
            'id' => '169', 
            'id_estado' =>'7',
            'municipio' => 'SILTEPEC', 
        ));

        Municipio::create(array(
            'id' => '170', 
            'id_estado' =>'7',
            'municipio' => 'SIMOJOVEL', 
        ));

        Municipio::create(array(
            'id' => '171', 
            'id_estado' =>'7',
            'municipio' => 'SITALÁ', 
        ));

        Municipio::create(array(
            'id' => '172', 
            'id_estado' =>'7',
            'municipio' => 'SOCOLTENANGO', 
        ));

        Municipio::create(array(
            'id' => '173', 
            'id_estado' =>'7',
            'municipio' => 'SOLOSUCHIAPA', 
        ));

        Municipio::create(array(
            'id' => '174', 
            'id_estado' =>'7',
            'municipio' => 'SOYALÓ', 
        ));

        Municipio::create(array(
            'id' => '175', 
            'id_estado' =>'7',
            'municipio' => 'SUCHIAPA', 
        ));

        Municipio::create(array(
            'id' => '176', 
            'id_estado' =>'7',
            'municipio' => 'SUCHIATE', 
        ));

        Municipio::create(array(
            'id' => '177', 
            'id_estado' =>'7',
            'municipio' => 'SUNUAPA', 
        ));

        Municipio::create(array(
            'id' => '178', 
            'id_estado' =>'7',
            'municipio' => 'TAPACHULA', 
        ));

        Municipio::create(array(
            'id' => '179', 
            'id_estado' =>'7',
            'municipio' => 'TAPALAPA', 
        ));

        Municipio::create(array(
            'id' => '180', 
            'id_estado' =>'7',
            'municipio' => 'TAPILULA', 
        ));

        Municipio::create(array(
            'id' => '181', 
            'id_estado' =>'7',
            'municipio' => 'TECPATÁN', 
        ));

        Municipio::create(array(
            'id' => '182', 
            'id_estado' =>'7',
            'municipio' => 'TENEJAPA', 
        ));

        Municipio::create(array(
            'id' => '183', 
            'id_estado' =>'7',
            'municipio' => 'TEOPISCA', 
        ));

        Municipio::create(array(
            'id' => '184', 
            'id_estado' =>'7',
            'municipio' => 'TILA', 
        ));

        Municipio::create(array(
            'id' => '185', 
            'id_estado' =>'7',
            'municipio' => 'TONALÁ', 
        ));

        Municipio::create(array(
            'id' => '186', 
            'id_estado' =>'7',
            'municipio' => 'TOTOLAPA', 
        ));

        Municipio::create(array(
            'id' => '187', 
            'id_estado' =>'7',
            'municipio' => 'TUMBALÁ', 
        ));

        Municipio::create(array(
            'id' => '188', 
            'id_estado' =>'7',
            'municipio' => 'TUXTLA CHICO', 
        ));

        Municipio::create(array(
            'id' => '189', 
            'id_estado' =>'7',
            'municipio' => 'TUXTLA GUTIÉRREZ', 
        ));

        Municipio::create(array(
            'id' => '190', 
            'id_estado' =>'7',
            'municipio' => 'TUZANTÁN', 
        ));

        Municipio::create(array(
            'id' => '191', 
            'id_estado' =>'7',
            'municipio' => 'TZIMOL', 
        ));

        Municipio::create(array(
            'id' => '192', 
            'id_estado' =>'7',
            'municipio' => 'UNIÓN JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '193', 
            'id_estado' =>'7',
            'municipio' => 'VENUSTIANO CARRANZA', 
        ));

        Municipio::create(array(
            'id' => '194', 
            'id_estado' =>'7',
            'municipio' => 'VILLA COMALTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '195', 
            'id_estado' =>'7',
            'municipio' => 'VILLA CORZO', 
        ));

        Municipio::create(array(
            'id' => '196', 
            'id_estado' =>'7',
            'municipio' => 'VILLAFLORES', 
        ));

        Municipio::create(array(
            'id' => '197', 
            'id_estado' =>'7',
            'municipio' => 'YAJALÓN', 
        ));

        Municipio::create(array(
            'id' => '198', 
            'id_estado' =>'7',
            'municipio' => 'ZINACANTÁN', 
        ));

        Municipio::create(array(
            'id' => '199', 
            'id_estado' =>'8',
            'municipio' => 'AHUMADA', 
        ));

        Municipio::create(array(
            'id' => '200', 
            'id_estado' =>'8',
            'municipio' => 'ALDAMA', 
        ));

        Municipio::create(array(
            'id' => '201', 
            'id_estado' =>'8',
            'municipio' => 'ALLENDE', 
        ));

        Municipio::create(array(
            'id' => '202', 
            'id_estado' =>'8',
            'municipio' => 'AQUILES SERDÁN', 
        ));

        Municipio::create(array(
            'id' => '203', 
            'id_estado' =>'8',
            'municipio' => 'ASCENSIÓN', 
        ));

        Municipio::create(array(
            'id' => '204', 
            'id_estado' =>'8',
            'municipio' => 'BACHÍNIVA', 
        ));

        Municipio::create(array(
            'id' => '205', 
            'id_estado' =>'8',
            'municipio' => 'BALLEZA', 
        ));

        Municipio::create(array(
            'id' => '206', 
            'id_estado' =>'8',
            'municipio' => 'BATOPILAS', 
        ));

        Municipio::create(array(
            'id' => '207', 
            'id_estado' =>'8',
            'municipio' => 'BOCOYNA', 
        ));

        Municipio::create(array(
            'id' => '208', 
            'id_estado' =>'8',
            'municipio' => 'BUENAVENTURA', 
        ));

        Municipio::create(array(
            'id' => '209', 
            'id_estado' =>'8',
            'municipio' => 'CAMARGO', 
        ));

        Municipio::create(array(
            'id' => '210', 
            'id_estado' =>'8',
            'municipio' => 'CARICHÍ', 
        ));

        Municipio::create(array(
            'id' => '211', 
            'id_estado' =>'8',
            'municipio' => 'CASAS GRANDES', 
        ));

        Municipio::create(array(
            'id' => '212', 
            'id_estado' =>'8',
            'municipio' => 'CHIHUAHUA', 
        ));

        Municipio::create(array(
            'id' => '213', 
            'id_estado' =>'8',
            'municipio' => 'CHÍNIPAS', 
        ));

        Municipio::create(array(
            'id' => '214', 
            'id_estado' =>'8',
            'municipio' => 'CORONADO', 
        ));

        Municipio::create(array(
            'id' => '215', 
            'id_estado' =>'8',
            'municipio' => 'COYAME DEL SOTOL', 
        ));

        Municipio::create(array(
            'id' => '216', 
            'id_estado' =>'8',
            'municipio' => 'CUAUHTÉMOC', 
        ));

        Municipio::create(array(
            'id' => '217', 
            'id_estado' =>'8',
            'municipio' => 'CUSIHUIRIACHI', 
        ));

        Municipio::create(array(
            'id' => '218', 
            'id_estado' =>'8',
            'municipio' => 'DELICIAS', 
        ));

        Municipio::create(array(
            'id' => '219', 
            'id_estado' =>'8',
            'municipio' => 'DR. BELISARIO DOMÍNGUEZ', 
        ));

        Municipio::create(array(
            'id' => '220', 
            'id_estado' =>'8',
            'municipio' => 'EL TULE', 
        ));

        Municipio::create(array(
            'id' => '221', 
            'id_estado' =>'8',
            'municipio' => 'GALEANA', 
        ));

        Municipio::create(array(
            'id' => '222', 
            'id_estado' =>'8',
            'municipio' => 'GÓMEZ FARÍAS', 
        ));

        Municipio::create(array(
            'id' => '223', 
            'id_estado' =>'8',
            'municipio' => 'GRAN MORELOS', 
        ));

        Municipio::create(array(
            'id' => '224', 
            'id_estado' =>'8',
            'municipio' => 'GUACHOCHI', 
        ));

        Municipio::create(array(
            'id' => '225', 
            'id_estado' =>'8',
            'municipio' => 'GUADALUPE', 
        ));

        Municipio::create(array(
            'id' => '226', 
            'id_estado' =>'8',
            'municipio' => 'GUADALUPE Y CALVO', 
        ));

        Municipio::create(array(
            'id' => '227', 
            'id_estado' =>'8',
            'municipio' => 'GUAZAPARES', 
        ));

        Municipio::create(array(
            'id' => '228', 
            'id_estado' =>'8',
            'municipio' => 'GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '229', 
            'id_estado' =>'8',
            'municipio' => 'HIDALGO DEL PARRAL', 
        ));

        Municipio::create(array(
            'id' => '230', 
            'id_estado' =>'8',
            'municipio' => 'HUEJOTITÁN', 
        ));

        Municipio::create(array(
            'id' => '231', 
            'id_estado' =>'8',
            'municipio' => 'IGNACIO ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '232', 
            'id_estado' =>'8',
            'municipio' => 'JANOS', 
        ));

        Municipio::create(array(
            'id' => '233', 
            'id_estado' =>'8',
            'municipio' => 'JIMÉNEZ', 
        ));

        Municipio::create(array(
            'id' => '234', 
            'id_estado' =>'8',
            'municipio' => 'JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '235', 
            'id_estado' =>'8',
            'municipio' => 'JULIMES', 
        ));

        Municipio::create(array(
            'id' => '236', 
            'id_estado' =>'8',
            'municipio' => 'LA CRUZ', 
        ));

        Municipio::create(array(
            'id' => '237', 
            'id_estado' =>'8',
            'municipio' => 'LÓPEZ', 
        ));

        Municipio::create(array(
            'id' => '238', 
            'id_estado' =>'8',
            'municipio' => 'MADERA', 
        ));

        Municipio::create(array(
            'id' => '239', 
            'id_estado' =>'8',
            'municipio' => 'MAGUARICHI', 
        ));

        Municipio::create(array(
            'id' => '240', 
            'id_estado' =>'8',
            'municipio' => 'MANUEL BENAVIDES', 
        ));

        Municipio::create(array(
            'id' => '241', 
            'id_estado' =>'8',
            'municipio' => 'MATACHÍ', 
        ));

        Municipio::create(array(
            'id' => '242', 
            'id_estado' =>'8',
            'municipio' => 'MATAMOROS', 
        ));

        Municipio::create(array(
            'id' => '243', 
            'id_estado' =>'8',
            'municipio' => 'MEOQUI', 
        ));

        Municipio::create(array(
            'id' => '244', 
            'id_estado' =>'8',
            'municipio' => 'MORELOS', 
        ));

        Municipio::create(array(
            'id' => '245', 
            'id_estado' =>'8',
            'municipio' => 'MORIS', 
        ));

        Municipio::create(array(
            'id' => '246', 
            'id_estado' =>'8',
            'municipio' => 'NAMIQUIPA', 
        ));

        Municipio::create(array(
            'id' => '247', 
            'id_estado' =>'8',
            'municipio' => 'NONOAVA', 
        ));

        Municipio::create(array(
            'id' => '248', 
            'id_estado' =>'8',
            'municipio' => 'NUEVO CASAS GRANDES', 
        ));

        Municipio::create(array(
            'id' => '249', 
            'id_estado' =>'8',
            'municipio' => 'OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '250', 
            'id_estado' =>'8',
            'municipio' => 'OJINAGA', 
        ));

        Municipio::create(array(
            'id' => '251', 
            'id_estado' =>'8',
            'municipio' => 'PRAXEDIS G. GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '252', 
            'id_estado' =>'8',
            'municipio' => 'RIVA PALACIO', 
        ));

        Municipio::create(array(
            'id' => '253', 
            'id_estado' =>'8',
            'municipio' => 'ROSALES', 
        ));

        Municipio::create(array(
            'id' => '254', 
            'id_estado' =>'8',
            'municipio' => 'ROSARIO', 
        ));

        Municipio::create(array(
            'id' => '255', 
            'id_estado' =>'8',
            'municipio' => 'SAN FRANCISCO DE BORJA', 
        ));

        Municipio::create(array(
            'id' => '256', 
            'id_estado' =>'8',
            'municipio' => 'SAN FRANCISCO DE CONCHOS', 
        ));

        Municipio::create(array(
            'id' => '257', 
            'id_estado' =>'8',
            'municipio' => 'SAN FRANCISCO DEL ORO', 
        ));

        Municipio::create(array(
            'id' => '258', 
            'id_estado' =>'8',
            'municipio' => 'SANTA BÁRBARA', 
        ));

        Municipio::create(array(
            'id' => '259', 
            'id_estado' =>'8',
            'municipio' => 'SANTA ISABEL', 
        ));

        Municipio::create(array(
            'id' => '260', 
            'id_estado' =>'8',
            'municipio' => 'SATEVÓ', 
        ));

        Municipio::create(array(
            'id' => '261', 
            'id_estado' =>'8',
            'municipio' => 'SAUCILLO', 
        ));

        Municipio::create(array(
            'id' => '262', 
            'id_estado' =>'8',
            'municipio' => 'TEMÓSACHI', 
        ));

        Municipio::create(array(
            'id' => '263', 
            'id_estado' =>'8',
            'municipio' => 'URIQUE', 
        ));

        Municipio::create(array(
            'id' => '264', 
            'id_estado' =>'8',
            'municipio' => 'URUACHI', 
        ));

        Municipio::create(array(
            'id' => '265', 
            'id_estado' =>'8',
            'municipio' => 'VALLE DE ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '266', 
            'id_estado' =>'9',
            'municipio' => 'ALVARO OBREGÓN', 
        ));

        Municipio::create(array(
            'id' => '267', 
            'id_estado' =>'9',
            'municipio' => 'AZCAPOTZALCO', 
        ));

        Municipio::create(array(
            'id' => '268', 
            'id_estado' =>'9',
            'municipio' => 'BENITO JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '269', 
            'id_estado' =>'9',
            'municipio' => 'COYOACÁN', 
        ));

        Municipio::create(array(
            'id' => '270', 
            'id_estado' =>'9',
            'municipio' => 'CUAJIMALPA DE MORELOS', 
        ));

        Municipio::create(array(
            'id' => '271', 
            'id_estado' =>'9',
            'municipio' => 'CUAUHTÉMOC', 
        ));

        Municipio::create(array(
            'id' => '272', 
            'id_estado' =>'9',
            'municipio' => 'GUSTAVO A. MADERO', 
        ));

        Municipio::create(array(
            'id' => '273', 
            'id_estado' =>'9',
            'municipio' => 'IZTACALCO', 
        ));

        Municipio::create(array(
            'id' => '274', 
            'id_estado' =>'9',
            'municipio' => 'IZTAPALAPA', 
        ));

        Municipio::create(array(
            'id' => '275', 
            'id_estado' =>'9',
            'municipio' => 'LA MAGDALENA CONTRERAS', 
        ));

        Municipio::create(array(
            'id' => '276', 
            'id_estado' =>'9',
            'municipio' => 'MIGUEL HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '277', 
            'id_estado' =>'9',
            'municipio' => 'MILPA ALTA', 
        ));

        Municipio::create(array(
            'id' => '278', 
            'id_estado' =>'9',
            'municipio' => 'TLÁHUAC', 
        ));

        Municipio::create(array(
            'id' => '279', 
            'id_estado' =>'9',
            'municipio' => 'TLALPAN', 
        ));

        Municipio::create(array(
            'id' => '280', 
            'id_estado' =>'9',
            'municipio' => 'VENUSTIANO CARRANZA', 
        ));

        Municipio::create(array(
            'id' => '281', 
            'id_estado' =>'9',
            'municipio' => 'XOCHIMILCO', 
        ));

        Municipio::create(array(
            'id' => '282', 
            'id_estado' =>'10',
            'municipio' => 'CANATLÁN', 
        ));

        Municipio::create(array(
            'id' => '283', 
            'id_estado' =>'10',
            'municipio' => 'CANELAS', 
        ));

        Municipio::create(array(
            'id' => '284', 
            'id_estado' =>'10',
            'municipio' => 'CONETO DE COMONFORT', 
        ));

        Municipio::create(array(
            'id' => '285', 
            'id_estado' =>'10',
            'municipio' => 'CUENCAMÉ', 
        ));

        Municipio::create(array(
            'id' => '286', 
            'id_estado' =>'10',
            'municipio' => 'DURANGO', 
        ));

        Municipio::create(array(
            'id' => '287', 
            'id_estado' =>'10',
            'municipio' => 'EL ORO', 
        ));

        Municipio::create(array(
            'id' => '288', 
            'id_estado' =>'10',
            'municipio' => 'GÓMEZ PALACIO', 
        ));

        Municipio::create(array(
            'id' => '289', 
            'id_estado' =>'10',
            'municipio' => 'GRAL. SIMÓN BOÍVAR', 
        ));

        Municipio::create(array(
            'id' => '290', 
            'id_estado' =>'10',
            'municipio' => 'GUADALUPE VICTORIA', 
        ));

        Municipio::create(array(
            'id' => '291', 
            'id_estado' =>'10',
            'municipio' => 'GUANACEVÍ', 
        ));

        Municipio::create(array(
            'id' => '292', 
            'id_estado' =>'10',
            'municipio' => 'HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '293', 
            'id_estado' =>'10',
            'municipio' => 'INDÉ', 
        ));

        Municipio::create(array(
            'id' => '294', 
            'id_estado' =>'10',
            'municipio' => 'LERDO', 
        ));

        Municipio::create(array(
            'id' => '295', 
            'id_estado' =>'10',
            'municipio' => 'MAPIMÍ', 
        ));

        Municipio::create(array(
            'id' => '296', 
            'id_estado' =>'10',
            'municipio' => 'MEZQUITAL', 
        ));

        Municipio::create(array(
            'id' => '297', 
            'id_estado' =>'10',
            'municipio' => 'NAZAS', 
        ));

        Municipio::create(array(
            'id' => '298', 
            'id_estado' =>'10',
            'municipio' => 'NOMBRE DE DIOS', 
        ));

        Municipio::create(array(
            'id' => '299', 
            'id_estado' =>'10',
            'municipio' => 'NUEVO IDEAL', 
        ));

        Municipio::create(array(
            'id' => '300', 
            'id_estado' =>'10',
            'municipio' => 'OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '301', 
            'id_estado' =>'10',
            'municipio' => 'OTÁEZ', 
        ));

        Municipio::create(array(
            'id' => '302', 
            'id_estado' =>'10',
            'municipio' => 'PÁNUCO DE CORONADO', 
        ));

        Municipio::create(array(
            'id' => '303', 
            'id_estado' =>'10',
            'municipio' => 'PEÑÓN BLANCO', 
        ));

        Municipio::create(array(
            'id' => '304', 
            'id_estado' =>'10',
            'municipio' => 'POANAS', 
        ));

        Municipio::create(array(
            'id' => '305', 
            'id_estado' =>'10',
            'municipio' => 'PUEBLO NUEVO', 
        ));

        Municipio::create(array(
            'id' => '306', 
            'id_estado' =>'10',
            'municipio' => 'RODEO', 
        ));

        Municipio::create(array(
            'id' => '307', 
            'id_estado' =>'10',
            'municipio' => 'SAN BERNARDO', 
        ));

        Municipio::create(array(
            'id' => '308', 
            'id_estado' =>'10',
            'municipio' => 'SAN DIMAS', 
        ));

        Municipio::create(array(
            'id' => '309', 
            'id_estado' =>'10',
            'municipio' => 'SAN JUAN DE GUADALUPE', 
        ));

        Municipio::create(array(
            'id' => '310', 
            'id_estado' =>'10',
            'municipio' => 'SAN JUAN DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '311', 
            'id_estado' =>'10',
            'municipio' => 'SAN LUIS DEL CORDERO', 
        ));

        Municipio::create(array(
            'id' => '312', 
            'id_estado' =>'10',
            'municipio' => 'SAN PEDRO DEL GALLO', 
        ));

        Municipio::create(array(
            'id' => '313', 
            'id_estado' =>'10',
            'municipio' => 'SANTA CLARA', 
        ));

        Municipio::create(array(
            'id' => '314', 
            'id_estado' =>'10',
            'municipio' => 'SANTIAGO PAPASQUIARO', 
        ));

        Municipio::create(array(
            'id' => '315', 
            'id_estado' =>'10',
            'municipio' => 'SÚCHIL', 
        ));

        Municipio::create(array(
            'id' => '316', 
            'id_estado' =>'10',
            'municipio' => 'TAMAZULA', 
        ));

        Municipio::create(array(
            'id' => '317', 
            'id_estado' =>'10',
            'municipio' => 'TEPEHUANES', 
        ));

        Municipio::create(array(
            'id' => '318', 
            'id_estado' =>'10',
            'municipio' => 'TLAHUALILO', 
        ));

        Municipio::create(array(
            'id' => '319', 
            'id_estado' =>'10',
            'municipio' => 'TOPIA', 
        ));

        Municipio::create(array(
            'id' => '320', 
            'id_estado' =>'10',
            'municipio' => 'VICENTE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '321', 
            'id_estado' =>'11',
            'municipio' => 'ABASOLO', 
        ));

        Municipio::create(array(
            'id' => '322', 
            'id_estado' =>'11',
            'municipio' => 'ACÁMBARO', 
        ));

        Municipio::create(array(
            'id' => '323', 
            'id_estado' =>'11',
            'municipio' => 'ALLENDE', 
        ));

        Municipio::create(array(
            'id' => '324', 
            'id_estado' =>'11',
            'municipio' => 'APASEO EL ALTO', 
        ));

        Municipio::create(array(
            'id' => '325', 
            'id_estado' =>'11',
            'municipio' => 'APASEO EL GRANDE', 
        ));

        Municipio::create(array(
            'id' => '326', 
            'id_estado' =>'11',
            'municipio' => 'ATARJEA', 
        ));

        Municipio::create(array(
            'id' => '327', 
            'id_estado' =>'11',
            'municipio' => 'CELAYA', 
        ));

        Municipio::create(array(
            'id' => '328', 
            'id_estado' =>'11',
            'municipio' => 'COMONFORT', 
        ));

        Municipio::create(array(
            'id' => '329', 
            'id_estado' =>'11',
            'municipio' => 'CORONEO', 
        ));

        Municipio::create(array(
            'id' => '330', 
            'id_estado' =>'11',
            'municipio' => 'CORTAZAR', 
        ));

        Municipio::create(array(
            'id' => '331', 
            'id_estado' =>'11',
            'municipio' => 'CUERÁMARO', 
        ));

        Municipio::create(array(
            'id' => '332', 
            'id_estado' =>'11',
            'municipio' => 'DOCTOR MORA', 
        ));

        Municipio::create(array(
            'id' => '333', 
            'id_estado' =>'11',
            'municipio' => 'DOLORES HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '334', 
            'id_estado' =>'11',
            'municipio' => 'GUANAJUATO', 
        ));

        Municipio::create(array(
            'id' => '335', 
            'id_estado' =>'11',
            'municipio' => 'HUANÍMARO', 
        ));

        Municipio::create(array(
            'id' => '336', 
            'id_estado' =>'11',
            'municipio' => 'IRAPUATO', 
        ));

        Municipio::create(array(
            'id' => '337', 
            'id_estado' =>'11',
            'municipio' => 'JARAL DEL PROGRESO', 
        ));

        Municipio::create(array(
            'id' => '338', 
            'id_estado' =>'11',
            'municipio' => 'JERÉCUARO', 
        ));

        Municipio::create(array(
            'id' => '339', 
            'id_estado' =>'11',
            'municipio' => 'LEÓN', 
        ));

        Municipio::create(array(
            'id' => '340', 
            'id_estado' =>'11',
            'municipio' => 'MANUEL DOBLADO', 
        ));

        Municipio::create(array(
            'id' => '341', 
            'id_estado' =>'11',
            'municipio' => 'MOROLEÓN', 
        ));

        Municipio::create(array(
            'id' => '342', 
            'id_estado' =>'11',
            'municipio' => 'OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '343', 
            'id_estado' =>'11',
            'municipio' => 'PÉNJAMO', 
        ));

        Municipio::create(array(
            'id' => '344', 
            'id_estado' =>'11',
            'municipio' => 'PUEBLO NUEVO', 
        ));

        Municipio::create(array(
            'id' => '345', 
            'id_estado' =>'11',
            'municipio' => 'PURÍSIMA DEL RINCÓN', 
        ));

        Municipio::create(array(
            'id' => '346', 
            'id_estado' =>'11',
            'municipio' => 'ROMITA', 
        ));

        Municipio::create(array(
            'id' => '347', 
            'id_estado' =>'11',
            'municipio' => 'SALAMANCA', 
        ));

        Municipio::create(array(
            'id' => '348', 
            'id_estado' =>'11',
            'municipio' => 'SALVATIERRA', 
        ));

        Municipio::create(array(
            'id' => '349', 
            'id_estado' =>'11',
            'municipio' => 'SAN DIEGO DE LA UNIÓN', 
        ));

        Municipio::create(array(
            'id' => '350', 
            'id_estado' =>'11',
            'municipio' => 'SAN FELIPE', 
        ));

        Municipio::create(array(
            'id' => '351', 
            'id_estado' =>'11',
            'municipio' => 'SAN FRANCISCO DEL RINCÓN', 
        ));

        Municipio::create(array(
            'id' => '352', 
            'id_estado' =>'11',
            'municipio' => 'SAN JOSÉ ITURBIDE', 
        ));

        Municipio::create(array(
            'id' => '353', 
            'id_estado' =>'11',
            'municipio' => 'SAN LUIS DE LA PAZ', 
        ));

        Municipio::create(array(
            'id' => '354', 
            'id_estado' =>'11',
            'municipio' => 'SANTA CATARINA', 
        ));

        Municipio::create(array(
            'id' => '355', 
            'id_estado' =>'11',
            'municipio' => 'SANTA CRUZ DE JUVENTINO ROSAS', 
        ));

        Municipio::create(array(
            'id' => '356', 
            'id_estado' =>'11',
            'municipio' => 'SANTIAGO MARAVATÍO', 
        ));

        Municipio::create(array(
            'id' => '357', 
            'id_estado' =>'11',
            'municipio' => 'SILAO', 
        ));

        Municipio::create(array(
            'id' => '358', 
            'id_estado' =>'11',
            'municipio' => 'TARANDACUAO', 
        ));

        Municipio::create(array(
            'id' => '359', 
            'id_estado' =>'11',
            'municipio' => 'TARIMORO', 
        ));

        Municipio::create(array(
            'id' => '360', 
            'id_estado' =>'11',
            'municipio' => 'TIERRA BLANCA', 
        ));

        Municipio::create(array(
            'id' => '361', 
            'id_estado' =>'11',
            'municipio' => 'URIANGATO', 
        ));

        Municipio::create(array(
            'id' => '362', 
            'id_estado' =>'11',
            'municipio' => 'VALLE DE SANTIAGO', 
        ));

        Municipio::create(array(
            'id' => '363', 
            'id_estado' =>'11',
            'municipio' => 'VICTORIA', 
        ));

        Municipio::create(array(
            'id' => '364', 
            'id_estado' =>'11',
            'municipio' => 'VILLAGRÁN', 
        ));

        Municipio::create(array(
            'id' => '365', 
            'id_estado' =>'11',
            'municipio' => 'XICHÚ', 
        ));

        Municipio::create(array(
            'id' => '366', 
            'id_estado' =>'11',
            'municipio' => 'YURIRIA', 
        ));

        Municipio::create(array(
            'id' => '367', 
            'id_estado' =>'12',
            'municipio' => 'ACAPULCO DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '368', 
            'id_estado' =>'12',
            'municipio' => 'ACATEPEC', 
        ));

        Municipio::create(array(
            'id' => '369', 
            'id_estado' =>'12',
            'municipio' => 'AHUACUOTZINGO', 
        ));

        Municipio::create(array(
            'id' => '370', 
            'id_estado' =>'12',
            'municipio' => 'AJUCHITLÁN DEL PROGRESO', 
        ));

        Municipio::create(array(
            'id' => '371', 
            'id_estado' =>'12',
            'municipio' => 'ALCOZAUCA DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '372', 
            'id_estado' =>'12',
            'municipio' => 'ALPOYECA', 
        ));

        Municipio::create(array(
            'id' => '373', 
            'id_estado' =>'12',
            'municipio' => 'APAXTLA', 
        ));

        Municipio::create(array(
            'id' => '374', 
            'id_estado' =>'12',
            'municipio' => 'ARCELIA', 
        ));

        Municipio::create(array(
            'id' => '375', 
            'id_estado' =>'12',
            'municipio' => 'ATENANGO DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '376', 
            'id_estado' =>'12',
            'municipio' => 'ATLAMAJALCINGO DEL MONTE', 
        ));

        Municipio::create(array(
            'id' => '377', 
            'id_estado' =>'12',
            'municipio' => 'ATLIXTAC', 
        ));

        Municipio::create(array(
            'id' => '378', 
            'id_estado' =>'12',
            'municipio' => 'ATOYAC DE ALVAREZ', 
        ));

        Municipio::create(array(
            'id' => '379', 
            'id_estado' =>'12',
            'municipio' => 'AYUTLA DE LOS LIBRES', 
        ));

        Municipio::create(array(
            'id' => '380', 
            'id_estado' =>'12',
            'municipio' => 'AZOYÚ', 
        ));

        Municipio::create(array(
            'id' => '381', 
            'id_estado' =>'12',
            'municipio' => 'BENITO JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '382', 
            'id_estado' =>'12',
            'municipio' => 'BUENAVISTA DE CUÉLLAR', 
        ));

        Municipio::create(array(
            'id' => '383', 
            'id_estado' =>'12',
            'municipio' => 'CHILAPA DE ALVAREZ', 
        ));

        Municipio::create(array(
            'id' => '384', 
            'id_estado' =>'12',
            'municipio' => 'CHILPANCINGO DE LOS BRAVO', 
        ));

        Municipio::create(array(
            'id' => '385', 
            'id_estado' =>'12',
            'municipio' => 'COAHUAYUTLA DE JOSÉ MARÍA IZAZAGA', 
        ));

        Municipio::create(array(
            'id' => '386', 
            'id_estado' =>'12',
            'municipio' => 'COCHOAPA EL GRANDE', 
        ));

        Municipio::create(array(
            'id' => '387', 
            'id_estado' =>'12',
            'municipio' => 'COCULA', 
        ));

        Municipio::create(array(
            'id' => '388', 
            'id_estado' =>'12',
            'municipio' => 'COPALA', 
        ));

        Municipio::create(array(
            'id' => '389', 
            'id_estado' =>'12',
            'municipio' => 'COPALILLO', 
        ));

        Municipio::create(array(
            'id' => '390', 
            'id_estado' =>'12',
            'municipio' => 'COPANATOYAC', 
        ));

        Municipio::create(array(
            'id' => '391', 
            'id_estado' =>'12',
            'municipio' => 'COYUCA DE BENÍTEZ', 
        ));

        Municipio::create(array(
            'id' => '392', 
            'id_estado' =>'12',
            'municipio' => 'COYUCA DE CATALÁN', 
        ));

        Municipio::create(array(
            'id' => '393', 
            'id_estado' =>'12',
            'municipio' => 'CUAJINICUILAPA', 
        ));

        Municipio::create(array(
            'id' => '394', 
            'id_estado' =>'12',
            'municipio' => 'CUALÁC', 
        ));

        Municipio::create(array(
            'id' => '395', 
            'id_estado' =>'12',
            'municipio' => 'CUAUTEPEC', 
        ));

        Municipio::create(array(
            'id' => '396', 
            'id_estado' =>'12',
            'municipio' => 'CUETZALA DEL PROGRESO', 
        ));

        Municipio::create(array(
            'id' => '397', 
            'id_estado' =>'12',
            'municipio' => 'CUTZAMALA DE PINZÓN', 
        ));

        Municipio::create(array(
            'id' => '398', 
            'id_estado' =>'12',
            'municipio' => 'EDUARDO NERI', 
        ));

        Municipio::create(array(
            'id' => '399', 
            'id_estado' =>'12',
            'municipio' => 'FLORENCIO VILLARREAL', 
        ));

        Municipio::create(array(
            'id' => '400', 
            'id_estado' =>'12',
            'municipio' => 'GENERAL CANUTO A. NERI', 
        ));

        Municipio::create(array(
            'id' => '401', 
            'id_estado' =>'12',
            'municipio' => 'GENERAL HELIODORO CASTILLO', 
        ));

        Municipio::create(array(
            'id' => '402', 
            'id_estado' =>'12',
            'municipio' => 'HUAMUXTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '403', 
            'id_estado' =>'12',
            'municipio' => 'HUITZUCO DE LOS FIGUEROA', 
        ));

        Municipio::create(array(
            'id' => '404', 
            'id_estado' =>'12',
            'municipio' => 'IGUALA DE LA INDEPENDENCIA', 
        ));

        Municipio::create(array(
            'id' => '405', 
            'id_estado' =>'12',
            'municipio' => 'IGUALAPA', 
        ));

        Municipio::create(array(
            'id' => '406', 
            'id_estado' =>'12',
            'municipio' => 'IXCATEOPAN DE CUAUHTÉMOC', 
        ));

        Municipio::create(array(
            'id' => '407', 
            'id_estado' =>'12',
            'municipio' => 'JOSÉ AZUETA', 
        ));

        Municipio::create(array(
            'id' => '408', 
            'id_estado' =>'12',
            'municipio' => 'JOSÉ JOAQUIN DE HERRERA', 
        ));

        Municipio::create(array(
            'id' => '409', 
            'id_estado' =>'12',
            'municipio' => 'JUAN R. ESCUDERO', 
        ));

        Municipio::create(array(
            'id' => '410', 
            'id_estado' =>'12',
            'municipio' => 'LA UNIÓN DE ISIDORO MONTES DE OCA', 
        ));

        Municipio::create(array(
            'id' => '411', 
            'id_estado' =>'12',
            'municipio' => 'LEONARDO BRAVO', 
        ));

        Municipio::create(array(
            'id' => '412', 
            'id_estado' =>'12',
            'municipio' => 'MALINALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '413', 
            'id_estado' =>'12',
            'municipio' => 'MARQUELIA', 
        ));

        Municipio::create(array(
            'id' => '414', 
            'id_estado' =>'12',
            'municipio' => 'MÁRTIR DE CUILAPAN', 
        ));

        Municipio::create(array(
            'id' => '415', 
            'id_estado' =>'12',
            'municipio' => 'METLATÓNOC', 
        ));

        Municipio::create(array(
            'id' => '416', 
            'id_estado' =>'12',
            'municipio' => 'MOCHITLÁN', 
        ));

        Municipio::create(array(
            'id' => '417', 
            'id_estado' =>'12',
            'municipio' => 'OLINALÁ', 
        ));

        Municipio::create(array(
            'id' => '418', 
            'id_estado' =>'12',
            'municipio' => 'OMETEPEC', 
        ));

        Municipio::create(array(
            'id' => '419', 
            'id_estado' =>'12',
            'municipio' => 'PEDRO ASCENCIO ALQUISIRAS', 
        ));

        Municipio::create(array(
            'id' => '420', 
            'id_estado' =>'12',
            'municipio' => 'PETATLÁN', 
        ));

        Municipio::create(array(
            'id' => '421', 
            'id_estado' =>'12',
            'municipio' => 'PILCAYA', 
        ));

        Municipio::create(array(
            'id' => '422', 
            'id_estado' =>'12',
            'municipio' => 'PUNGARABATO', 
        ));

        Municipio::create(array(
            'id' => '423', 
            'id_estado' =>'12',
            'municipio' => 'QUECHULTENANGO', 
        ));

        Municipio::create(array(
            'id' => '424', 
            'id_estado' =>'12',
            'municipio' => 'SAN LUIS ACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '425', 
            'id_estado' =>'12',
            'municipio' => 'SAN MARCOS', 
        ));

        Municipio::create(array(
            'id' => '426', 
            'id_estado' =>'12',
            'municipio' => 'SAN MIGUEL TOTOLAPAN', 
        ));

        Municipio::create(array(
            'id' => '427', 
            'id_estado' =>'12',
            'municipio' => 'TAXCO DE ALARCÓN', 
        ));

        Municipio::create(array(
            'id' => '428', 
            'id_estado' =>'12',
            'municipio' => 'TECOANAPA', 
        ));

        Municipio::create(array(
            'id' => '429', 
            'id_estado' =>'12',
            'municipio' => 'TÉCPAN DE GALEANA', 
        ));

        Municipio::create(array(
            'id' => '430', 
            'id_estado' =>'12',
            'municipio' => 'TELOLOAPAN', 
        ));

        Municipio::create(array(
            'id' => '431', 
            'id_estado' =>'12',
            'municipio' => 'TEPECOACUILCO DE TRUJANO', 
        ));

        Municipio::create(array(
            'id' => '432', 
            'id_estado' =>'12',
            'municipio' => 'TETIPAC', 
        ));

        Municipio::create(array(
            'id' => '433', 
            'id_estado' =>'12',
            'municipio' => 'TIXTLA DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '434', 
            'id_estado' =>'12',
            'municipio' => 'TLACOACHISTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '435', 
            'id_estado' =>'12',
            'municipio' => 'TLACOAPA', 
        ));

        Municipio::create(array(
            'id' => '436', 
            'id_estado' =>'12',
            'municipio' => 'TLALCHAPA', 
        ));

        Municipio::create(array(
            'id' => '437', 
            'id_estado' =>'12',
            'municipio' => 'TLALIXTAQUILLA DE MALDONADO', 
        ));

        Municipio::create(array(
            'id' => '438', 
            'id_estado' =>'12',
            'municipio' => 'TLAPA DE COMONFORT', 
        ));

        Municipio::create(array(
            'id' => '439', 
            'id_estado' =>'12',
            'municipio' => 'TLAPEHUALA', 
        ));

        Municipio::create(array(
            'id' => '440', 
            'id_estado' =>'12',
            'municipio' => 'XALPATLÁHUAC', 
        ));

        Municipio::create(array(
            'id' => '441', 
            'id_estado' =>'12',
            'municipio' => 'XOCHIHUEHUETLÁN', 
        ));

        Municipio::create(array(
            'id' => '442', 
            'id_estado' =>'12',
            'municipio' => 'XOCHISTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '443', 
            'id_estado' =>'12',
            'municipio' => 'ZAPOTITLÁN TABLAS', 
        ));

        Municipio::create(array(
            'id' => '444', 
            'id_estado' =>'12',
            'municipio' => 'ZIRÁNDARO', 
        ));

        Municipio::create(array(
            'id' => '445', 
            'id_estado' =>'12',
            'municipio' => 'ZITLALA', 
        ));

        Municipio::create(array(
            'id' => '446', 
            'id_estado' =>'13',
            'municipio' => 'ACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '447', 
            'id_estado' =>'13',
            'municipio' => 'ACAXOCHITLÁN', 
        ));

        Municipio::create(array(
            'id' => '448', 
            'id_estado' =>'13',
            'municipio' => 'ACTOPAN', 
        ));

        Municipio::create(array(
            'id' => '449', 
            'id_estado' =>'13',
            'municipio' => 'AGUA BLANCA DE ITURBIDE', 
        ));

        Municipio::create(array(
            'id' => '450', 
            'id_estado' =>'13',
            'municipio' => 'AJACUBA', 
        ));

        Municipio::create(array(
            'id' => '451', 
            'id_estado' =>'13',
            'municipio' => 'ALFAJAYUCAN', 
        ));

        Municipio::create(array(
            'id' => '452', 
            'id_estado' =>'13',
            'municipio' => 'ALMOLOYA', 
        ));

        Municipio::create(array(
            'id' => '453', 
            'id_estado' =>'13',
            'municipio' => 'APAN', 
        ));

        Municipio::create(array(
            'id' => '454', 
            'id_estado' =>'13',
            'municipio' => 'ATITALAQUIA', 
        ));

        Municipio::create(array(
            'id' => '455', 
            'id_estado' =>'13',
            'municipio' => 'ATLAPEXCO', 
        ));

        Municipio::create(array(
            'id' => '456', 
            'id_estado' =>'13',
            'municipio' => 'ATOTONILCO DE TULA', 
        ));

        Municipio::create(array(
            'id' => '457', 
            'id_estado' =>'13',
            'municipio' => 'ATOTONILCO EL GRANDE', 
        ));

        Municipio::create(array(
            'id' => '458', 
            'id_estado' =>'13',
            'municipio' => 'CALNALI', 
        ));

        Municipio::create(array(
            'id' => '459', 
            'id_estado' =>'13',
            'municipio' => 'CARDONAL', 
        ));

        Municipio::create(array(
            'id' => '460', 
            'id_estado' =>'13',
            'municipio' => 'CHAPANTONGO', 
        ));

        Municipio::create(array(
            'id' => '461', 
            'id_estado' =>'13',
            'municipio' => 'CHAPULHUACÁN', 
        ));

        Municipio::create(array(
            'id' => '462', 
            'id_estado' =>'13',
            'municipio' => 'CHILCUAUTLA', 
        ));

        Municipio::create(array(
            'id' => '463', 
            'id_estado' =>'13',
            'municipio' => 'CUAUTEPEC DE HINOJOSA', 
        ));

        Municipio::create(array(
            'id' => '464', 
            'id_estado' =>'13',
            'municipio' => 'EL ARENAL', 
        ));

        Municipio::create(array(
            'id' => '465', 
            'id_estado' =>'13',
            'municipio' => 'ELOXOCHITLÁN', 
        ));

        Municipio::create(array(
            'id' => '466', 
            'id_estado' =>'13',
            'municipio' => 'EMILIANO ZAPATA', 
        ));

        Municipio::create(array(
            'id' => '467', 
            'id_estado' =>'13',
            'municipio' => 'EPAZOYUCAN', 
        ));

        Municipio::create(array(
            'id' => '468', 
            'id_estado' =>'13',
            'municipio' => 'FRANCISCO I. MADERO', 
        ));

        Municipio::create(array(
            'id' => '469', 
            'id_estado' =>'13',
            'municipio' => 'HUASCA DE OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '470', 
            'id_estado' =>'13',
            'municipio' => 'HUAUTLA', 
        ));

        Municipio::create(array(
            'id' => '471', 
            'id_estado' =>'13',
            'municipio' => 'HUAZALINGO', 
        ));

        Municipio::create(array(
            'id' => '472', 
            'id_estado' =>'13',
            'municipio' => 'HUEHUETLA', 
        ));

        Municipio::create(array(
            'id' => '473', 
            'id_estado' =>'13',
            'municipio' => 'HUEJUTLA DE REYES', 
        ));

        Municipio::create(array(
            'id' => '474', 
            'id_estado' =>'13',
            'municipio' => 'HUICHAPAN', 
        ));

        Municipio::create(array(
            'id' => '475', 
            'id_estado' =>'13',
            'municipio' => 'IXMIQUILPAN', 
        ));

        Municipio::create(array(
            'id' => '476', 
            'id_estado' =>'13',
            'municipio' => 'JACALA DE LEDEZMA', 
        ));

        Municipio::create(array(
            'id' => '477', 
            'id_estado' =>'13',
            'municipio' => 'JALTOCÁN', 
        ));

        Municipio::create(array(
            'id' => '478', 
            'id_estado' =>'13',
            'municipio' => 'JUÁREZ HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '479', 
            'id_estado' =>'13',
            'municipio' => 'LA MISIÓN', 
        ));

        Municipio::create(array(
            'id' => '480', 
            'id_estado' =>'13',
            'municipio' => 'LOLOTLA', 
        ));

        Municipio::create(array(
            'id' => '481', 
            'id_estado' =>'13',
            'municipio' => 'METEPEC', 
        ));

        Municipio::create(array(
            'id' => '482', 
            'id_estado' =>'13',
            'municipio' => 'METZTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '483', 
            'id_estado' =>'13',
            'municipio' => 'MINERAL DE LA REFORMA', 
        ));

        Municipio::create(array(
            'id' => '484', 
            'id_estado' =>'13',
            'municipio' => 'MINERAL DEL CHICO', 
        ));

        Municipio::create(array(
            'id' => '485', 
            'id_estado' =>'13',
            'municipio' => 'MINERAL DEL MONTE', 
        ));

        Municipio::create(array(
            'id' => '486', 
            'id_estado' =>'13',
            'municipio' => 'MIXQUIAHUALA DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '487', 
            'id_estado' =>'13',
            'municipio' => 'MOLANGO DE ESCAMILLA', 
        ));

        Municipio::create(array(
            'id' => '488', 
            'id_estado' =>'13',
            'municipio' => 'NICOLÁS FLORES', 
        ));

        Municipio::create(array(
            'id' => '489', 
            'id_estado' =>'13',
            'municipio' => 'NOPALA DE VILLAGRÁN', 
        ));

        Municipio::create(array(
            'id' => '490', 
            'id_estado' =>'13',
            'municipio' => 'OMITLÁN DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '491', 
            'id_estado' =>'13',
            'municipio' => 'PACHUCA DE SOTO', 
        ));

        Municipio::create(array(
            'id' => '492', 
            'id_estado' =>'13',
            'municipio' => 'PACULA', 
        ));

        Municipio::create(array(
            'id' => '493', 
            'id_estado' =>'13',
            'municipio' => 'PISAFLORES', 
        ));

        Municipio::create(array(
            'id' => '494', 
            'id_estado' =>'13',
            'municipio' => 'PROGRESO DE OBREGÓN', 
        ));

        Municipio::create(array(
            'id' => '495', 
            'id_estado' =>'13',
            'municipio' => 'SAN AGUSTÍN METZQUITITLÁN', 
        ));

        Municipio::create(array(
            'id' => '496', 
            'id_estado' =>'13',
            'municipio' => 'SAN AGUSTÍN TLAXIACA', 
        ));

        Municipio::create(array(
            'id' => '497', 
            'id_estado' =>'13',
            'municipio' => 'SAN BARTOLO TUTOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '498', 
            'id_estado' =>'13',
            'municipio' => 'SAN FELIPE ORIZATLÁN', 
        ));

        Municipio::create(array(
            'id' => '499', 
            'id_estado' =>'13',
            'municipio' => 'SAN SALVADOR', 
        ));

        Municipio::create(array(
            'id' => '500', 
            'id_estado' =>'13',
            'municipio' => 'SANTIAGO DE ANAYA', 
        ));

        Municipio::create(array(
            'id' => '501', 
            'id_estado' =>'13',
            'municipio' => 'SANTIAGO TULANTEPEC DE LUGO GUERRE', 
        ));

        Municipio::create(array(
            'id' => '502', 
            'id_estado' =>'13',
            'municipio' => 'SINGUILUCAN', 
        ));

        Municipio::create(array(
            'id' => '503', 
            'id_estado' =>'13',
            'municipio' => 'TASQUILLO', 
        ));

        Municipio::create(array(
            'id' => '504', 
            'id_estado' =>'13',
            'municipio' => 'TECOZAUTLA', 
        ));

        Municipio::create(array(
            'id' => '505', 
            'id_estado' =>'13',
            'municipio' => 'TENANGO DE DORIA', 
        ));

        Municipio::create(array(
            'id' => '506', 
            'id_estado' =>'13',
            'municipio' => 'TEPEAPULCO', 
        ));

        Municipio::create(array(
            'id' => '507', 
            'id_estado' =>'13',
            'municipio' => 'TEPEHUACÁN DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '508', 
            'id_estado' =>'13',
            'municipio' => 'TEPEJI DEL RÍO DE OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '509', 
            'id_estado' =>'13',
            'municipio' => 'TEPETITLÁN', 
        ));

        Municipio::create(array(
            'id' => '510', 
            'id_estado' =>'13',
            'municipio' => 'TETEPANGO', 
        ));

        Municipio::create(array(
            'id' => '511', 
            'id_estado' =>'13',
            'municipio' => 'TEZONTEPEC DE ALDAMA', 
        ));

        Municipio::create(array(
            'id' => '512', 
            'id_estado' =>'13',
            'municipio' => 'TIANGUISTENGO', 
        ));

        Municipio::create(array(
            'id' => '513', 
            'id_estado' =>'13',
            'municipio' => 'TIZAYUCA', 
        ));

        Municipio::create(array(
            'id' => '514', 
            'id_estado' =>'13',
            'municipio' => 'TLAHUELILPAN', 
        ));

        Municipio::create(array(
            'id' => '515', 
            'id_estado' =>'13',
            'municipio' => 'TLAHUILTEPA', 
        ));

        Municipio::create(array(
            'id' => '516', 
            'id_estado' =>'13',
            'municipio' => 'TLANALAPA', 
        ));

        Municipio::create(array(
            'id' => '517', 
            'id_estado' =>'13',
            'municipio' => 'TLANCHINOL', 
        ));

        Municipio::create(array(
            'id' => '518', 
            'id_estado' =>'13',
            'municipio' => 'TLAXCOAPAN', 
        ));

        Municipio::create(array(
            'id' => '519', 
            'id_estado' =>'13',
            'municipio' => 'TOLCAYUCA', 
        ));

        Municipio::create(array(
            'id' => '520', 
            'id_estado' =>'13',
            'municipio' => 'TULA DE ALLENDE', 
        ));

        Municipio::create(array(
            'id' => '521', 
            'id_estado' =>'13',
            'municipio' => 'TULANCINGO DE BRAVO', 
        ));

        Municipio::create(array(
            'id' => '522', 
            'id_estado' =>'13',
            'municipio' => 'VILLA DE TEZONTEPEC', 
        ));

        Municipio::create(array(
            'id' => '523', 
            'id_estado' =>'13',
            'municipio' => 'XOCHIATIPAN', 
        ));

        Municipio::create(array(
            'id' => '524', 
            'id_estado' =>'13',
            'municipio' => 'XOCHICOATLÁN', 
        ));

        Municipio::create(array(
            'id' => '525', 
            'id_estado' =>'13',
            'municipio' => 'YAHUALICA', 
        ));

        Municipio::create(array(
            'id' => '526', 
            'id_estado' =>'13',
            'municipio' => 'ZACUALTIPÁN DE ?NGELES', 
        ));

        Municipio::create(array(
            'id' => '527', 
            'id_estado' =>'13',
            'municipio' => 'ZAPOTLÁN DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '528', 
            'id_estado' =>'13',
            'municipio' => 'ZEMPOALA', 
        ));

        Municipio::create(array(
            'id' => '529', 
            'id_estado' =>'13',
            'municipio' => 'ZIMAPÁN', 
        ));

        Municipio::create(array(
            'id' => '530', 
            'id_estado' =>'14',
            'municipio' => 'ACATIC', 
        ));

        Municipio::create(array(
            'id' => '531', 
            'id_estado' =>'14',
            'municipio' => 'ACATLÁN DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '532', 
            'id_estado' =>'14',
            'municipio' => 'AHUALULCO DE MERCADO', 
        ));

        Municipio::create(array(
            'id' => '533', 
            'id_estado' =>'14',
            'municipio' => 'AMACUECA', 
        ));

        Municipio::create(array(
            'id' => '534', 
            'id_estado' =>'14',
            'municipio' => 'AMATITÁN', 
        ));

        Municipio::create(array(
            'id' => '535', 
            'id_estado' =>'14',
            'municipio' => 'AMECA', 
        ));

        Municipio::create(array(
            'id' => '536', 
            'id_estado' =>'14',
            'municipio' => 'ARANDAS', 
        ));

        Municipio::create(array(
            'id' => '537', 
            'id_estado' =>'14',
            'municipio' => 'ATEMAJAC DE BRIZUELA', 
        ));

        Municipio::create(array(
            'id' => '538', 
            'id_estado' =>'14',
            'municipio' => 'ATENGO', 
        ));

        Municipio::create(array(
            'id' => '539', 
            'id_estado' =>'14',
            'municipio' => 'ATENGUILLO', 
        ));

        Municipio::create(array(
            'id' => '540', 
            'id_estado' =>'14',
            'municipio' => 'ATOTONILCO EL ALTO', 
        ));

        Municipio::create(array(
            'id' => '541', 
            'id_estado' =>'14',
            'municipio' => 'ATOYAC', 
        ));

        Municipio::create(array(
            'id' => '542', 
            'id_estado' =>'14',
            'municipio' => 'AUTLÁN DE NAVARRO', 
        ));

        Municipio::create(array(
            'id' => '543', 
            'id_estado' =>'14',
            'municipio' => 'AYOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '544', 
            'id_estado' =>'14',
            'municipio' => 'AYUTLA', 
        ));

        Municipio::create(array(
            'id' => '545', 
            'id_estado' =>'14',
            'municipio' => 'BOLAÑOS', 
        ));

        Municipio::create(array(
            'id' => '546', 
            'id_estado' =>'14',
            'municipio' => 'CABO CORRIENTES', 
        ));

        Municipio::create(array(
            'id' => '547', 
            'id_estado' =>'14',
            'municipio' => 'CAÑADAS DE OBREGÓN', 
        ));

        Municipio::create(array(
            'id' => '548', 
            'id_estado' =>'14',
            'municipio' => 'CASIMIRO CASTILLO', 
        ));

        Municipio::create(array(
            'id' => '549', 
            'id_estado' =>'14',
            'municipio' => 'CHAPALA', 
        ));

        Municipio::create(array(
            'id' => '550', 
            'id_estado' =>'14',
            'municipio' => 'CHIMALTITÁN', 
        ));

        Municipio::create(array(
            'id' => '551', 
            'id_estado' =>'14',
            'municipio' => 'CHIQUILISTLÁN', 
        ));

        Municipio::create(array(
            'id' => '552', 
            'id_estado' =>'14',
            'municipio' => 'CIHUATLÁN', 
        ));

        Municipio::create(array(
            'id' => '553', 
            'id_estado' =>'14',
            'municipio' => 'COCULA', 
        ));

        Municipio::create(array(
            'id' => '554', 
            'id_estado' =>'14',
            'municipio' => 'COLOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '555', 
            'id_estado' =>'14',
            'municipio' => 'CONCEPCIÓN DE BUENOS AIRES', 
        ));

        Municipio::create(array(
            'id' => '556', 
            'id_estado' =>'14',
            'municipio' => 'CUAUTITLÁN DE GARCÍA BARRAGÁN', 
        ));

        Municipio::create(array(
            'id' => '557', 
            'id_estado' =>'14',
            'municipio' => 'CUAUTLA', 
        ));

        Municipio::create(array(
            'id' => '558', 
            'id_estado' =>'14',
            'municipio' => 'CUQUÍO', 
        ));

        Municipio::create(array(
            'id' => '559', 
            'id_estado' =>'14',
            'municipio' => 'DEGOLLADO', 
        ));

        Municipio::create(array(
            'id' => '560', 
            'id_estado' =>'14',
            'municipio' => 'EJUTLA', 
        ));

        Municipio::create(array(
            'id' => '561', 
            'id_estado' =>'14',
            'municipio' => 'EL ARENAL', 
        ));

        Municipio::create(array(
            'id' => '562', 
            'id_estado' =>'14',
            'municipio' => 'EL GRULLO', 
        ));

        Municipio::create(array(
            'id' => '563', 
            'id_estado' =>'14',
            'municipio' => 'EL LIMÓN', 
        ));

        Municipio::create(array(
            'id' => '564', 
            'id_estado' =>'14',
            'municipio' => 'EL SALTO', 
        ));

        Municipio::create(array(
            'id' => '565', 
            'id_estado' =>'14',
            'municipio' => 'ENCARNACIÓN DE DÍAZ', 
        ));

        Municipio::create(array(
            'id' => '566', 
            'id_estado' =>'14',
            'municipio' => 'ETZATLÁN', 
        ));

        Municipio::create(array(
            'id' => '567', 
            'id_estado' =>'14',
            'municipio' => 'GÓMEZ FARÍAS', 
        ));

        Municipio::create(array(
            'id' => '568', 
            'id_estado' =>'14',
            'municipio' => 'GUACHINANGO', 
        ));

        Municipio::create(array(
            'id' => '569', 
            'id_estado' =>'14',
            'municipio' => 'GUADALAJARA', 
        ));

        Municipio::create(array(
            'id' => '570', 
            'id_estado' =>'14',
            'municipio' => 'HOSTOTIPAQUILLO', 
        ));

        Municipio::create(array(
            'id' => '571', 
            'id_estado' =>'14',
            'municipio' => 'HUEJÚCAR', 
        ));

        Municipio::create(array(
            'id' => '572', 
            'id_estado' =>'14',
            'municipio' => 'HUEJUQUILLA EL ALTO', 
        ));

        Municipio::create(array(
            'id' => '573', 
            'id_estado' =>'14',
            'municipio' => 'IXTLAHUACÁN DE LOS MEMBRILLOS', 
        ));

        Municipio::create(array(
            'id' => '574', 
            'id_estado' =>'14',
            'municipio' => 'IXTLAHUACÁN DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '575', 
            'id_estado' =>'14',
            'municipio' => 'JALOSTOTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '576', 
            'id_estado' =>'14',
            'municipio' => 'JAMAY', 
        ));

        Municipio::create(array(
            'id' => '577', 
            'id_estado' =>'14',
            'municipio' => 'JESÚS MARÍA', 
        ));

        Municipio::create(array(
            'id' => '578', 
            'id_estado' =>'14',
            'municipio' => 'JILOTLÁN DE LOS DOLORES', 
        ));

        Municipio::create(array(
            'id' => '579', 
            'id_estado' =>'14',
            'municipio' => 'JOCOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '580', 
            'id_estado' =>'14',
            'municipio' => 'JUANACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '581', 
            'id_estado' =>'14',
            'municipio' => 'JUCHITLÁN', 
        ));

        Municipio::create(array(
            'id' => '582', 
            'id_estado' =>'14',
            'municipio' => 'LA BARCA', 
        ));

        Municipio::create(array(
            'id' => '583', 
            'id_estado' =>'14',
            'municipio' => 'LA HUERTA', 
        ));

        Municipio::create(array(
            'id' => '584', 
            'id_estado' =>'14',
            'municipio' => 'LA MANZANILLA DE LA PAZ', 
        ));

        Municipio::create(array(
            'id' => '585', 
            'id_estado' =>'14',
            'municipio' => 'LAGOS DE MORENO', 
        ));

        Municipio::create(array(
            'id' => '586', 
            'id_estado' =>'14',
            'municipio' => 'MAGDALENA', 
        ));

        Municipio::create(array(
            'id' => '587', 
            'id_estado' =>'14',
            'municipio' => 'MASCOTA', 
        ));

        Municipio::create(array(
            'id' => '588', 
            'id_estado' =>'14',
            'municipio' => 'MAZAMITLA', 
        ));

        Municipio::create(array(
            'id' => '589', 
            'id_estado' =>'14',
            'municipio' => 'MEXTICACÁN', 
        ));

        Municipio::create(array(
            'id' => '590', 
            'id_estado' =>'14',
            'municipio' => 'MEZQUITIC', 
        ));

        Municipio::create(array(
            'id' => '591', 
            'id_estado' =>'14',
            'municipio' => 'MIXTLÁN', 
        ));

        Municipio::create(array(
            'id' => '592', 
            'id_estado' =>'14',
            'municipio' => 'OCOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '593', 
            'id_estado' =>'14',
            'municipio' => 'OJUELOS DE JALISCO', 
        ));

        Municipio::create(array(
            'id' => '594', 
            'id_estado' =>'14',
            'municipio' => 'PIHUAMO', 
        ));

        Municipio::create(array(
            'id' => '595', 
            'id_estado' =>'14',
            'municipio' => 'PONCITLÁN', 
        ));

        Municipio::create(array(
            'id' => '596', 
            'id_estado' =>'14',
            'municipio' => 'PUERTO VALLARTA', 
        ));

        Municipio::create(array(
            'id' => '597', 
            'id_estado' =>'14',
            'municipio' => 'QUITUPAN', 
        ));

        Municipio::create(array(
            'id' => '598', 
            'id_estado' =>'14',
            'municipio' => 'SAN CRISTÓBAL DE LA BARRANCA', 
        ));

        Municipio::create(array(
            'id' => '599', 
            'id_estado' =>'14',
            'municipio' => 'SAN DIEGO DE ALEJANDRÍA', 
        ));

        Municipio::create(array(
            'id' => '600', 
            'id_estado' =>'14',
            'municipio' => 'SAN GABRIEL', 
        ));

        Municipio::create(array(
            'id' => '601', 
            'id_estado' =>'14',
            'municipio' => 'SAN JUAN DE LOS LAGOS', 
        ));

        Municipio::create(array(
            'id' => '602', 
            'id_estado' =>'14',
            'municipio' => 'SAN JUANITO DE ESCOBEDO', 
        ));

        Municipio::create(array(
            'id' => '603', 
            'id_estado' =>'14',
            'municipio' => 'SAN JULIÁN', 
        ));

        Municipio::create(array(
            'id' => '604', 
            'id_estado' =>'14',
            'municipio' => 'SAN MARCOS', 
        ));

        Municipio::create(array(
            'id' => '605', 
            'id_estado' =>'14',
            'municipio' => 'SAN MARTÍN DE BOLAÑOS', 
        ));

        Municipio::create(array(
            'id' => '606', 
            'id_estado' =>'14',
            'municipio' => 'SAN MARTÍN HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '607', 
            'id_estado' =>'14',
            'municipio' => 'SAN MIGUEL EL ALTO', 
        ));

        Municipio::create(array(
            'id' => '608', 
            'id_estado' =>'14',
            'municipio' => 'SAN SEBASTIÁN DEL OESTE', 
        ));

        Municipio::create(array(
            'id' => '609', 
            'id_estado' =>'14',
            'municipio' => 'SANTA MARÍA DE LOS ?NGELES', 
        ));

        Municipio::create(array(
            'id' => '610', 
            'id_estado' =>'14',
            'municipio' => 'SANTA MARÍA DEL ORO', 
        ));

        Municipio::create(array(
            'id' => '611', 
            'id_estado' =>'14',
            'municipio' => 'SAYULA', 
        ));

        Municipio::create(array(
            'id' => '612', 
            'id_estado' =>'14',
            'municipio' => 'TALA', 
        ));

        Municipio::create(array(
            'id' => '613', 
            'id_estado' =>'14',
            'municipio' => 'TALPA DE ALLENDE', 
        ));

        Municipio::create(array(
            'id' => '614', 
            'id_estado' =>'14',
            'municipio' => 'TAMAZULA DE GORDIANO', 
        ));

        Municipio::create(array(
            'id' => '615', 
            'id_estado' =>'14',
            'municipio' => 'TAPALPA', 
        ));

        Municipio::create(array(
            'id' => '616', 
            'id_estado' =>'14',
            'municipio' => 'TECALITLÁN', 
        ));

        Municipio::create(array(
            'id' => '617', 
            'id_estado' =>'14',
            'municipio' => 'TECHALUTA DE MONTENEGRO', 
        ));

        Municipio::create(array(
            'id' => '618', 
            'id_estado' =>'14',
            'municipio' => 'TECOLOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '619', 
            'id_estado' =>'14',
            'municipio' => 'TENAMAXTLÁN', 
        ));

        Municipio::create(array(
            'id' => '620', 
            'id_estado' =>'14',
            'municipio' => 'TEOCALTICHE', 
        ));

        Municipio::create(array(
            'id' => '621', 
            'id_estado' =>'14',
            'municipio' => 'TEOCUITATLÁN DE CORONA', 
        ));

        Municipio::create(array(
            'id' => '622', 
            'id_estado' =>'14',
            'municipio' => 'TEPATITLÁN DE MORELOS', 
        ));

        Municipio::create(array(
            'id' => '623', 
            'id_estado' =>'14',
            'municipio' => 'TEQUILA', 
        ));

        Municipio::create(array(
            'id' => '624', 
            'id_estado' =>'14',
            'municipio' => 'TEUCHITLÁN', 
        ));

        Municipio::create(array(
            'id' => '625', 
            'id_estado' =>'14',
            'municipio' => 'TIZAPÁN EL ALTO', 
        ));

        Municipio::create(array(
            'id' => '626', 
            'id_estado' =>'14',
            'municipio' => 'TLAJOMULCO DE ZÚÑIGA', 
        ));

        Municipio::create(array(
            'id' => '627', 
            'id_estado' =>'14',
            'municipio' => 'TLAQUEPAQUE', 
        ));

        Municipio::create(array(
            'id' => '628', 
            'id_estado' =>'14',
            'municipio' => 'TOLIMÁN', 
        ));

        Municipio::create(array(
            'id' => '629', 
            'id_estado' =>'14',
            'municipio' => 'TOMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '630', 
            'id_estado' =>'14',
            'municipio' => 'TONALÁ', 
        ));

        Municipio::create(array(
            'id' => '631', 
            'id_estado' =>'14',
            'municipio' => 'TONAYA', 
        ));

        Municipio::create(array(
            'id' => '632', 
            'id_estado' =>'14',
            'municipio' => 'TONILA', 
        ));

        Municipio::create(array(
            'id' => '633', 
            'id_estado' =>'14',
            'municipio' => 'TOTATICHE', 
        ));

        Municipio::create(array(
            'id' => '634', 
            'id_estado' =>'14',
            'municipio' => 'TOTOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '635', 
            'id_estado' =>'14',
            'municipio' => 'TUXCACUESCO', 
        ));

        Municipio::create(array(
            'id' => '636', 
            'id_estado' =>'14',
            'municipio' => 'TUXCUECA', 
        ));

        Municipio::create(array(
            'id' => '637', 
            'id_estado' =>'14',
            'municipio' => 'TUXPAN', 
        ));

        Municipio::create(array(
            'id' => '638', 
            'id_estado' =>'14',
            'municipio' => 'UNIÓN DE SAN ANTONIO', 
        ));

        Municipio::create(array(
            'id' => '639', 
            'id_estado' =>'14',
            'municipio' => 'UNIÓN DE TULA', 
        ));

        Municipio::create(array(
            'id' => '640', 
            'id_estado' =>'14',
            'municipio' => 'VALLE DE GUADALUPE', 
        ));

        Municipio::create(array(
            'id' => '641', 
            'id_estado' =>'14',
            'municipio' => 'VALLE DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '642', 
            'id_estado' =>'14',
            'municipio' => 'VILLA CORONA', 
        ));

        Municipio::create(array(
            'id' => '643', 
            'id_estado' =>'14',
            'municipio' => 'VILLA GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '644', 
            'id_estado' =>'14',
            'municipio' => 'VILLA HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '645', 
            'id_estado' =>'14',
            'municipio' => 'VILLA PURIFICACIÓN', 
        ));

        Municipio::create(array(
            'id' => '646', 
            'id_estado' =>'14',
            'municipio' => 'YAHUALICA DE GONZÁLEZ GALLO', 
        ));

        Municipio::create(array(
            'id' => '647', 
            'id_estado' =>'14',
            'municipio' => 'ZACOALCO DE TORRES', 
        ));

        Municipio::create(array(
            'id' => '648', 
            'id_estado' =>'14',
            'municipio' => 'ZAPOPAN', 
        ));

        Municipio::create(array(
            'id' => '649', 
            'id_estado' =>'14',
            'municipio' => 'ZAPOTILTIC', 
        ));

        Municipio::create(array(
            'id' => '650', 
            'id_estado' =>'14',
            'municipio' => 'ZAPOTITLÁN DE VADILLO', 
        ));

        Municipio::create(array(
            'id' => '651', 
            'id_estado' =>'14',
            'municipio' => 'ZAPOTLÁN DEL REY', 
        ));

        Municipio::create(array(
            'id' => '652', 
            'id_estado' =>'14',
            'municipio' => 'ZAPOTLÁN EL GRANDE', 
        ));

        Municipio::create(array(
            'id' => '653', 
            'id_estado' =>'14',
            'municipio' => 'ZAPOTLANEJO', 
        ));

        Municipio::create(array(
            'id' => '654', 
            'id_estado' =>'15',
            'municipio' => 'ACAMBAY', 
        ));

        Municipio::create(array(
            'id' => '655', 
            'id_estado' =>'15',
            'municipio' => 'ACOLMAN', 
        ));

        Municipio::create(array(
            'id' => '656', 
            'id_estado' =>'15',
            'municipio' => 'ACULCO', 
        ));

        Municipio::create(array(
            'id' => '657', 
            'id_estado' =>'15',
            'municipio' => 'ALMOLOYA DE ALQUISIRAS', 
        ));

        Municipio::create(array(
            'id' => '658', 
            'id_estado' =>'15',
            'municipio' => 'ALMOLOYA DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '659', 
            'id_estado' =>'15',
            'municipio' => 'ALMOLOYA DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '660', 
            'id_estado' =>'15',
            'municipio' => 'AMANALCO', 
        ));

        Municipio::create(array(
            'id' => '661', 
            'id_estado' =>'15',
            'municipio' => 'AMATEPEC', 
        ));

        Municipio::create(array(
            'id' => '662', 
            'id_estado' =>'15',
            'municipio' => 'AMECAMECA', 
        ));

        Municipio::create(array(
            'id' => '663', 
            'id_estado' =>'15',
            'municipio' => 'APAXCO', 
        ));

        Municipio::create(array(
            'id' => '664', 
            'id_estado' =>'15',
            'municipio' => 'ATENCO', 
        ));

        Municipio::create(array(
            'id' => '665', 
            'id_estado' =>'15',
            'municipio' => 'ATIZAPÁN', 
        ));

        Municipio::create(array(
            'id' => '666', 
            'id_estado' =>'15',
            'municipio' => 'ATIZAPÁN DE ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '667', 
            'id_estado' =>'15',
            'municipio' => 'ATLACOMULCO', 
        ));

        Municipio::create(array(
            'id' => '668', 
            'id_estado' =>'15',
            'municipio' => 'ATLAUTLA', 
        ));

        Municipio::create(array(
            'id' => '669', 
            'id_estado' =>'15',
            'municipio' => 'AXAPUSCO', 
        ));

        Municipio::create(array(
            'id' => '670', 
            'id_estado' =>'15',
            'municipio' => 'AYAPANGO', 
        ));

        Municipio::create(array(
            'id' => '671', 
            'id_estado' =>'15',
            'municipio' => 'CALIMAYA', 
        ));

        Municipio::create(array(
            'id' => '672', 
            'id_estado' =>'15',
            'municipio' => 'CAPULHUAC', 
        ));

        Municipio::create(array(
            'id' => '673', 
            'id_estado' =>'15',
            'municipio' => 'CHALCO', 
        ));

        Municipio::create(array(
            'id' => '674', 
            'id_estado' =>'15',
            'municipio' => 'CHAPA DE MOTA', 
        ));

        Municipio::create(array(
            'id' => '675', 
            'id_estado' =>'15',
            'municipio' => 'CHAPULTEPEC', 
        ));

        Municipio::create(array(
            'id' => '676', 
            'id_estado' =>'15',
            'municipio' => 'CHIAUTLA', 
        ));

        Municipio::create(array(
            'id' => '677', 
            'id_estado' =>'15',
            'municipio' => 'CHICOLOAPAN', 
        ));

        Municipio::create(array(
            'id' => '678', 
            'id_estado' =>'15',
            'municipio' => 'CHICONCUAC', 
        ));

        Municipio::create(array(
            'id' => '679', 
            'id_estado' =>'15',
            'municipio' => 'CHIMALHUACÁN', 
        ));

        Municipio::create(array(
            'id' => '680', 
            'id_estado' =>'15',
            'municipio' => 'COACALCO DE BERRIOZÁBAL', 
        ));

        Municipio::create(array(
            'id' => '681', 
            'id_estado' =>'15',
            'municipio' => 'COATEPEC HARINAS', 
        ));

        Municipio::create(array(
            'id' => '682', 
            'id_estado' =>'15',
            'municipio' => 'COCOTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '683', 
            'id_estado' =>'15',
            'municipio' => 'COYOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '684', 
            'id_estado' =>'15',
            'municipio' => 'CUAUTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '685', 
            'id_estado' =>'15',
            'municipio' => 'CUAUTITLÁN IZCALLI', 
        ));

        Municipio::create(array(
            'id' => '686', 
            'id_estado' =>'15',
            'municipio' => 'DONATO GUERRA', 
        ));

        Municipio::create(array(
            'id' => '687', 
            'id_estado' =>'15',
            'municipio' => 'ECATEPEC DE MORELOS', 
        ));

        Municipio::create(array(
            'id' => '688', 
            'id_estado' =>'15',
            'municipio' => 'ECATZINGO', 
        ));

        Municipio::create(array(
            'id' => '689', 
            'id_estado' =>'15',
            'municipio' => 'EL ORO', 
        ));

        Municipio::create(array(
            'id' => '690', 
            'id_estado' =>'15',
            'municipio' => 'HUEHUETOCA', 
        ));

        Municipio::create(array(
            'id' => '691', 
            'id_estado' =>'15',
            'municipio' => 'HUEYPOXTLA', 
        ));

        Municipio::create(array(
            'id' => '692', 
            'id_estado' =>'15',
            'municipio' => 'HUIXQUILUCAN', 
        ));

        Municipio::create(array(
            'id' => '693', 
            'id_estado' =>'15',
            'municipio' => 'ISIDRO FABELA', 
        ));

        Municipio::create(array(
            'id' => '694', 
            'id_estado' =>'15',
            'municipio' => 'IXTAPALUCA', 
        ));

        Municipio::create(array(
            'id' => '695', 
            'id_estado' =>'15',
            'municipio' => 'IXTAPAN DE LA SAL', 
        ));

        Municipio::create(array(
            'id' => '696', 
            'id_estado' =>'15',
            'municipio' => 'IXTAPAN DEL ORO', 
        ));

        Municipio::create(array(
            'id' => '697', 
            'id_estado' =>'15',
            'municipio' => 'IXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '698', 
            'id_estado' =>'15',
            'municipio' => 'JALTENCO', 
        ));

        Municipio::create(array(
            'id' => '699', 
            'id_estado' =>'15',
            'municipio' => 'JILOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '700', 
            'id_estado' =>'15',
            'municipio' => 'JILOTZINGO', 
        ));

        Municipio::create(array(
            'id' => '701', 
            'id_estado' =>'15',
            'municipio' => 'JIQUIPILCO', 
        ));

        Municipio::create(array(
            'id' => '702', 
            'id_estado' =>'15',
            'municipio' => 'JOCOTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '703', 
            'id_estado' =>'15',
            'municipio' => 'JOQUICINGO', 
        ));

        Municipio::create(array(
            'id' => '704', 
            'id_estado' =>'15',
            'municipio' => 'JUCHITEPEC', 
        ));

        Municipio::create(array(
            'id' => '705', 
            'id_estado' =>'15',
            'municipio' => 'LA PAZ', 
        ));

        Municipio::create(array(
            'id' => '706', 
            'id_estado' =>'15',
            'municipio' => 'LERMA', 
        ));

        Municipio::create(array(
            'id' => '707', 
            'id_estado' =>'15',
            'municipio' => 'LUVIANOS', 
        ));

        Municipio::create(array(
            'id' => '708', 
            'id_estado' =>'15',
            'municipio' => 'MALINALCO', 
        ));

        Municipio::create(array(
            'id' => '709', 
            'id_estado' =>'15',
            'municipio' => 'MELCHOR OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '710', 
            'id_estado' =>'15',
            'municipio' => 'METEPEC', 
        ));

        Municipio::create(array(
            'id' => '711', 
            'id_estado' =>'15',
            'municipio' => 'MEXICALTZINGO', 
        ));

        Municipio::create(array(
            'id' => '712', 
            'id_estado' =>'15',
            'municipio' => 'MORELOS', 
        ));

        Municipio::create(array(
            'id' => '713', 
            'id_estado' =>'15',
            'municipio' => 'NAUCALPAN DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '714', 
            'id_estado' =>'15',
            'municipio' => 'NEXTLALPAN', 
        ));

        Municipio::create(array(
            'id' => '715', 
            'id_estado' =>'15',
            'municipio' => 'NEZAHUALCÓYOTL', 
        ));

        Municipio::create(array(
            'id' => '716', 
            'id_estado' =>'15',
            'municipio' => 'NICOLÁS ROMERO', 
        ));

        Municipio::create(array(
            'id' => '717', 
            'id_estado' =>'15',
            'municipio' => 'NOPALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '718', 
            'id_estado' =>'15',
            'municipio' => 'OCOYOACAC', 
        ));

        Municipio::create(array(
            'id' => '719', 
            'id_estado' =>'15',
            'municipio' => 'OCUILAN', 
        ));

        Municipio::create(array(
            'id' => '720', 
            'id_estado' =>'15',
            'municipio' => 'OTUMBA', 
        ));

        Municipio::create(array(
            'id' => '721', 
            'id_estado' =>'15',
            'municipio' => 'OTZOLOAPAN', 
        ));

        Municipio::create(array(
            'id' => '722', 
            'id_estado' =>'15',
            'municipio' => 'OTZOLOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '723', 
            'id_estado' =>'15',
            'municipio' => 'OZUMBA', 
        ));

        Municipio::create(array(
            'id' => '724', 
            'id_estado' =>'15',
            'municipio' => 'PAPALOTLA', 
        ));

        Municipio::create(array(
            'id' => '725', 
            'id_estado' =>'15',
            'municipio' => 'POLOTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '726', 
            'id_estado' =>'15',
            'municipio' => 'RAYÓN', 
        ));

        Municipio::create(array(
            'id' => '727', 
            'id_estado' =>'15',
            'municipio' => 'SAN ANTONIO LA ISLA', 
        ));

        Municipio::create(array(
            'id' => '728', 
            'id_estado' =>'15',
            'municipio' => 'SAN FELIPE DEL PROGRESO', 
        ));

        Municipio::create(array(
            'id' => '729', 
            'id_estado' =>'15',
            'municipio' => 'SAN JOSÉ DEL RINCÓN', 
        ));

        Municipio::create(array(
            'id' => '730', 
            'id_estado' =>'15',
            'municipio' => 'SAN MARTÍN DE LAS PIRÁMIDES', 
        ));

        Municipio::create(array(
            'id' => '731', 
            'id_estado' =>'15',
            'municipio' => 'SAN MATEO ATENCO', 
        ));

        Municipio::create(array(
            'id' => '732', 
            'id_estado' =>'15',
            'municipio' => 'SAN SIMÓN DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '733', 
            'id_estado' =>'15',
            'municipio' => 'SANTO TOMÁS', 
        ));

        Municipio::create(array(
            'id' => '734', 
            'id_estado' =>'15',
            'municipio' => 'SOYANIQUILPAN DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '735', 
            'id_estado' =>'15',
            'municipio' => 'SULTEPEC', 
        ));

        Municipio::create(array(
            'id' => '736', 
            'id_estado' =>'15',
            'municipio' => 'TECÁMAC', 
        ));

        Municipio::create(array(
            'id' => '737', 
            'id_estado' =>'15',
            'municipio' => 'TEJUPILCO', 
        ));

        Municipio::create(array(
            'id' => '738', 
            'id_estado' =>'15',
            'municipio' => 'TEMAMATLA', 
        ));

        Municipio::create(array(
            'id' => '739', 
            'id_estado' =>'15',
            'municipio' => 'TEMASCALAPA', 
        ));

        Municipio::create(array(
            'id' => '740', 
            'id_estado' =>'15',
            'municipio' => 'TEMASCALCINGO', 
        ));

        Municipio::create(array(
            'id' => '741', 
            'id_estado' =>'15',
            'municipio' => 'TEMASCALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '742', 
            'id_estado' =>'15',
            'municipio' => 'TEMOAYA', 
        ));

        Municipio::create(array(
            'id' => '743', 
            'id_estado' =>'15',
            'municipio' => 'TENANCINGO', 
        ));

        Municipio::create(array(
            'id' => '744', 
            'id_estado' =>'15',
            'municipio' => 'TENANGO DEL AIRE', 
        ));

        Municipio::create(array(
            'id' => '745', 
            'id_estado' =>'15',
            'municipio' => 'TENANGO DEL VALLE', 
        ));

        Municipio::create(array(
            'id' => '746', 
            'id_estado' =>'15',
            'municipio' => 'TEOLOYUCÁN', 
        ));

        Municipio::create(array(
            'id' => '747', 
            'id_estado' =>'15',
            'municipio' => 'TEOTIHUACÁN', 
        ));

        Municipio::create(array(
            'id' => '748', 
            'id_estado' =>'15',
            'municipio' => 'TEPETLAOXTOC', 
        ));

        Municipio::create(array(
            'id' => '749', 
            'id_estado' =>'15',
            'municipio' => 'TEPETLIXPA', 
        ));

        Municipio::create(array(
            'id' => '750', 
            'id_estado' =>'15',
            'municipio' => 'TEPOTZOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '751', 
            'id_estado' =>'15',
            'municipio' => 'TEQUIXQUIAC', 
        ));

        Municipio::create(array(
            'id' => '752', 
            'id_estado' =>'15',
            'municipio' => 'TEXCALTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '753', 
            'id_estado' =>'15',
            'municipio' => 'TEXCALYACAC', 
        ));

        Municipio::create(array(
            'id' => '754', 
            'id_estado' =>'15',
            'municipio' => 'TEXCOCO', 
        ));

        Municipio::create(array(
            'id' => '755', 
            'id_estado' =>'15',
            'municipio' => 'TEZOYUCA', 
        ));

        Municipio::create(array(
            'id' => '756', 
            'id_estado' =>'15',
            'municipio' => 'TIANGUISTENCO', 
        ));

        Municipio::create(array(
            'id' => '757', 
            'id_estado' =>'15',
            'municipio' => 'TIMILPAN', 
        ));

        Municipio::create(array(
            'id' => '758', 
            'id_estado' =>'15',
            'municipio' => 'TLALMANALCO', 
        ));

        Municipio::create(array(
            'id' => '759', 
            'id_estado' =>'15',
            'municipio' => 'TLALNEPANTLA DE BAZ', 
        ));

        Municipio::create(array(
            'id' => '760', 
            'id_estado' =>'15',
            'municipio' => 'TLATLAYA', 
        ));

        Municipio::create(array(
            'id' => '761', 
            'id_estado' =>'15',
            'municipio' => 'TOLUCA', 
        ));

        Municipio::create(array(
            'id' => '762', 
            'id_estado' =>'15',
            'municipio' => 'TONANITLA', 
        ));

        Municipio::create(array(
            'id' => '763', 
            'id_estado' =>'15',
            'municipio' => 'TONATICO', 
        ));

        Municipio::create(array(
            'id' => '764', 
            'id_estado' =>'15',
            'municipio' => 'TULTEPEC', 
        ));

        Municipio::create(array(
            'id' => '765', 
            'id_estado' =>'15',
            'municipio' => 'TULTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '766', 
            'id_estado' =>'15',
            'municipio' => 'VALLE DE BRAVO', 
        ));

        Municipio::create(array(
            'id' => '767', 
            'id_estado' =>'15',
            'municipio' => 'VALLE DE CHALCO SOLIDARIDAD', 
        ));

        Municipio::create(array(
            'id' => '768', 
            'id_estado' =>'15',
            'municipio' => 'VILLA DE ALLENDE', 
        ));

        Municipio::create(array(
            'id' => '769', 
            'id_estado' =>'15',
            'municipio' => 'VILLA DEL CARBÓN', 
        ));

        Municipio::create(array(
            'id' => '770', 
            'id_estado' =>'15',
            'municipio' => 'VILLA GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '771', 
            'id_estado' =>'15',
            'municipio' => 'VILLA VICTORIA', 
        ));

        Municipio::create(array(
            'id' => '772', 
            'id_estado' =>'15',
            'municipio' => 'XALATLACO', 
        ));

        Municipio::create(array(
            'id' => '773', 
            'id_estado' =>'15',
            'municipio' => 'XONACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '774', 
            'id_estado' =>'15',
            'municipio' => 'ZACAZONAPAN', 
        ));

        Municipio::create(array(
            'id' => '775', 
            'id_estado' =>'15',
            'municipio' => 'ZACUALPAN', 
        ));

        Municipio::create(array(
            'id' => '776', 
            'id_estado' =>'15',
            'municipio' => 'ZINACANTEPEC', 
        ));

        Municipio::create(array(
            'id' => '777', 
            'id_estado' =>'15',
            'municipio' => 'ZUMPAHUACÁN', 
        ));

        Municipio::create(array(
            'id' => '778', 
            'id_estado' =>'15',
            'municipio' => 'ZUMPANGO', 
        ));

        Municipio::create(array(
            'id' => '779', 
            'id_estado' =>'16',
            'municipio' => 'ACUITZIO', 
        ));

        Municipio::create(array(
            'id' => '780', 
            'id_estado' =>'16',
            'municipio' => 'AGUILILLA', 
        ));

        Municipio::create(array(
            'id' => '781', 
            'id_estado' =>'16',
            'municipio' => 'ALVARO OBREGÓN', 
        ));

        Municipio::create(array(
            'id' => '782', 
            'id_estado' =>'16',
            'municipio' => 'ANGAMACUTIRO', 
        ));

        Municipio::create(array(
            'id' => '783', 
            'id_estado' =>'16',
            'municipio' => 'ANGANGUEO', 
        ));

        Municipio::create(array(
            'id' => '784', 
            'id_estado' =>'16',
            'municipio' => 'APATZINGÁN', 
        ));

        Municipio::create(array(
            'id' => '785', 
            'id_estado' =>'16',
            'municipio' => 'APORO', 
        ));

        Municipio::create(array(
            'id' => '786', 
            'id_estado' =>'16',
            'municipio' => 'AQUILA', 
        ));

        Municipio::create(array(
            'id' => '787', 
            'id_estado' =>'16',
            'municipio' => 'ARIO', 
        ));

        Municipio::create(array(
            'id' => '788', 
            'id_estado' =>'16',
            'municipio' => 'ARTEAGA', 
        ));

        Municipio::create(array(
            'id' => '789', 
            'id_estado' =>'16',
            'municipio' => 'BRISEÑAS', 
        ));

        Municipio::create(array(
            'id' => '790', 
            'id_estado' =>'16',
            'municipio' => 'BUENAVISTA', 
        ));

        Municipio::create(array(
            'id' => '791', 
            'id_estado' =>'16',
            'municipio' => 'CARÁCUARO', 
        ));

        Municipio::create(array(
            'id' => '792', 
            'id_estado' =>'16',
            'municipio' => 'CHARAPAN', 
        ));

        Municipio::create(array(
            'id' => '793', 
            'id_estado' =>'16',
            'municipio' => 'CHARO', 
        ));

        Municipio::create(array(
            'id' => '794', 
            'id_estado' =>'16',
            'municipio' => 'CHAVINDA', 
        ));

        Municipio::create(array(
            'id' => '795', 
            'id_estado' =>'16',
            'municipio' => 'CHERÁN', 
        ));

        Municipio::create(array(
            'id' => '796', 
            'id_estado' =>'16',
            'municipio' => 'CHILCHOTA', 
        ));

        Municipio::create(array(
            'id' => '797', 
            'id_estado' =>'16',
            'municipio' => 'CHINICUILA', 
        ));

        Municipio::create(array(
            'id' => '798', 
            'id_estado' =>'16',
            'municipio' => 'CHUCÁNDIRO', 
        ));

        Municipio::create(array(
            'id' => '799', 
            'id_estado' =>'16',
            'municipio' => 'CHURINTZIO', 
        ));

        Municipio::create(array(
            'id' => '800', 
            'id_estado' =>'16',
            'municipio' => 'CHURUMUCO', 
        ));

        Municipio::create(array(
            'id' => '801', 
            'id_estado' =>'16',
            'municipio' => 'COAHUAYANA', 
        ));

        Municipio::create(array(
            'id' => '802', 
            'id_estado' =>'16',
            'municipio' => 'COALCOMÁN DE VÁZQUEZ PALLARES', 
        ));

        Municipio::create(array(
            'id' => '803', 
            'id_estado' =>'16',
            'municipio' => 'COENEO', 
        ));

        Municipio::create(array(
            'id' => '804', 
            'id_estado' =>'16',
            'municipio' => 'COJUMATLÁN DE RÉGULES', 
        ));

        Municipio::create(array(
            'id' => '805', 
            'id_estado' =>'16',
            'municipio' => 'CONTEPEC', 
        ));

        Municipio::create(array(
            'id' => '806', 
            'id_estado' =>'16',
            'municipio' => 'COPÁNDARO', 
        ));

        Municipio::create(array(
            'id' => '807', 
            'id_estado' =>'16',
            'municipio' => 'COTIJA', 
        ));

        Municipio::create(array(
            'id' => '808', 
            'id_estado' =>'16',
            'municipio' => 'CUITZEO', 
        ));

        Municipio::create(array(
            'id' => '809', 
            'id_estado' =>'16',
            'municipio' => 'ECUANDUREO', 
        ));

        Municipio::create(array(
            'id' => '810', 
            'id_estado' =>'16',
            'municipio' => 'EPITACIO HUERTA', 
        ));

        Municipio::create(array(
            'id' => '811', 
            'id_estado' =>'16',
            'municipio' => 'ERONGARÍCUARO', 
        ));

        Municipio::create(array(
            'id' => '812', 
            'id_estado' =>'16',
            'municipio' => 'GABRIEL ZAMORA', 
        ));

        Municipio::create(array(
            'id' => '813', 
            'id_estado' =>'16',
            'municipio' => 'HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '814', 
            'id_estado' =>'16',
            'municipio' => 'HUANDACAREO', 
        ));

        Municipio::create(array(
            'id' => '815', 
            'id_estado' =>'16',
            'municipio' => 'HUANIQUEO', 
        ));

        Municipio::create(array(
            'id' => '816', 
            'id_estado' =>'16',
            'municipio' => 'HUETAMO', 
        ));

        Municipio::create(array(
            'id' => '817', 
            'id_estado' =>'16',
            'municipio' => 'HUIRAMBA', 
        ));

        Municipio::create(array(
            'id' => '818', 
            'id_estado' =>'16',
            'municipio' => 'INDAPARAPEO', 
        ));

        Municipio::create(array(
            'id' => '819', 
            'id_estado' =>'16',
            'municipio' => 'IRIMBO', 
        ));

        Municipio::create(array(
            'id' => '820', 
            'id_estado' =>'16',
            'municipio' => 'IXTLÁN', 
        ));

        Municipio::create(array(
            'id' => '821', 
            'id_estado' =>'16',
            'municipio' => 'JACONA', 
        ));

        Municipio::create(array(
            'id' => '822', 
            'id_estado' =>'16',
            'municipio' => 'JIMÉNEZ', 
        ));

        Municipio::create(array(
            'id' => '823', 
            'id_estado' =>'16',
            'municipio' => 'JIQUILPAN', 
        ));

        Municipio::create(array(
            'id' => '824', 
            'id_estado' =>'16',
            'municipio' => 'JOSÉ SIXTO VERDUZCO', 
        ));

        Municipio::create(array(
            'id' => '825', 
            'id_estado' =>'16',
            'municipio' => 'JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '826', 
            'id_estado' =>'16',
            'municipio' => 'JUNGAPEO', 
        ));

        Municipio::create(array(
            'id' => '827', 
            'id_estado' =>'16',
            'municipio' => 'LA HUACANA', 
        ));

        Municipio::create(array(
            'id' => '828', 
            'id_estado' =>'16',
            'municipio' => 'LA PIEDAD', 
        ));

        Municipio::create(array(
            'id' => '829', 
            'id_estado' =>'16',
            'municipio' => 'LAGUNILLAS', 
        ));

        Municipio::create(array(
            'id' => '830', 
            'id_estado' =>'16',
            'municipio' => 'LÁZARO CÁRDENAS', 
        ));

        Municipio::create(array(
            'id' => '831', 
            'id_estado' =>'16',
            'municipio' => 'LOS REYES', 
        ));

        Municipio::create(array(
            'id' => '832', 
            'id_estado' =>'16',
            'municipio' => 'MADERO', 
        ));

        Municipio::create(array(
            'id' => '833', 
            'id_estado' =>'16',
            'municipio' => 'MARAVATÍO', 
        ));

        Municipio::create(array(
            'id' => '834', 
            'id_estado' =>'16',
            'municipio' => 'MARCOS CASTELLANOS', 
        ));

        Municipio::create(array(
            'id' => '835', 
            'id_estado' =>'16',
            'municipio' => 'MORELIA', 
        ));

        Municipio::create(array(
            'id' => '836', 
            'id_estado' =>'16',
            'municipio' => 'MORELOS', 
        ));

        Municipio::create(array(
            'id' => '837', 
            'id_estado' =>'16',
            'municipio' => 'MÚGICA', 
        ));

        Municipio::create(array(
            'id' => '838', 
            'id_estado' =>'16',
            'municipio' => 'NAHUATZEN', 
        ));

        Municipio::create(array(
            'id' => '839', 
            'id_estado' =>'16',
            'municipio' => 'NOCUPÉTARO', 
        ));

        Municipio::create(array(
            'id' => '840', 
            'id_estado' =>'16',
            'municipio' => 'NUEVO PARANGARICUTIRO', 
        ));

        Municipio::create(array(
            'id' => '841', 
            'id_estado' =>'16',
            'municipio' => 'NUEVO URECHO', 
        ));

        Municipio::create(array(
            'id' => '842', 
            'id_estado' =>'16',
            'municipio' => 'NUMARÁN', 
        ));

        Municipio::create(array(
            'id' => '843', 
            'id_estado' =>'16',
            'municipio' => 'OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '844', 
            'id_estado' =>'16',
            'municipio' => 'PAJACUARÁN', 
        ));

        Municipio::create(array(
            'id' => '845', 
            'id_estado' =>'16',
            'municipio' => 'PANINDÍCUARO', 
        ));

        Municipio::create(array(
            'id' => '846', 
            'id_estado' =>'16',
            'municipio' => 'PARACHO', 
        ));

        Municipio::create(array(
            'id' => '847', 
            'id_estado' =>'16',
            'municipio' => 'PARÁCUARO', 
        ));

        Municipio::create(array(
            'id' => '848', 
            'id_estado' =>'16',
            'municipio' => 'PÁTZCUARO', 
        ));

        Municipio::create(array(
            'id' => '849', 
            'id_estado' =>'16',
            'municipio' => 'PENJAMILLO', 
        ));

        Municipio::create(array(
            'id' => '850', 
            'id_estado' =>'16',
            'municipio' => 'PERIBÁN', 
        ));

        Municipio::create(array(
            'id' => '851', 
            'id_estado' =>'16',
            'municipio' => 'PURÉPERO', 
        ));

        Municipio::create(array(
            'id' => '852', 
            'id_estado' =>'16',
            'municipio' => 'PURUÁNDIRO', 
        ));

        Municipio::create(array(
            'id' => '853', 
            'id_estado' =>'16',
            'municipio' => 'QUERÉNDARO', 
        ));

        Municipio::create(array(
            'id' => '854', 
            'id_estado' =>'16',
            'municipio' => 'QUIROGA', 
        ));

        Municipio::create(array(
            'id' => '855', 
            'id_estado' =>'16',
            'municipio' => 'SAHUAYO', 
        ));

        Municipio::create(array(
            'id' => '856', 
            'id_estado' =>'16',
            'municipio' => 'SALVADOR ESCALANTE', 
        ));

        Municipio::create(array(
            'id' => '857', 
            'id_estado' =>'16',
            'municipio' => 'SAN LUCAS', 
        ));

        Municipio::create(array(
            'id' => '858', 
            'id_estado' =>'16',
            'municipio' => 'SANTA ANA MAYA', 
        ));

        Municipio::create(array(
            'id' => '859', 
            'id_estado' =>'16',
            'municipio' => 'SENGUIO', 
        ));

        Municipio::create(array(
            'id' => '860', 
            'id_estado' =>'16',
            'municipio' => 'SUSUPUATO', 
        ));

        Municipio::create(array(
            'id' => '861', 
            'id_estado' =>'16',
            'municipio' => 'TACÁMBARO', 
        ));

        Municipio::create(array(
            'id' => '862', 
            'id_estado' =>'16',
            'municipio' => 'TANCÍTARO', 
        ));

        Municipio::create(array(
            'id' => '863', 
            'id_estado' =>'16',
            'municipio' => 'TANGAMANDAPIO', 
        ));

        Municipio::create(array(
            'id' => '864', 
            'id_estado' =>'16',
            'municipio' => 'TANGANCÍCUARO', 
        ));

        Municipio::create(array(
            'id' => '865', 
            'id_estado' =>'16',
            'municipio' => 'TANHUATO', 
        ));

        Municipio::create(array(
            'id' => '866', 
            'id_estado' =>'16',
            'municipio' => 'TARETAN', 
        ));

        Municipio::create(array(
            'id' => '867', 
            'id_estado' =>'16',
            'municipio' => 'TARÍMBARO', 
        ));

        Municipio::create(array(
            'id' => '868', 
            'id_estado' =>'16',
            'municipio' => 'TEPALCATEPEC', 
        ));

        Municipio::create(array(
            'id' => '869', 
            'id_estado' =>'16',
            'municipio' => 'TING?INDÍN', 
        ));

        Municipio::create(array(
            'id' => '870', 
            'id_estado' =>'16',
            'municipio' => 'TINGAMBATO', 
        ));

        Municipio::create(array(
            'id' => '871', 
            'id_estado' =>'16',
            'municipio' => 'TIQUICHEO DE NICOLÁS ROMERO', 
        ));

        Municipio::create(array(
            'id' => '872', 
            'id_estado' =>'16',
            'municipio' => 'TLALPUJAHUA', 
        ));

        Municipio::create(array(
            'id' => '873', 
            'id_estado' =>'16',
            'municipio' => 'TLAZAZALCA', 
        ));

        Municipio::create(array(
            'id' => '874', 
            'id_estado' =>'16',
            'municipio' => 'TOCUMBO', 
        ));

        Municipio::create(array(
            'id' => '875', 
            'id_estado' =>'16',
            'municipio' => 'TUMBISCATÍO', 
        ));

        Municipio::create(array(
            'id' => '876', 
            'id_estado' =>'16',
            'municipio' => 'TURICATO', 
        ));

        Municipio::create(array(
            'id' => '877', 
            'id_estado' =>'16',
            'municipio' => 'TUXPAN', 
        ));

        Municipio::create(array(
            'id' => '878', 
            'id_estado' =>'16',
            'municipio' => 'TUZANTLA', 
        ));

        Municipio::create(array(
            'id' => '879', 
            'id_estado' =>'16',
            'municipio' => 'TZINTZUNTZAN', 
        ));

        Municipio::create(array(
            'id' => '880', 
            'id_estado' =>'16',
            'municipio' => 'TZITZIO', 
        ));

        Municipio::create(array(
            'id' => '881', 
            'id_estado' =>'16',
            'municipio' => 'URUAPAN', 
        ));

        Municipio::create(array(
            'id' => '882', 
            'id_estado' =>'16',
            'municipio' => 'VENUSTIANO CARRANZA', 
        ));

        Municipio::create(array(
            'id' => '883', 
            'id_estado' =>'16',
            'municipio' => 'VILLAMAR', 
        ));

        Municipio::create(array(
            'id' => '884', 
            'id_estado' =>'16',
            'municipio' => 'VISTA HERMOSA', 
        ));

        Municipio::create(array(
            'id' => '885', 
            'id_estado' =>'16',
            'municipio' => 'YURÉCUARO', 
        ));

        Municipio::create(array(
            'id' => '886', 
            'id_estado' =>'16',
            'municipio' => 'ZACAPU', 
        ));

        Municipio::create(array(
            'id' => '887', 
            'id_estado' =>'16',
            'municipio' => 'ZAMORA', 
        ));

        Municipio::create(array(
            'id' => '888', 
            'id_estado' =>'16',
            'municipio' => 'ZINÁPARO', 
        ));

        Municipio::create(array(
            'id' => '889', 
            'id_estado' =>'16',
            'municipio' => 'ZINAPÉCUARO', 
        ));

        Municipio::create(array(
            'id' => '890', 
            'id_estado' =>'16',
            'municipio' => 'ZIRACUARETIRO', 
        ));

        Municipio::create(array(
            'id' => '891', 
            'id_estado' =>'16',
            'municipio' => 'ZITÁCUARO', 
        ));

        Municipio::create(array(
            'id' => '892', 
            'id_estado' =>'17',
            'municipio' => 'AMACUZAC', 
        ));

        Municipio::create(array(
            'id' => '893', 
            'id_estado' =>'17',
            'municipio' => 'ATLATLAHUCAN', 
        ));

        Municipio::create(array(
            'id' => '894', 
            'id_estado' =>'17',
            'municipio' => 'AXOCHIAPAN', 
        ));

        Municipio::create(array(
            'id' => '895', 
            'id_estado' =>'17',
            'municipio' => 'AYALA', 
        ));

        Municipio::create(array(
            'id' => '896', 
            'id_estado' =>'17',
            'municipio' => 'COATLÁN DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '897', 
            'id_estado' =>'17',
            'municipio' => 'CUAUTLA', 
        ));

        Municipio::create(array(
            'id' => '898', 
            'id_estado' =>'17',
            'municipio' => 'CUERNAVACA', 
        ));

        Municipio::create(array(
            'id' => '899', 
            'id_estado' =>'17',
            'municipio' => 'EMILIANO ZAPATA', 
        ));

        Municipio::create(array(
            'id' => '900', 
            'id_estado' =>'17',
            'municipio' => 'HUITZILAC', 
        ));

        Municipio::create(array(
            'id' => '901', 
            'id_estado' =>'17',
            'municipio' => 'JANTETELCO', 
        ));

        Municipio::create(array(
            'id' => '902', 
            'id_estado' =>'17',
            'municipio' => 'JIUTEPEC', 
        ));

        Municipio::create(array(
            'id' => '903', 
            'id_estado' =>'17',
            'municipio' => 'JOJUTLA', 
        ));

        Municipio::create(array(
            'id' => '904', 
            'id_estado' =>'17',
            'municipio' => 'JONACATEPEC', 
        ));

        Municipio::create(array(
            'id' => '905', 
            'id_estado' =>'17',
            'municipio' => 'MAZATEPEC', 
        ));

        Municipio::create(array(
            'id' => '906', 
            'id_estado' =>'17',
            'municipio' => 'MIACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '907', 
            'id_estado' =>'17',
            'municipio' => 'OCUITUCO', 
        ));

        Municipio::create(array(
            'id' => '908', 
            'id_estado' =>'17',
            'municipio' => 'PUENTE DE IXTLA', 
        ));

        Municipio::create(array(
            'id' => '909', 
            'id_estado' =>'17',
            'municipio' => 'TEMIXCO', 
        ));

        Municipio::create(array(
            'id' => '910', 
            'id_estado' =>'17',
            'municipio' => 'TEMOAC', 
        ));

        Municipio::create(array(
            'id' => '911', 
            'id_estado' =>'17',
            'municipio' => 'TEPALCINGO', 
        ));

        Municipio::create(array(
            'id' => '912', 
            'id_estado' =>'17',
            'municipio' => 'TEPOZTLÁN', 
        ));

        Municipio::create(array(
            'id' => '913', 
            'id_estado' =>'17',
            'municipio' => 'TETECALA', 
        ));

        Municipio::create(array(
            'id' => '914', 
            'id_estado' =>'17',
            'municipio' => 'TETELA DEL VOLCÁN', 
        ));

        Municipio::create(array(
            'id' => '915', 
            'id_estado' =>'17',
            'municipio' => 'TLALNEPANTLA', 
        ));

        Municipio::create(array(
            'id' => '916', 
            'id_estado' =>'17',
            'municipio' => 'TLALTIZAPÁN', 
        ));

        Municipio::create(array(
            'id' => '917', 
            'id_estado' =>'17',
            'municipio' => 'TLAQUILTENANGO', 
        ));

        Municipio::create(array(
            'id' => '918', 
            'id_estado' =>'17',
            'municipio' => 'TLAYACAPAN', 
        ));

        Municipio::create(array(
            'id' => '919', 
            'id_estado' =>'17',
            'municipio' => 'TOTOLAPAN', 
        ));

        Municipio::create(array(
            'id' => '920', 
            'id_estado' =>'17',
            'municipio' => 'XOCHITEPEC', 
        ));

        Municipio::create(array(
            'id' => '921', 
            'id_estado' =>'17',
            'municipio' => 'YAUTEPEC', 
        ));

        Municipio::create(array(
            'id' => '922', 
            'id_estado' =>'17',
            'municipio' => 'YECAPIXTLA', 
        ));

        Municipio::create(array(
            'id' => '923', 
            'id_estado' =>'17',
            'municipio' => 'ZACATEPEC DE HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '924', 
            'id_estado' =>'17',
            'municipio' => 'ZACUALPAN DE AMILPAS', 
        ));

        Municipio::create(array(
            'id' => '925', 
            'id_estado' =>'18',
            'municipio' => 'ACAPONETA', 
        ));

        Municipio::create(array(
            'id' => '926', 
            'id_estado' =>'18',
            'municipio' => 'AHUACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '927', 
            'id_estado' =>'18',
            'municipio' => 'AMATLÁN DE CAÑAS', 
        ));

        Municipio::create(array(
            'id' => '928', 
            'id_estado' =>'18',
            'municipio' => 'BAHÍA DE BANDERAS', 
        ));

        Municipio::create(array(
            'id' => '929', 
            'id_estado' =>'18',
            'municipio' => 'COMPOSTELA', 
        ));

        Municipio::create(array(
            'id' => '930', 
            'id_estado' =>'18',
            'municipio' => 'DEL NAYAR', 
        ));

        Municipio::create(array(
            'id' => '931', 
            'id_estado' =>'18',
            'municipio' => 'HUAJICORI', 
        ));

        Municipio::create(array(
            'id' => '932', 
            'id_estado' =>'18',
            'municipio' => 'IXTLÁN DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '933', 
            'id_estado' =>'18',
            'municipio' => 'JALA', 
        ));

        Municipio::create(array(
            'id' => '934', 
            'id_estado' =>'18',
            'municipio' => 'LA YESCA', 
        ));

        Municipio::create(array(
            'id' => '935', 
            'id_estado' =>'18',
            'municipio' => 'ROSAMORADA', 
        ));

        Municipio::create(array(
            'id' => '936', 
            'id_estado' =>'18',
            'municipio' => 'RUÍZ', 
        ));

        Municipio::create(array(
            'id' => '937', 
            'id_estado' =>'18',
            'municipio' => 'SAN BLAS', 
        ));

        Municipio::create(array(
            'id' => '938', 
            'id_estado' =>'18',
            'municipio' => 'SAN PEDRO LAGUNILLAS', 
        ));

        Municipio::create(array(
            'id' => '939', 
            'id_estado' =>'18',
            'municipio' => 'SANTA MARÍA DEL ORO', 
        ));

        Municipio::create(array(
            'id' => '940', 
            'id_estado' =>'18',
            'municipio' => 'SANTIAGO IXCUINTLA', 
        ));

        Municipio::create(array(
            'id' => '941', 
            'id_estado' =>'18',
            'municipio' => 'TECUALA', 
        ));

        Municipio::create(array(
            'id' => '942', 
            'id_estado' =>'18',
            'municipio' => 'TEPIC', 
        ));

        Municipio::create(array(
            'id' => '943', 
            'id_estado' =>'18',
            'municipio' => 'TUXPAN', 
        ));

        Municipio::create(array(
            'id' => '944', 
            'id_estado' =>'18',
            'municipio' => 'XALISCO', 
        ));

        Municipio::create(array(
            'id' => '945', 
            'id_estado' =>'19',
            'municipio' => 'ABASOLO', 
        ));

        Municipio::create(array(
            'id' => '946', 
            'id_estado' =>'19',
            'municipio' => 'AGUALEGUAS', 
        ));

        Municipio::create(array(
            'id' => '947', 
            'id_estado' =>'19',
            'municipio' => 'ALLENDE', 
        ));

        Municipio::create(array(
            'id' => '948', 
            'id_estado' =>'19',
            'municipio' => 'ANÁHUAC', 
        ));

        Municipio::create(array(
            'id' => '949', 
            'id_estado' =>'19',
            'municipio' => 'APODACA', 
        ));

        Municipio::create(array(
            'id' => '950', 
            'id_estado' =>'19',
            'municipio' => 'ARAMBERRI', 
        ));

        Municipio::create(array(
            'id' => '951', 
            'id_estado' =>'19',
            'municipio' => 'BUSTAMANTE', 
        ));

        Municipio::create(array(
            'id' => '952', 
            'id_estado' =>'19',
            'municipio' => 'CADEREYTA JIMÉNEZ', 
        ));

        Municipio::create(array(
            'id' => '953', 
            'id_estado' =>'19',
            'municipio' => 'CARMEN', 
        ));

        Municipio::create(array(
            'id' => '954', 
            'id_estado' =>'19',
            'municipio' => 'CERRALVO', 
        ));

        Municipio::create(array(
            'id' => '955', 
            'id_estado' =>'19',
            'municipio' => 'CHINA', 
        ));

        Municipio::create(array(
            'id' => '956', 
            'id_estado' =>'19',
            'municipio' => 'CIÉNEGA DE FLORES', 
        ));

        Municipio::create(array(
            'id' => '957', 
            'id_estado' =>'19',
            'municipio' => 'DR. COSS', 
        ));

        Municipio::create(array(
            'id' => '958', 
            'id_estado' =>'19',
            'municipio' => 'DR. ARROYO', 
        ));

        Municipio::create(array(
            'id' => '959', 
            'id_estado' =>'19',
            'municipio' => 'DR. GONZÁLEZ', 
        ));

        Municipio::create(array(
            'id' => '960', 
            'id_estado' =>'19',
            'municipio' => 'GALEANA', 
        ));

        Municipio::create(array(
            'id' => '961', 
            'id_estado' =>'19',
            'municipio' => 'GARCÍA', 
        ));

        Municipio::create(array(
            'id' => '962', 
            'id_estado' =>'19',
            'municipio' => 'GRAL. ESCOBEDO', 
        ));

        Municipio::create(array(
            'id' => '963', 
            'id_estado' =>'19',
            'municipio' => 'GRAL. TERÁN', 
        ));

        Municipio::create(array(
            'id' => '964', 
            'id_estado' =>'19',
            'municipio' => 'GRAL. TREVIÑO', 
        ));

        Municipio::create(array(
            'id' => '965', 
            'id_estado' =>'19',
            'municipio' => 'GRAL. ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '966', 
            'id_estado' =>'19',
            'municipio' => 'GRAL. ZUAZUA', 
        ));

        Municipio::create(array(
            'id' => '967', 
            'id_estado' =>'19',
            'municipio' => 'GRAL. BRAVO', 
        ));

        Municipio::create(array(
            'id' => '968', 
            'id_estado' =>'19',
            'municipio' => 'GUADALUPE', 
        ));

        Municipio::create(array(
            'id' => '969', 
            'id_estado' =>'19',
            'municipio' => 'HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '970', 
            'id_estado' =>'19',
            'municipio' => 'HIGUERAS', 
        ));

        Municipio::create(array(
            'id' => '971', 
            'id_estado' =>'19',
            'municipio' => 'HUALAHUISES', 
        ));

        Municipio::create(array(
            'id' => '972', 
            'id_estado' =>'19',
            'municipio' => 'ITURBIDE', 
        ));

        Municipio::create(array(
            'id' => '973', 
            'id_estado' =>'19',
            'municipio' => 'JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '974', 
            'id_estado' =>'19',
            'municipio' => 'LAMPAZOS DE NARANJO', 
        ));

        Municipio::create(array(
            'id' => '975', 
            'id_estado' =>'19',
            'municipio' => 'LINARES', 
        ));

        Municipio::create(array(
            'id' => '976', 
            'id_estado' =>'19',
            'municipio' => 'LOS ALDAMAS', 
        ));

        Municipio::create(array(
            'id' => '977', 
            'id_estado' =>'19',
            'municipio' => 'LOS HERRERAS', 
        ));

        Municipio::create(array(
            'id' => '978', 
            'id_estado' =>'19',
            'municipio' => 'LOS RAMONES', 
        ));

        Municipio::create(array(
            'id' => '979', 
            'id_estado' =>'19',
            'municipio' => 'MARÍN', 
        ));

        Municipio::create(array(
            'id' => '980', 
            'id_estado' =>'19',
            'municipio' => 'MELCHOR OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '981', 
            'id_estado' =>'19',
            'municipio' => 'MIER Y NORIEGA', 
        ));

        Municipio::create(array(
            'id' => '982', 
            'id_estado' =>'19',
            'municipio' => 'MINA', 
        ));

        Municipio::create(array(
            'id' => '983', 
            'id_estado' =>'19',
            'municipio' => 'MONTEMORELOS', 
        ));

        Municipio::create(array(
            'id' => '984', 
            'id_estado' =>'19',
            'municipio' => 'MONTERREY', 
        ));

        Municipio::create(array(
            'id' => '985', 
            'id_estado' =>'19',
            'municipio' => 'PARÁS', 
        ));

        Municipio::create(array(
            'id' => '986', 
            'id_estado' =>'19',
            'municipio' => 'PESQUERÍA', 
        ));

        Municipio::create(array(
            'id' => '987', 
            'id_estado' =>'19',
            'municipio' => 'RAYONES', 
        ));

        Municipio::create(array(
            'id' => '988', 
            'id_estado' =>'19',
            'municipio' => 'SABINAS HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '989', 
            'id_estado' =>'19',
            'municipio' => 'SALINAS VICTORIA', 
        ));

        Municipio::create(array(
            'id' => '990', 
            'id_estado' =>'19',
            'municipio' => 'SAN NICOLÁS DE LOS GARZA', 
        ));

        Municipio::create(array(
            'id' => '991', 
            'id_estado' =>'19',
            'municipio' => 'SAN PEDRO GARZA GARCÍA', 
        ));

        Municipio::create(array(
            'id' => '992', 
            'id_estado' =>'19',
            'municipio' => 'SANTA CATARINA', 
        ));

        Municipio::create(array(
            'id' => '993', 
            'id_estado' =>'19',
            'municipio' => 'SANTIAGO', 
        ));

        Municipio::create(array(
            'id' => '994', 
            'id_estado' =>'19',
            'municipio' => 'VALLECILLO', 
        ));

        Municipio::create(array(
            'id' => '995', 
            'id_estado' =>'19',
            'municipio' => 'VILLALDAMA', 
        ));

        Municipio::create(array(
            'id' => '996', 
            'id_estado' =>'20',
            'municipio' => 'ABEJONES', 
        ));

        Municipio::create(array(
            'id' => '997', 
            'id_estado' =>'20',
            'municipio' => 'ACATLÁN DE PÉREZ FIGUEROA', 
        ));

        Municipio::create(array(
            'id' => '998', 
            'id_estado' =>'20',
            'municipio' => 'ANIMAS TRUJANO', 
        ));

        Municipio::create(array(
            'id' => '999', 
            'id_estado' =>'20',
            'municipio' => 'ASUNCIÓN CACALOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1000', 
            'id_estado' =>'20',
            'municipio' => 'ASUNCIÓN CUYOTEPEJI', 
        ));

        Municipio::create(array(
            'id' => '1001', 
            'id_estado' =>'20',
            'municipio' => 'ASUNCIÓN IXTALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1002', 
            'id_estado' =>'20',
            'municipio' => 'ASUNCIÓN NOCHIXTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1003', 
            'id_estado' =>'20',
            'municipio' => 'ASUNCIÓN OCOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1004', 
            'id_estado' =>'20',
            'municipio' => 'ASUNCIÓN TLACOLULITA', 
        ));

        Municipio::create(array(
            'id' => '1005', 
            'id_estado' =>'20',
            'municipio' => 'AYOQUEZCO DE ALDAMA', 
        ));

        Municipio::create(array(
            'id' => '1006', 
            'id_estado' =>'20',
            'municipio' => 'AYOTZINTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1007', 
            'id_estado' =>'20',
            'municipio' => 'CALIHUALÁ', 
        ));

        Municipio::create(array(
            'id' => '1008', 
            'id_estado' =>'20',
            'municipio' => 'CANDELARIA LOXICHA', 
        ));

        Municipio::create(array(
            'id' => '1009', 
            'id_estado' =>'20',
            'municipio' => 'CAPULÁLPAM DE MÉNDEZ', 
        ));

        Municipio::create(array(
            'id' => '1010', 
            'id_estado' =>'20',
            'municipio' => 'CHAHUITES', 
        ));

        Municipio::create(array(
            'id' => '1011', 
            'id_estado' =>'20',
            'municipio' => 'CHALCATONGO DE HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '1012', 
            'id_estado' =>'20',
            'municipio' => 'CHIQUIHUITLÁN DE BENITO JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1013', 
            'id_estado' =>'20',
            'municipio' => 'CIÉNEGA DE ZIMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1014', 
            'id_estado' =>'20',
            'municipio' => 'CIUDAD IXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1015', 
            'id_estado' =>'20',
            'municipio' => 'COATECAS ALTAS', 
        ));

        Municipio::create(array(
            'id' => '1016', 
            'id_estado' =>'20',
            'municipio' => 'COICOYÁN DE LAS FLORES', 
        ));

        Municipio::create(array(
            'id' => '1017', 
            'id_estado' =>'20',
            'municipio' => 'CONCEPCIÓN BUENAVISTA', 
        ));

        Municipio::create(array(
            'id' => '1018', 
            'id_estado' =>'20',
            'municipio' => 'CONCEPCIÓN PÁPALO', 
        ));

        Municipio::create(array(
            'id' => '1019', 
            'id_estado' =>'20',
            'municipio' => 'CONSTANCIA DEL ROSARIO', 
        ));

        Municipio::create(array(
            'id' => '1020', 
            'id_estado' =>'20',
            'municipio' => 'COSOLAPA', 
        ));

        Municipio::create(array(
            'id' => '1021', 
            'id_estado' =>'20',
            'municipio' => 'COSOLTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1022', 
            'id_estado' =>'20',
            'municipio' => 'CUILÁPAM DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '1023', 
            'id_estado' =>'20',
            'municipio' => 'CUYAMECALCO VILLA DE ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '1024', 
            'id_estado' =>'20',
            'municipio' => 'EL BARRIO DE LA SOLEDAD', 
        ));

        Municipio::create(array(
            'id' => '1025', 
            'id_estado' =>'20',
            'municipio' => 'EL ESPINAL', 
        ));

        Municipio::create(array(
            'id' => '1026', 
            'id_estado' =>'20',
            'municipio' => 'ELOXOCHITLÁN DE FLORES MAGÓN', 
        ));

        Municipio::create(array(
            'id' => '1027', 
            'id_estado' =>'20',
            'municipio' => 'FRESNILLO DE TRUJANO', 
        ));

        Municipio::create(array(
            'id' => '1028', 
            'id_estado' =>'20',
            'municipio' => 'GUADALUPE DE RAMÍREZ', 
        ));

        Municipio::create(array(
            'id' => '1029', 
            'id_estado' =>'20',
            'municipio' => 'GUADALUPE ETLA', 
        ));

        Municipio::create(array(
            'id' => '1030', 
            'id_estado' =>'20',
            'municipio' => 'GUELATAO DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1031', 
            'id_estado' =>'20',
            'municipio' => 'GUEVEA DE HUMBOLDT', 
        ));

        Municipio::create(array(
            'id' => '1032', 
            'id_estado' =>'20',
            'municipio' => 'HEROICA CIUDAD DE EJUTLA DE CRESPO', 
        ));

        Municipio::create(array(
            'id' => '1033', 
            'id_estado' =>'20',
            'municipio' => 'HEROICA CIUDAD DE HUAJUAPAN DE LEÓ', 
        ));

        Municipio::create(array(
            'id' => '1034', 
            'id_estado' =>'20',
            'municipio' => 'HEROICA CIUDAD DE TLAXIACO', 
        ));

        Municipio::create(array(
            'id' => '1035', 
            'id_estado' =>'20',
            'municipio' => 'HUAUTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1036', 
            'id_estado' =>'20',
            'municipio' => 'HUAUTLA DE JIMÉNEZ', 
        ));

        Municipio::create(array(
            'id' => '1037', 
            'id_estado' =>'20',
            'municipio' => 'IXPANTEPEC NIEVES', 
        ));

        Municipio::create(array(
            'id' => '1038', 
            'id_estado' =>'20',
            'municipio' => 'IXTLÁN DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1039', 
            'id_estado' =>'20',
            'municipio' => 'JUCHITÁN DE ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '1040', 
            'id_estado' =>'20',
            'municipio' => 'LA COMPAÑÍA', 
        ));

        Municipio::create(array(
            'id' => '1041', 
            'id_estado' =>'20',
            'municipio' => 'LA PE', 
        ));

        Municipio::create(array(
            'id' => '1042', 
            'id_estado' =>'20',
            'municipio' => 'LA REFORMA', 
        ));

        Municipio::create(array(
            'id' => '1043', 
            'id_estado' =>'20',
            'municipio' => 'LA TRINIDAD VISTA HERMOSA', 
        ));

        Municipio::create(array(
            'id' => '1044', 
            'id_estado' =>'20',
            'municipio' => 'LOMA BONITA', 
        ));

        Municipio::create(array(
            'id' => '1045', 
            'id_estado' =>'20',
            'municipio' => 'MAGDALENA APASCO', 
        ));

        Municipio::create(array(
            'id' => '1046', 
            'id_estado' =>'20',
            'municipio' => 'MAGDALENA JALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1047', 
            'id_estado' =>'20',
            'municipio' => 'MAGDALENA MIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1048', 
            'id_estado' =>'20',
            'municipio' => 'MAGDALENA OCOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1049', 
            'id_estado' =>'20',
            'municipio' => 'MAGDALENA PEÑASCO', 
        ));

        Municipio::create(array(
            'id' => '1050', 
            'id_estado' =>'20',
            'municipio' => 'MAGDALENA TEITIPAC', 
        ));

        Municipio::create(array(
            'id' => '1051', 
            'id_estado' =>'20',
            'municipio' => 'MAGDALENA TEQUISISTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1052', 
            'id_estado' =>'20',
            'municipio' => 'MAGDALENA TLACOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1053', 
            'id_estado' =>'20',
            'municipio' => 'MAGDALENA YODOCONO DE PORFIRIO DÍA', 
        ));

        Municipio::create(array(
            'id' => '1054', 
            'id_estado' =>'20',
            'municipio' => 'MAGDALENA ZAHUATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1055', 
            'id_estado' =>'20',
            'municipio' => 'MARISCALA DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1056', 
            'id_estado' =>'20',
            'municipio' => 'MÁRTIRES DE TACUBAYA', 
        ));

        Municipio::create(array(
            'id' => '1057', 
            'id_estado' =>'20',
            'municipio' => 'MATÍAS ROMERO AVENDAÑO', 
        ));

        Municipio::create(array(
            'id' => '1058', 
            'id_estado' =>'20',
            'municipio' => 'MAZATLÁN VILLA DE FLORES', 
        ));

        Municipio::create(array(
            'id' => '1059', 
            'id_estado' =>'20',
            'municipio' => 'MESONES HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '1060', 
            'id_estado' =>'20',
            'municipio' => 'MIAHUATLÁN DE PORFIRIO DÍAZ', 
        ));

        Municipio::create(array(
            'id' => '1061', 
            'id_estado' =>'20',
            'municipio' => 'MIXISTLÁN DE LA REFORMA', 
        ));

        Municipio::create(array(
            'id' => '1062', 
            'id_estado' =>'20',
            'municipio' => 'MONJAS', 
        ));

        Municipio::create(array(
            'id' => '1063', 
            'id_estado' =>'20',
            'municipio' => 'NATIVIDAD', 
        ));

        Municipio::create(array(
            'id' => '1064', 
            'id_estado' =>'20',
            'municipio' => 'NAZARENO ETLA', 
        ));

        Municipio::create(array(
            'id' => '1065', 
            'id_estado' =>'20',
            'municipio' => 'NEJAPA DE MADERO', 
        ));

        Municipio::create(array(
            'id' => '1066', 
            'id_estado' =>'20',
            'municipio' => 'NUEVO ZOQUIAPAM', 
        ));

        Municipio::create(array(
            'id' => '1067', 
            'id_estado' =>'20',
            'municipio' => 'OAXACA DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1068', 
            'id_estado' =>'20',
            'municipio' => 'OCOTLÁN DE MORELOS', 
        ));

        Municipio::create(array(
            'id' => '1069', 
            'id_estado' =>'20',
            'municipio' => 'PINOTEPA DE DON LUIS', 
        ));

        Municipio::create(array(
            'id' => '1070', 
            'id_estado' =>'20',
            'municipio' => 'PLUMA HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '1071', 
            'id_estado' =>'20',
            'municipio' => 'PUTLA VILLA DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '1072', 
            'id_estado' =>'20',
            'municipio' => 'REFORMA DE PINEDA', 
        ));

        Municipio::create(array(
            'id' => '1073', 
            'id_estado' =>'20',
            'municipio' => 'REYES ETLA', 
        ));

        Municipio::create(array(
            'id' => '1074', 
            'id_estado' =>'20',
            'municipio' => 'ROJAS DE CUAUHTÉMOC', 
        ));

        Municipio::create(array(
            'id' => '1075', 
            'id_estado' =>'20',
            'municipio' => 'SALINA CRUZ', 
        ));

        Municipio::create(array(
            'id' => '1076', 
            'id_estado' =>'20',
            'municipio' => 'SAN AGUSTÍN AMATENGO', 
        ));

        Municipio::create(array(
            'id' => '1077', 
            'id_estado' =>'20',
            'municipio' => 'SAN AGUSTÍN ATENANGO', 
        ));

        Municipio::create(array(
            'id' => '1078', 
            'id_estado' =>'20',
            'municipio' => 'SAN AGUSTÍN CHAYUCO', 
        ));

        Municipio::create(array(
            'id' => '1079', 
            'id_estado' =>'20',
            'municipio' => 'SAN AGUSTÍN DE LAS JUNTAS', 
        ));

        Municipio::create(array(
            'id' => '1080', 
            'id_estado' =>'20',
            'municipio' => 'SAN AGUSTÍN ETLA', 
        ));

        Municipio::create(array(
            'id' => '1081', 
            'id_estado' =>'20',
            'municipio' => 'SAN AGUSTÍN LOXICHA', 
        ));

        Municipio::create(array(
            'id' => '1082', 
            'id_estado' =>'20',
            'municipio' => 'SAN AGUSTÍN TLACOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1083', 
            'id_estado' =>'20',
            'municipio' => 'SAN AGUSTÍN YATARENI', 
        ));

        Municipio::create(array(
            'id' => '1084', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS CABECERA NUEVA', 
        ));

        Municipio::create(array(
            'id' => '1085', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS DINICUITI', 
        ));

        Municipio::create(array(
            'id' => '1086', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS HUAXPALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1087', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS HUAYAPAM', 
        ));

        Municipio::create(array(
            'id' => '1088', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS IXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '1089', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS LAGUNAS', 
        ));

        Municipio::create(array(
            'id' => '1090', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS NUXIÑO', 
        ));

        Municipio::create(array(
            'id' => '1091', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS PAXTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1092', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS SINAXTLA', 
        ));

        Municipio::create(array(
            'id' => '1093', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS SOLAGA', 
        ));

        Municipio::create(array(
            'id' => '1094', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS TEOTILALPAM', 
        ));

        Municipio::create(array(
            'id' => '1095', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS TEPETLAPA', 
        ));

        Municipio::create(array(
            'id' => '1096', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS YAÁ', 
        ));

        Municipio::create(array(
            'id' => '1097', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS ZABACHE', 
        ));

        Municipio::create(array(
            'id' => '1098', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANDRÉS ZAUTLA', 
        ));

        Municipio::create(array(
            'id' => '1099', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANTONINO CASTILLO VELASCO', 
        ));

        Municipio::create(array(
            'id' => '1100', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANTONINO EL ALTO', 
        ));

        Municipio::create(array(
            'id' => '1101', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANTONINO MONTE VERDE', 
        ));

        Municipio::create(array(
            'id' => '1102', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANTONIO ACUTLA', 
        ));

        Municipio::create(array(
            'id' => '1103', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANTONIO DE LA CAL', 
        ));

        Municipio::create(array(
            'id' => '1104', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANTONIO HUITEPEC', 
        ));

        Municipio::create(array(
            'id' => '1105', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANTONIO NANAHUATÍPAM', 
        ));

        Municipio::create(array(
            'id' => '1106', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANTONIO SINICAHUA', 
        ));

        Municipio::create(array(
            'id' => '1107', 
            'id_estado' =>'20',
            'municipio' => 'SAN ANTONIO TEPETLAPA', 
        ));

        Municipio::create(array(
            'id' => '1108', 
            'id_estado' =>'20',
            'municipio' => 'SAN BALTAZAR CHICHICÁPAM', 
        ));

        Municipio::create(array(
            'id' => '1109', 
            'id_estado' =>'20',
            'municipio' => 'SAN BALTAZAR LOXICHA', 
        ));

        Municipio::create(array(
            'id' => '1110', 
            'id_estado' =>'20',
            'municipio' => 'SAN BALTAZAR YATZACHI EL BAJO', 
        ));

        Municipio::create(array(
            'id' => '1111', 
            'id_estado' =>'20',
            'municipio' => 'SAN BARTOLO COYOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1112', 
            'id_estado' =>'20',
            'municipio' => 'SAN BARTOLO SOYALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1113', 
            'id_estado' =>'20',
            'municipio' => 'SAN BARTOLO YAUTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1114', 
            'id_estado' =>'20',
            'municipio' => 'SAN BARTOLOMÉ AYAUTLA', 
        ));

        Municipio::create(array(
            'id' => '1115', 
            'id_estado' =>'20',
            'municipio' => 'SAN BARTOLOMÉ LOXICHA', 
        ));

        Municipio::create(array(
            'id' => '1116', 
            'id_estado' =>'20',
            'municipio' => 'SAN BARTOLOMÉ QUIALANA', 
        ));

        Municipio::create(array(
            'id' => '1117', 
            'id_estado' =>'20',
            'municipio' => 'SAN BARTOLOMÉ YUCUAÑE', 
        ));

        Municipio::create(array(
            'id' => '1118', 
            'id_estado' =>'20',
            'municipio' => 'SAN BARTOLOMÉ ZOOGOCHO', 
        ));

        Municipio::create(array(
            'id' => '1119', 
            'id_estado' =>'20',
            'municipio' => 'SAN BERNARDO MIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1120', 
            'id_estado' =>'20',
            'municipio' => 'SAN BLAS ATEMPA', 
        ));

        Municipio::create(array(
            'id' => '1121', 
            'id_estado' =>'20',
            'municipio' => 'SAN CARLOS YAUTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1122', 
            'id_estado' =>'20',
            'municipio' => 'SAN CRISTÓBAL AMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1123', 
            'id_estado' =>'20',
            'municipio' => 'SAN CRISTÓBAL AMOLTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1124', 
            'id_estado' =>'20',
            'municipio' => 'SAN CRISTÓBAL LACHIRIOAG', 
        ));

        Municipio::create(array(
            'id' => '1125', 
            'id_estado' =>'20',
            'municipio' => 'SAN CRISTÓBAL SUCHIXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '1126', 
            'id_estado' =>'20',
            'municipio' => 'SAN DIONISIO DEL MAR', 
        ));

        Municipio::create(array(
            'id' => '1127', 
            'id_estado' =>'20',
            'municipio' => 'SAN DIONISIO OCOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1128', 
            'id_estado' =>'20',
            'municipio' => 'SAN DIONISIO OCOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1129', 
            'id_estado' =>'20',
            'municipio' => 'SAN ESTEBAN ATATLAHUCA', 
        ));

        Municipio::create(array(
            'id' => '1130', 
            'id_estado' =>'20',
            'municipio' => 'SAN FELIPE JALAPA DE DÍAZ', 
        ));

        Municipio::create(array(
            'id' => '1131', 
            'id_estado' =>'20',
            'municipio' => 'SAN FELIPE TEJALAPAM', 
        ));

        Municipio::create(array(
            'id' => '1132', 
            'id_estado' =>'20',
            'municipio' => 'SAN FELIPE USILA', 
        ));

        Municipio::create(array(
            'id' => '1133', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO CAHUACUÁ', 
        ));

        Municipio::create(array(
            'id' => '1134', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO CAJONOS', 
        ));

        Municipio::create(array(
            'id' => '1135', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO CHAPULAPA', 
        ));

        Municipio::create(array(
            'id' => '1136', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO CHINDÚA', 
        ));

        Municipio::create(array(
            'id' => '1137', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO DEL MAR', 
        ));

        Municipio::create(array(
            'id' => '1138', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO HUEHUETLÁN', 
        ));

        Municipio::create(array(
            'id' => '1139', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO IXHUATÁN', 
        ));

        Municipio::create(array(
            'id' => '1140', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO JALTEPETONGO', 
        ));

        Municipio::create(array(
            'id' => '1141', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO LACHIGOLÓ', 
        ));

        Municipio::create(array(
            'id' => '1142', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO LOGUECHE', 
        ));

        Municipio::create(array(
            'id' => '1143', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO NUXAÑO', 
        ));

        Municipio::create(array(
            'id' => '1144', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO OZOLOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1145', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO SOLA', 
        ));

        Municipio::create(array(
            'id' => '1146', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO TELIXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '1147', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO TEOPAN', 
        ));

        Municipio::create(array(
            'id' => '1148', 
            'id_estado' =>'20',
            'municipio' => 'SAN FRANCISCO TLAPANCINGO', 
        ));

        Municipio::create(array(
            'id' => '1149', 
            'id_estado' =>'20',
            'municipio' => 'SAN GABRIEL MIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1150', 
            'id_estado' =>'20',
            'municipio' => 'SAN ILDEFONSO AMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1151', 
            'id_estado' =>'20',
            'municipio' => 'SAN ILDEFONSO SOLA', 
        ));

        Municipio::create(array(
            'id' => '1152', 
            'id_estado' =>'20',
            'municipio' => 'SAN ILDEFONSO VILLA ALTA', 
        ));

        Municipio::create(array(
            'id' => '1153', 
            'id_estado' =>'20',
            'municipio' => 'SAN JACINTO AMILPAS', 
        ));

        Municipio::create(array(
            'id' => '1154', 
            'id_estado' =>'20',
            'municipio' => 'SAN JACINTO TLACOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1155', 
            'id_estado' =>'20',
            'municipio' => 'SAN JERÓNIMO COATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1156', 
            'id_estado' =>'20',
            'municipio' => 'SAN JERÓNIMO SILACAYOAPILLA', 
        ));

        Municipio::create(array(
            'id' => '1157', 
            'id_estado' =>'20',
            'municipio' => 'SAN JERÓNIMO SOSOLA', 
        ));

        Municipio::create(array(
            'id' => '1158', 
            'id_estado' =>'20',
            'municipio' => 'SAN JERÓNIMO TAVICHE', 
        ));

        Municipio::create(array(
            'id' => '1159', 
            'id_estado' =>'20',
            'municipio' => 'SAN JERÓNIMO TECOÁTL', 
        ));

        Municipio::create(array(
            'id' => '1160', 
            'id_estado' =>'20',
            'municipio' => 'SAN JERÓNIMO TLACOCHAHUAYA', 
        ));

        Municipio::create(array(
            'id' => '1161', 
            'id_estado' =>'20',
            'municipio' => 'SAN JORGE NUCHITA', 
        ));

        Municipio::create(array(
            'id' => '1162', 
            'id_estado' =>'20',
            'municipio' => 'SAN JOSÉ AYUQUILA', 
        ));

        Municipio::create(array(
            'id' => '1163', 
            'id_estado' =>'20',
            'municipio' => 'SAN JOSÉ CHILTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1164', 
            'id_estado' =>'20',
            'municipio' => 'SAN JOSÉ DEL PEÑASCO', 
        ));

        Municipio::create(array(
            'id' => '1165', 
            'id_estado' =>'20',
            'municipio' => 'SAN JOSÉ DEL PROGRESO', 
        ));

        Municipio::create(array(
            'id' => '1166', 
            'id_estado' =>'20',
            'municipio' => 'SAN JOSÉ ESTANCIA GRANDE', 
        ));

        Municipio::create(array(
            'id' => '1167', 
            'id_estado' =>'20',
            'municipio' => 'SAN JOSÉ INDEPENDENCIA', 
        ));

        Municipio::create(array(
            'id' => '1168', 
            'id_estado' =>'20',
            'municipio' => 'SAN JOSÉ LACHIGUIRI', 
        ));

        Municipio::create(array(
            'id' => '1169', 
            'id_estado' =>'20',
            'municipio' => 'SAN JOSÉ TENANGO', 
        ));

        Municipio::create(array(
            'id' => '1170', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN ?UMÍ', 
        ));

        Municipio::create(array(
            'id' => '1171', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN ACHIUTLA', 
        ));

        Municipio::create(array(
            'id' => '1172', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN ATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1173', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA ATATLAHUCA', 
        ));

        Municipio::create(array(
            'id' => '1174', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA COIXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '1175', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA CUICATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1176', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA GUELACHE', 
        ));

        Municipio::create(array(
            'id' => '1177', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA JAYACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1178', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA LO DE SOTO', 
        ));

        Municipio::create(array(
            'id' => '1179', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA SUCHITEPEC', 
        ));

        Municipio::create(array(
            'id' => '1180', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA TLACHICHILCO', 
        ));

        Municipio::create(array(
            'id' => '1181', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA TLACOATZINTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1182', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA TUXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1183', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN BAUTISTA VALLE NACIONAL', 
        ));

        Municipio::create(array(
            'id' => '1184', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN CACAHUATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1185', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN CHICOMEZÚCHIL', 
        ));

        Municipio::create(array(
            'id' => '1186', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN CHILATECA', 
        ));

        Municipio::create(array(
            'id' => '1187', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN CIENEGUILLA', 
        ));

        Municipio::create(array(
            'id' => '1188', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN COATZÓSPAM', 
        ));

        Municipio::create(array(
            'id' => '1189', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN COLORADO', 
        ));

        Municipio::create(array(
            'id' => '1190', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN COMALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1191', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN COTZOCÓN', 
        ));

        Municipio::create(array(
            'id' => '1192', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN DE LOS CUÉS', 
        ));

        Municipio::create(array(
            'id' => '1193', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN DEL ESTADO', 
        ));

        Municipio::create(array(
            'id' => '1194', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '1195', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN DIUXI', 
        ));

        Municipio::create(array(
            'id' => '1196', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN EVANGELISTA ANALCO', 
        ));

        Municipio::create(array(
            'id' => '1197', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN GUELAVÍA', 
        ));

        Municipio::create(array(
            'id' => '1198', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN GUICHICOVI', 
        ));

        Municipio::create(array(
            'id' => '1199', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN IHUALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1200', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN JUQUILA MIXES', 
        ));

        Municipio::create(array(
            'id' => '1201', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN JUQUILA VIJANOS', 
        ));

        Municipio::create(array(
            'id' => '1202', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN LACHAO', 
        ));

        Municipio::create(array(
            'id' => '1203', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN LACHIGALLA', 
        ));

        Municipio::create(array(
            'id' => '1204', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN LAJARCIA', 
        ));

        Municipio::create(array(
            'id' => '1205', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN LALANA', 
        ));

        Municipio::create(array(
            'id' => '1206', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN MAZATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1207', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN MIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1208', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN MIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1209', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN OZOLOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1210', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN PETLAPA', 
        ));

        Municipio::create(array(
            'id' => '1211', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN QUIAHIJE', 
        ));

        Municipio::create(array(
            'id' => '1212', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN QUIOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1213', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN SAYULTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1214', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN TABAÁ', 
        ));

        Municipio::create(array(
            'id' => '1215', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN TAMAZOLA', 
        ));

        Municipio::create(array(
            'id' => '1216', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN TEITA', 
        ));

        Municipio::create(array(
            'id' => '1217', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN TEITIPAC', 
        ));

        Municipio::create(array(
            'id' => '1218', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN TEPEUXILA', 
        ));

        Municipio::create(array(
            'id' => '1219', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN TEPOSCOLULA', 
        ));

        Municipio::create(array(
            'id' => '1220', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN YAEÉ', 
        ));

        Municipio::create(array(
            'id' => '1221', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN YATZONA', 
        ));

        Municipio::create(array(
            'id' => '1222', 
            'id_estado' =>'20',
            'municipio' => 'SAN JUAN YUCUITA', 
        ));

        Municipio::create(array(
            'id' => '1223', 
            'id_estado' =>'20',
            'municipio' => 'SAN LORENZO', 
        ));

        Municipio::create(array(
            'id' => '1224', 
            'id_estado' =>'20',
            'municipio' => 'SAN LORENZO ALBARRADAS', 
        ));

        Municipio::create(array(
            'id' => '1225', 
            'id_estado' =>'20',
            'municipio' => 'SAN LORENZO CACAOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1226', 
            'id_estado' =>'20',
            'municipio' => 'SAN LORENZO CUAUNECUILTITLA', 
        ));

        Municipio::create(array(
            'id' => '1227', 
            'id_estado' =>'20',
            'municipio' => 'SAN LORENZO TEXMELUCAN', 
        ));

        Municipio::create(array(
            'id' => '1228', 
            'id_estado' =>'20',
            'municipio' => 'SAN LORENZO VICTORIA', 
        ));

        Municipio::create(array(
            'id' => '1229', 
            'id_estado' =>'20',
            'municipio' => 'SAN LUCAS CAMOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1230', 
            'id_estado' =>'20',
            'municipio' => 'SAN LUCAS OJITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1231', 
            'id_estado' =>'20',
            'municipio' => 'SAN LUCAS QUIAVINÍ', 
        ));

        Municipio::create(array(
            'id' => '1232', 
            'id_estado' =>'20',
            'municipio' => 'SAN LUCAS ZOQUIÁPAM', 
        ));

        Municipio::create(array(
            'id' => '1233', 
            'id_estado' =>'20',
            'municipio' => 'SAN LUIS AMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1234', 
            'id_estado' =>'20',
            'municipio' => 'SAN MARCIAL OZOLOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1235', 
            'id_estado' =>'20',
            'municipio' => 'SAN MARCOS ARTEAGA', 
        ));

        Municipio::create(array(
            'id' => '1236', 
            'id_estado' =>'20',
            'municipio' => 'SAN MARTÍN DE LOS CANSECOS', 
        ));

        Municipio::create(array(
            'id' => '1237', 
            'id_estado' =>'20',
            'municipio' => 'SAN MARTÍN HUAMELÚLPAM', 
        ));

        Municipio::create(array(
            'id' => '1238', 
            'id_estado' =>'20',
            'municipio' => 'SAN MARTÍN ITUNYOSO', 
        ));

        Municipio::create(array(
            'id' => '1239', 
            'id_estado' =>'20',
            'municipio' => 'SAN MARTÍN LACHILÁ', 
        ));

        Municipio::create(array(
            'id' => '1240', 
            'id_estado' =>'20',
            'municipio' => 'SAN MARTÍN PERAS', 
        ));

        Municipio::create(array(
            'id' => '1241', 
            'id_estado' =>'20',
            'municipio' => 'SAN MARTÍN TILCAJETE', 
        ));

        Municipio::create(array(
            'id' => '1242', 
            'id_estado' =>'20',
            'municipio' => 'SAN MARTÍN TOXPALAN', 
        ));

        Municipio::create(array(
            'id' => '1243', 
            'id_estado' =>'20',
            'municipio' => 'SAN MARTÍN ZACATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1244', 
            'id_estado' =>'20',
            'municipio' => 'SAN MATEO CAJONOS', 
        ));

        Municipio::create(array(
            'id' => '1245', 
            'id_estado' =>'20',
            'municipio' => 'SAN MATEO DEL MAR', 
        ));

        Municipio::create(array(
            'id' => '1246', 
            'id_estado' =>'20',
            'municipio' => 'SAN MATEO ETLATONGO', 
        ));

        Municipio::create(array(
            'id' => '1247', 
            'id_estado' =>'20',
            'municipio' => 'SAN MATEO NEJÁPAM', 
        ));

        Municipio::create(array(
            'id' => '1248', 
            'id_estado' =>'20',
            'municipio' => 'SAN MATEO PEÑASCO', 
        ));

        Municipio::create(array(
            'id' => '1249', 
            'id_estado' =>'20',
            'municipio' => 'SAN MATEO PIÑAS', 
        ));

        Municipio::create(array(
            'id' => '1250', 
            'id_estado' =>'20',
            'municipio' => 'SAN MATEO RÍO HONDO', 
        ));

        Municipio::create(array(
            'id' => '1251', 
            'id_estado' =>'20',
            'municipio' => 'SAN MATEO SINDIHUI', 
        ));

        Municipio::create(array(
            'id' => '1252', 
            'id_estado' =>'20',
            'municipio' => 'SAN MATEO TLAPILTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1253', 
            'id_estado' =>'20',
            'municipio' => 'SAN MATEO YOLOXOCHITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1254', 
            'id_estado' =>'20',
            'municipio' => 'SAN MELCHOR BETAZA', 
        ));

        Municipio::create(array(
            'id' => '1255', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL ACHIUTLA', 
        ));

        Municipio::create(array(
            'id' => '1256', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL AHUEHUETITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1257', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL ALOÁPAM', 
        ));

        Municipio::create(array(
            'id' => '1258', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL AMATITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1259', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL AMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1260', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL CHICAHUA', 
        ));

        Municipio::create(array(
            'id' => '1261', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL CHIMALAPA', 
        ));

        Municipio::create(array(
            'id' => '1262', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL COATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1263', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL DEL PUERTO', 
        ));

        Municipio::create(array(
            'id' => '1264', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '1265', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL EJUTLA', 
        ));

        Municipio::create(array(
            'id' => '1266', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL EL GRANDE', 
        ));

        Municipio::create(array(
            'id' => '1267', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL HUAUTLA', 
        ));

        Municipio::create(array(
            'id' => '1268', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL MIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1269', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL PANIXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '1270', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL PERAS', 
        ));

        Municipio::create(array(
            'id' => '1271', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL PIEDRAS', 
        ));

        Municipio::create(array(
            'id' => '1272', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL QUETZALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1273', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL SANTA FLOR', 
        ));

        Municipio::create(array(
            'id' => '1274', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL SOYALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1275', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL SUCHIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1276', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL TECOMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1277', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL TENANGO', 
        ));

        Municipio::create(array(
            'id' => '1278', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL TEQUIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1279', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL TILQUIÁPAM', 
        ));

        Municipio::create(array(
            'id' => '1280', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL TLACAMAMA', 
        ));

        Municipio::create(array(
            'id' => '1281', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL TLACOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1282', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL TULANCINGO', 
        ));

        Municipio::create(array(
            'id' => '1283', 
            'id_estado' =>'20',
            'municipio' => 'SAN MIGUEL YOTAO', 
        ));

        Municipio::create(array(
            'id' => '1284', 
            'id_estado' =>'20',
            'municipio' => 'SAN NICOLÁS', 
        ));

        Municipio::create(array(
            'id' => '1285', 
            'id_estado' =>'20',
            'municipio' => 'SAN NICOLÁS HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '1286', 
            'id_estado' =>'20',
            'municipio' => 'SAN PABLO COATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1287', 
            'id_estado' =>'20',
            'municipio' => 'SAN PABLO CUATRO VENADOS', 
        ));

        Municipio::create(array(
            'id' => '1288', 
            'id_estado' =>'20',
            'municipio' => 'SAN PABLO ETLA', 
        ));

        Municipio::create(array(
            'id' => '1289', 
            'id_estado' =>'20',
            'municipio' => 'SAN PABLO HUITZO', 
        ));

        Municipio::create(array(
            'id' => '1290', 
            'id_estado' =>'20',
            'municipio' => 'SAN PABLO HUIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1291', 
            'id_estado' =>'20',
            'municipio' => 'SAN PABLO MACUILTIANGUIS', 
        ));

        Municipio::create(array(
            'id' => '1292', 
            'id_estado' =>'20',
            'municipio' => 'SAN PABLO TIJALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1293', 
            'id_estado' =>'20',
            'municipio' => 'SAN PABLO VILLA DE MITLA', 
        ));

        Municipio::create(array(
            'id' => '1294', 
            'id_estado' =>'20',
            'municipio' => 'SAN PABLO YAGANIZA', 
        ));

        Municipio::create(array(
            'id' => '1295', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO AMUZGOS', 
        ));

        Municipio::create(array(
            'id' => '1296', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO APÓSTOL', 
        ));

        Municipio::create(array(
            'id' => '1297', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO ATOYAC', 
        ));

        Municipio::create(array(
            'id' => '1298', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO CAJONOS', 
        ));

        Municipio::create(array(
            'id' => '1299', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO COMITANCILLO', 
        ));

        Municipio::create(array(
            'id' => '1300', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO COXCALTEPEC CÁNTAROS', 
        ));

        Municipio::create(array(
            'id' => '1301', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO EL ALTO', 
        ));

        Municipio::create(array(
            'id' => '1302', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO HUAMELULA', 
        ));

        Municipio::create(array(
            'id' => '1303', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO HUILOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1304', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO IXCATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1305', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO IXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '1306', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO JALTEPETONGO', 
        ));

        Municipio::create(array(
            'id' => '1307', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO JICAYÁN', 
        ));

        Municipio::create(array(
            'id' => '1308', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO JOCOTIPAC', 
        ));

        Municipio::create(array(
            'id' => '1309', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO JUCHATENGO', 
        ));

        Municipio::create(array(
            'id' => '1310', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO MÁRTIR', 
        ));

        Municipio::create(array(
            'id' => '1311', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO MÁRTIR QUIECHAPA', 
        ));

        Municipio::create(array(
            'id' => '1312', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO MÁRTIR YUCUXACO', 
        ));

        Municipio::create(array(
            'id' => '1313', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO MIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1314', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO MIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1315', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO MOLINOS', 
        ));

        Municipio::create(array(
            'id' => '1316', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO NOPALA', 
        ));

        Municipio::create(array(
            'id' => '1317', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO OCOPETATILLO', 
        ));

        Municipio::create(array(
            'id' => '1318', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO OCOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1319', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO POCHUTLA', 
        ));

        Municipio::create(array(
            'id' => '1320', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO QUIATONI', 
        ));

        Municipio::create(array(
            'id' => '1321', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO SOCHIAPAM', 
        ));

        Municipio::create(array(
            'id' => '1322', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO TAPANATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1323', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO TAVICHE', 
        ));

        Municipio::create(array(
            'id' => '1324', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO TEOZACOALCO', 
        ));

        Municipio::create(array(
            'id' => '1325', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO TEUTILA', 
        ));

        Municipio::create(array(
            'id' => '1326', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO TIDAÁ', 
        ));

        Municipio::create(array(
            'id' => '1327', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO TOPILTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1328', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO TOTOLAPA', 
        ));

        Municipio::create(array(
            'id' => '1329', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO Y SAN PABLO AYUTLA', 
        ));

        Municipio::create(array(
            'id' => '1330', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO Y SAN PABLO TEPOSCOLULA', 
        ));

        Municipio::create(array(
            'id' => '1331', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO Y SAN PABLO TEQUIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1332', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO YANERI', 
        ));

        Municipio::create(array(
            'id' => '1333', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO YÓLOX', 
        ));

        Municipio::create(array(
            'id' => '1334', 
            'id_estado' =>'20',
            'municipio' => 'SAN PEDRO YUCUNAMA', 
        ));

        Municipio::create(array(
            'id' => '1335', 
            'id_estado' =>'20',
            'municipio' => 'SAN RAYMUNDO JALPAN', 
        ));

        Municipio::create(array(
            'id' => '1336', 
            'id_estado' =>'20',
            'municipio' => 'SAN SEBASTIÁN ABASOLO', 
        ));

        Municipio::create(array(
            'id' => '1337', 
            'id_estado' =>'20',
            'municipio' => 'SAN SEBASTIÁN COATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1338', 
            'id_estado' =>'20',
            'municipio' => 'SAN SEBASTIÁN IXCAPA', 
        ));

        Municipio::create(array(
            'id' => '1339', 
            'id_estado' =>'20',
            'municipio' => 'SAN SEBASTIÁN NICANANDUTA', 
        ));

        Municipio::create(array(
            'id' => '1340', 
            'id_estado' =>'20',
            'municipio' => 'SAN SEBASTIÁN RÍO HONDO', 
        ));

        Municipio::create(array(
            'id' => '1341', 
            'id_estado' =>'20',
            'municipio' => 'SAN SEBASTIÁN TECOMAXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '1342', 
            'id_estado' =>'20',
            'municipio' => 'SAN SEBASTIÁN TEITIPAC', 
        ));

        Municipio::create(array(
            'id' => '1343', 
            'id_estado' =>'20',
            'municipio' => 'SAN SEBASTIÁN TUTLA', 
        ));

        Municipio::create(array(
            'id' => '1344', 
            'id_estado' =>'20',
            'municipio' => 'SAN SIMÓN ALMOLONGAS', 
        ));

        Municipio::create(array(
            'id' => '1345', 
            'id_estado' =>'20',
            'municipio' => 'SAN SIMÓN ZAHUATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1346', 
            'id_estado' =>'20',
            'municipio' => 'SAN VICENTE COATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1347', 
            'id_estado' =>'20',
            'municipio' => 'SAN VICENTE LACHIXÍO', 
        ));

        Municipio::create(array(
            'id' => '1348', 
            'id_estado' =>'20',
            'municipio' => 'SAN VICENTE NUÑÚ', 
        ));

        Municipio::create(array(
            'id' => '1349', 
            'id_estado' =>'20',
            'municipio' => 'SANTA ANA', 
        ));

        Municipio::create(array(
            'id' => '1350', 
            'id_estado' =>'20',
            'municipio' => 'SANTA ANA ATEIXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '1351', 
            'id_estado' =>'20',
            'municipio' => 'SANTA ANA CUAUHTÉMOC', 
        ));

        Municipio::create(array(
            'id' => '1352', 
            'id_estado' =>'20',
            'municipio' => 'SANTA ANA DEL VALLE', 
        ));

        Municipio::create(array(
            'id' => '1353', 
            'id_estado' =>'20',
            'municipio' => 'SANTA ANA TAVELA', 
        ));

        Municipio::create(array(
            'id' => '1354', 
            'id_estado' =>'20',
            'municipio' => 'SANTA ANA TLAPACOYAN', 
        ));

        Municipio::create(array(
            'id' => '1355', 
            'id_estado' =>'20',
            'municipio' => 'SANTA ANA YARENI', 
        ));

        Municipio::create(array(
            'id' => '1356', 
            'id_estado' =>'20',
            'municipio' => 'SANTA ANA ZEGACHE', 
        ));

        Municipio::create(array(
            'id' => '1357', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATALINA QUIERÍ', 
        ));

        Municipio::create(array(
            'id' => '1358', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA CUIXTLA', 
        ));

        Municipio::create(array(
            'id' => '1359', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA IXTEPEJI', 
        ));

        Municipio::create(array(
            'id' => '1360', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA JUQUILA', 
        ));

        Municipio::create(array(
            'id' => '1361', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA LACHATAO', 
        ));

        Municipio::create(array(
            'id' => '1362', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA LOXICHA', 
        ));

        Municipio::create(array(
            'id' => '1363', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA MECHOACÁN', 
        ));

        Municipio::create(array(
            'id' => '1364', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA MINAS', 
        ));

        Municipio::create(array(
            'id' => '1365', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA QUIANÉ', 
        ));

        Municipio::create(array(
            'id' => '1366', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA QUIOQUITANI', 
        ));

        Municipio::create(array(
            'id' => '1367', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA TAYATA', 
        ));

        Municipio::create(array(
            'id' => '1368', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA TICUÁ', 
        ));

        Municipio::create(array(
            'id' => '1369', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA YOSONOTÚ', 
        ));

        Municipio::create(array(
            'id' => '1370', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CATARINA ZAPOQUILA', 
        ));

        Municipio::create(array(
            'id' => '1371', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ ACATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1372', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ AMILPAS', 
        ));

        Municipio::create(array(
            'id' => '1373', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ DE BRAVO', 
        ));

        Municipio::create(array(
            'id' => '1374', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ ITUNDUJIA', 
        ));

        Municipio::create(array(
            'id' => '1375', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ MIXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1376', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ NUNDACO', 
        ));

        Municipio::create(array(
            'id' => '1377', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ PAPALUTLA', 
        ));

        Municipio::create(array(
            'id' => '1378', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ TACACHE DE MINA', 
        ));

        Municipio::create(array(
            'id' => '1379', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ TACAHUA', 
        ));

        Municipio::create(array(
            'id' => '1380', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ TAYATA', 
        ));

        Municipio::create(array(
            'id' => '1381', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ XITLA', 
        ));

        Municipio::create(array(
            'id' => '1382', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ XOXOCOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1383', 
            'id_estado' =>'20',
            'municipio' => 'SANTA CRUZ ZENZONTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1384', 
            'id_estado' =>'20',
            'municipio' => 'SANTA GERTRUDIS', 
        ));

        Municipio::create(array(
            'id' => '1385', 
            'id_estado' =>'20',
            'municipio' => 'SANTA INÉS DE ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '1386', 
            'id_estado' =>'20',
            'municipio' => 'SANTA INÉS DEL MONTE', 
        ));

        Municipio::create(array(
            'id' => '1387', 
            'id_estado' =>'20',
            'municipio' => 'SANTA INÉS YATZECHE', 
        ));

        Municipio::create(array(
            'id' => '1388', 
            'id_estado' =>'20',
            'municipio' => 'SANTA LUCÍA DEL CAMINO', 
        ));

        Municipio::create(array(
            'id' => '1389', 
            'id_estado' =>'20',
            'municipio' => 'SANTA LUCÍA MIAHUATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1390', 
            'id_estado' =>'20',
            'municipio' => 'SANTA LUCÍA MONTEVERDE', 
        ));

        Municipio::create(array(
            'id' => '1391', 
            'id_estado' =>'20',
            'municipio' => 'SANTA LUCÍA OCOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1392', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MAGDALENA JICOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1393', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA ALOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1394', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA APAZCO', 
        ));

        Municipio::create(array(
            'id' => '1395', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA ATZOMPA', 
        ));

        Municipio::create(array(
            'id' => '1396', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA CAMOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1397', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA CHACHOÁPAM', 
        ));

        Municipio::create(array(
            'id' => '1398', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA CHILCHOTLA', 
        ));

        Municipio::create(array(
            'id' => '1399', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA CHIMALAPA', 
        ));

        Municipio::create(array(
            'id' => '1400', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA COLOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1401', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA CORTIJO', 
        ));

        Municipio::create(array(
            'id' => '1402', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA COYOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1403', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA DEL ROSARIO', 
        ));

        Municipio::create(array(
            'id' => '1404', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA DEL TULE', 
        ));

        Municipio::create(array(
            'id' => '1405', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA ECATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1406', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA GUELACÉ', 
        ));

        Municipio::create(array(
            'id' => '1407', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA GUIENAGATI', 
        ));

        Municipio::create(array(
            'id' => '1408', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA HUATULCO', 
        ));

        Municipio::create(array(
            'id' => '1409', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA HUAZOLOTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1410', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA IPALAPA', 
        ));

        Municipio::create(array(
            'id' => '1411', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA IXCATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1412', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA JACATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1413', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA JALAPA DEL MARQUÉS', 
        ));

        Municipio::create(array(
            'id' => '1414', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA JALTIANGUIS', 
        ));

        Municipio::create(array(
            'id' => '1415', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA LA ASUNCIÓN', 
        ));

        Municipio::create(array(
            'id' => '1416', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA LACHIXÍO', 
        ));

        Municipio::create(array(
            'id' => '1417', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA MIXTEQUILLA', 
        ));

        Municipio::create(array(
            'id' => '1418', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA NATIVITAS', 
        ));

        Municipio::create(array(
            'id' => '1419', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA NDUAYACO', 
        ));

        Municipio::create(array(
            'id' => '1420', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA OZOLOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1421', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA PÁPALO', 
        ));

        Municipio::create(array(
            'id' => '1422', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA PEÑOLES', 
        ));

        Municipio::create(array(
            'id' => '1423', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA PETAPA', 
        ));

        Municipio::create(array(
            'id' => '1424', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA QUIEGOLANI', 
        ));

        Municipio::create(array(
            'id' => '1425', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA SOLA', 
        ));

        Municipio::create(array(
            'id' => '1426', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TATALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1427', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TECOMAVACA', 
        ));

        Municipio::create(array(
            'id' => '1428', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TEMAXCALAPA', 
        ));

        Municipio::create(array(
            'id' => '1429', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TEMAXCALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1430', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TEOPOXCO', 
        ));

        Municipio::create(array(
            'id' => '1431', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TEPANTLALI', 
        ));

        Municipio::create(array(
            'id' => '1432', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TEXCATITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1433', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TLAHUITOLTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1434', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TLALIXTAC', 
        ));

        Municipio::create(array(
            'id' => '1435', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TONAMECA', 
        ));

        Municipio::create(array(
            'id' => '1436', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA TOTOLAPILLA', 
        ));

        Municipio::create(array(
            'id' => '1437', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA XADANI', 
        ));

        Municipio::create(array(
            'id' => '1438', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA YALINA', 
        ));

        Municipio::create(array(
            'id' => '1439', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA YAVESÍA', 
        ));

        Municipio::create(array(
            'id' => '1440', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA YOLOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1441', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA YOSOYÚA', 
        ));

        Municipio::create(array(
            'id' => '1442', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA YUCUHITI', 
        ));

        Municipio::create(array(
            'id' => '1443', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA ZACATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1444', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA ZANIZA', 
        ));

        Municipio::create(array(
            'id' => '1445', 
            'id_estado' =>'20',
            'municipio' => 'SANTA MARÍA ZOQUITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1446', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO AMOLTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1447', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO APOALA', 
        ));

        Municipio::create(array(
            'id' => '1448', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO APÓSTOL', 
        ));

        Municipio::create(array(
            'id' => '1449', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO ASTATA', 
        ));

        Municipio::create(array(
            'id' => '1450', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO ATITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1451', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO AYUQUILILLA', 
        ));

        Municipio::create(array(
            'id' => '1452', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO CACALOXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1453', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO CAMOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1454', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO CHAZUMBA', 
        ));

        Municipio::create(array(
            'id' => '1455', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO CHOAPAM', 
        ));

        Municipio::create(array(
            'id' => '1456', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO COMALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1457', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '1458', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO HUAJOLOTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1459', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO HUAUCLILLA', 
        ));

        Municipio::create(array(
            'id' => '1460', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO IHUITLÁN PLUMAS', 
        ));

        Municipio::create(array(
            'id' => '1461', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO IXCUINTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1462', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO IXTAYUTLA', 
        ));

        Municipio::create(array(
            'id' => '1463', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO JAMILTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1464', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO JOCOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1465', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO JUXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '1466', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO LACHIGUIRI', 
        ));

        Municipio::create(array(
            'id' => '1467', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO LALOPA', 
        ));

        Municipio::create(array(
            'id' => '1468', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO LAOLLAGA', 
        ));

        Municipio::create(array(
            'id' => '1469', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO LAXOPA', 
        ));

        Municipio::create(array(
            'id' => '1470', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO LLANO GRANDE', 
        ));

        Municipio::create(array(
            'id' => '1471', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO MATATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1472', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO MILTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1473', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO MINAS', 
        ));

        Municipio::create(array(
            'id' => '1474', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO NACALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1475', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO NEJAPILLA', 
        ));

        Municipio::create(array(
            'id' => '1476', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO NILTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1477', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO NUNDICHE', 
        ));

        Municipio::create(array(
            'id' => '1478', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO NUYOÓ', 
        ));

        Municipio::create(array(
            'id' => '1479', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO PINOTEPA NACIONAL', 
        ));

        Municipio::create(array(
            'id' => '1480', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO SUCHILQUITONGO', 
        ));

        Municipio::create(array(
            'id' => '1481', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO TAMAZOLA', 
        ));

        Municipio::create(array(
            'id' => '1482', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO TAPEXTLA', 
        ));

        Municipio::create(array(
            'id' => '1483', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO TENANGO', 
        ));

        Municipio::create(array(
            'id' => '1484', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO TEPETLAPA', 
        ));

        Municipio::create(array(
            'id' => '1485', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO TETEPEC', 
        ));

        Municipio::create(array(
            'id' => '1486', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO TEXCALCINGO', 
        ));

        Municipio::create(array(
            'id' => '1487', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO TEXTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1488', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO TILANTONGO', 
        ));

        Municipio::create(array(
            'id' => '1489', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO TILLO', 
        ));

        Municipio::create(array(
            'id' => '1490', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO TLAZOYALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1491', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO XANICA', 
        ));

        Municipio::create(array(
            'id' => '1492', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO XIACUÍ', 
        ));

        Municipio::create(array(
            'id' => '1493', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO YAITEPEC', 
        ));

        Municipio::create(array(
            'id' => '1494', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO YAVEO', 
        ));

        Municipio::create(array(
            'id' => '1495', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO YOLOMÉCATL', 
        ));

        Municipio::create(array(
            'id' => '1496', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO YOSONDÚA', 
        ));

        Municipio::create(array(
            'id' => '1497', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO YUCUYACHI', 
        ));

        Municipio::create(array(
            'id' => '1498', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO ZACATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1499', 
            'id_estado' =>'20',
            'municipio' => 'SANTIAGO ZOOCHILA', 
        ));

        Municipio::create(array(
            'id' => '1500', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO ALBARRADAS', 
        ));

        Municipio::create(array(
            'id' => '1501', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO ARMENTA', 
        ));

        Municipio::create(array(
            'id' => '1502', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO CHIHUITÁN', 
        ));

        Municipio::create(array(
            'id' => '1503', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO DE MORELOS', 
        ));

        Municipio::create(array(
            'id' => '1504', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO INGENIO', 
        ));

        Municipio::create(array(
            'id' => '1505', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO IXCATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1506', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO NUXAÁ', 
        ));

        Municipio::create(array(
            'id' => '1507', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO OZOLOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1508', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO PETAPA', 
        ));

        Municipio::create(array(
            'id' => '1509', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO ROAYAGA', 
        ));

        Municipio::create(array(
            'id' => '1510', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO TEHUANTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1511', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO TEOJOMULCO', 
        ));

        Municipio::create(array(
            'id' => '1512', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO TEPUXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1513', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO TLATAYÁPAM', 
        ));

        Municipio::create(array(
            'id' => '1514', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO TOMALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1515', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO TONALÁ', 
        ));

        Municipio::create(array(
            'id' => '1516', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO TONALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1517', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO XAGACÍA', 
        ));

        Municipio::create(array(
            'id' => '1518', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO YANHUITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1519', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO YODOHINO', 
        ));

        Municipio::create(array(
            'id' => '1520', 
            'id_estado' =>'20',
            'municipio' => 'SANTO DOMINGO ZANATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1521', 
            'id_estado' =>'20',
            'municipio' => 'SANTO TOMÁS JALIEZA', 
        ));

        Municipio::create(array(
            'id' => '1522', 
            'id_estado' =>'20',
            'municipio' => 'SANTO TOMÁS MAZALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1523', 
            'id_estado' =>'20',
            'municipio' => 'SANTO TOMÁS OCOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1524', 
            'id_estado' =>'20',
            'municipio' => 'SANTO TOMÁS TAMAZULAPAN', 
        ));

        Municipio::create(array(
            'id' => '1525', 
            'id_estado' =>'20',
            'municipio' => 'SANTOS REYES NOPALA', 
        ));

        Municipio::create(array(
            'id' => '1526', 
            'id_estado' =>'20',
            'municipio' => 'SANTOS REYES PÁPALO', 
        ));

        Municipio::create(array(
            'id' => '1527', 
            'id_estado' =>'20',
            'municipio' => 'SANTOS REYES TEPEJILLO', 
        ));

        Municipio::create(array(
            'id' => '1528', 
            'id_estado' =>'20',
            'municipio' => 'SANTOS REYES YUCUNÁ', 
        ));

        Municipio::create(array(
            'id' => '1529', 
            'id_estado' =>'20',
            'municipio' => 'SILACAYOÁPAM', 
        ));

        Municipio::create(array(
            'id' => '1530', 
            'id_estado' =>'20',
            'municipio' => 'SITIO DE XITLAPEHUA', 
        ));

        Municipio::create(array(
            'id' => '1531', 
            'id_estado' =>'20',
            'municipio' => 'SOLEDAD ETLA', 
        ));

        Municipio::create(array(
            'id' => '1532', 
            'id_estado' =>'20',
            'municipio' => 'TAMAZULAPAM DEL ESPÍRITU SANTO', 
        ));

        Municipio::create(array(
            'id' => '1533', 
            'id_estado' =>'20',
            'municipio' => 'TANETZE DE ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '1534', 
            'id_estado' =>'20',
            'municipio' => 'TANICHE', 
        ));

        Municipio::create(array(
            'id' => '1535', 
            'id_estado' =>'20',
            'municipio' => 'TATALTEPEC DE VALDÉS', 
        ));

        Municipio::create(array(
            'id' => '1536', 
            'id_estado' =>'20',
            'municipio' => 'TEOCOCUILCO DE MARCOS PÉREZ', 
        ));

        Municipio::create(array(
            'id' => '1537', 
            'id_estado' =>'20',
            'municipio' => 'TEOTITLÁN DE FLORES MAGÓN', 
        ));

        Municipio::create(array(
            'id' => '1538', 
            'id_estado' =>'20',
            'municipio' => 'TEOTITLÁN DEL VALLE', 
        ));

        Municipio::create(array(
            'id' => '1539', 
            'id_estado' =>'20',
            'municipio' => 'TEOTONGO', 
        ));

        Municipio::create(array(
            'id' => '1540', 
            'id_estado' =>'20',
            'municipio' => 'TEPELMEME VILLA DE MORELOS', 
        ));

        Municipio::create(array(
            'id' => '1541', 
            'id_estado' =>'20',
            'municipio' => 'TEZOATLÁN DE SEGURA Y LUNA', 
        ));

        Municipio::create(array(
            'id' => '1542', 
            'id_estado' =>'20',
            'municipio' => 'TLACOLULA DE MATAMOROS', 
        ));

        Municipio::create(array(
            'id' => '1543', 
            'id_estado' =>'20',
            'municipio' => 'TLACOTEPEC PLUMAS', 
        ));

        Municipio::create(array(
            'id' => '1544', 
            'id_estado' =>'20',
            'municipio' => 'TLALIXTAC DE CABRERA', 
        ));

        Municipio::create(array(
            'id' => '1545', 
            'id_estado' =>'20',
            'municipio' => 'TOTONTEPEC VILLA DE MORELOS', 
        ));

        Municipio::create(array(
            'id' => '1546', 
            'id_estado' =>'20',
            'municipio' => 'TRINIDAD ZAACHILA', 
        ));

        Municipio::create(array(
            'id' => '1547', 
            'id_estado' =>'20',
            'municipio' => 'UNIÓN HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '1548', 
            'id_estado' =>'20',
            'municipio' => 'VALERIO TRUJANO', 
        ));

        Municipio::create(array(
            'id' => '1549', 
            'id_estado' =>'20',
            'municipio' => 'VILLA DE CHILAPA DE DÍAZ', 
        ));

        Municipio::create(array(
            'id' => '1550', 
            'id_estado' =>'20',
            'municipio' => 'VILLA DE ETLA', 
        ));

        Municipio::create(array(
            'id' => '1551', 
            'id_estado' =>'20',
            'municipio' => 'VILLA DE TAMAZULÁPAM DEL PROGRESO', 
        ));

        Municipio::create(array(
            'id' => '1552', 
            'id_estado' =>'20',
            'municipio' => 'VILLA DE TUTUTEPEC DE MELCHOR OCAM', 
        ));

        Municipio::create(array(
            'id' => '1553', 
            'id_estado' =>'20',
            'municipio' => 'VILLA DE ZAACHILA', 
        ));

        Municipio::create(array(
            'id' => '1554', 
            'id_estado' =>'20',
            'municipio' => 'VILLA DÍAZ ORDAZ', 
        ));

        Municipio::create(array(
            'id' => '1555', 
            'id_estado' =>'20',
            'municipio' => 'VILLA HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '1556', 
            'id_estado' =>'20',
            'municipio' => 'VILLA SOLA DE VEGA', 
        ));

        Municipio::create(array(
            'id' => '1557', 
            'id_estado' =>'20',
            'municipio' => 'VILLA TALEA DE CASTRO', 
        ));

        Municipio::create(array(
            'id' => '1558', 
            'id_estado' =>'20',
            'municipio' => 'VILLA TEJÚPAM DE LA UNIÓN', 
        ));

        Municipio::create(array(
            'id' => '1559', 
            'id_estado' =>'20',
            'municipio' => 'YAXE', 
        ));

        Municipio::create(array(
            'id' => '1560', 
            'id_estado' =>'20',
            'municipio' => 'YOGANA', 
        ));

        Municipio::create(array(
            'id' => '1561', 
            'id_estado' =>'20',
            'municipio' => 'YUTANDUCHI DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '1562', 
            'id_estado' =>'20',
            'municipio' => 'ZAPOTITLÁN DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '1563', 
            'id_estado' =>'20',
            'municipio' => 'ZAPOTITLÁN LAGUNAS', 
        ));

        Municipio::create(array(
            'id' => '1564', 
            'id_estado' =>'20',
            'municipio' => 'ZAPOTITLÁN PALMAS', 
        ));

        Municipio::create(array(
            'id' => '1565', 
            'id_estado' =>'20',
            'municipio' => 'ZIMATLÁN DE ALVAREZ', 
        ));

        Municipio::create(array(
            'id' => '1566', 
            'id_estado' =>'21',
            'municipio' => 'ACAJETE', 
        ));

        Municipio::create(array(
            'id' => '1567', 
            'id_estado' =>'21',
            'municipio' => 'ACATENO', 
        ));

        Municipio::create(array(
            'id' => '1568', 
            'id_estado' =>'21',
            'municipio' => 'ACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1569', 
            'id_estado' =>'21',
            'municipio' => 'ACATZINGO', 
        ));

        Municipio::create(array(
            'id' => '1570', 
            'id_estado' =>'21',
            'municipio' => 'ACTEOPAN', 
        ));

        Municipio::create(array(
            'id' => '1571', 
            'id_estado' =>'21',
            'municipio' => 'AHUACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1572', 
            'id_estado' =>'21',
            'municipio' => 'AHUATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1573', 
            'id_estado' =>'21',
            'municipio' => 'AHUAZOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1574', 
            'id_estado' =>'21',
            'municipio' => 'AHUEHUETITLA', 
        ));

        Municipio::create(array(
            'id' => '1575', 
            'id_estado' =>'21',
            'municipio' => 'AJALPAN', 
        ));

        Municipio::create(array(
            'id' => '1576', 
            'id_estado' =>'21',
            'municipio' => 'ALBINO ZERTUCHE', 
        ));

        Municipio::create(array(
            'id' => '1577', 
            'id_estado' =>'21',
            'municipio' => 'ALJOJUCA', 
        ));

        Municipio::create(array(
            'id' => '1578', 
            'id_estado' =>'21',
            'municipio' => 'ALTEPEXI', 
        ));

        Municipio::create(array(
            'id' => '1579', 
            'id_estado' =>'21',
            'municipio' => 'AMIXTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1580', 
            'id_estado' =>'21',
            'municipio' => 'AMOZOC', 
        ));

        Municipio::create(array(
            'id' => '1581', 
            'id_estado' =>'21',
            'municipio' => 'AQUIXTLA', 
        ));

        Municipio::create(array(
            'id' => '1582', 
            'id_estado' =>'21',
            'municipio' => 'ATEMPAN', 
        ));

        Municipio::create(array(
            'id' => '1583', 
            'id_estado' =>'21',
            'municipio' => 'ATEXCAL', 
        ));

        Municipio::create(array(
            'id' => '1584', 
            'id_estado' =>'21',
            'municipio' => 'ATLEQUIZAYAN', 
        ));

        Municipio::create(array(
            'id' => '1585', 
            'id_estado' =>'21',
            'municipio' => 'ATLIXCO', 
        ));

        Municipio::create(array(
            'id' => '1586', 
            'id_estado' =>'21',
            'municipio' => 'ATOYATEMPAN', 
        ));

        Municipio::create(array(
            'id' => '1587', 
            'id_estado' =>'21',
            'municipio' => 'ATZALA', 
        ));

        Municipio::create(array(
            'id' => '1588', 
            'id_estado' =>'21',
            'municipio' => 'ATZITZIHUACÁN', 
        ));

        Municipio::create(array(
            'id' => '1589', 
            'id_estado' =>'21',
            'municipio' => 'ATZITZINTLA', 
        ));

        Municipio::create(array(
            'id' => '1590', 
            'id_estado' =>'21',
            'municipio' => 'AXUTLA', 
        ));

        Municipio::create(array(
            'id' => '1591', 
            'id_estado' =>'21',
            'municipio' => 'AYOTOXCO DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '1592', 
            'id_estado' =>'21',
            'municipio' => 'CALPAN', 
        ));

        Municipio::create(array(
            'id' => '1593', 
            'id_estado' =>'21',
            'municipio' => 'CALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1594', 
            'id_estado' =>'21',
            'municipio' => 'CAMOCUAUTLA', 
        ));

        Municipio::create(array(
            'id' => '1595', 
            'id_estado' =>'21',
            'municipio' => 'CAÑADA MORELOS', 
        ));

        Municipio::create(array(
            'id' => '1596', 
            'id_estado' =>'21',
            'municipio' => 'CAXHUACAN', 
        ));

        Municipio::create(array(
            'id' => '1597', 
            'id_estado' =>'21',
            'municipio' => 'CHALCHICOMULA DE SESMA', 
        ));

        Municipio::create(array(
            'id' => '1598', 
            'id_estado' =>'21',
            'municipio' => 'CHAPULCO', 
        ));

        Municipio::create(array(
            'id' => '1599', 
            'id_estado' =>'21',
            'municipio' => 'CHIAUTLA', 
        ));

        Municipio::create(array(
            'id' => '1600', 
            'id_estado' =>'21',
            'municipio' => 'CHIAUTZINGO', 
        ));

        Municipio::create(array(
            'id' => '1601', 
            'id_estado' =>'21',
            'municipio' => 'CHICHIQUILA', 
        ));

        Municipio::create(array(
            'id' => '1602', 
            'id_estado' =>'21',
            'municipio' => 'CHICONCUAUTLA', 
        ));

        Municipio::create(array(
            'id' => '1603', 
            'id_estado' =>'21',
            'municipio' => 'CHIETLA', 
        ));

        Municipio::create(array(
            'id' => '1604', 
            'id_estado' =>'21',
            'municipio' => 'CHIGMECATITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1605', 
            'id_estado' =>'21',
            'municipio' => 'CHIGNAHUAPAN', 
        ));

        Municipio::create(array(
            'id' => '1606', 
            'id_estado' =>'21',
            'municipio' => 'CHIGNAUTLA', 
        ));

        Municipio::create(array(
            'id' => '1607', 
            'id_estado' =>'21',
            'municipio' => 'CHILA', 
        ));

        Municipio::create(array(
            'id' => '1608', 
            'id_estado' =>'21',
            'municipio' => 'CHILA DE LA SAL', 
        ));

        Municipio::create(array(
            'id' => '1609', 
            'id_estado' =>'21',
            'municipio' => 'CHILCHOTLA', 
        ));

        Municipio::create(array(
            'id' => '1610', 
            'id_estado' =>'21',
            'municipio' => 'CHINANTLA', 
        ));

        Municipio::create(array(
            'id' => '1611', 
            'id_estado' =>'21',
            'municipio' => 'COATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1612', 
            'id_estado' =>'21',
            'municipio' => 'COATZINGO', 
        ));

        Municipio::create(array(
            'id' => '1613', 
            'id_estado' =>'21',
            'municipio' => 'COHETZALA', 
        ));

        Municipio::create(array(
            'id' => '1614', 
            'id_estado' =>'21',
            'municipio' => 'COHUECÁN', 
        ));

        Municipio::create(array(
            'id' => '1615', 
            'id_estado' =>'21',
            'municipio' => 'CORONANGO', 
        ));

        Municipio::create(array(
            'id' => '1616', 
            'id_estado' =>'21',
            'municipio' => 'COXCATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1617', 
            'id_estado' =>'21',
            'municipio' => 'COYOMEAPAN', 
        ));

        Municipio::create(array(
            'id' => '1618', 
            'id_estado' =>'21',
            'municipio' => 'COYOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1619', 
            'id_estado' =>'21',
            'municipio' => 'CUAPIAXTLA DE MADERO', 
        ));

        Municipio::create(array(
            'id' => '1620', 
            'id_estado' =>'21',
            'municipio' => 'CUAUTEMPAN', 
        ));

        Municipio::create(array(
            'id' => '1621', 
            'id_estado' =>'21',
            'municipio' => 'CUAUTINCHÁN', 
        ));

        Municipio::create(array(
            'id' => '1622', 
            'id_estado' =>'21',
            'municipio' => 'CUAUTLANCINGO', 
        ));

        Municipio::create(array(
            'id' => '1623', 
            'id_estado' =>'21',
            'municipio' => 'CUAYUCA DE ANDRADE', 
        ));

        Municipio::create(array(
            'id' => '1624', 
            'id_estado' =>'21',
            'municipio' => 'CUETZALAN DEL PROGRESO', 
        ));

        Municipio::create(array(
            'id' => '1625', 
            'id_estado' =>'21',
            'municipio' => 'CUYOACO', 
        ));

        Municipio::create(array(
            'id' => '1626', 
            'id_estado' =>'21',
            'municipio' => 'DOMINGO ARENAS', 
        ));

        Municipio::create(array(
            'id' => '1627', 
            'id_estado' =>'21',
            'municipio' => 'ELOXOCHITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1628', 
            'id_estado' =>'21',
            'municipio' => 'EPATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1629', 
            'id_estado' =>'21',
            'municipio' => 'ESPERANZA', 
        ));

        Municipio::create(array(
            'id' => '1630', 
            'id_estado' =>'21',
            'municipio' => 'FRANCISCO Z. MENA', 
        ));

        Municipio::create(array(
            'id' => '1631', 
            'id_estado' =>'21',
            'municipio' => 'GENERAL FELIPE ?NGELES', 
        ));

        Municipio::create(array(
            'id' => '1632', 
            'id_estado' =>'21',
            'municipio' => 'GUADALUPE', 
        ));

        Municipio::create(array(
            'id' => '1633', 
            'id_estado' =>'21',
            'municipio' => 'GUADALUPE VICTORIA', 
        ));

        Municipio::create(array(
            'id' => '1634', 
            'id_estado' =>'21',
            'municipio' => 'HERMENEGILDO GALEANA', 
        ));

        Municipio::create(array(
            'id' => '1635', 
            'id_estado' =>'21',
            'municipio' => 'HONEY', 
        ));

        Municipio::create(array(
            'id' => '1636', 
            'id_estado' =>'21',
            'municipio' => 'HUAQUECHULA', 
        ));

        Municipio::create(array(
            'id' => '1637', 
            'id_estado' =>'21',
            'municipio' => 'HUATLATLAUCA', 
        ));

        Municipio::create(array(
            'id' => '1638', 
            'id_estado' =>'21',
            'municipio' => 'HUAUCHINANGO', 
        ));

        Municipio::create(array(
            'id' => '1639', 
            'id_estado' =>'21',
            'municipio' => 'HUEHUETLA', 
        ));

        Municipio::create(array(
            'id' => '1640', 
            'id_estado' =>'21',
            'municipio' => 'HUEHUETLÁN EL CHICO', 
        ));

        Municipio::create(array(
            'id' => '1641', 
            'id_estado' =>'21',
            'municipio' => 'HUEHUETLÁN EL GRANDE', 
        ));

        Municipio::create(array(
            'id' => '1642', 
            'id_estado' =>'21',
            'municipio' => 'HUEJOTZINGO', 
        ));

        Municipio::create(array(
            'id' => '1643', 
            'id_estado' =>'21',
            'municipio' => 'HUEYAPAN', 
        ));

        Municipio::create(array(
            'id' => '1644', 
            'id_estado' =>'21',
            'municipio' => 'HUEYTAMALCO', 
        ));

        Municipio::create(array(
            'id' => '1645', 
            'id_estado' =>'21',
            'municipio' => 'HUEYTLALPAN', 
        ));

        Municipio::create(array(
            'id' => '1646', 
            'id_estado' =>'21',
            'municipio' => 'HUITZILAN DE SERDÁN', 
        ));

        Municipio::create(array(
            'id' => '1647', 
            'id_estado' =>'21',
            'municipio' => 'HUITZILTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1648', 
            'id_estado' =>'21',
            'municipio' => 'IXCAMILPA DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '1649', 
            'id_estado' =>'21',
            'municipio' => 'IXCAQUIXTLA', 
        ));

        Municipio::create(array(
            'id' => '1650', 
            'id_estado' =>'21',
            'municipio' => 'IXTACAMAXTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1651', 
            'id_estado' =>'21',
            'municipio' => 'IXTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1652', 
            'id_estado' =>'21',
            'municipio' => 'IZÚCAR DE MATAMOROS', 
        ));

        Municipio::create(array(
            'id' => '1653', 
            'id_estado' =>'21',
            'municipio' => 'JALPAN', 
        ));

        Municipio::create(array(
            'id' => '1654', 
            'id_estado' =>'21',
            'municipio' => 'JOLALPAN', 
        ));

        Municipio::create(array(
            'id' => '1655', 
            'id_estado' =>'21',
            'municipio' => 'JONOTLA', 
        ));

        Municipio::create(array(
            'id' => '1656', 
            'id_estado' =>'21',
            'municipio' => 'JOPALA', 
        ));

        Municipio::create(array(
            'id' => '1657', 
            'id_estado' =>'21',
            'municipio' => 'JUAN C. BONILLA', 
        ));

        Municipio::create(array(
            'id' => '1658', 
            'id_estado' =>'21',
            'municipio' => 'JUAN GALINDO', 
        ));

        Municipio::create(array(
            'id' => '1659', 
            'id_estado' =>'21',
            'municipio' => 'JUAN N. MÉNDEZ', 
        ));

        Municipio::create(array(
            'id' => '1660', 
            'id_estado' =>'21',
            'municipio' => 'LA MAGDALENA TLATLAUQUITEPEC', 
        ));

        Municipio::create(array(
            'id' => '1661', 
            'id_estado' =>'21',
            'municipio' => 'LAFRAGUA', 
        ));

        Municipio::create(array(
            'id' => '1662', 
            'id_estado' =>'21',
            'municipio' => 'LIBRES', 
        ));

        Municipio::create(array(
            'id' => '1663', 
            'id_estado' =>'21',
            'municipio' => 'LOS REYES DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1664', 
            'id_estado' =>'21',
            'municipio' => 'MAZAPILTEPEC DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1665', 
            'id_estado' =>'21',
            'municipio' => 'MIXTLA', 
        ));

        Municipio::create(array(
            'id' => '1666', 
            'id_estado' =>'21',
            'municipio' => 'MOLCAXAC', 
        ));

        Municipio::create(array(
            'id' => '1667', 
            'id_estado' =>'21',
            'municipio' => 'NAUPAN', 
        ));

        Municipio::create(array(
            'id' => '1668', 
            'id_estado' =>'21',
            'municipio' => 'NAUZONTLA', 
        ));

        Municipio::create(array(
            'id' => '1669', 
            'id_estado' =>'21',
            'municipio' => 'NEALTICAN', 
        ));

        Municipio::create(array(
            'id' => '1670', 
            'id_estado' =>'21',
            'municipio' => 'NICOLÁS BRAVO', 
        ));

        Municipio::create(array(
            'id' => '1671', 
            'id_estado' =>'21',
            'municipio' => 'NOPALUCAN', 
        ));

        Municipio::create(array(
            'id' => '1672', 
            'id_estado' =>'21',
            'municipio' => 'OCOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1673', 
            'id_estado' =>'21',
            'municipio' => 'OCOYUCAN', 
        ));

        Municipio::create(array(
            'id' => '1674', 
            'id_estado' =>'21',
            'municipio' => 'OLINTLA', 
        ));

        Municipio::create(array(
            'id' => '1675', 
            'id_estado' =>'21',
            'municipio' => 'ORIENTAL', 
        ));

        Municipio::create(array(
            'id' => '1676', 
            'id_estado' =>'21',
            'municipio' => 'PAHUATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1677', 
            'id_estado' =>'21',
            'municipio' => 'PALMAR DE BRAVO', 
        ));

        Municipio::create(array(
            'id' => '1678', 
            'id_estado' =>'21',
            'municipio' => 'PANTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1679', 
            'id_estado' =>'21',
            'municipio' => 'PETLALCINGO', 
        ));

        Municipio::create(array(
            'id' => '1680', 
            'id_estado' =>'21',
            'municipio' => 'PIAXTLA', 
        ));

        Municipio::create(array(
            'id' => '1681', 
            'id_estado' =>'21',
            'municipio' => 'PUEBLA', 
        ));

        Municipio::create(array(
            'id' => '1682', 
            'id_estado' =>'21',
            'municipio' => 'QUECHOLAC', 
        ));

        Municipio::create(array(
            'id' => '1683', 
            'id_estado' =>'21',
            'municipio' => 'QUIMIXTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1684', 
            'id_estado' =>'21',
            'municipio' => 'RAFAEL LARA GRAJALES', 
        ));

        Municipio::create(array(
            'id' => '1685', 
            'id_estado' =>'21',
            'municipio' => 'SAN ANDRÉS CHOLULA', 
        ));

        Municipio::create(array(
            'id' => '1686', 
            'id_estado' =>'21',
            'municipio' => 'SAN ANTONIO CAÑADA', 
        ));

        Municipio::create(array(
            'id' => '1687', 
            'id_estado' =>'21',
            'municipio' => 'SAN DIEGO LA MESA TOCHIMILTZINGO', 
        ));

        Municipio::create(array(
            'id' => '1688', 
            'id_estado' =>'21',
            'municipio' => 'SAN FELIPE TEOTLALCINGO', 
        ));

        Municipio::create(array(
            'id' => '1689', 
            'id_estado' =>'21',
            'municipio' => 'SAN FELIPE TEPATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1690', 
            'id_estado' =>'21',
            'municipio' => 'SAN GABRIEL CHILAC', 
        ));

        Municipio::create(array(
            'id' => '1691', 
            'id_estado' =>'21',
            'municipio' => 'SAN GREGORIO ATZOMPA', 
        ));

        Municipio::create(array(
            'id' => '1692', 
            'id_estado' =>'21',
            'municipio' => 'SAN JERÓNIMO TECUANIPAN', 
        ));

        Municipio::create(array(
            'id' => '1693', 
            'id_estado' =>'21',
            'municipio' => 'SAN JERÓNIMO XAYACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1694', 
            'id_estado' =>'21',
            'municipio' => 'SAN JOSÉ CHIAPA', 
        ));

        Municipio::create(array(
            'id' => '1695', 
            'id_estado' =>'21',
            'municipio' => 'SAN JOSÉ MIAHUATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1696', 
            'id_estado' =>'21',
            'municipio' => 'SAN JUAN ATENCO', 
        ));

        Municipio::create(array(
            'id' => '1697', 
            'id_estado' =>'21',
            'municipio' => 'SAN JUAN ATZOMPA', 
        ));

        Municipio::create(array(
            'id' => '1698', 
            'id_estado' =>'21',
            'municipio' => 'SAN MARTÍN TEXMELUCAN', 
        ));

        Municipio::create(array(
            'id' => '1699', 
            'id_estado' =>'21',
            'municipio' => 'SAN MARTÍN TOTOLTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1700', 
            'id_estado' =>'21',
            'municipio' => 'SAN MATÍAS TLALANCALECA', 
        ));

        Municipio::create(array(
            'id' => '1701', 
            'id_estado' =>'21',
            'municipio' => 'SAN MIGUEL IXITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1702', 
            'id_estado' =>'21',
            'municipio' => 'SAN MIGUEL XOXTLA', 
        ));

        Municipio::create(array(
            'id' => '1703', 
            'id_estado' =>'21',
            'municipio' => 'SAN NICOLÁS BUENOS AIRES', 
        ));

        Municipio::create(array(
            'id' => '1704', 
            'id_estado' =>'21',
            'municipio' => 'SAN NICOLÁS DE LOS RANCHOS', 
        ));

        Municipio::create(array(
            'id' => '1705', 
            'id_estado' =>'21',
            'municipio' => 'SAN PABLO ANICANO', 
        ));

        Municipio::create(array(
            'id' => '1706', 
            'id_estado' =>'21',
            'municipio' => 'SAN PEDRO CHOLULA', 
        ));

        Municipio::create(array(
            'id' => '1707', 
            'id_estado' =>'21',
            'municipio' => 'SAN PEDRO YELOIXTLAHUACA', 
        ));

        Municipio::create(array(
            'id' => '1708', 
            'id_estado' =>'21',
            'municipio' => 'SAN SALVADOR EL SECO', 
        ));

        Municipio::create(array(
            'id' => '1709', 
            'id_estado' =>'21',
            'municipio' => 'SAN SALVADOR EL VERDE', 
        ));

        Municipio::create(array(
            'id' => '1710', 
            'id_estado' =>'21',
            'municipio' => 'SAN SALVADOR HUIXCOLOTLA', 
        ));

        Municipio::create(array(
            'id' => '1711', 
            'id_estado' =>'21',
            'municipio' => 'SAN SEBASTIÁN TLACOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1712', 
            'id_estado' =>'21',
            'municipio' => 'SANTA CATARINA TLALTEMPAN', 
        ));

        Municipio::create(array(
            'id' => '1713', 
            'id_estado' =>'21',
            'municipio' => 'SANTA INÉS AHUATEMPAN', 
        ));

        Municipio::create(array(
            'id' => '1714', 
            'id_estado' =>'21',
            'municipio' => 'SANTA ISABEL CHOLULA', 
        ));

        Municipio::create(array(
            'id' => '1715', 
            'id_estado' =>'21',
            'municipio' => 'SANTIAGO MIAHUATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1716', 
            'id_estado' =>'21',
            'municipio' => 'SANTO TOMÁS HUEYOTLIPAN', 
        ));

        Municipio::create(array(
            'id' => '1717', 
            'id_estado' =>'21',
            'municipio' => 'SOLTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1718', 
            'id_estado' =>'21',
            'municipio' => 'TECALI DE HERRERA', 
        ));

        Municipio::create(array(
            'id' => '1719', 
            'id_estado' =>'21',
            'municipio' => 'TECAMACHALCO', 
        ));

        Municipio::create(array(
            'id' => '1720', 
            'id_estado' =>'21',
            'municipio' => 'TECOMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1721', 
            'id_estado' =>'21',
            'municipio' => 'TEHUACÁN', 
        ));

        Municipio::create(array(
            'id' => '1722', 
            'id_estado' =>'21',
            'municipio' => 'TEHUITZINGO', 
        ));

        Municipio::create(array(
            'id' => '1723', 
            'id_estado' =>'21',
            'municipio' => 'TENAMPULCO', 
        ));

        Municipio::create(array(
            'id' => '1724', 
            'id_estado' =>'21',
            'municipio' => 'TEOPANTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1725', 
            'id_estado' =>'21',
            'municipio' => 'TEOTLALCO', 
        ));

        Municipio::create(array(
            'id' => '1726', 
            'id_estado' =>'21',
            'municipio' => 'TEPANCO DE LÓPEZ', 
        ));

        Municipio::create(array(
            'id' => '1727', 
            'id_estado' =>'21',
            'municipio' => 'TEPANGO DE RODRÍGUEZ', 
        ));

        Municipio::create(array(
            'id' => '1728', 
            'id_estado' =>'21',
            'municipio' => 'TEPATLAXCO DE HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '1729', 
            'id_estado' =>'21',
            'municipio' => 'TEPEACA', 
        ));

        Municipio::create(array(
            'id' => '1730', 
            'id_estado' =>'21',
            'municipio' => 'TEPEMAXALCO', 
        ));

        Municipio::create(array(
            'id' => '1731', 
            'id_estado' =>'21',
            'municipio' => 'TEPEOJUMA', 
        ));

        Municipio::create(array(
            'id' => '1732', 
            'id_estado' =>'21',
            'municipio' => 'TEPETZINTLA', 
        ));

        Municipio::create(array(
            'id' => '1733', 
            'id_estado' =>'21',
            'municipio' => 'TEPEXCO', 
        ));

        Municipio::create(array(
            'id' => '1734', 
            'id_estado' =>'21',
            'municipio' => 'TEPEXI DE RODRÍGUEZ', 
        ));

        Municipio::create(array(
            'id' => '1735', 
            'id_estado' =>'21',
            'municipio' => 'TEPEYAHUALCO', 
        ));

        Municipio::create(array(
            'id' => '1736', 
            'id_estado' =>'21',
            'municipio' => 'TEPEYAHUALCO DE CUAUHTÉMOC', 
        ));

        Municipio::create(array(
            'id' => '1737', 
            'id_estado' =>'21',
            'municipio' => 'TETELA DE OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '1738', 
            'id_estado' =>'21',
            'municipio' => 'TETELES DE AVILA CASTILLO', 
        ));

        Municipio::create(array(
            'id' => '1739', 
            'id_estado' =>'21',
            'municipio' => 'TEZIUTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1740', 
            'id_estado' =>'21',
            'municipio' => 'TIANGUISMANALCO', 
        ));

        Municipio::create(array(
            'id' => '1741', 
            'id_estado' =>'21',
            'municipio' => 'TILAPA', 
        ));

        Municipio::create(array(
            'id' => '1742', 
            'id_estado' =>'21',
            'municipio' => 'TLACHICHUCA', 
        ));

        Municipio::create(array(
            'id' => '1743', 
            'id_estado' =>'21',
            'municipio' => 'TLACOTEPEC DE BENITO JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1744', 
            'id_estado' =>'21',
            'municipio' => 'TLACUILOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1745', 
            'id_estado' =>'21',
            'municipio' => 'TLAHUAPAN', 
        ));

        Municipio::create(array(
            'id' => '1746', 
            'id_estado' =>'21',
            'municipio' => 'TLALTENANGO', 
        ));

        Municipio::create(array(
            'id' => '1747', 
            'id_estado' =>'21',
            'municipio' => 'TLANEPANTLA', 
        ));

        Municipio::create(array(
            'id' => '1748', 
            'id_estado' =>'21',
            'municipio' => 'TLAOLA', 
        ));

        Municipio::create(array(
            'id' => '1749', 
            'id_estado' =>'21',
            'municipio' => 'TLAPACOYA', 
        ));

        Municipio::create(array(
            'id' => '1750', 
            'id_estado' =>'21',
            'municipio' => 'TLAPANALÁ', 
        ));

        Municipio::create(array(
            'id' => '1751', 
            'id_estado' =>'21',
            'municipio' => 'TLATLAUQUITEPEC', 
        ));

        Municipio::create(array(
            'id' => '1752', 
            'id_estado' =>'21',
            'municipio' => 'TLAXCO', 
        ));

        Municipio::create(array(
            'id' => '1753', 
            'id_estado' =>'21',
            'municipio' => 'TOCHIMILCO', 
        ));

        Municipio::create(array(
            'id' => '1754', 
            'id_estado' =>'21',
            'municipio' => 'TOCHTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1755', 
            'id_estado' =>'21',
            'municipio' => 'TOTOLTEPEC DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '1756', 
            'id_estado' =>'21',
            'municipio' => 'TULCINGO', 
        ));

        Municipio::create(array(
            'id' => '1757', 
            'id_estado' =>'21',
            'municipio' => 'TUZAMAPAN DE GALEANA', 
        ));

        Municipio::create(array(
            'id' => '1758', 
            'id_estado' =>'21',
            'municipio' => 'TZICATLACOYAN', 
        ));

        Municipio::create(array(
            'id' => '1759', 
            'id_estado' =>'21',
            'municipio' => 'VENUSTIANO CARRANZA', 
        ));

        Municipio::create(array(
            'id' => '1760', 
            'id_estado' =>'21',
            'municipio' => 'VICENTE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '1761', 
            'id_estado' =>'21',
            'municipio' => 'XAYACATLÁN DE BRAVO', 
        ));

        Municipio::create(array(
            'id' => '1762', 
            'id_estado' =>'21',
            'municipio' => 'XICOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1763', 
            'id_estado' =>'21',
            'municipio' => 'XICOTLÁN', 
        ));

        Municipio::create(array(
            'id' => '1764', 
            'id_estado' =>'21',
            'municipio' => 'XIUTETELCO', 
        ));

        Municipio::create(array(
            'id' => '1765', 
            'id_estado' =>'21',
            'municipio' => 'XOCHIAPULCO', 
        ));

        Municipio::create(array(
            'id' => '1766', 
            'id_estado' =>'21',
            'municipio' => 'XOCHILTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1767', 
            'id_estado' =>'21',
            'municipio' => 'XOCHITLÁN DE VICENTE SUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1768', 
            'id_estado' =>'21',
            'municipio' => 'XOCHITLÁN TODOS SANTOS', 
        ));

        Municipio::create(array(
            'id' => '1769', 
            'id_estado' =>'21',
            'municipio' => 'YAONÁHUAC', 
        ));

        Municipio::create(array(
            'id' => '1770', 
            'id_estado' =>'21',
            'municipio' => 'YEHUALTEPEC', 
        ));

        Municipio::create(array(
            'id' => '1771', 
            'id_estado' =>'21',
            'municipio' => 'ZACAPALA', 
        ));

        Municipio::create(array(
            'id' => '1772', 
            'id_estado' =>'21',
            'municipio' => 'ZACAPOAXTLA', 
        ));

        Municipio::create(array(
            'id' => '1773', 
            'id_estado' =>'21',
            'municipio' => 'ZACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1774', 
            'id_estado' =>'21',
            'municipio' => 'ZAPOTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1775', 
            'id_estado' =>'21',
            'municipio' => 'ZAPOTITLÁN DE MÉNDEZ', 
        ));

        Municipio::create(array(
            'id' => '1776', 
            'id_estado' =>'21',
            'municipio' => 'ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '1777', 
            'id_estado' =>'21',
            'municipio' => 'ZAUTLA', 
        ));

        Municipio::create(array(
            'id' => '1778', 
            'id_estado' =>'21',
            'municipio' => 'ZIHUATEUTLA', 
        ));

        Municipio::create(array(
            'id' => '1779', 
            'id_estado' =>'21',
            'municipio' => 'ZINACATEPEC', 
        ));

        Municipio::create(array(
            'id' => '1780', 
            'id_estado' =>'21',
            'municipio' => 'ZONGOZOTLA', 
        ));

        Municipio::create(array(
            'id' => '1781', 
            'id_estado' =>'21',
            'municipio' => 'ZOQUIAPAN', 
        ));

        Municipio::create(array(
            'id' => '1782', 
            'id_estado' =>'21',
            'municipio' => 'ZOQUITLÁN', 
        ));

        Municipio::create(array(
            'id' => '1783', 
            'id_estado' =>'22',
            'municipio' => 'AMEALCO DE BONFIL', 
        ));

        Municipio::create(array(
            'id' => '1784', 
            'id_estado' =>'22',
            'municipio' => 'ARROYO SECO', 
        ));

        Municipio::create(array(
            'id' => '1785', 
            'id_estado' =>'22',
            'municipio' => 'CADEREYTA DE MONTES', 
        ));

        Municipio::create(array(
            'id' => '1786', 
            'id_estado' =>'22',
            'municipio' => 'COLÓN', 
        ));

        Municipio::create(array(
            'id' => '1787', 
            'id_estado' =>'22',
            'municipio' => 'CORREGIDORA', 
        ));

        Municipio::create(array(
            'id' => '1788', 
            'id_estado' =>'22',
            'municipio' => 'EL MARQUÉS', 
        ));

        Municipio::create(array(
            'id' => '1789', 
            'id_estado' =>'22',
            'municipio' => 'EZEQUIEL MONTES', 
        ));

        Municipio::create(array(
            'id' => '1790', 
            'id_estado' =>'22',
            'municipio' => 'HUIMILPAN', 
        ));

        Municipio::create(array(
            'id' => '1791', 
            'id_estado' =>'22',
            'municipio' => 'JALPAN DE SERRA', 
        ));

        Municipio::create(array(
            'id' => '1792', 
            'id_estado' =>'22',
            'municipio' => 'LANDA DE MATAMOROS', 
        ));

        Municipio::create(array(
            'id' => '1793', 
            'id_estado' =>'22',
            'municipio' => 'PEDRO ESCOBEDO', 
        ));

        Municipio::create(array(
            'id' => '1794', 
            'id_estado' =>'22',
            'municipio' => 'PEÑAMILLER', 
        ));

        Municipio::create(array(
            'id' => '1795', 
            'id_estado' =>'22',
            'municipio' => 'PINAL DE AMOLES', 
        ));

        Municipio::create(array(
            'id' => '1796', 
            'id_estado' =>'22',
            'municipio' => 'QUERÉTARO', 
        ));

        Municipio::create(array(
            'id' => '1797', 
            'id_estado' =>'22',
            'municipio' => 'SAN JOAQUÍN', 
        ));

        Municipio::create(array(
            'id' => '1798', 
            'id_estado' =>'22',
            'municipio' => 'SAN JUAN DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '1799', 
            'id_estado' =>'22',
            'municipio' => 'TEQUISQUIAPAN', 
        ));

        Municipio::create(array(
            'id' => '1800', 
            'id_estado' =>'22',
            'municipio' => 'TOLIMÁN', 
        ));

        Municipio::create(array(
            'id' => '1801', 
            'id_estado' =>'23',
            'municipio' => 'BENITO JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1802', 
            'id_estado' =>'23',
            'municipio' => 'COZUMEL', 
        ));

        Municipio::create(array(
            'id' => '1803', 
            'id_estado' =>'23',
            'municipio' => 'FELIPE CARRILLO PUERTO', 
        ));

        Municipio::create(array(
            'id' => '1804', 
            'id_estado' =>'23',
            'municipio' => 'ISLA MUJERES', 
        ));

        Municipio::create(array(
            'id' => '1805', 
            'id_estado' =>'23',
            'municipio' => 'JOSÉ MARÍA MORELOS', 
        ));

        Municipio::create(array(
            'id' => '1806', 
            'id_estado' =>'23',
            'municipio' => 'LÁZARO CÁRDENAS', 
        ));

        Municipio::create(array(
            'id' => '1807', 
            'id_estado' =>'23',
            'municipio' => 'OTHÓN P. BLANCO', 
        ));

        Municipio::create(array(
            'id' => '1808', 
            'id_estado' =>'23',
            'municipio' => 'SOLIDARIDAD', 
        ));

        Municipio::create(array(
            'id' => '1809', 
            'id_estado' =>'24',
            'municipio' => 'AHUALULCO', 
        ));

        Municipio::create(array(
            'id' => '1810', 
            'id_estado' =>'24',
            'municipio' => 'ALAQUINES', 
        ));

        Municipio::create(array(
            'id' => '1811', 
            'id_estado' =>'24',
            'municipio' => 'AQUISMÓN', 
        ));

        Municipio::create(array(
            'id' => '1812', 
            'id_estado' =>'24',
            'municipio' => 'ARMADILLO DE LOS INFANTE', 
        ));

        Municipio::create(array(
            'id' => '1813', 
            'id_estado' =>'24',
            'municipio' => 'AXTLA DE TERRAZAS', 
        ));

        Municipio::create(array(
            'id' => '1814', 
            'id_estado' =>'24',
            'municipio' => 'CÁRDENAS', 
        ));

        Municipio::create(array(
            'id' => '1815', 
            'id_estado' =>'24',
            'municipio' => 'CATORCE', 
        ));

        Municipio::create(array(
            'id' => '1816', 
            'id_estado' =>'24',
            'municipio' => 'CEDRAL', 
        ));

        Municipio::create(array(
            'id' => '1817', 
            'id_estado' =>'24',
            'municipio' => 'CERRITOS', 
        ));

        Municipio::create(array(
            'id' => '1818', 
            'id_estado' =>'24',
            'municipio' => 'CERRO DE SAN PEDRO', 
        ));

        Municipio::create(array(
            'id' => '1819', 
            'id_estado' =>'24',
            'municipio' => 'CHARCAS', 
        ));

        Municipio::create(array(
            'id' => '1820', 
            'id_estado' =>'24',
            'municipio' => 'CIUDAD DEL MAÍZ', 
        ));

        Municipio::create(array(
            'id' => '1821', 
            'id_estado' =>'24',
            'municipio' => 'CIUDAD FERNÁNDEZ', 
        ));

        Municipio::create(array(
            'id' => '1822', 
            'id_estado' =>'24',
            'municipio' => 'CIUDAD VALLES', 
        ));

        Municipio::create(array(
            'id' => '1823', 
            'id_estado' =>'24',
            'municipio' => 'COXCATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1824', 
            'id_estado' =>'24',
            'municipio' => 'EBANO', 
        ));

        Municipio::create(array(
            'id' => '1825', 
            'id_estado' =>'24',
            'municipio' => 'EL NARANJO', 
        ));

        Municipio::create(array(
            'id' => '1826', 
            'id_estado' =>'24',
            'municipio' => 'GUADALCÁZAR', 
        ));

        Municipio::create(array(
            'id' => '1827', 
            'id_estado' =>'24',
            'municipio' => 'HUEHUETLÁN', 
        ));

        Municipio::create(array(
            'id' => '1828', 
            'id_estado' =>'24',
            'municipio' => 'LAGUNILLAS', 
        ));

        Municipio::create(array(
            'id' => '1829', 
            'id_estado' =>'24',
            'municipio' => 'MATEHUALA', 
        ));

        Municipio::create(array(
            'id' => '1830', 
            'id_estado' =>'24',
            'municipio' => 'MATLAPA', 
        ));

        Municipio::create(array(
            'id' => '1831', 
            'id_estado' =>'24',
            'municipio' => 'MEXQUITIC DE CARMONA', 
        ));

        Municipio::create(array(
            'id' => '1832', 
            'id_estado' =>'24',
            'municipio' => 'MOCTEZUMA', 
        ));

        Municipio::create(array(
            'id' => '1833', 
            'id_estado' =>'24',
            'municipio' => 'RAYÓN', 
        ));

        Municipio::create(array(
            'id' => '1834', 
            'id_estado' =>'24',
            'municipio' => 'RIOVERDE', 
        ));

        Municipio::create(array(
            'id' => '1835', 
            'id_estado' =>'24',
            'municipio' => 'SALINAS', 
        ));

        Municipio::create(array(
            'id' => '1836', 
            'id_estado' =>'24',
            'municipio' => 'SAN ANTONIO', 
        ));

        Municipio::create(array(
            'id' => '1837', 
            'id_estado' =>'24',
            'municipio' => 'SAN CIRO DE ACOSTA', 
        ));

        Municipio::create(array(
            'id' => '1838', 
            'id_estado' =>'24',
            'municipio' => 'SAN LUIS POTOSÍ', 
        ));

        Municipio::create(array(
            'id' => '1839', 
            'id_estado' =>'24',
            'municipio' => 'SAN MARTÍN CHALCHICUAUTLA', 
        ));

        Municipio::create(array(
            'id' => '1840', 
            'id_estado' =>'24',
            'municipio' => 'SAN NICOLÁS TOLENTINO', 
        ));

        Municipio::create(array(
            'id' => '1841', 
            'id_estado' =>'24',
            'municipio' => 'SAN VICENTE TANCUAYALAB', 
        ));

        Municipio::create(array(
            'id' => '1842', 
            'id_estado' =>'24',
            'municipio' => 'SANTA CATARINA', 
        ));

        Municipio::create(array(
            'id' => '1843', 
            'id_estado' =>'24',
            'municipio' => 'SANTA MARÍA DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '1844', 
            'id_estado' =>'24',
            'municipio' => 'SANTO DOMINGO', 
        ));

        Municipio::create(array(
            'id' => '1845', 
            'id_estado' =>'24',
            'municipio' => 'SOLEDAD DE GRACIANO SÁNCHEZ', 
        ));

        Municipio::create(array(
            'id' => '1846', 
            'id_estado' =>'24',
            'municipio' => 'TAMASOPO', 
        ));

        Municipio::create(array(
            'id' => '1847', 
            'id_estado' =>'24',
            'municipio' => 'TAMAZUNCHALE', 
        ));

        Municipio::create(array(
            'id' => '1848', 
            'id_estado' =>'24',
            'municipio' => 'TAMPACÁN', 
        ));

        Municipio::create(array(
            'id' => '1849', 
            'id_estado' =>'24',
            'municipio' => 'TAMPAMOLÓN CORONA', 
        ));

        Municipio::create(array(
            'id' => '1850', 
            'id_estado' =>'24',
            'municipio' => 'TAMUÍN', 
        ));

        Municipio::create(array(
            'id' => '1851', 
            'id_estado' =>'24',
            'municipio' => 'TANCANHUITZ', 
        ));

        Municipio::create(array(
            'id' => '1852', 
            'id_estado' =>'24',
            'municipio' => 'TANLAJÁS', 
        ));

        Municipio::create(array(
            'id' => '1853', 
            'id_estado' =>'24',
            'municipio' => 'TANQUIÁN DE ESCOBEDO', 
        ));

        Municipio::create(array(
            'id' => '1854', 
            'id_estado' =>'24',
            'municipio' => 'TIERRA NUEVA', 
        ));

        Municipio::create(array(
            'id' => '1855', 
            'id_estado' =>'24',
            'municipio' => 'VANEGAS', 
        ));

        Municipio::create(array(
            'id' => '1856', 
            'id_estado' =>'24',
            'municipio' => 'VENADO', 
        ));

        Municipio::create(array(
            'id' => '1857', 
            'id_estado' =>'24',
            'municipio' => 'VILLA DE ARISTA', 
        ));

        Municipio::create(array(
            'id' => '1858', 
            'id_estado' =>'24',
            'municipio' => 'VILLA DE ARRIAGA', 
        ));

        Municipio::create(array(
            'id' => '1859', 
            'id_estado' =>'24',
            'municipio' => 'VILLA DE GUADALUPE', 
        ));

        Municipio::create(array(
            'id' => '1860', 
            'id_estado' =>'24',
            'municipio' => 'VILLA DE LA PAZ', 
        ));

        Municipio::create(array(
            'id' => '1861', 
            'id_estado' =>'24',
            'municipio' => 'VILLA DE RAMOS', 
        ));

        Municipio::create(array(
            'id' => '1862', 
            'id_estado' =>'24',
            'municipio' => 'VILLA DE REYES', 
        ));

        Municipio::create(array(
            'id' => '1863', 
            'id_estado' =>'24',
            'municipio' => 'VILLA HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '1864', 
            'id_estado' =>'24',
            'municipio' => 'VILLA JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1865', 
            'id_estado' =>'24',
            'municipio' => 'XILITLA', 
        ));

        Municipio::create(array(
            'id' => '1866', 
            'id_estado' =>'24',
            'municipio' => 'ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '1867', 
            'id_estado' =>'25',
            'municipio' => 'AHOME', 
        ));

        Municipio::create(array(
            'id' => '1868', 
            'id_estado' =>'25',
            'municipio' => 'ANGOSTURA', 
        ));

        Municipio::create(array(
            'id' => '1869', 
            'id_estado' =>'25',
            'municipio' => 'BADIRAGUATO', 
        ));

        Municipio::create(array(
            'id' => '1870', 
            'id_estado' =>'25',
            'municipio' => 'CHOIX', 
        ));

        Municipio::create(array(
            'id' => '1871', 
            'id_estado' =>'25',
            'municipio' => 'CONCORDIA', 
        ));

        Municipio::create(array(
            'id' => '1872', 
            'id_estado' =>'25',
            'municipio' => 'COSALÁ', 
        ));

        Municipio::create(array(
            'id' => '1873', 
            'id_estado' =>'25',
            'municipio' => 'CULIACÁN', 
        ));

        Municipio::create(array(
            'id' => '1874', 
            'id_estado' =>'25',
            'municipio' => 'EL FUERTE', 
        ));

        Municipio::create(array(
            'id' => '1875', 
            'id_estado' =>'25',
            'municipio' => 'ELOTA', 
        ));

        Municipio::create(array(
            'id' => '1876', 
            'id_estado' =>'25',
            'municipio' => 'ESCUINAPA', 
        ));

        Municipio::create(array(
            'id' => '1877', 
            'id_estado' =>'25',
            'municipio' => 'GUASAVE', 
        ));

        Municipio::create(array(
            'id' => '1878', 
            'id_estado' =>'25',
            'municipio' => 'MAZATLÁN', 
        ));

        Municipio::create(array(
            'id' => '1879', 
            'id_estado' =>'25',
            'municipio' => 'MOCORITO', 
        ));

        Municipio::create(array(
            'id' => '1880', 
            'id_estado' =>'25',
            'municipio' => 'NAVOLATO', 
        ));

        Municipio::create(array(
            'id' => '1881', 
            'id_estado' =>'25',
            'municipio' => 'ROSARIO', 
        ));

        Municipio::create(array(
            'id' => '1882', 
            'id_estado' =>'25',
            'municipio' => 'SALVADOR ALVARADO', 
        ));

        Municipio::create(array(
            'id' => '1883', 
            'id_estado' =>'25',
            'municipio' => 'SAN IGNACIO', 
        ));

        Municipio::create(array(
            'id' => '1884', 
            'id_estado' =>'25',
            'municipio' => 'SINALOA', 
        ));

        Municipio::create(array(
            'id' => '1885', 
            'id_estado' =>'26',
            'municipio' => 'ACONCHI', 
        ));

        Municipio::create(array(
            'id' => '1886', 
            'id_estado' =>'26',
            'municipio' => 'AGUA PRIETA', 
        ));

        Municipio::create(array(
            'id' => '1887', 
            'id_estado' =>'26',
            'municipio' => 'ALAMOS', 
        ));

        Municipio::create(array(
            'id' => '1888', 
            'id_estado' =>'26',
            'municipio' => 'ALTAR', 
        ));

        Municipio::create(array(
            'id' => '1889', 
            'id_estado' =>'26',
            'municipio' => 'ARIVECHI', 
        ));

        Municipio::create(array(
            'id' => '1890', 
            'id_estado' =>'26',
            'municipio' => 'ARIZPE', 
        ));

        Municipio::create(array(
            'id' => '1891', 
            'id_estado' =>'26',
            'municipio' => 'ATIL', 
        ));

        Municipio::create(array(
            'id' => '1892', 
            'id_estado' =>'26',
            'municipio' => 'BACADÉHUACHI', 
        ));

        Municipio::create(array(
            'id' => '1893', 
            'id_estado' =>'26',
            'municipio' => 'BACANORA', 
        ));

        Municipio::create(array(
            'id' => '1894', 
            'id_estado' =>'26',
            'municipio' => 'BACERAC', 
        ));

        Municipio::create(array(
            'id' => '1895', 
            'id_estado' =>'26',
            'municipio' => 'BACOACHI', 
        ));

        Municipio::create(array(
            'id' => '1896', 
            'id_estado' =>'26',
            'municipio' => 'BÁCUM', 
        ));

        Municipio::create(array(
            'id' => '1897', 
            'id_estado' =>'26',
            'municipio' => 'BANÁMICHI', 
        ));

        Municipio::create(array(
            'id' => '1898', 
            'id_estado' =>'26',
            'municipio' => 'BAVIÁCORA', 
        ));

        Municipio::create(array(
            'id' => '1899', 
            'id_estado' =>'26',
            'municipio' => 'BAVISPE', 
        ));

        Municipio::create(array(
            'id' => '1900', 
            'id_estado' =>'26',
            'municipio' => 'BENITO JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '1901', 
            'id_estado' =>'26',
            'municipio' => 'BENJAMÍN HILL', 
        ));

        Municipio::create(array(
            'id' => '1902', 
            'id_estado' =>'26',
            'municipio' => 'CABORCA', 
        ));

        Municipio::create(array(
            'id' => '1903', 
            'id_estado' =>'26',
            'municipio' => 'CAJEME', 
        ));

        Municipio::create(array(
            'id' => '1904', 
            'id_estado' =>'26',
            'municipio' => 'CANANEA', 
        ));

        Municipio::create(array(
            'id' => '1905', 
            'id_estado' =>'26',
            'municipio' => 'CARBÓ', 
        ));

        Municipio::create(array(
            'id' => '1906', 
            'id_estado' =>'26',
            'municipio' => 'CUCURPE', 
        ));

        Municipio::create(array(
            'id' => '1907', 
            'id_estado' =>'26',
            'municipio' => 'CUMPAS', 
        ));

        Municipio::create(array(
            'id' => '1908', 
            'id_estado' =>'26',
            'municipio' => 'DIVISADEROS', 
        ));

        Municipio::create(array(
            'id' => '1909', 
            'id_estado' =>'26',
            'municipio' => 'EMPALME', 
        ));

        Municipio::create(array(
            'id' => '1910', 
            'id_estado' =>'26',
            'municipio' => 'ETCHOJOA', 
        ));

        Municipio::create(array(
            'id' => '1911', 
            'id_estado' =>'26',
            'municipio' => 'FRONTERAS', 
        ));

        Municipio::create(array(
            'id' => '1912', 
            'id_estado' =>'26',
            'municipio' => 'GENERAL PLUTARCO ELÍAS CALLES', 
        ));

        Municipio::create(array(
            'id' => '1913', 
            'id_estado' =>'26',
            'municipio' => 'GRANADOS', 
        ));

        Municipio::create(array(
            'id' => '1914', 
            'id_estado' =>'26',
            'municipio' => 'GUAYMAS', 
        ));

        Municipio::create(array(
            'id' => '1915', 
            'id_estado' =>'26',
            'municipio' => 'HERMOSILLO', 
        ));

        Municipio::create(array(
            'id' => '1916', 
            'id_estado' =>'26',
            'municipio' => 'HEROICA NOGALES', 
        ));

        Municipio::create(array(
            'id' => '1917', 
            'id_estado' =>'26',
            'municipio' => 'HUACHINERA', 
        ));

        Municipio::create(array(
            'id' => '1918', 
            'id_estado' =>'26',
            'municipio' => 'HUÁSABAS', 
        ));

        Municipio::create(array(
            'id' => '1919', 
            'id_estado' =>'26',
            'municipio' => 'HUATABAMPO', 
        ));

        Municipio::create(array(
            'id' => '1920', 
            'id_estado' =>'26',
            'municipio' => 'HUÉPAC', 
        ));

        Municipio::create(array(
            'id' => '1921', 
            'id_estado' =>'26',
            'municipio' => 'IMURIS', 
        ));

        Municipio::create(array(
            'id' => '1922', 
            'id_estado' =>'26',
            'municipio' => 'LA COLORADA', 
        ));

        Municipio::create(array(
            'id' => '1923', 
            'id_estado' =>'26',
            'municipio' => 'MAGDALENA', 
        ));

        Municipio::create(array(
            'id' => '1924', 
            'id_estado' =>'26',
            'municipio' => 'MAZATÁN', 
        ));

        Municipio::create(array(
            'id' => '1925', 
            'id_estado' =>'26',
            'municipio' => 'MOCTEZUMA', 
        ));

        Municipio::create(array(
            'id' => '1926', 
            'id_estado' =>'26',
            'municipio' => 'NACO', 
        ));

        Municipio::create(array(
            'id' => '1927', 
            'id_estado' =>'26',
            'municipio' => 'NÁCORI CHICO', 
        ));

        Municipio::create(array(
            'id' => '1928', 
            'id_estado' =>'26',
            'municipio' => 'NACOZARI DE GARCÍA', 
        ));

        Municipio::create(array(
            'id' => '1929', 
            'id_estado' =>'26',
            'municipio' => 'NAVOJOA', 
        ));

        Municipio::create(array(
            'id' => '1930', 
            'id_estado' =>'26',
            'municipio' => 'ONAVAS', 
        ));

        Municipio::create(array(
            'id' => '1931', 
            'id_estado' =>'26',
            'municipio' => 'OPODEPE', 
        ));

        Municipio::create(array(
            'id' => '1932', 
            'id_estado' =>'26',
            'municipio' => 'OQUITOA', 
        ));

        Municipio::create(array(
            'id' => '1933', 
            'id_estado' =>'26',
            'municipio' => 'PITIQUITO', 
        ));

        Municipio::create(array(
            'id' => '1934', 
            'id_estado' =>'26',
            'municipio' => 'PUERTO PEÑASCO', 
        ));

        Municipio::create(array(
            'id' => '1935', 
            'id_estado' =>'26',
            'municipio' => 'QUIRIEGO', 
        ));

        Municipio::create(array(
            'id' => '1936', 
            'id_estado' =>'26',
            'municipio' => 'RAYÓN', 
        ));

        Municipio::create(array(
            'id' => '1937', 
            'id_estado' =>'26',
            'municipio' => 'ROSARIO', 
        ));

        Municipio::create(array(
            'id' => '1938', 
            'id_estado' =>'26',
            'municipio' => 'SAHUARIPA', 
        ));

        Municipio::create(array(
            'id' => '1939', 
            'id_estado' =>'26',
            'municipio' => 'SAN FELIPE DE JESÚS', 
        ));

        Municipio::create(array(
            'id' => '1940', 
            'id_estado' =>'26',
            'municipio' => 'SAN IGNACIO RÍO MUERTO', 
        ));

        Municipio::create(array(
            'id' => '1941', 
            'id_estado' =>'26',
            'municipio' => 'SAN JAVIER', 
        ));

        Municipio::create(array(
            'id' => '1942', 
            'id_estado' =>'26',
            'municipio' => 'SAN LUIS RÍO COLORADO', 
        ));

        Municipio::create(array(
            'id' => '1943', 
            'id_estado' =>'26',
            'municipio' => 'SAN MIGUEL DE HORCASITAS', 
        ));

        Municipio::create(array(
            'id' => '1944', 
            'id_estado' =>'26',
            'municipio' => 'SAN PEDRO DE LA CUEVA', 
        ));

        Municipio::create(array(
            'id' => '1945', 
            'id_estado' =>'26',
            'municipio' => 'SANTA ANA', 
        ));

        Municipio::create(array(
            'id' => '1946', 
            'id_estado' =>'26',
            'municipio' => 'SANTA CRUZ', 
        ));

        Municipio::create(array(
            'id' => '1947', 
            'id_estado' =>'26',
            'municipio' => 'SÁRIC', 
        ));

        Municipio::create(array(
            'id' => '1948', 
            'id_estado' =>'26',
            'municipio' => 'SOYOPA', 
        ));

        Municipio::create(array(
            'id' => '1949', 
            'id_estado' =>'26',
            'municipio' => 'SUAQUI GRANDE', 
        ));

        Municipio::create(array(
            'id' => '1950', 
            'id_estado' =>'26',
            'municipio' => 'TEPACHE', 
        ));

        Municipio::create(array(
            'id' => '1951', 
            'id_estado' =>'26',
            'municipio' => 'TRINCHERAS', 
        ));

        Municipio::create(array(
            'id' => '1952', 
            'id_estado' =>'26',
            'municipio' => 'TUBUTAMA', 
        ));

        Municipio::create(array(
            'id' => '1953', 
            'id_estado' =>'26',
            'municipio' => 'URES', 
        ));

        Municipio::create(array(
            'id' => '1954', 
            'id_estado' =>'26',
            'municipio' => 'VILLA HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '1955', 
            'id_estado' =>'26',
            'municipio' => 'VILLA PESQUEIRA', 
        ));

        Municipio::create(array(
            'id' => '1956', 
            'id_estado' =>'26',
            'municipio' => 'YÉCORA', 
        ));

        Municipio::create(array(
            'id' => '1957', 
            'id_estado' =>'27',
            'municipio' => 'BALANCÁN', 
        ));

        Municipio::create(array(
            'id' => '1958', 
            'id_estado' =>'27',
            'municipio' => 'CÁRDENAS', 
        ));

        Municipio::create(array(
            'id' => '1959', 
            'id_estado' =>'27',
            'municipio' => 'CENTLA', 
        ));

        Municipio::create(array(
            'id' => '1960', 
            'id_estado' =>'27',
            'municipio' => 'CENTRO', 
        ));

        Municipio::create(array(
            'id' => '1961', 
            'id_estado' =>'27',
            'municipio' => 'COMALCALCO', 
        ));

        Municipio::create(array(
            'id' => '1962', 
            'id_estado' =>'27',
            'municipio' => 'CUNDUACÁN', 
        ));

        Municipio::create(array(
            'id' => '1963', 
            'id_estado' =>'27',
            'municipio' => 'EMILIANO ZAPATA', 
        ));

        Municipio::create(array(
            'id' => '1964', 
            'id_estado' =>'27',
            'municipio' => 'HUIMANGUILLO', 
        ));

        Municipio::create(array(
            'id' => '1965', 
            'id_estado' =>'27',
            'municipio' => 'JALAPA', 
        ));

        Municipio::create(array(
            'id' => '1966', 
            'id_estado' =>'27',
            'municipio' => 'JALPA DE MÉNDEZ', 
        ));

        Municipio::create(array(
            'id' => '1967', 
            'id_estado' =>'27',
            'municipio' => 'JONUTA', 
        ));

        Municipio::create(array(
            'id' => '1968', 
            'id_estado' =>'27',
            'municipio' => 'MACUSPANA', 
        ));

        Municipio::create(array(
            'id' => '1969', 
            'id_estado' =>'27',
            'municipio' => 'NACAJUCA', 
        ));

        Municipio::create(array(
            'id' => '1970', 
            'id_estado' =>'27',
            'municipio' => 'PARAÍSO', 
        ));

        Municipio::create(array(
            'id' => '1971', 
            'id_estado' =>'27',
            'municipio' => 'TACOTALPA', 
        ));

        Municipio::create(array(
            'id' => '1972', 
            'id_estado' =>'27',
            'municipio' => 'TEAPA', 
        ));

        Municipio::create(array(
            'id' => '1973', 
            'id_estado' =>'27',
            'municipio' => 'TENOSIQUE', 
        ));

        Municipio::create(array(
            'id' => '1974', 
            'id_estado' =>'28',
            'municipio' => 'ABASOLO', 
        ));

        Municipio::create(array(
            'id' => '1975', 
            'id_estado' =>'28',
            'municipio' => 'ALDAMA', 
        ));

        Municipio::create(array(
            'id' => '1976', 
            'id_estado' =>'28',
            'municipio' => 'ALTAMIRA', 
        ));

        Municipio::create(array(
            'id' => '1977', 
            'id_estado' =>'28',
            'municipio' => 'ANTIGUO MORELOS', 
        ));

        Municipio::create(array(
            'id' => '1978', 
            'id_estado' =>'28',
            'municipio' => 'BURGOS', 
        ));

        Municipio::create(array(
            'id' => '1979', 
            'id_estado' =>'28',
            'municipio' => 'BUSTAMANTE', 
        ));

        Municipio::create(array(
            'id' => '1980', 
            'id_estado' =>'28',
            'municipio' => 'CAMARGO', 
        ));

        Municipio::create(array(
            'id' => '1981', 
            'id_estado' =>'28',
            'municipio' => 'CASAS', 
        ));

        Municipio::create(array(
            'id' => '1982', 
            'id_estado' =>'28',
            'municipio' => 'CIUDAD MADERO', 
        ));

        Municipio::create(array(
            'id' => '1983', 
            'id_estado' =>'28',
            'municipio' => 'CRUILLAS', 
        ));

        Municipio::create(array(
            'id' => '1984', 
            'id_estado' =>'28',
            'municipio' => 'EL MANTE', 
        ));

        Municipio::create(array(
            'id' => '1985', 
            'id_estado' =>'28',
            'municipio' => 'G?ÉMEZ', 
        ));

        Municipio::create(array(
            'id' => '1986', 
            'id_estado' =>'28',
            'municipio' => 'GÓMEZ FARÍAS', 
        ));

        Municipio::create(array(
            'id' => '1987', 
            'id_estado' =>'28',
            'municipio' => 'GONZÁLEZ', 
        ));

        Municipio::create(array(
            'id' => '1988', 
            'id_estado' =>'28',
            'municipio' => 'GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '1989', 
            'id_estado' =>'28',
            'municipio' => 'GUSTAVO DÍAZ ORDAZ', 
        ));

        Municipio::create(array(
            'id' => '1990', 
            'id_estado' =>'28',
            'municipio' => 'HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '1991', 
            'id_estado' =>'28',
            'municipio' => 'JAUMAVE', 
        ));

        Municipio::create(array(
            'id' => '1992', 
            'id_estado' =>'28',
            'municipio' => 'JIMÉNEZ', 
        ));

        Municipio::create(array(
            'id' => '1993', 
            'id_estado' =>'28',
            'municipio' => 'LLERA', 
        ));

        Municipio::create(array(
            'id' => '1994', 
            'id_estado' =>'28',
            'municipio' => 'MAINERO', 
        ));

        Municipio::create(array(
            'id' => '1995', 
            'id_estado' =>'28',
            'municipio' => 'MATAMOROS', 
        ));

        Municipio::create(array(
            'id' => '1996', 
            'id_estado' =>'28',
            'municipio' => 'MÉNDEZ', 
        ));

        Municipio::create(array(
            'id' => '1997', 
            'id_estado' =>'28',
            'municipio' => 'MIER', 
        ));

        Municipio::create(array(
            'id' => '1998', 
            'id_estado' =>'28',
            'municipio' => 'MIGUEL ALEMÁN', 
        ));

        Municipio::create(array(
            'id' => '1999', 
            'id_estado' =>'28',
            'municipio' => 'MIQUIHUANA', 
        ));

        Municipio::create(array(
            'id' => '2000', 
            'id_estado' =>'28',
            'municipio' => 'NUEVO LAREDO', 
        ));

        Municipio::create(array(
            'id' => '2001', 
            'id_estado' =>'28',
            'municipio' => 'NUEVO MORELOS', 
        ));

        Municipio::create(array(
            'id' => '2002', 
            'id_estado' =>'28',
            'municipio' => 'OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '2003', 
            'id_estado' =>'28',
            'municipio' => 'PADILLA', 
        ));

        Municipio::create(array(
            'id' => '2004', 
            'id_estado' =>'28',
            'municipio' => 'PALMILLAS', 
        ));

        Municipio::create(array(
            'id' => '2005', 
            'id_estado' =>'28',
            'municipio' => 'REYNOSA', 
        ));

        Municipio::create(array(
            'id' => '2006', 
            'id_estado' =>'28',
            'municipio' => 'RÍO BRAVO', 
        ));

        Municipio::create(array(
            'id' => '2007', 
            'id_estado' =>'28',
            'municipio' => 'SAN CARLOS', 
        ));

        Municipio::create(array(
            'id' => '2008', 
            'id_estado' =>'28',
            'municipio' => 'SAN FERNANDO', 
        ));

        Municipio::create(array(
            'id' => '2009', 
            'id_estado' =>'28',
            'municipio' => 'SAN NICOLÁS', 
        ));

        Municipio::create(array(
            'id' => '2010', 
            'id_estado' =>'28',
            'municipio' => 'SOTO LA MARINA', 
        ));

        Municipio::create(array(
            'id' => '2011', 
            'id_estado' =>'28',
            'municipio' => 'TAMPICO', 
        ));

        Municipio::create(array(
            'id' => '2012', 
            'id_estado' =>'28',
            'municipio' => 'TULA', 
        ));

        Municipio::create(array(
            'id' => '2013', 
            'id_estado' =>'28',
            'municipio' => 'VALLE HERMOSO', 
        ));

        Municipio::create(array(
            'id' => '2014', 
            'id_estado' =>'28',
            'municipio' => 'VICTORIA', 
        ));

        Municipio::create(array(
            'id' => '2015', 
            'id_estado' =>'28',
            'municipio' => 'VILLAGRÁN', 
        ));

        Municipio::create(array(
            'id' => '2016', 
            'id_estado' =>'28',
            'municipio' => 'XICOTÉNCATL', 
        ));

        Municipio::create(array(
            'id' => '2017', 
            'id_estado' =>'29',
            'municipio' => 'ACUAMANALA DE MIGUEL HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '2018', 
            'id_estado' =>'29',
            'municipio' => 'ALTZAYANCA', 
        ));

        Municipio::create(array(
            'id' => '2019', 
            'id_estado' =>'29',
            'municipio' => 'AMAXAC DE GUERRERO', 
        ));

        Municipio::create(array(
            'id' => '2020', 
            'id_estado' =>'29',
            'municipio' => 'APETATITLÁN DE ANTONIO CARVAJAL', 
        ));

        Municipio::create(array(
            'id' => '2021', 
            'id_estado' =>'29',
            'municipio' => 'APIZACO', 
        ));

        Municipio::create(array(
            'id' => '2022', 
            'id_estado' =>'29',
            'municipio' => 'ATLANGATEPEC', 
        ));

        Municipio::create(array(
            'id' => '2023', 
            'id_estado' =>'29',
            'municipio' => 'BENITO JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '2024', 
            'id_estado' =>'29',
            'municipio' => 'CALPULALPAN', 
        ));

        Municipio::create(array(
            'id' => '2025', 
            'id_estado' =>'29',
            'municipio' => 'CHIAUTEMPAN', 
        ));

        Municipio::create(array(
            'id' => '2026', 
            'id_estado' =>'29',
            'municipio' => 'CONTLA DE JUAN CUAMATZI', 
        ));

        Municipio::create(array(
            'id' => '2027', 
            'id_estado' =>'29',
            'municipio' => 'CUAPIAXTLA', 
        ));

        Municipio::create(array(
            'id' => '2028', 
            'id_estado' =>'29',
            'municipio' => 'CUAXOMULCO', 
        ));

        Municipio::create(array(
            'id' => '2029', 
            'id_estado' =>'29',
            'municipio' => 'EL CARMEN TEQUEXQUITLA', 
        ));

        Municipio::create(array(
            'id' => '2030', 
            'id_estado' =>'29',
            'municipio' => 'EMILIANO ZAPATA', 
        ));

        Municipio::create(array(
            'id' => '2031', 
            'id_estado' =>'29',
            'municipio' => 'ESPAÑITA', 
        ));

        Municipio::create(array(
            'id' => '2032', 
            'id_estado' =>'29',
            'municipio' => 'HUAMANTLA', 
        ));

        Municipio::create(array(
            'id' => '2033', 
            'id_estado' =>'29',
            'municipio' => 'HUEYOTLIPAN', 
        ));

        Municipio::create(array(
            'id' => '2034', 
            'id_estado' =>'29',
            'municipio' => 'IXTACUIXTLA DE MARIANO MATAMOROS', 
        ));

        Municipio::create(array(
            'id' => '2035', 
            'id_estado' =>'29',
            'municipio' => 'IXTENCO', 
        ));

        Municipio::create(array(
            'id' => '2036', 
            'id_estado' =>'29',
            'municipio' => 'LA MAGDALENA TLALTELULCO', 
        ));

        Municipio::create(array(
            'id' => '2037', 
            'id_estado' =>'29',
            'municipio' => 'LÁZARO CÁRDENAS', 
        ));

        Municipio::create(array(
            'id' => '2038', 
            'id_estado' =>'29',
            'municipio' => 'MAZATECOCHCO DE JOSÉ MARÍA MORELOS', 
        ));

        Municipio::create(array(
            'id' => '2039', 
            'id_estado' =>'29',
            'municipio' => 'MUÑOZ DE DOMINGO ARENAS', 
        ));

        Municipio::create(array(
            'id' => '2040', 
            'id_estado' =>'29',
            'municipio' => 'NANACAMILPA DE MARIANO ARISTA', 
        ));

        Municipio::create(array(
            'id' => '2041', 
            'id_estado' =>'29',
            'municipio' => 'NATÍVITAS', 
        ));

        Municipio::create(array(
            'id' => '2042', 
            'id_estado' =>'29',
            'municipio' => 'PANOTLA', 
        ));

        Municipio::create(array(
            'id' => '2043', 
            'id_estado' =>'29',
            'municipio' => 'PAPALOTLA DE XICOHTÉNCATL', 
        ));

        Municipio::create(array(
            'id' => '2044', 
            'id_estado' =>'29',
            'municipio' => 'SAN DAMIÁN TEXOLOC', 
        ));

        Municipio::create(array(
            'id' => '2045', 
            'id_estado' =>'29',
            'municipio' => 'SAN FRANCISCO TETLANOHCAN', 
        ));

        Municipio::create(array(
            'id' => '2046', 
            'id_estado' =>'29',
            'municipio' => 'SAN JERÓNIMO ZACUALPAN', 
        ));

        Municipio::create(array(
            'id' => '2047', 
            'id_estado' =>'29',
            'municipio' => 'SAN JOSÉ TEACALCO', 
        ));

        Municipio::create(array(
            'id' => '2048', 
            'id_estado' =>'29',
            'municipio' => 'SAN JUAN HUACTZINCO', 
        ));

        Municipio::create(array(
            'id' => '2049', 
            'id_estado' =>'29',
            'municipio' => 'SAN LORENZO AXOCOMANITLA', 
        ));

        Municipio::create(array(
            'id' => '2050', 
            'id_estado' =>'29',
            'municipio' => 'SAN LUCAS TECOPILCO', 
        ));

        Municipio::create(array(
            'id' => '2051', 
            'id_estado' =>'29',
            'municipio' => 'SAN PABLO DEL MONTE', 
        ));

        Municipio::create(array(
            'id' => '2052', 
            'id_estado' =>'29',
            'municipio' => 'SANCTÓRUM DE LÁZARO CÁRDENAS', 
        ));

        Municipio::create(array(
            'id' => '2053', 
            'id_estado' =>'29',
            'municipio' => 'SANTA ANA NOPALUCAN', 
        ));

        Municipio::create(array(
            'id' => '2054', 
            'id_estado' =>'29',
            'municipio' => 'SANTA APOLONIA TEACALCO', 
        ));

        Municipio::create(array(
            'id' => '2055', 
            'id_estado' =>'29',
            'municipio' => 'SANTA CATARINA AYOMETLA', 
        ));

        Municipio::create(array(
            'id' => '2056', 
            'id_estado' =>'29',
            'municipio' => 'SANTA CRUZ QUILEHTLA', 
        ));

        Municipio::create(array(
            'id' => '2057', 
            'id_estado' =>'29',
            'municipio' => 'SANTA CRUZ TLAXCALA', 
        ));

        Municipio::create(array(
            'id' => '2058', 
            'id_estado' =>'29',
            'municipio' => 'SANTA ISABEL XILOXOXTLA', 
        ));

        Municipio::create(array(
            'id' => '2059', 
            'id_estado' =>'29',
            'municipio' => 'TENANCINGO', 
        ));

        Municipio::create(array(
            'id' => '2060', 
            'id_estado' =>'29',
            'municipio' => 'TEOLOCHOLCO', 
        ));

        Municipio::create(array(
            'id' => '2061', 
            'id_estado' =>'29',
            'municipio' => 'TEPETITLA DE LARDIZÁBAL', 
        ));

        Municipio::create(array(
            'id' => '2062', 
            'id_estado' =>'29',
            'municipio' => 'TEPEYANCO', 
        ));

        Municipio::create(array(
            'id' => '2063', 
            'id_estado' =>'29',
            'municipio' => 'TERRENATE', 
        ));

        Municipio::create(array(
            'id' => '2064', 
            'id_estado' =>'29',
            'municipio' => 'TETLA DE LA SOLIDARIDAD', 
        ));

        Municipio::create(array(
            'id' => '2065', 
            'id_estado' =>'29',
            'municipio' => 'TETLATLAHUCA', 
        ));

        Municipio::create(array(
            'id' => '2066', 
            'id_estado' =>'29',
            'municipio' => 'TLAXCALA', 
        ));

        Municipio::create(array(
            'id' => '2067', 
            'id_estado' =>'29',
            'municipio' => 'TLAXCO', 
        ));

        Municipio::create(array(
            'id' => '2068', 
            'id_estado' =>'29',
            'municipio' => 'TOCATLÁN', 
        ));

        Municipio::create(array(
            'id' => '2069', 
            'id_estado' =>'29',
            'municipio' => 'TOTOLAC', 
        ));

        Municipio::create(array(
            'id' => '2070', 
            'id_estado' =>'29',
            'municipio' => 'TZOMPANTEPEC', 
        ));

        Municipio::create(array(
            'id' => '2071', 
            'id_estado' =>'29',
            'municipio' => 'XALOZTOC', 
        ));

        Municipio::create(array(
            'id' => '2072', 
            'id_estado' =>'29',
            'municipio' => 'XALTOCAN', 
        ));

        Municipio::create(array(
            'id' => '2073', 
            'id_estado' =>'29',
            'municipio' => 'XICOHTZINCO', 
        ));

        Municipio::create(array(
            'id' => '2074', 
            'id_estado' =>'29',
            'municipio' => 'YAUHQUEMECAN', 
        ));

        Municipio::create(array(
            'id' => '2075', 
            'id_estado' =>'29',
            'municipio' => 'ZACATELCO', 
        ));

        Municipio::create(array(
            'id' => '2076', 
            'id_estado' =>'29',
            'municipio' => 'ZITLALTEPEC DE TRINIDAD SÁNCHEZ SA', 
        ));

        Municipio::create(array(
            'id' => '2077', 
            'id_estado' =>'30',
            'municipio' => 'ACAJETE', 
        ));

        Municipio::create(array(
            'id' => '2078', 
            'id_estado' =>'30',
            'municipio' => 'ACATLÁN', 
        ));

        Municipio::create(array(
            'id' => '2079', 
            'id_estado' =>'30',
            'municipio' => 'ACAYUCAN', 
        ));

        Municipio::create(array(
            'id' => '2080', 
            'id_estado' =>'30',
            'municipio' => 'ACTOPAN', 
        ));

        Municipio::create(array(
            'id' => '2081', 
            'id_estado' =>'30',
            'municipio' => 'ACULA', 
        ));

        Municipio::create(array(
            'id' => '2082', 
            'id_estado' =>'30',
            'municipio' => 'ACULTZINGO', 
        ));

        Municipio::create(array(
            'id' => '2083', 
            'id_estado' =>'30',
            'municipio' => 'AGUA DULCE', 
        ));

        Municipio::create(array(
            'id' => '2084', 
            'id_estado' =>'30',
            'municipio' => 'ALPATLÁHUAC', 
        ));

        Municipio::create(array(
            'id' => '2085', 
            'id_estado' =>'30',
            'municipio' => 'ALTO LUCERO DE GUTIÉRREZ BARRIOS', 
        ));

        Municipio::create(array(
            'id' => '2086', 
            'id_estado' =>'30',
            'municipio' => 'ALTOTONGA', 
        ));

        Municipio::create(array(
            'id' => '2087', 
            'id_estado' =>'30',
            'municipio' => 'ALVARADO', 
        ));

        Municipio::create(array(
            'id' => '2088', 
            'id_estado' =>'30',
            'municipio' => 'AMATITLÁN', 
        ));

        Municipio::create(array(
            'id' => '2089', 
            'id_estado' =>'30',
            'municipio' => 'AMATLÁN DE LOS REYES', 
        ));

        Municipio::create(array(
            'id' => '2090', 
            'id_estado' =>'30',
            'municipio' => 'ANGEL R. CABADA', 
        ));

        Municipio::create(array(
            'id' => '2091', 
            'id_estado' =>'30',
            'municipio' => 'APAZAPAN', 
        ));

        Municipio::create(array(
            'id' => '2092', 
            'id_estado' =>'30',
            'municipio' => 'AQUILA', 
        ));

        Municipio::create(array(
            'id' => '2093', 
            'id_estado' =>'30',
            'municipio' => 'ASTACINGA', 
        ));

        Municipio::create(array(
            'id' => '2094', 
            'id_estado' =>'30',
            'municipio' => 'ATLAHUILCO', 
        ));

        Municipio::create(array(
            'id' => '2095', 
            'id_estado' =>'30',
            'municipio' => 'ATOYAC', 
        ));

        Municipio::create(array(
            'id' => '2096', 
            'id_estado' =>'30',
            'municipio' => 'ATZACAN', 
        ));

        Municipio::create(array(
            'id' => '2097', 
            'id_estado' =>'30',
            'municipio' => 'ATZALAN', 
        ));

        Municipio::create(array(
            'id' => '2098', 
            'id_estado' =>'30',
            'municipio' => 'AYAHUALULCO', 
        ));

        Municipio::create(array(
            'id' => '2099', 
            'id_estado' =>'30',
            'municipio' => 'BANDERILLA', 
        ));

        Municipio::create(array(
            'id' => '2100', 
            'id_estado' =>'30',
            'municipio' => 'BENITO JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '2101', 
            'id_estado' =>'30',
            'municipio' => 'BOCA DEL RÍO', 
        ));

        Municipio::create(array(
            'id' => '2102', 
            'id_estado' =>'30',
            'municipio' => 'CALCAHUALCO', 
        ));

        Municipio::create(array(
            'id' => '2103', 
            'id_estado' =>'30',
            'municipio' => 'CAMARÓN DE TEJEDA', 
        ));

        Municipio::create(array(
            'id' => '2104', 
            'id_estado' =>'30',
            'municipio' => 'CAMERINO Z. MENDOZA', 
        ));

        Municipio::create(array(
            'id' => '2105', 
            'id_estado' =>'30',
            'municipio' => 'CARLOS A. CARRILLO', 
        ));

        Municipio::create(array(
            'id' => '2106', 
            'id_estado' =>'30',
            'municipio' => 'CARRILLO PUERTO', 
        ));

        Municipio::create(array(
            'id' => '2107', 
            'id_estado' =>'30',
            'municipio' => 'CASTILLO DE TEAYO', 
        ));

        Municipio::create(array(
            'id' => '2108', 
            'id_estado' =>'30',
            'municipio' => 'CATEMACO', 
        ));

        Municipio::create(array(
            'id' => '2109', 
            'id_estado' =>'30',
            'municipio' => 'CAZONES', 
        ));

        Municipio::create(array(
            'id' => '2110', 
            'id_estado' =>'30',
            'municipio' => 'CERRO AZUL', 
        ));

        Municipio::create(array(
            'id' => '2111', 
            'id_estado' =>'30',
            'municipio' => 'CHACALTIANGUIS', 
        ));

        Municipio::create(array(
            'id' => '2112', 
            'id_estado' =>'30',
            'municipio' => 'CHALMA', 
        ));

        Municipio::create(array(
            'id' => '2113', 
            'id_estado' =>'30',
            'municipio' => 'CHICONAMEL', 
        ));

        Municipio::create(array(
            'id' => '2114', 
            'id_estado' =>'30',
            'municipio' => 'CHICONQUIACO', 
        ));

        Municipio::create(array(
            'id' => '2115', 
            'id_estado' =>'30',
            'municipio' => 'CHICONTEPEC', 
        ));

        Municipio::create(array(
            'id' => '2116', 
            'id_estado' =>'30',
            'municipio' => 'CHINAMECA', 
        ));

        Municipio::create(array(
            'id' => '2117', 
            'id_estado' =>'30',
            'municipio' => 'CHINAMPA DE GOROSTIZA', 
        ));

        Municipio::create(array(
            'id' => '2118', 
            'id_estado' =>'30',
            'municipio' => 'CHOCAMÁN', 
        ));

        Municipio::create(array(
            'id' => '2119', 
            'id_estado' =>'30',
            'municipio' => 'CHONTLA', 
        ));

        Municipio::create(array(
            'id' => '2120', 
            'id_estado' =>'30',
            'municipio' => 'CHUMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '2121', 
            'id_estado' =>'30',
            'municipio' => 'CITLALTÉPETL', 
        ));

        Municipio::create(array(
            'id' => '2122', 
            'id_estado' =>'30',
            'municipio' => 'COACOATZINTLA', 
        ));

        Municipio::create(array(
            'id' => '2123', 
            'id_estado' =>'30',
            'municipio' => 'COAHUITLÁN', 
        ));

        Municipio::create(array(
            'id' => '2124', 
            'id_estado' =>'30',
            'municipio' => 'COATEPEC', 
        ));

        Municipio::create(array(
            'id' => '2125', 
            'id_estado' =>'30',
            'municipio' => 'COATZACOALCOS', 
        ));

        Municipio::create(array(
            'id' => '2126', 
            'id_estado' =>'30',
            'municipio' => 'COATZINTLA', 
        ));

        Municipio::create(array(
            'id' => '2127', 
            'id_estado' =>'30',
            'municipio' => 'COETZALA', 
        ));

        Municipio::create(array(
            'id' => '2128', 
            'id_estado' =>'30',
            'municipio' => 'COLIPA', 
        ));

        Municipio::create(array(
            'id' => '2129', 
            'id_estado' =>'30',
            'municipio' => 'COMAPA', 
        ));

        Municipio::create(array(
            'id' => '2130', 
            'id_estado' =>'30',
            'municipio' => 'CÓRDOBA', 
        ));

        Municipio::create(array(
            'id' => '2131', 
            'id_estado' =>'30',
            'municipio' => 'COSAMALOAPAN DE CARPIO', 
        ));

        Municipio::create(array(
            'id' => '2132', 
            'id_estado' =>'30',
            'municipio' => 'COSAUTLÁN DE CARVAJAL', 
        ));

        Municipio::create(array(
            'id' => '2133', 
            'id_estado' =>'30',
            'municipio' => 'COSCOMATEPEC', 
        ));

        Municipio::create(array(
            'id' => '2134', 
            'id_estado' =>'30',
            'municipio' => 'COSOLEACAQUE', 
        ));

        Municipio::create(array(
            'id' => '2135', 
            'id_estado' =>'30',
            'municipio' => 'COTAXTLA', 
        ));

        Municipio::create(array(
            'id' => '2136', 
            'id_estado' =>'30',
            'municipio' => 'COXQUIHUI', 
        ));

        Municipio::create(array(
            'id' => '2137', 
            'id_estado' =>'30',
            'municipio' => 'COYUTLA', 
        ));

        Municipio::create(array(
            'id' => '2138', 
            'id_estado' =>'30',
            'municipio' => 'CUICHAPA', 
        ));

        Municipio::create(array(
            'id' => '2139', 
            'id_estado' =>'30',
            'municipio' => 'CUITLÁHUAC', 
        ));

        Municipio::create(array(
            'id' => '2140', 
            'id_estado' =>'30',
            'municipio' => 'EL HIGO', 
        ));

        Municipio::create(array(
            'id' => '2141', 
            'id_estado' =>'30',
            'municipio' => 'EMILIANO ZAPATA', 
        ));

        Municipio::create(array(
            'id' => '2142', 
            'id_estado' =>'30',
            'municipio' => 'ESPINAL', 
        ));

        Municipio::create(array(
            'id' => '2143', 
            'id_estado' =>'30',
            'municipio' => 'FILOMENO MATA', 
        ));

        Municipio::create(array(
            'id' => '2144', 
            'id_estado' =>'30',
            'municipio' => 'FORTÍN', 
        ));

        Municipio::create(array(
            'id' => '2145', 
            'id_estado' =>'30',
            'municipio' => 'GUTIÉRREZ ZAMORA', 
        ));

        Municipio::create(array(
            'id' => '2146', 
            'id_estado' =>'30',
            'municipio' => 'HIDALGOTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '2147', 
            'id_estado' =>'30',
            'municipio' => 'HUATUSCO', 
        ));

        Municipio::create(array(
            'id' => '2148', 
            'id_estado' =>'30',
            'municipio' => 'HUAYACOCOTLA', 
        ));

        Municipio::create(array(
            'id' => '2149', 
            'id_estado' =>'30',
            'municipio' => 'HUEYAPAN DE OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '2150', 
            'id_estado' =>'30',
            'municipio' => 'HUILOAPAN', 
        ));

        Municipio::create(array(
            'id' => '2151', 
            'id_estado' =>'30',
            'municipio' => 'IGNACIO DE LA LLAVE', 
        ));

        Municipio::create(array(
            'id' => '2152', 
            'id_estado' =>'30',
            'municipio' => 'ILAMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '2153', 
            'id_estado' =>'30',
            'municipio' => 'ISLA', 
        ));

        Municipio::create(array(
            'id' => '2154', 
            'id_estado' =>'30',
            'municipio' => 'IXCATEPEC', 
        ));

        Municipio::create(array(
            'id' => '2155', 
            'id_estado' =>'30',
            'municipio' => 'IXHUACÁN DE LOS REYES', 
        ));

        Municipio::create(array(
            'id' => '2156', 
            'id_estado' =>'30',
            'municipio' => 'IXHUATLÁN DE MADERO', 
        ));

        Municipio::create(array(
            'id' => '2157', 
            'id_estado' =>'30',
            'municipio' => 'IXHUATLÁN DEL CAFÉ', 
        ));

        Municipio::create(array(
            'id' => '2158', 
            'id_estado' =>'30',
            'municipio' => 'IXHUATLÁN DEL SURESTE', 
        ));

        Municipio::create(array(
            'id' => '2159', 
            'id_estado' =>'30',
            'municipio' => 'IXHUATLANCILLO', 
        ));

        Municipio::create(array(
            'id' => '2160', 
            'id_estado' =>'30',
            'municipio' => 'IXMATLAHUACAN', 
        ));

        Municipio::create(array(
            'id' => '2161', 
            'id_estado' =>'30',
            'municipio' => 'IXTACZOQUITLÁN', 
        ));

        Municipio::create(array(
            'id' => '2162', 
            'id_estado' =>'30',
            'municipio' => 'JALACINGO', 
        ));

        Municipio::create(array(
            'id' => '2163', 
            'id_estado' =>'30',
            'municipio' => 'JALCOMULCO', 
        ));

        Municipio::create(array(
            'id' => '2164', 
            'id_estado' =>'30',
            'municipio' => 'JÁLTIPAN', 
        ));

        Municipio::create(array(
            'id' => '2165', 
            'id_estado' =>'30',
            'municipio' => 'JAMAPA', 
        ));

        Municipio::create(array(
            'id' => '2166', 
            'id_estado' =>'30',
            'municipio' => 'JESÚS CARRANZA', 
        ));

        Municipio::create(array(
            'id' => '2167', 
            'id_estado' =>'30',
            'municipio' => 'JILOTEPEC', 
        ));

        Municipio::create(array(
            'id' => '2168', 
            'id_estado' =>'30',
            'municipio' => 'JOSÉ AZUETA', 
        ));

        Municipio::create(array(
            'id' => '2169', 
            'id_estado' =>'30',
            'municipio' => 'JUAN RODRÍGUEZ CLARA', 
        ));

        Municipio::create(array(
            'id' => '2170', 
            'id_estado' =>'30',
            'municipio' => 'JUCHIQUE DE FERRER', 
        ));

        Municipio::create(array(
            'id' => '2171', 
            'id_estado' =>'30',
            'municipio' => 'LA ANTIGUA', 
        ));

        Municipio::create(array(
            'id' => '2172', 
            'id_estado' =>'30',
            'municipio' => 'LA PERLA', 
        ));

        Municipio::create(array(
            'id' => '2173', 
            'id_estado' =>'30',
            'municipio' => 'LANDERO Y COSS', 
        ));

        Municipio::create(array(
            'id' => '2174', 
            'id_estado' =>'30',
            'municipio' => 'LAS CHOAPAS', 
        ));

        Municipio::create(array(
            'id' => '2175', 
            'id_estado' =>'30',
            'municipio' => 'LAS MINAS', 
        ));

        Municipio::create(array(
            'id' => '2176', 
            'id_estado' =>'30',
            'municipio' => 'LAS VIGAS DE RAMÍREZ', 
        ));

        Municipio::create(array(
            'id' => '2177', 
            'id_estado' =>'30',
            'municipio' => 'LERDO DE TEJADA', 
        ));

        Municipio::create(array(
            'id' => '2178', 
            'id_estado' =>'30',
            'municipio' => 'LOS REYES', 
        ));

        Municipio::create(array(
            'id' => '2179', 
            'id_estado' =>'30',
            'municipio' => 'MAGDALENA', 
        ));

        Municipio::create(array(
            'id' => '2180', 
            'id_estado' =>'30',
            'municipio' => 'MALTRATA', 
        ));

        Municipio::create(array(
            'id' => '2181', 
            'id_estado' =>'30',
            'municipio' => 'MANLIO FABIO ALTAMIRANO', 
        ));

        Municipio::create(array(
            'id' => '2182', 
            'id_estado' =>'30',
            'municipio' => 'MARIANO ESCOBEDO', 
        ));

        Municipio::create(array(
            'id' => '2183', 
            'id_estado' =>'30',
            'municipio' => 'MARTÍNEZ DE LA TORRE', 
        ));

        Municipio::create(array(
            'id' => '2184', 
            'id_estado' =>'30',
            'municipio' => 'MECATLÁN', 
        ));

        Municipio::create(array(
            'id' => '2185', 
            'id_estado' =>'30',
            'municipio' => 'MECAYAPAN', 
        ));

        Municipio::create(array(
            'id' => '2186', 
            'id_estado' =>'30',
            'municipio' => 'MEDELLÍN', 
        ));

        Municipio::create(array(
            'id' => '2187', 
            'id_estado' =>'30',
            'municipio' => 'MIAHUATLÁN', 
        ));

        Municipio::create(array(
            'id' => '2188', 
            'id_estado' =>'30',
            'municipio' => 'MINATITLÁN', 
        ));

        Municipio::create(array(
            'id' => '2189', 
            'id_estado' =>'30',
            'municipio' => 'MISANTLA', 
        ));

        Municipio::create(array(
            'id' => '2190', 
            'id_estado' =>'30',
            'municipio' => 'MIXTLA DE ALTAMIRANO', 
        ));

        Municipio::create(array(
            'id' => '2191', 
            'id_estado' =>'30',
            'municipio' => 'MOLOACÁN', 
        ));

        Municipio::create(array(
            'id' => '2192', 
            'id_estado' =>'30',
            'municipio' => 'NANCHITAL DE LÁZARO CÁRDENAS DEL R', 
        ));

        Municipio::create(array(
            'id' => '2193', 
            'id_estado' =>'30',
            'municipio' => 'NAOLINCO', 
        ));

        Municipio::create(array(
            'id' => '2194', 
            'id_estado' =>'30',
            'municipio' => 'NARANJAL', 
        ));

        Municipio::create(array(
            'id' => '2195', 
            'id_estado' =>'30',
            'municipio' => 'NARANJOS AMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '2196', 
            'id_estado' =>'30',
            'municipio' => 'NAUTLA', 
        ));

        Municipio::create(array(
            'id' => '2197', 
            'id_estado' =>'30',
            'municipio' => 'NOGALES', 
        ));

        Municipio::create(array(
            'id' => '2198', 
            'id_estado' =>'30',
            'municipio' => 'OLUTA', 
        ));

        Municipio::create(array(
            'id' => '2199', 
            'id_estado' =>'30',
            'municipio' => 'OMEALCA', 
        ));

        Municipio::create(array(
            'id' => '2200', 
            'id_estado' =>'30',
            'municipio' => 'ORIZABA', 
        ));

        Municipio::create(array(
            'id' => '2201', 
            'id_estado' =>'30',
            'municipio' => 'OTATITLÁN', 
        ));

        Municipio::create(array(
            'id' => '2202', 
            'id_estado' =>'30',
            'municipio' => 'OTEAPAN', 
        ));

        Municipio::create(array(
            'id' => '2203', 
            'id_estado' =>'30',
            'municipio' => 'OZULUAMA DE MASCAREÑAS', 
        ));

        Municipio::create(array(
            'id' => '2204', 
            'id_estado' =>'30',
            'municipio' => 'PAJAPAN', 
        ));

        Municipio::create(array(
            'id' => '2205', 
            'id_estado' =>'30',
            'municipio' => 'PÁNUCO', 
        ));

        Municipio::create(array(
            'id' => '2206', 
            'id_estado' =>'30',
            'municipio' => 'PAPANTLA', 
        ));

        Municipio::create(array(
            'id' => '2207', 
            'id_estado' =>'30',
            'municipio' => 'PASO DE OVEJAS', 
        ));

        Municipio::create(array(
            'id' => '2208', 
            'id_estado' =>'30',
            'municipio' => 'PASO DEL MACHO', 
        ));

        Municipio::create(array(
            'id' => '2209', 
            'id_estado' =>'30',
            'municipio' => 'PEROTE', 
        ));

        Municipio::create(array(
            'id' => '2210', 
            'id_estado' =>'30',
            'municipio' => 'PLATÓN SÁNCHEZ', 
        ));

        Municipio::create(array(
            'id' => '2211', 
            'id_estado' =>'30',
            'municipio' => 'PLAYA VICENTE', 
        ));

        Municipio::create(array(
            'id' => '2212', 
            'id_estado' =>'30',
            'municipio' => 'POZA RICA DE HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '2213', 
            'id_estado' =>'30',
            'municipio' => 'PUEBLO VIEJO', 
        ));

        Municipio::create(array(
            'id' => '2214', 
            'id_estado' =>'30',
            'municipio' => 'PUENTE NACIONAL', 
        ));

        Municipio::create(array(
            'id' => '2215', 
            'id_estado' =>'30',
            'municipio' => 'RAFAEL DELGADO', 
        ));

        Municipio::create(array(
            'id' => '2216', 
            'id_estado' =>'30',
            'municipio' => 'RAFAEL LUCIO', 
        ));

        Municipio::create(array(
            'id' => '2217', 
            'id_estado' =>'30',
            'municipio' => 'RÍO BLANCO', 
        ));

        Municipio::create(array(
            'id' => '2218', 
            'id_estado' =>'30',
            'municipio' => 'SALTABARRANCA', 
        ));

        Municipio::create(array(
            'id' => '2219', 
            'id_estado' =>'30',
            'municipio' => 'SAN ANDRÉS TENEJAPAN', 
        ));

        Municipio::create(array(
            'id' => '2220', 
            'id_estado' =>'30',
            'municipio' => 'SAN ANDRÉS TUXTLA', 
        ));

        Municipio::create(array(
            'id' => '2221', 
            'id_estado' =>'30',
            'municipio' => 'SAN JUAN EVANGELISTA', 
        ));

        Municipio::create(array(
            'id' => '2222', 
            'id_estado' =>'30',
            'municipio' => 'SAN RAFAEL', 
        ));

        Municipio::create(array(
            'id' => '2223', 
            'id_estado' =>'30',
            'municipio' => 'SANTIAGO SOCHIAPAN', 
        ));

        Municipio::create(array(
            'id' => '2224', 
            'id_estado' =>'30',
            'municipio' => 'SANTIAGO TUXTLA', 
        ));

        Municipio::create(array(
            'id' => '2225', 
            'id_estado' =>'30',
            'municipio' => 'SAYULA DE ALEMÁN', 
        ));

        Municipio::create(array(
            'id' => '2226', 
            'id_estado' =>'30',
            'municipio' => 'SOCHIAPA', 
        ));

        Municipio::create(array(
            'id' => '2227', 
            'id_estado' =>'30',
            'municipio' => 'SOCONUSCO', 
        ));

        Municipio::create(array(
            'id' => '2228', 
            'id_estado' =>'30',
            'municipio' => 'SOLEDAD ATZOMPA', 
        ));

        Municipio::create(array(
            'id' => '2229', 
            'id_estado' =>'30',
            'municipio' => 'SOLEDAD DE DOBLADO', 
        ));

        Municipio::create(array(
            'id' => '2230', 
            'id_estado' =>'30',
            'municipio' => 'SOTEAPAN', 
        ));

        Municipio::create(array(
            'id' => '2231', 
            'id_estado' =>'30',
            'municipio' => 'TAMALÍN', 
        ));

        Municipio::create(array(
            'id' => '2232', 
            'id_estado' =>'30',
            'municipio' => 'TAMIAHUA', 
        ));

        Municipio::create(array(
            'id' => '2233', 
            'id_estado' =>'30',
            'municipio' => 'TAMPICO ALTO', 
        ));

        Municipio::create(array(
            'id' => '2234', 
            'id_estado' =>'30',
            'municipio' => 'TANCOCO', 
        ));

        Municipio::create(array(
            'id' => '2235', 
            'id_estado' =>'30',
            'municipio' => 'TANTIMA', 
        ));

        Municipio::create(array(
            'id' => '2236', 
            'id_estado' =>'30',
            'municipio' => 'TANTOYUCA', 
        ));

        Municipio::create(array(
            'id' => '2237', 
            'id_estado' =>'30',
            'municipio' => 'TATAHUICAPAN DE JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '2238', 
            'id_estado' =>'30',
            'municipio' => 'TATATILA', 
        ));

        Municipio::create(array(
            'id' => '2239', 
            'id_estado' =>'30',
            'municipio' => 'TECOLUTLA', 
        ));

        Municipio::create(array(
            'id' => '2240', 
            'id_estado' =>'30',
            'municipio' => 'TEHUIPANGO', 
        ));

        Municipio::create(array(
            'id' => '2241', 
            'id_estado' =>'30',
            'municipio' => 'TEMAPACHE', 
        ));

        Municipio::create(array(
            'id' => '2242', 
            'id_estado' =>'30',
            'municipio' => 'TEMPOAL', 
        ));

        Municipio::create(array(
            'id' => '2243', 
            'id_estado' =>'30',
            'municipio' => 'TENAMPA', 
        ));

        Municipio::create(array(
            'id' => '2244', 
            'id_estado' =>'30',
            'municipio' => 'TENOCHTITLÁN', 
        ));

        Municipio::create(array(
            'id' => '2245', 
            'id_estado' =>'30',
            'municipio' => 'TEOCELO', 
        ));

        Municipio::create(array(
            'id' => '2246', 
            'id_estado' =>'30',
            'municipio' => 'TEPATLAXCO', 
        ));

        Municipio::create(array(
            'id' => '2247', 
            'id_estado' =>'30',
            'municipio' => 'TEPETLÁN', 
        ));

        Municipio::create(array(
            'id' => '2248', 
            'id_estado' =>'30',
            'municipio' => 'TEPETZINTLA', 
        ));

        Municipio::create(array(
            'id' => '2249', 
            'id_estado' =>'30',
            'municipio' => 'TEQUILA', 
        ));

        Municipio::create(array(
            'id' => '2250', 
            'id_estado' =>'30',
            'municipio' => 'TEXCATEPEC', 
        ));

        Municipio::create(array(
            'id' => '2251', 
            'id_estado' =>'30',
            'municipio' => 'TEXHUACÁN', 
        ));

        Municipio::create(array(
            'id' => '2252', 
            'id_estado' =>'30',
            'municipio' => 'TEXISTEPEC', 
        ));

        Municipio::create(array(
            'id' => '2253', 
            'id_estado' =>'30',
            'municipio' => 'TEZONAPA', 
        ));

        Municipio::create(array(
            'id' => '2254', 
            'id_estado' =>'30',
            'municipio' => 'TIERRA BLANCA', 
        ));

        Municipio::create(array(
            'id' => '2255', 
            'id_estado' =>'30',
            'municipio' => 'TIHUATLÁN', 
        ));

        Municipio::create(array(
            'id' => '2256', 
            'id_estado' =>'30',
            'municipio' => 'TLACHICHILCO', 
        ));

        Municipio::create(array(
            'id' => '2257', 
            'id_estado' =>'30',
            'municipio' => 'TLACOJALPAN', 
        ));

        Municipio::create(array(
            'id' => '2258', 
            'id_estado' =>'30',
            'municipio' => 'TLACOLULAN', 
        ));

        Municipio::create(array(
            'id' => '2259', 
            'id_estado' =>'30',
            'municipio' => 'TLACOTALPAN', 
        ));

        Municipio::create(array(
            'id' => '2260', 
            'id_estado' =>'30',
            'municipio' => 'TLACOTEPEC DE MEJÍA', 
        ));

        Municipio::create(array(
            'id' => '2261', 
            'id_estado' =>'30',
            'municipio' => 'TLALIXCOYAN', 
        ));

        Municipio::create(array(
            'id' => '2262', 
            'id_estado' =>'30',
            'municipio' => 'TLALNELHUAYOCAN', 
        ));

        Municipio::create(array(
            'id' => '2263', 
            'id_estado' =>'30',
            'municipio' => 'TLALTETELA', 
        ));

        Municipio::create(array(
            'id' => '2264', 
            'id_estado' =>'30',
            'municipio' => 'TLAPACOYAN', 
        ));

        Municipio::create(array(
            'id' => '2265', 
            'id_estado' =>'30',
            'municipio' => 'TLAQUILPA', 
        ));

        Municipio::create(array(
            'id' => '2266', 
            'id_estado' =>'30',
            'municipio' => 'TLILAPAN', 
        ));

        Municipio::create(array(
            'id' => '2267', 
            'id_estado' =>'30',
            'municipio' => 'TOMATLÁN', 
        ));

        Municipio::create(array(
            'id' => '2268', 
            'id_estado' =>'30',
            'municipio' => 'TONAYÁN', 
        ));

        Municipio::create(array(
            'id' => '2269', 
            'id_estado' =>'30',
            'municipio' => 'TOTUTLA', 
        ));

        Municipio::create(array(
            'id' => '2270', 
            'id_estado' =>'30',
            'municipio' => 'TRES VALLES', 
        ));

        Municipio::create(array(
            'id' => '2271', 
            'id_estado' =>'30',
            'municipio' => 'TÚXPAM', 
        ));

        Municipio::create(array(
            'id' => '2272', 
            'id_estado' =>'30',
            'municipio' => 'TUXTILLA', 
        ));

        Municipio::create(array(
            'id' => '2273', 
            'id_estado' =>'30',
            'municipio' => 'URSULO GALVÁN', 
        ));

        Municipio::create(array(
            'id' => '2274', 
            'id_estado' =>'30',
            'municipio' => 'UXPANAPA', 
        ));

        Municipio::create(array(
            'id' => '2275', 
            'id_estado' =>'30',
            'municipio' => 'VEGA DE ALATORRE', 
        ));

        Municipio::create(array(
            'id' => '2276', 
            'id_estado' =>'30',
            'municipio' => 'VERACRUZ', 
        ));

        Municipio::create(array(
            'id' => '2277', 
            'id_estado' =>'30',
            'municipio' => 'VILLA ALDAMA', 
        ));

        Municipio::create(array(
            'id' => '2278', 
            'id_estado' =>'30',
            'municipio' => 'XALAPA', 
        ));

        Municipio::create(array(
            'id' => '2279', 
            'id_estado' =>'30',
            'municipio' => 'XICO', 
        ));

        Municipio::create(array(
            'id' => '2280', 
            'id_estado' =>'30',
            'municipio' => 'XOXOCOTLA', 
        ));

        Municipio::create(array(
            'id' => '2281', 
            'id_estado' =>'30',
            'municipio' => 'YANGA', 
        ));

        Municipio::create(array(
            'id' => '2282', 
            'id_estado' =>'30',
            'municipio' => 'YECUATLA', 
        ));

        Municipio::create(array(
            'id' => '2283', 
            'id_estado' =>'30',
            'municipio' => 'ZACUALPAN', 
        ));

        Municipio::create(array(
            'id' => '2284', 
            'id_estado' =>'30',
            'municipio' => 'ZARAGOZA', 
        ));

        Municipio::create(array(
            'id' => '2285', 
            'id_estado' =>'30',
            'municipio' => 'ZENTLA', 
        ));

        Municipio::create(array(
            'id' => '2286', 
            'id_estado' =>'30',
            'municipio' => 'ZONGOLICA', 
        ));

        Municipio::create(array(
            'id' => '2287', 
            'id_estado' =>'30',
            'municipio' => 'ZONTECOMATLÁN DE LÓPEZ Y FUENTES', 
        ));

        Municipio::create(array(
            'id' => '2288', 
            'id_estado' =>'30',
            'municipio' => 'ZOZOCOLCO DE HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '2289', 
            'id_estado' =>'31',
            'municipio' => 'ABALÁ', 
        ));

        Municipio::create(array(
            'id' => '2290', 
            'id_estado' =>'31',
            'municipio' => 'ACANCEH', 
        ));

        Municipio::create(array(
            'id' => '2291', 
            'id_estado' =>'31',
            'municipio' => 'AKIL', 
        ));

        Municipio::create(array(
            'id' => '2292', 
            'id_estado' =>'31',
            'municipio' => 'BACA', 
        ));

        Municipio::create(array(
            'id' => '2293', 
            'id_estado' =>'31',
            'municipio' => 'BOKOBÁ', 
        ));

        Municipio::create(array(
            'id' => '2294', 
            'id_estado' =>'31',
            'municipio' => 'BUCTZOTZ', 
        ));

        Municipio::create(array(
            'id' => '2295', 
            'id_estado' =>'31',
            'municipio' => 'CACALCHÉN', 
        ));

        Municipio::create(array(
            'id' => '2296', 
            'id_estado' =>'31',
            'municipio' => 'CALOTMUL', 
        ));

        Municipio::create(array(
            'id' => '2297', 
            'id_estado' =>'31',
            'municipio' => 'CANSAHCAB', 
        ));

        Municipio::create(array(
            'id' => '2298', 
            'id_estado' =>'31',
            'municipio' => 'CANTAMAYEC', 
        ));

        Municipio::create(array(
            'id' => '2299', 
            'id_estado' =>'31',
            'municipio' => 'CELESTÚN', 
        ));

        Municipio::create(array(
            'id' => '2300', 
            'id_estado' =>'31',
            'municipio' => 'CENOTILLO', 
        ));

        Municipio::create(array(
            'id' => '2301', 
            'id_estado' =>'31',
            'municipio' => 'CHACSINKÍN', 
        ));

        Municipio::create(array(
            'id' => '2302', 
            'id_estado' =>'31',
            'municipio' => 'CHANKOM', 
        ));

        Municipio::create(array(
            'id' => '2303', 
            'id_estado' =>'31',
            'municipio' => 'CHAPAB', 
        ));

        Municipio::create(array(
            'id' => '2304', 
            'id_estado' =>'31',
            'municipio' => 'CHEMAX', 
        ));

        Municipio::create(array(
            'id' => '2305', 
            'id_estado' =>'31',
            'municipio' => 'CHICHIMILÁ', 
        ));

        Municipio::create(array(
            'id' => '2306', 
            'id_estado' =>'31',
            'municipio' => 'CHICXULUB PUEBLO', 
        ));

        Municipio::create(array(
            'id' => '2307', 
            'id_estado' =>'31',
            'municipio' => 'CHIKINDZONOT', 
        ));

        Municipio::create(array(
            'id' => '2308', 
            'id_estado' =>'31',
            'municipio' => 'CHOCHOLÁ', 
        ));

        Municipio::create(array(
            'id' => '2309', 
            'id_estado' =>'31',
            'municipio' => 'CHUMAYEL', 
        ));

        Municipio::create(array(
            'id' => '2310', 
            'id_estado' =>'31',
            'municipio' => 'CONKAL', 
        ));

        Municipio::create(array(
            'id' => '2311', 
            'id_estado' =>'31',
            'municipio' => 'CUNCUNUL', 
        ));

        Municipio::create(array(
            'id' => '2312', 
            'id_estado' =>'31',
            'municipio' => 'CUZAMÁ', 
        ));

        Municipio::create(array(
            'id' => '2313', 
            'id_estado' =>'31',
            'municipio' => 'DZÁN', 
        ));

        Municipio::create(array(
            'id' => '2314', 
            'id_estado' =>'31',
            'municipio' => 'DZEMUL', 
        ));

        Municipio::create(array(
            'id' => '2315', 
            'id_estado' =>'31',
            'municipio' => 'DZIDZANTÚN', 
        ));

        Municipio::create(array(
            'id' => '2316', 
            'id_estado' =>'31',
            'municipio' => 'DZILAM DE BRAVO', 
        ));

        Municipio::create(array(
            'id' => '2317', 
            'id_estado' =>'31',
            'municipio' => 'DZILAM GONZÁLEZ', 
        ));

        Municipio::create(array(
            'id' => '2318', 
            'id_estado' =>'31',
            'municipio' => 'DZITÁS', 
        ));

        Municipio::create(array(
            'id' => '2319', 
            'id_estado' =>'31',
            'municipio' => 'DZONCAUICH', 
        ));

        Municipio::create(array(
            'id' => '2320', 
            'id_estado' =>'31',
            'municipio' => 'ESPITA', 
        ));

        Municipio::create(array(
            'id' => '2321', 
            'id_estado' =>'31',
            'municipio' => 'HALACHÓ', 
        ));

        Municipio::create(array(
            'id' => '2322', 
            'id_estado' =>'31',
            'municipio' => 'HOCABÁ', 
        ));

        Municipio::create(array(
            'id' => '2323', 
            'id_estado' =>'31',
            'municipio' => 'HOCTÚN', 
        ));

        Municipio::create(array(
            'id' => '2324', 
            'id_estado' =>'31',
            'municipio' => 'HOMÚN', 
        ));

        Municipio::create(array(
            'id' => '2325', 
            'id_estado' =>'31',
            'municipio' => 'HUHÍ', 
        ));

        Municipio::create(array(
            'id' => '2326', 
            'id_estado' =>'31',
            'municipio' => 'HUNUCMÁ', 
        ));

        Municipio::create(array(
            'id' => '2327', 
            'id_estado' =>'31',
            'municipio' => 'IXIL', 
        ));

        Municipio::create(array(
            'id' => '2328', 
            'id_estado' =>'31',
            'municipio' => 'IZAMAL', 
        ));

        Municipio::create(array(
            'id' => '2329', 
            'id_estado' =>'31',
            'municipio' => 'KANASÍN', 
        ));

        Municipio::create(array(
            'id' => '2330', 
            'id_estado' =>'31',
            'municipio' => 'KANTUNIL', 
        ));

        Municipio::create(array(
            'id' => '2331', 
            'id_estado' =>'31',
            'municipio' => 'KAUA', 
        ));

        Municipio::create(array(
            'id' => '2332', 
            'id_estado' =>'31',
            'municipio' => 'KINCHIL', 
        ));

        Municipio::create(array(
            'id' => '2333', 
            'id_estado' =>'31',
            'municipio' => 'KOPOMÁ', 
        ));

        Municipio::create(array(
            'id' => '2334', 
            'id_estado' =>'31',
            'municipio' => 'MAMA', 
        ));

        Municipio::create(array(
            'id' => '2335', 
            'id_estado' =>'31',
            'municipio' => 'MANÍ', 
        ));

        Municipio::create(array(
            'id' => '2336', 
            'id_estado' =>'31',
            'municipio' => 'MAXCANÚ', 
        ));

        Municipio::create(array(
            'id' => '2337', 
            'id_estado' =>'31',
            'municipio' => 'MAYAPÁN', 
        ));

        Municipio::create(array(
            'id' => '2338', 
            'id_estado' =>'31',
            'municipio' => 'MÉRIDA', 
        ));

        Municipio::create(array(
            'id' => '2339', 
            'id_estado' =>'31',
            'municipio' => 'MOCOCHÁ', 
        ));

        Municipio::create(array(
            'id' => '2340', 
            'id_estado' =>'31',
            'municipio' => 'MOTUL', 
        ));

        Municipio::create(array(
            'id' => '2341', 
            'id_estado' =>'31',
            'municipio' => 'MUNA', 
        ));

        Municipio::create(array(
            'id' => '2342', 
            'id_estado' =>'31',
            'municipio' => 'MUXUPIP', 
        ));

        Municipio::create(array(
            'id' => '2343', 
            'id_estado' =>'31',
            'municipio' => 'OPICHÉN', 
        ));

        Municipio::create(array(
            'id' => '2344', 
            'id_estado' =>'31',
            'municipio' => 'OXKUTZCAB', 
        ));

        Municipio::create(array(
            'id' => '2345', 
            'id_estado' =>'31',
            'municipio' => 'PANABÁ', 
        ));

        Municipio::create(array(
            'id' => '2346', 
            'id_estado' =>'31',
            'municipio' => 'PETO', 
        ));

        Municipio::create(array(
            'id' => '2347', 
            'id_estado' =>'31',
            'municipio' => 'PROGRESO', 
        ));

        Municipio::create(array(
            'id' => '2348', 
            'id_estado' =>'31',
            'municipio' => 'QUINTANA ROO', 
        ));

        Municipio::create(array(
            'id' => '2349', 
            'id_estado' =>'31',
            'municipio' => 'RÍO LAGARTOS', 
        ));

        Municipio::create(array(
            'id' => '2350', 
            'id_estado' =>'31',
            'municipio' => 'SACALUM', 
        ));

        Municipio::create(array(
            'id' => '2351', 
            'id_estado' =>'31',
            'municipio' => 'SAMAHIL', 
        ));

        Municipio::create(array(
            'id' => '2352', 
            'id_estado' =>'31',
            'municipio' => 'SAN FELIPE', 
        ));

        Municipio::create(array(
            'id' => '2353', 
            'id_estado' =>'31',
            'municipio' => 'SANAHCAT', 
        ));

        Municipio::create(array(
            'id' => '2354', 
            'id_estado' =>'31',
            'municipio' => 'SANTA ELENA', 
        ));

        Municipio::create(array(
            'id' => '2355', 
            'id_estado' =>'31',
            'municipio' => 'SEYÉ', 
        ));

        Municipio::create(array(
            'id' => '2356', 
            'id_estado' =>'31',
            'municipio' => 'SINANCHÉ', 
        ));

        Municipio::create(array(
            'id' => '2357', 
            'id_estado' =>'31',
            'municipio' => 'SOTUTA', 
        ));

        Municipio::create(array(
            'id' => '2358', 
            'id_estado' =>'31',
            'municipio' => 'SUCILÁ', 
        ));

        Municipio::create(array(
            'id' => '2359', 
            'id_estado' =>'31',
            'municipio' => 'SUDZAL', 
        ));

        Municipio::create(array(
            'id' => '2360', 
            'id_estado' =>'31',
            'municipio' => 'SUMA', 
        ));

        Municipio::create(array(
            'id' => '2361', 
            'id_estado' =>'31',
            'municipio' => 'TAHDZIÚ', 
        ));

        Municipio::create(array(
            'id' => '2362', 
            'id_estado' =>'31',
            'municipio' => 'TAHMEK', 
        ));

        Municipio::create(array(
            'id' => '2363', 
            'id_estado' =>'31',
            'municipio' => 'TEABO', 
        ));

        Municipio::create(array(
            'id' => '2364', 
            'id_estado' =>'31',
            'municipio' => 'TECOH', 
        ));

        Municipio::create(array(
            'id' => '2365', 
            'id_estado' =>'31',
            'municipio' => 'TEKAL DE VENEGAS', 
        ));

        Municipio::create(array(
            'id' => '2366', 
            'id_estado' =>'31',
            'municipio' => 'TEKANTÓ', 
        ));

        Municipio::create(array(
            'id' => '2367', 
            'id_estado' =>'31',
            'municipio' => 'TEKAX', 
        ));

        Municipio::create(array(
            'id' => '2368', 
            'id_estado' =>'31',
            'municipio' => 'TEKIT', 
        ));

        Municipio::create(array(
            'id' => '2369', 
            'id_estado' =>'31',
            'municipio' => 'TEKOM', 
        ));

        Municipio::create(array(
            'id' => '2370', 
            'id_estado' =>'31',
            'municipio' => 'TELCHAC PUEBLO', 
        ));

        Municipio::create(array(
            'id' => '2371', 
            'id_estado' =>'31',
            'municipio' => 'TELCHAC PUERTO', 
        ));

        Municipio::create(array(
            'id' => '2372', 
            'id_estado' =>'31',
            'municipio' => 'TEMAX', 
        ));

        Municipio::create(array(
            'id' => '2373', 
            'id_estado' =>'31',
            'municipio' => 'TEMOZÓN', 
        ));

        Municipio::create(array(
            'id' => '2374', 
            'id_estado' =>'31',
            'municipio' => 'TEPAKÁN', 
        ));

        Municipio::create(array(
            'id' => '2375', 
            'id_estado' =>'31',
            'municipio' => 'TETIZ', 
        ));

        Municipio::create(array(
            'id' => '2376', 
            'id_estado' =>'31',
            'municipio' => 'TEYA', 
        ));

        Municipio::create(array(
            'id' => '2377', 
            'id_estado' =>'31',
            'municipio' => 'TICUL', 
        ));

        Municipio::create(array(
            'id' => '2378', 
            'id_estado' =>'31',
            'municipio' => 'TIMUCUY', 
        ));

        Municipio::create(array(
            'id' => '2379', 
            'id_estado' =>'31',
            'municipio' => 'TINUM', 
        ));

        Municipio::create(array(
            'id' => '2380', 
            'id_estado' =>'31',
            'municipio' => 'TIXCACALCUPUL', 
        ));

        Municipio::create(array(
            'id' => '2381', 
            'id_estado' =>'31',
            'municipio' => 'TIXKOKOB', 
        ));

        Municipio::create(array(
            'id' => '2382', 
            'id_estado' =>'31',
            'municipio' => 'TIXMEHUAC', 
        ));

        Municipio::create(array(
            'id' => '2383', 
            'id_estado' =>'31',
            'municipio' => 'TIXPÉHUAL', 
        ));

        Municipio::create(array(
            'id' => '2384', 
            'id_estado' =>'31',
            'municipio' => 'TIZIMÍN', 
        ));

        Municipio::create(array(
            'id' => '2385', 
            'id_estado' =>'31',
            'municipio' => 'TUNKÁS', 
        ));

        Municipio::create(array(
            'id' => '2386', 
            'id_estado' =>'31',
            'municipio' => 'TZUCACAB', 
        ));

        Municipio::create(array(
            'id' => '2387', 
            'id_estado' =>'31',
            'municipio' => 'UAYMA', 
        ));

        Municipio::create(array(
            'id' => '2388', 
            'id_estado' =>'31',
            'municipio' => 'UCÚ', 
        ));

        Municipio::create(array(
            'id' => '2389', 
            'id_estado' =>'31',
            'municipio' => 'UMÁN', 
        ));

        Municipio::create(array(
            'id' => '2390', 
            'id_estado' =>'31',
            'municipio' => 'VALLADOLID', 
        ));

        Municipio::create(array(
            'id' => '2391', 
            'id_estado' =>'31',
            'municipio' => 'XOCCHEL', 
        ));

        Municipio::create(array(
            'id' => '2392', 
            'id_estado' =>'31',
            'municipio' => 'YAXCABÁ', 
        ));

        Municipio::create(array(
            'id' => '2393', 
            'id_estado' =>'31',
            'municipio' => 'YAXKUKUL', 
        ));

        Municipio::create(array(
            'id' => '2394', 
            'id_estado' =>'31',
            'municipio' => 'YOBAÍN', 
        ));

        Municipio::create(array(
            'id' => '2395', 
            'id_estado' =>'32',
            'municipio' => 'APOZOL', 
        ));

        Municipio::create(array(
            'id' => '2396', 
            'id_estado' =>'32',
            'municipio' => 'APULCO', 
        ));

        Municipio::create(array(
            'id' => '2397', 
            'id_estado' =>'32',
            'municipio' => 'ATOLINGA', 
        ));

        Municipio::create(array(
            'id' => '2398', 
            'id_estado' =>'32',
            'municipio' => 'BENITO JUÁREZ', 
        ));

        Municipio::create(array(
            'id' => '2399', 
            'id_estado' =>'32',
            'municipio' => 'CALERA', 
        ));

        Municipio::create(array(
            'id' => '2400', 
            'id_estado' =>'32',
            'municipio' => 'CAÑITAS DE FELIPE PESCADOR', 
        ));

        Municipio::create(array(
            'id' => '2401', 
            'id_estado' =>'32',
            'municipio' => 'CHALCHIHUITES', 
        ));

        Municipio::create(array(
            'id' => '2402', 
            'id_estado' =>'32',
            'municipio' => 'CONCEPCIÓN DEL ORO', 
        ));

        Municipio::create(array(
            'id' => '2403', 
            'id_estado' =>'32',
            'municipio' => 'CUAUHTÉMOC', 
        ));

        Municipio::create(array(
            'id' => '2404', 
            'id_estado' =>'32',
            'municipio' => 'EL PLATEADO DE JOAQUÍN AMARO', 
        ));

        Municipio::create(array(
            'id' => '2405', 
            'id_estado' =>'32',
            'municipio' => 'EL SALVADOR', 
        ));

        Municipio::create(array(
            'id' => '2406', 
            'id_estado' =>'32',
            'municipio' => 'FRESNILLO', 
        ));

        Municipio::create(array(
            'id' => '2407', 
            'id_estado' =>'32',
            'municipio' => 'GENARO CODINA', 
        ));

        Municipio::create(array(
            'id' => '2408', 
            'id_estado' =>'32',
            'municipio' => 'GENERAL ENRIQUE ESTRADA', 
        ));

        Municipio::create(array(
            'id' => '2409', 
            'id_estado' =>'32',
            'municipio' => 'GENERAL FRANCISCO R. MURGUÍA', 
        ));

        Municipio::create(array(
            'id' => '2410', 
            'id_estado' =>'32',
            'municipio' => 'GENERAL PÁNFILO NATERA', 
        ));

        Municipio::create(array(
            'id' => '2411', 
            'id_estado' =>'32',
            'municipio' => 'GUADALUPE', 
        ));

        Municipio::create(array(
            'id' => '2412', 
            'id_estado' =>'32',
            'municipio' => 'HUANUSCO', 
        ));

        Municipio::create(array(
            'id' => '2413', 
            'id_estado' =>'32',
            'municipio' => 'JALPA', 
        ));

        Municipio::create(array(
            'id' => '2414', 
            'id_estado' =>'32',
            'municipio' => 'JEREZ', 
        ));

        Municipio::create(array(
            'id' => '2415', 
            'id_estado' =>'32',
            'municipio' => 'JIMÉNEZ DEL TEUL', 
        ));

        Municipio::create(array(
            'id' => '2416', 
            'id_estado' =>'32',
            'municipio' => 'JUAN ALDAMA', 
        ));

        Municipio::create(array(
            'id' => '2417', 
            'id_estado' =>'32',
            'municipio' => 'JUCHIPILA', 
        ));

        Municipio::create(array(
            'id' => '2418', 
            'id_estado' =>'32',
            'municipio' => 'LORETO', 
        ));

        Municipio::create(array(
            'id' => '2419', 
            'id_estado' =>'32',
            'municipio' => 'LUIS MOYA', 
        ));

        Municipio::create(array(
            'id' => '2420', 
            'id_estado' =>'32',
            'municipio' => 'MAZAPIL', 
        ));

        Municipio::create(array(
            'id' => '2421', 
            'id_estado' =>'32',
            'municipio' => 'MELCHOR OCAMPO', 
        ));

        Municipio::create(array(
            'id' => '2422', 
            'id_estado' =>'32',
            'municipio' => 'MEZQUITAL DEL ORO', 
        ));

        Municipio::create(array(
            'id' => '2423', 
            'id_estado' =>'32',
            'municipio' => 'MIGUEL AUZA', 
        ));

        Municipio::create(array(
            'id' => '2424', 
            'id_estado' =>'32',
            'municipio' => 'MOMAX', 
        ));

        Municipio::create(array(
            'id' => '2425', 
            'id_estado' =>'32',
            'municipio' => 'MONTE ESCOBEDO', 
        ));

        Municipio::create(array(
            'id' => '2426', 
            'id_estado' =>'32',
            'municipio' => 'MORELOS', 
        ));

        Municipio::create(array(
            'id' => '2427', 
            'id_estado' =>'32',
            'municipio' => 'MOYAHUA DE ESTRADA', 
        ));

        Municipio::create(array(
            'id' => '2428', 
            'id_estado' =>'32',
            'municipio' => 'NOCHISTLÁN DE MEJÍA', 
        ));

        Municipio::create(array(
            'id' => '2429', 
            'id_estado' =>'32',
            'municipio' => 'NORIA DE ?NGELES', 
        ));

        Municipio::create(array(
            'id' => '2430', 
            'id_estado' =>'32',
            'municipio' => 'OJOCALIENTE', 
        ));

        Municipio::create(array(
            'id' => '2431', 
            'id_estado' =>'32',
            'municipio' => 'PÁNUCO', 
        ));

        Municipio::create(array(
            'id' => '2432', 
            'id_estado' =>'32',
            'municipio' => 'PINOS', 
        ));

        Municipio::create(array(
            'id' => '2433', 
            'id_estado' =>'32',
            'municipio' => 'RÍO GRANDE', 
        ));

        Municipio::create(array(
            'id' => '2434', 
            'id_estado' =>'32',
            'municipio' => 'SAIN ALTO', 
        ));

        Municipio::create(array(
            'id' => '2435', 
            'id_estado' =>'32',
            'municipio' => 'SANTA MARÍA DE LA PAZ', 
        ));

        Municipio::create(array(
            'id' => '2436', 
            'id_estado' =>'32',
            'municipio' => 'SOMBRERETE', 
        ));

        Municipio::create(array(
            'id' => '2437', 
            'id_estado' =>'32',
            'municipio' => 'SUSTICACÁN', 
        ));

        Municipio::create(array(
            'id' => '2438', 
            'id_estado' =>'32',
            'municipio' => 'TABASCO', 
        ));

        Municipio::create(array(
            'id' => '2439', 
            'id_estado' =>'32',
            'municipio' => 'TEPECHITLÁN', 
        ));

        Municipio::create(array(
            'id' => '2440', 
            'id_estado' =>'32',
            'municipio' => 'TEPETONGO', 
        ));

        Municipio::create(array(
            'id' => '2441', 
            'id_estado' =>'32',
            'municipio' => 'TEUL DE GONZÁLEZ ORTEGA', 
        ));

        Municipio::create(array(
            'id' => '2442', 
            'id_estado' =>'32',
            'municipio' => 'TLALTENANGO DE SÁNCHEZ ROMÁN', 
        ));

        Municipio::create(array(
            'id' => '2443', 
            'id_estado' =>'32',
            'municipio' => 'TRANCOSO', 
        ));

        Municipio::create(array(
            'id' => '2444', 
            'id_estado' =>'32',
            'municipio' => 'TRINIDAD GARCÍA DE LA CADENA', 
        ));

        Municipio::create(array(
            'id' => '2445', 
            'id_estado' =>'32',
            'municipio' => 'VALPARAÍSO', 
        ));

        Municipio::create(array(
            'id' => '2446', 
            'id_estado' =>'32',
            'municipio' => 'VETAGRANDE', 
        ));

        Municipio::create(array(
            'id' => '2447', 
            'id_estado' =>'32',
            'municipio' => 'VILLA DE COS', 
        ));

        Municipio::create(array(
            'id' => '2448', 
            'id_estado' =>'32',
            'municipio' => 'VILLA GARCÍA', 
        ));

        Municipio::create(array(
            'id' => '2449', 
            'id_estado' =>'32',
            'municipio' => 'VILLA GONZÁLEZ ORTEGA', 
        ));

        Municipio::create(array(
            'id' => '2450', 
            'id_estado' =>'32',
            'municipio' => 'VILLA HIDALGO', 
        ));

        Municipio::create(array(
            'id' => '2451', 
            'id_estado' =>'32',
            'municipio' => 'VILLANUEVA', 
        ));

        Municipio::create(array(
            'id' => '2452', 
            'id_estado' =>'32',
            'municipio' => 'ZACATECAS', 
        ));

        

    }

}