<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
        	'username' => 'administrador', 
        	'email' => 'administrador@dominio.com',
        	'privilegios' => 2, 
        	'password' => Hash::make('prueba'),
        	));

        User::create(array(
            'username' => 'Edgar Azael', 
            'email' => 'azael_dg@hotmail.com',
            'privilegios' => 1, 
            'password' => Hash::make('Edgar'),
            ));

        User::create(array(
            'username' => 'Felipe', 
            'email' => 'solislf@gmail.com',
            'privilegios' => 1, 
            'password' => Hash::make('Felipe'),
            ));
    }

}