<?php

class UnidadesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('unidades')->delete();

        Unidad::create(array(
            'id' => '1', 
            'unidad' => 'ENES LEÓN', 
        	));


        Unidad::create(array(
            'id' => '2', 
            'unidad' => 'ENES SAN MIGUEL', 
            ));
    }

}