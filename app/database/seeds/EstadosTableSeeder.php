<?php

class EstadosTableSeeder extends Seeder {

    public function run()
    {
        DB::table('estados')->delete();

        Estado::create(array(
            'id' => '1', 
            'Estado' => 'AGUASCALIENTES', 
        ));

        Estado::create(array(
            'id' => '2', 
            'Estado' => 'BAJA CALIFORNIA', 
        ));

        Estado::create(array(
            'id' => '3', 
            'Estado' => 'BAJA CALIFORNIA SUR', 
        ));

        Estado::create(array(
            'id' => '4', 
            'Estado' => 'CAMPECHE', 
        ));

        Estado::create(array(
            'id' => '5', 
            'Estado' => 'COAHUILA', 
        ));

        Estado::create(array(
            'id' => '6', 
            'Estado' => 'COLIMA', 
        ));

        Estado::create(array(
            'id' => '7', 
            'Estado' => 'CHIAPAS', 
        ));

        Estado::create(array(
            'id' => '8', 
            'Estado' => 'CHIHUAHUA', 
        ));

        Estado::create(array(
            'id' => '9', 
            'Estado' => 'DISTRITO FEDERAL', 
        ));

        Estado::create(array(
            'id' => '10', 
            'Estado' => 'DURANGO', 
        ));

        Estado::create(array(
            'id' => '11', 
            'Estado' => 'GUANAJUATO', 
        ));

        Estado::create(array(
            'id' => '12', 
            'Estado' => 'GUERRERO', 
        ));

        Estado::create(array(
            'id' => '13', 
            'Estado' => 'HIDALGO', 
        ));

        Estado::create(array(
            'id' => '14', 
            'Estado' => 'JALISCO', 
        ));

        Estado::create(array(
            'id' => '15', 
            'Estado' => 'MEXICO', 
        ));

        Estado::create(array(
            'id' => '16', 
            'Estado' => 'MICHOACAN', 
        ));

        Estado::create(array(
            'id' => '17', 
            'Estado' => 'MORELOS', 
        ));

        Estado::create(array(
            'id' => '18', 
            'Estado' => 'NAYARIT', 
        ));

        Estado::create(array(
            'id' => '19', 
            'Estado' => 'NUEVO LEON', 
        ));

        Estado::create(array(
            'id' => '20', 
            'Estado' => 'OAXACA', 
        ));

        Estado::create(array(
            'id' => '21', 
            'Estado' => 'PUEBLA', 
        ));

        Estado::create(array(
            'id' => '22', 
            'Estado' => 'QUERETARO', 
        ));

        Estado::create(array(
            'id' => '23', 
            'Estado' => 'QUINTANA ROO', 
        ));

        Estado::create(array(
            'id' => '24', 
            'Estado' => 'SAN LUIS POTOSI', 
        ));

        Estado::create(array(
            'id' => '25', 
            'Estado' => 'SINALOA', 
        ));

        Estado::create(array(
            'id' => '26', 
            'Estado' => 'SONORA', 
        ));

        Estado::create(array(
            'id' => '27', 
            'Estado' => 'TABASCO', 
        ));

        Estado::create(array(
            'id' => '28', 
            'Estado' => 'TAMAULIPAS', 
        ));

        Estado::create(array(
            'id' => '29', 
            'Estado' => 'TLAXCALA', 
        ));

        Estado::create(array(
            'id' => '30', 
            'Estado' => 'VERACRUZ', 
        ));

        Estado::create(array(
            'id' => '31', 
            'Estado' => 'YUCATAN', 
        ));

        Estado::create(array(
            'id' => '32', 
            'Estado' => 'ZACATECAS', 
        ));




    }

}