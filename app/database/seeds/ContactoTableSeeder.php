<?php

class ContactoTableSeeder extends Seeder {

    public function run()
    {
        DB::table('contacto')->delete();

        Contacto::create(array(
            'id_alumno' => '1', 
            'calle' => 'SAN RAFAEL NORTE', 
            'num_int' => '2', 
            'num_ext' => '', 
            'colonia' => 'SAN ANTONIO', 
            'ciudad' => '323', 
            'estado' => '11',
            'nacionalidad' => 48,  
            'cp' => '37750', 
            'rfc' => 'DEGE256ALGO', 
            'telefono' => '1526645', 
            'movil' => '0444151097774', 
            'email' => 'azael_dg@hotmail.com',
        	));


        Contacto::create(array(
            'id_alumno' => '2', 
            'calle' => 'CALZADA DE LA LUZ', 
            'num_int' => '23', 
            'num_ext' => '', 
            'colonia' => 'CALZADA DE LA PRESA', 
            'ciudad' => '323', 
            'nacionalidad' => 48,  
            'estado' => '11', 
            'cp' => '37700', 
            'rfc' => 'SOEL123ALGO', 
            'telefono' => '1523456', 
            'movil' => '0444151234567', 
            'email' => 'solislf@gmail.com',
            ));
    }

}