<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		$this->call('NacionalidadesTableSeeder');
		$this->call('EstadosTableSeeder');
		$this->call('MunicipiosTableSeeder');
		$this->call('TiposTableSeeder');
		$this->call('ModalidadesTableSeeder');
		$this->call('AdmisionesTableSeeder');
		$this->call('UnidadesTableSeeder');
		$this->call('AlumnosTableSeeder');
		$this->call('ContactoTableSeeder');
		$this->call('ProgramasTableSeeder');	
		$this->call('AspirantesTableSeeder');
		$this->call('AcademicosTableSeeder');	
		$this->call('ContactoAcademicoTableSeeder');	

	}

}
