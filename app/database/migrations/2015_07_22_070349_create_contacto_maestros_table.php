<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactoMaestrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos la tabla alumnos
		Schema::table('contacto_academicos',function($table)
		{
		$table->create();

		//Clave primaria
		$table->increments('id_contacto');

		//Campos que contendran las claves foraneas
      	$table->integer('id_maestro')->unsigned()->index();
      	$table->integer('ciudad')->unsigned()->index();
		$table->integer('estado')->unsigned()->index();
		$table->integer('nacionalidad')->unsigned()->index();

      	//Establecemos las relaciones
		$table->foreign('id_maestro')
      		->references('id')->on('academicos')
      		->onDelete('cascade')
      		->onUpdate('cascade');

		$table->foreign('nacionalidad')
      		->references('id')->on('nacionalidades')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	$table->foreign('ciudad')
      		->references('id')->on('municipios')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	$table->foreign('estado')
      		->references('id')->on('estados')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	//Definimos campos de contacto con el alumno
		$table->string('calle',64)->nullable();
		$table->string('num_int',4)->nullable();
		$table->string('num_ext',4)->nullable();
		$table->string('colonia',64)->nullable();
		$table->string('cp',5)->nullable();
		$table->string('rfc',16)->nullable();
		$table->string('telefono',15)->nullable();
		$table->string('movil',64)->nullable();
		$table->string('email',32)->nullable();
		$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacto_academicos');
	}

}
