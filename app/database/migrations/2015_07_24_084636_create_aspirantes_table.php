<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAspirantesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('aspirantes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_alumno')->unsigned()->index();
			$table->integer('id_programa')->unsigned()->index();

			//Establecemos las relaciones
			$table->foreign('id_alumno')
      		->references('id')->on('alumnos')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      		$table->foreign('id_programa')
      		->references('id')->on('programas')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      		$table->string('estatus')->default("PENDIENTE");
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('aspirantes');
	}

}
