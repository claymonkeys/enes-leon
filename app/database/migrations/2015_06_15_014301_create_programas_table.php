<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('programas', function(Blueprint $table)
		{
			$table->increments('id');

			//Campos que contendran las claves foraneas
	      	$table->integer('tipo')->unsigned()->index();
	        $table->integer('modalidad')->unsigned()->index();
	        $table->integer('admision')->unsigned()->index();


	      	//Establecemos las relaciones
			$table->foreign('tipo')
	      		->references('id')->on('tipos')
	      		->onDelete('cascade')
	      		->onUpdate('cascade');

	      	$table->foreign('modalidad')
	      		->references('id')->on('modalidad')
	      		->onDelete('cascade')
	      		->onUpdate('cascade');

			$table->foreign('admision')
	      		->references('id')->on('admision')
	      		->onDelete('cascade')
	      		->onUpdate('cascade');


			$table->string('programa');
			$table->date('fecha_inicio');
			$table->date('fecha_fin');
			$table->string('estatus')->default("Inactivo");
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('programas');
	}

}
