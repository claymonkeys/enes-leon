<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos la tabla municipios
		Schema::table('municipios',function($table)
		{
		$table->create();

		//Clave primaria
		$table->increments('id');

		//Campos que contendran las claves foraneas
      	$table->integer('id_estado')->unsigned()->index();

      	//Establecemos las relaciones
		$table->foreign('id_estado')
      		->references('id')->on('estados')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	//Definimos campos de los municipios
		$table->string('municipio')->nullable();
		$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//eliminamos tabla municipios
		Schema::drop('municipios');
	}

}
