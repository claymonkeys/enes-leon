<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuposTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos la tabla grupos
		Schema::table('grupos',function($table)
		{
		$table->create();

		//Clave primaria
		$table->increments('id');

		$table->integer('academico')->unsigned()->index();
		$table->integer('programa')->unsigned()->index();

		//Establecemos las relaciones
		$table->foreign('academico')
      		->references('id')->on('academicos')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	$table->foreign('programa')
      		->references('id')->on('programas')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	//Campos de la tabla
		$table->string('grupo',128);
		$table->integer('cupo');
		$table->string('dias',128)->nullable();
		$table->string('horarios',128)->nullable();
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grupos');
	}

}
