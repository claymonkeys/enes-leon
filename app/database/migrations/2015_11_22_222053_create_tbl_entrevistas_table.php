<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblEntrevistasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos la tabla entrevistas
		Schema::table('entrevistas',function($table)
		{
		$table->create();

		//Clave primaria
		$table->increments('id');

		$table->integer('aspirante')->unsigned()->index();

		//Establecemos las relaciones
		$table->foreign('aspirante')
      		->references('id')->on('aspirantes')
      		->onDelete('cascade')
      		->onUpdate('cascade');

		$table->string('estatus')->default("PENDIENTE");
		$table->date('fecha');
		$table->time('hora');
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entrevistas');
	}

}
