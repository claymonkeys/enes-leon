<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos la tabla alumnos
		Schema::table('alumnos',function($table)
		{
		$table->create();

		//Clave primaria
		$table->increments('id');

		$table->integer('unidad_academica')->unsigned()->index();

		//Establecemos las relaciones
		$table->foreign('unidad_academica')
      		->references('id')->on('unidades')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	//Campos de la tabla
		$table->string('nombre',64);
		$table->string('a_paterno',64)->nullable();
		$table->string('a_materno',64)->nullable();
		$table->string('sexo',10)->nullable();
		$table->date('fecha_nac',64)->nullable();
		$table->string('nivel_estudios',12)->nullable();
		$table->string('perfil_profesional',64)->nullable();
		$table->string('termino_estudios',8)->nullable();
		$table->string('institucion_estudios',128)->nullable();
		$table->boolean("facturacion")->nullable()->default(0);
		$table->string('estatus')->default("Activo");
		$table->string('como_se_entero');
		$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alumnos');
	}

}
