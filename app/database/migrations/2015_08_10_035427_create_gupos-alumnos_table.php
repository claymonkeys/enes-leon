<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuposAlumnosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos la tabla grupos-alumnos
		Schema::table('grupos-alumnos',function($table)
		{
		$table->create();

		//Clave primaria
		$table->increments('id');

		$table->integer('grupo')->unsigned()->index();
		$table->integer('alumno')->unsigned()->index();


		//Establecemos las relaciones
		$table->foreign('grupo')
      		->references('id')->on('grupos')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	//Establecemos las relaciones
		$table->foreign('alumno')
      		->references('id')->on('alumnos')
      		->onDelete('cascade')
      		->onUpdate('cascade');


		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grupos-alumnos');
	}

}
