<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos la tabla tipos de programas
		Schema::table('tipos',function($table)
		{
		$table->create();

		//Clave primaria
		$table->increments('id');

      	//Definimos campos de los tipos de programas
		$table->string('tipo_programa')->nullable();
		$table->timestamps();

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//eliminamos tabla tipos de programas
		Schema::drop('tipos');
	}

}
