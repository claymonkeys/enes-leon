<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Creamos la tabla academicos
		Schema::table('academicos',function($table)
		{
		$table->create();

		//Clave primaria
		$table->increments('id');

		$table->integer('unidad_academica')->unsigned()->index();

		//Establecemos las relaciones
		$table->foreign('unidad_academica')
      		->references('id')->on('unidades')
      		->onDelete('cascade')
      		->onUpdate('cascade');

      	//Campos de la tabla
		$table->string('nombre',64);
		$table->string('a_paterno',64)->nullable();
		$table->string('a_materno',64)->nullable();
		$table->string('sexo',10)->nullable();
		$table->date('fecha_nac',64)->nullable();
		$table->string('titulo_profesional',64)->nullable();
		$table->string('estatus')->default("Activo");
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('academicos');
	}

}
