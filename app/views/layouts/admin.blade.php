@extends('layouts.master')

@section('menu')
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          {{-- <a class="navbar-brand" href="#">Project name</a> --}}
        </div>
        @if (Auth::check())
          @if (Auth::user()->privilegios == 2)
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="{{URL::to('/')}}"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Programa Académico <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li>{{HTML::link('programas/create','Programas')}}</li>
                    <li>{{HTML::link('maestros/create','Académicos')}}</li>
                    <li>{{HTML::link('grupos/create','Grupos')}}</li>
                    <li><a href="#">Estatus</a></li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Aspirantes <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li>{{HTML::link('aspirantes/','Aspirantes')}}</li>
                    {{-- <li class="dropdown-submenu">{{HTML::link('entrevistas/','Entrevistas')}}</li> --}}
                    <li class="divider"></li>
                    <li class="dropdown-header">Entrevistas</li>
                    <li class="dropdown-submenu">{{HTML::link('entrevistas/pendientes','Pendientes')}}</li>
                    <li class="dropdown-submenu">{{HTML::link('entrevistas/programadas','Programadas')}}</li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Referencias de Pago <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Agregar</a></li>
                    <li><a href="#">Estatus</a></li>
                  </ul>
                </li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->username }} <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ URL::to('logout') }}"><i class="fa fa-sign-out"></i> Cerrar Sesión</a></li>
                  </ul>
                </li>
              </ul>
            </div><!--/.nav-collapse -->
          @endif
        @endif
      </div>
    </nav>
@stop

@section('content')
      @yield('content')
@stop


@section('javascript')
  {{ HTML::script('assets/js/admin.min.js') }}
  {{-- {{ HTML::script('assets/js/admin.min.js.map') }} --}}
@stop