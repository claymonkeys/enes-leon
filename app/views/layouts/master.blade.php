<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.ico') }}}">
    <title>Escuela Nacional de Estudios Superiores - Unidad León</title>
    {{ HTML::style('assets/css/app.min.css') }}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    @yield('menu')

    <!-- Begin page content -->
    <div class="container">

      <div class="row">
        <div class="col-sm-3">
          @if (Auth::check())
            @if (Auth::user()->privilegios == 1)
            <a href="{{URL::to('/alumnos')}}">{{ HTML::image('assets/img/logo_unam_azul.png') }}</a>
            @elseif (Auth::user()->privilegios == 2)
            <a href="{{URL::to('/')}}">{{ HTML::image('assets/img/logo_unam_azul.png') }}</a>
            @endif
          @else
            <a href="{{URL::to('/login')}}">{{ HTML::image('assets/img/logo_unam_azul.png') }}</a>
          @endif

        </div>
        <div class="col-sm-3 col-sm-offset-6 text-right">
          {{ HTML::image('assets/img/logo_UNAM.jpg') }}
        </div>
      </div>

      @if(Session::has('message'))
      <div class="row">
        <div class="col-sm-12">
        <?php $class = Session::get('class') ?>
        <div class="alert alert-{{ $class }} alert-dismissable fade in" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          @if($class=='success')
            <i class="fa fa-check-circle"></i>
          @endif
          @if($class=='info')
            <i class="fa fa-check-circle"></i>
          @endif
          @if($class=='warning')
              <i class="fa fa-exclamation-circle"></i>
          @endif
          @if($class=='danger')
            <i class="fa fa-exclamation-circle"></i>
          @endif
          {{ Session::get('message') }}
        </div>
        </div>
      </div>
      @endif
      
      @yield('content')
    </div><!-- /.container -->

    <footer class="footer">
      <div class="container">
        <p class="text-muted text-center">Hecho en México, Universidad Nacional Autónoma de México (UNAM), todos los derechos reservados 2011 - {{ date('Y') }}. Esta página puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. De otra forma, 
        requiere permiso previo por escrito de la institución.</p>
      </div>
    </footer>

    <!-- Absolute URL -->
    <script> var baseURL = "<?php echo URL::to('/'); ?>"; </script>
    <!-- Placed at the end of the document so the pages load faster -->
    {{-- {{ HTML::script('assets/js/app.min.js') }} --}}
    @yield('javascript')
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    {{ HTML::script('assets/js/ie10-viewport-bug-workaround.js') }}
  </body>
</html>
