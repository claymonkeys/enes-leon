@extends('layouts.master')

@section('menu')
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          {{-- <a class="navbar-brand" href="#">Project name</a> --}}
        </div>
        
        @if (Auth::check())
          @if (Auth::user()->privilegios == 1)
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="{{URL::to('alumnos')}}"><i class="fa fa-home"></i> Inicio</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Ajustes <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li>{{HTML::link('alumnos/resumen','Resumen')}}</li>
                    <li class="divider"></li>
                    <li >{{HTML::link('alumnos/datos','Datos Generales')}}</li>
                    <li class="divider"></li>
                    <li>{{HTML::link('alumnos/facturacion','Datos Facturacion')}}</li>
                    <li class="divider"></li>
                    <li>{{HTML::link('alumnos/pagos','Pagos')}}</li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Inscripcion <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li>{{HTML::link('alumnos/cursos','Cursos')}}</li>
                    <li class="divider"></li>
                    <li>{{HTML::link('alumnos/eventos','Eventos')}}</li>
                  </ul>
                </li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->username }} <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ URL::to('logout') }}"><i class="fa fa-sign-out"></i> Cerrar Sesión</a></li>
                  </ul>
                </li>
              </ul>
            </div><!--/.nav-collapse -->
          @endif
        @endif
      </div>
    </nav>
@stop


@section('content')
      @yield('content')
@stop

@section('javascript')
  {{ HTML::script('assets/js/alumnos.min.js') }}
  {{-- {{ HTML::script('assets/js/alumnos.min.js.map') }} --}}
@stop