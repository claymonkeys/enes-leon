@extends('layouts.master')

@section('menu')
    <nav class="navbar navbar-default navbar-fixed-top">
    	<div class="container">
      	</div>
    </nav>
@stop

@section('content')
      @yield('content')
@stop


@section('javascript')
  {{ HTML::script('assets/js/alumnos.min.js') }}
  {{-- {{ HTML::script('assets/js/alumnos.min.js.map') }} --}}
@stop
