@extends('layouts.admin')

@section('content')

  	<div class="row">
		<div class="col-sm-12" id="center-div">
			<h3>Programas Académico</h3>
		</div>
	</div>

	<hr>

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  		<div class="panel panel-default">
    		<div class="panel-heading" role="tab" id="headingOne">
      			<h4 class="panel-title">
        			<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          				Editar Programa Académico
        			</a>
      			</h4>
    		</div>

		    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		    	<div class="panel-body">

					{{ Form::open(array('url'=>'programas/update/'.$programa->id, 'method'=>'POST', 'role'=>'form')) }}			  	
					<div class="row">
						<div class="form-group col-sm-2">
							{{ Form::label('tipo_programa', 'Tipo de programa') }}
							{{ Form::select('tipo_programa', array(''=>'Tipo de programa', 'curso'=>'Curso', 'taller'=>'Taller', 'diplomado'=>'Diplomado', 'mesa clinica'=>'Mesa Clínica', 'seminario'=>'Seminario'), $programa->tipo, array('class'=>'form-control')) }}
						</div>
						<div class="form-group col-sm-2">
							{{ Form::label('modalidad', 'Modalidad') }}
							{{ Form::select('modalidad', array('0'=>'', '1'=>'PRESENCIAL', '2'=>'EN LINEA'), $programa->modalidad, array('class'=>'form-control')) }}
						</div>
						<div class="form-group col-sm-2">
							{{ Form::label('admision', 'Admisión') }}
							{{ Form::select('admision', array('0'=>'','1'=>'ENTREVISTA','2'=>'DIRECTO'), $programa->admision, array('class'=>'form-control')) }}
						</div>
						<div class="form-group col-sm-3">
							{{ Form::label('fecha_inicio', '*Fecha de inicio') }}
							<div class="input-group">
								{{ Form::text('fecha_inicio', $programa->fecha_inicio, array('class'=>'form-control date')) }}
								<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
							</div>
						</div>
						<div class="form-group col-sm-3">
							{{ Form::label('fecha_fin', '*Fecha de fin') }}
							<div class="input-group">
								{{ Form::text('fecha_fin', $programa->fecha_fin, array('class'=>'form-control date')) }}
								<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
							</div>
						</div>
					</div> 

					<div class="row">
						<div class="form-group col-sm-12">
							{{ Form::label('programa', 'Nombre del programa') }}
							{{ Form::text('programa',$programa->programa, array('class'=>'form-control')) }}
						</div>
					</div>  
								
					<div class="row">
						<div class="form-group col-sm-12" id="center">
							{{ Form::submit('Actualizar Programa', array('class'=>'btn btn-primary','onclick'=>'this.disabled=true;this.form.submit();')) }}
							{{HTML::link('programas/create','Cancelar',array('class'=>'btn btn-primary'))}}

						</div>
							{{ Form::close() }}
					</div>

				</div>
		    </div>
    	</div>
	</div>
	
	<hr>
	
	<div class='container-fluid'>
		<div class="row">
			@if (!empty($programas))
				<table class="table table-striped">
					<th>Programa Academico</th>
					<th>Modalidad</th>
					<th>Admisión</th>
					<th>Inicio</th>
					<th>Fin</th>
					<th>Estatus</th>
					<th>Acciones</th>
							
					@foreach($programas as $lista)
						<tr>
							<td>{{ $lista->programa }}</td>
							<td>{{ $lista->modalidad }}</td>
							<td>{{ $lista->admision }}</td>
							<td>{{ $lista->fecha_inicio }}</td>
							<td>{{ $lista->fecha_fin }}</td>
							<td>{{ $lista->estatus }}</td>
							<td>
								{{HTML::link('programas/edit/'.$lista->id,'Editar',array('class'=>'btn btn-primary btn-xs'))}}
								{{HTML::link('programas/destroy/'.$lista->id,'Eliminar',array('class'=>'btn btn-danger btn-xs'))}}
								{{HTML::link('programas/activar/'.$lista->id,'Habilitar',array('class'=>'btn btn-success btn-xs'))}}
								{{HTML::link('programas/desactivar/'.$lista->id,'Deshabilitar',array('btn btn-default btn-xs'))}}
							</td>
						</tr>
					@endforeach
				</table>
			@else
				<h4>No Existen Programas<h4>
			@endif
		</div>
	</div>

@stop