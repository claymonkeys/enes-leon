@extends('layouts.admin')

@section('content')

	<div class="row">
		<div class="col-sm-12" id="center-div">
			<h3>Programas Académicos</h3>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<table id="table-programas" class="table table-striped table-condensed table-hover text-nowrap" cellspacing="0" width="100%">
				<thead>
					<th>Programa Académico</th>
					<th>Tipo</th>
					<th>Modalidad</th>
					<th>Admisión</th>
					<th>Inicio</th>
					<th>Fin</th>
					<th>Estatus</th>
					<th class="all">Acciones</th>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>

<div class="modal fade" id="modal-programas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
    	{{ Form::open(array('url'=>'', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'form-programas', 'role'=>'form')) }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      	<div class="form-group">
      		{{ Form::label('programa', 'Nombre del programa', array('class'=>'col-sm-4 control-label')) }}
      			<div class="col-sm-8">
							{{ Form::text('programa', '', array('class'=>'form-control')) }}
						</div>
      	</div>
        <div class="form-group">
					{{ Form::label('tipo_programa', 'Tipo de programa', array('class'=>'col-sm-4 control-label')) }}
						<div class="col-sm-8">
							{{ Form::select('tipo_programa', $tipos, '', array('class'=>'form-control')) }}
						</div>
				</div>
				<div class="form-group">
					{{ Form::label('modalidad', 'Modalidad', array('class'=>'col-sm-4 control-label')) }}
						<div class="col-sm-8">
							{{ Form::select('modalidad', $modalidad, '', array('class'=>'form-control')) }}
						</div>
				</div>
				<div class="form-group">
					{{ Form::label('admision', 'Admisión', array('class'=>'col-sm-4 control-label')) }}
						<div class="col-sm-8">
							{{ Form::select('admision', $admision, '', array('class'=>'form-control')) }}
						</div>
				</div>
				<div class="form-group">
					{{ Form::label('fecha_inicio', '*Fecha de inicio', array('class'=>'col-sm-4 control-label')) }}
						<div class="col-sm-8">
							<div class="input-group date">
								{{ Form::text('fecha_inicio', '', array('class'=>'form-control date')) }}
								<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
							</div>
						</div>
				</div>
				<div class="form-group">
					{{ Form::label('fecha_fin', '*Fecha de fin', array('class'=>'col-sm-4 control-label')) }}
						<div class="col-sm-8">
							<div class="input-group date">
								{{ Form::text('fecha_fin', '', array('class'=>'form-control date')) }}
								<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
							</div>
						</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-8">
						<div class="checkbox">
							<label>
								{{ Form::checkbox('estatus', 'activo') }} Activo
							</label>
						</div>
					</div>
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar cambios</button>
      </div>
      {{ Form::close() }}
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
			  	
@stop