@extends('layouts.alumnos')

@section('content')

<div class="row">
	<div class="col-sm-12">

		<div class="page-header">
    	<h1>Datos Generales</h1>
    </div>

    					<div class="table-responsive">
						<table class="table table-striped " id="center">
		    				<tr>
			    				<th>Nombre</th>
			    				<th>Apellido Paterno</th>
			    				<th>Apellido Materno</th>
			    				<th>Sexo</th>
			    				<th>Fecha de Nacimiento</th>
		    				</tr>
		    				<tr>
		    					<td>{{$alumno->nombre}}</td>
		    					<td>{{$alumno->a_paterno}}</td>
		    					<td>{{$alumno->a_materno}}</td>
		    					<td>{{$alumno->sexo}}</td>
		    					<td>{{$alumno->fecha_nac}}</td>
		    				</tr>
		    				<tr>
			    				<th>Correo</th>
			    				<th>Nacionalidad</th>
			    				<th>Estado</th>
			    				<th>Municipio</th>
			    				<th>Calle</th>
		    				</tr>
		    				<tr>
		    					<td>{{$contacto->email}}</td>
		    					<td>{{$nacionalidad->nacionalidad}}</td>
		    					<td>{{$estado->estado}}</td>
		    					<td>{{$municipio->municipio}}</td>
		    					<td>{{$contacto->calle}}</td>
		    				</tr>
		    				<tr>
			    				<th>Numero Ext</th>
			    				<th>Numero Int</th>
			    				<th>Colonia</th>
			    				<th>Codigo Postal</th>
			    				<th>Telefono</th>
		    				</tr>
		    				<tr>
		    					<td>{{$contacto->num_ext}}</td>
		    					<td>{{$contacto->num_int}}</td>
		    					<td>{{$contacto->colonia}}</td>
		    					<td>{{$contacto->cp}}</td>
		    					<td>{{$contacto->telefono}}</td>
		    				</tr>
		    				<tr>
			    				<th>Movil</th>
			    				<th>RFC</th>
			    				<th>Unidad Academica</th>
			    				<th>Perfil Academico</th>
			    				<th>Institucion</th>
		    				</tr>
		    				<tr>
		    					<td>{{$contacto->movil}}</td>
		    					<td>{{$contacto->rfc}}</td>
		    					<td>{{$unidad->unidad}}</td>
		    					<td>{{$alumno->perfil_profesional}}</td>
		    					<td>{{$alumno->institucion_estudios}}</td>
		    				</tr>
		    				<tr>
			    				<th>Nivel de Estudios</th>
			    				<th>Ultimo Grado Cursado</th>
			    				<th>Como se Entero</th>
			    				<th>Titulo Profesional</th>
			    				<th>INE</th>
		    				</tr>
		    				<tr>
		    					<td>{{$alumno->nivel_estudios}}</td>
		    					<td>{{$alumno->termino_estudios}}</td>
		    					<td>{{$alumno->como_se_entero}}</td>
		    					<td>{{HTML::link('/assets/documentacion/'.$alumno->id.'/Titulo Profesional.pdf','Ver Titulo Profesional',array('target'=>'_blank','class'=>'Link'))}}</td>
		    					<td>{{HTML::link('/assets/documentacion/'.$alumno->id.'/INE.pdf','Ver INE',array('target'=>'_blank','class'=>'Link'))}}</td>
		    				</tr>
		    				<tr>
			    				<th>Cedula Profesional</th>
			    				<th>Curriculum Vitae</th>
			    				<th></th>
			    				<th></th>
			    				<th></th>
		    				</tr>
		    				<tr>
		    					<td>{{HTML::link('/assets/documentacion/'.$alumno->id.'/Cedula.pdf','Ver Cedula',array('target'=>'_blank','class'=>'Link'))}}</td>
		    					<td>{{HTML::link('/assets/documentacion/'.$alumno->id.'/CV.pdf','Ver CV',array('target'=>'_blank','class'=>'Link'))}}</td>
		    					<td></td>
		    					<td></td>
		    					<td></td>
		    				</tr>
		    			</table>
					</div>
	

		<div class="row">
			<div class="col-sm-12" id="center">
				<a class="btn btn-primary" role="button" href="{{URL::to('alumnos/editdatos')}}"><i class="fa fa-pencil"></i> Editar información</a>
			</div>
		</div>

	</div><!--/.col-sm-12-->
	</div><!-- /.row -->

@stop