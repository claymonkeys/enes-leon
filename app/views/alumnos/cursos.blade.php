@extends('layouts.alumnos')

@section('content')

    <div class="page-header">
    	<h1>Inscribirse a Curso</h1>
    </div>

    <div class="row">
    	<div class="col-md-6">
    		<br><br>
			<p> Por medio de este panel podras inscribirte a los programas disponibles según el tipo de programa que se elija. 
			solo se mostraran los programas que estan actualmente disponibles.</p>

			<p>
				Despues de elegir el programa te responderemos lo antes posible para informarte si fuiste aceptado al programa elegido
				asi como hacerte saber los horarios y dias en los que se llevara a cabo dicha capacitacion.
			</p>

    	</div>
    	<div class="col-md-6">
			
				{{ Form::open(array('url'=>'alumnos/registro', 'method'=>'POST', 'id'=>'registro', 'role'=>'form')) }}
			  	<div class="row">
			  		<div class="form-group col-sm-12">
			  			{{ Form::label('tipo_programa', '*Tipo de programa') }}
			  			@if (!empty($tipos))
				    			{{ Form::select('tipo_programa', $tipos, 'Unidad Academica', array('class'=>'form-control')) }}
			  			@else
				    		{{ Form::select('tipo_programa', array(''=>'Tipo de programa'), '', array('class'=>'form-control')) }}
			  			@endif
			  		</div>
			  	</div>

			  	<div class="row">
			  		<div class="form-group col-sm-12">
			  			{{ Form::label('programa_academico', '*Programa académico') }}
			  			{{ Form::select('programa_academico', array(), '', array('class'=>'form-control')) }}
			  		</div>
			  		<div class="form-group col-sm-12">
			  			{{ Form::label('como_se_entero', '*¿Cómo se enteró?') }}
				    	{{ Form::select('como_se_entero', array(''=>'Seleccione una opción', 'MEDIOS ESCRITOS'=>'MEDIOS ESCRITOS', 'RECOMENDACIÓN'=>'RECOMENDACIÓN', 'FACEBOOK'=>'FACEBOOK', 'TWITTER'=>'TWITTER', 'CORREO ELECTRÓNICO'=>'CORREO ELECTRÓNICO', 'PAGINA DE INTERNET'=>'PAGINA DE INTERNET'), '', array('class'=>'form-control')) }}
			  		</div>
			  	</div>

			  <div class="row">
				<div class="form-group col-sm-12" id="center">
					{{ Form::submit('Enviar Solicitud', array('class'=>'btn btn-primary','onclick'=>'this.disabled=true;this.form.submit();')) }}
			  	</div>
			  </div>
			  {{ Form::close() }}

    	</div>
    </div>

@stop