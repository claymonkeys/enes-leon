@extends('layouts.alumnos')

@section('content')

    <div class="page-header">
      <h1>Datos de Facturacion</h1>
    </div>

    <div class="table-responsive">
						<table class="table table-striped " id="center">
							<tr>
			    				<th>Calle</th>
			    				<th>Numero Externo</th>
			    				<th>Numero Interno</th>
			    				<th>Colonia</th>
		    				</tr>
		    				<tr>
		    					<td>{{$contacto->calle}}</td>
		    					<td>{{$contacto->num_ext}}</td>
		    					<td>{{$contacto->num_int}}</td>
		    					<td>{{$contacto->colonia}}</td>
		    				</tr>

		    				<tr>
			    				<th>Codigo Postal</th>
			    				<th>Telefono</th>
			    				<th>Celular</th>
			    				<th>RFC</th>
		    				</tr>
		    				<tr>
		    					<td>{{$contacto->cp}}</td>
		    					<td>{{$contacto->telefono}}</td>
		    					<td>{{$contacto->movil}}</td>
		    					<td>{{$contacto->rfc}}</td>
		    				</tr>

		    			</table>
					</div>

	<div class="row">
		<div class="form-group col-sm-12" id="center">
			<a class="btn btn-primary" role="button" href="{{URL::to('alumnos/editfacturacion')}}"><i class="fa fa-pencil"></i> Editar información</a>
		</div>
	</div>


@stop