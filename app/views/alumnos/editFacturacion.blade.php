@extends('layouts.alumnos')

@section('content')

    <div class="page-header">
      <h1>Modificar Datos de Facturacion</h1>
    </div>
	
	{{ Form::open(array('url'=>'alumnos/updatefacturacion', 'method'=>'POST', 'role'=>'form')) }}
	<div class="row">
		<div class="form-group col-sm-4">
			{{ Form::label('calle', '*Calle') }}
			{{ Form::text('calle', $contacto->calle , array('class'=>'form-control')) }}
		</div>
		<div class="form-group col-sm-2">
			{{ Form::label('num_ext', 'Número externo') }}
			{{ Form::text('num_ext', $contacto->num_ext, array('class'=>'form-control')) }}
		</div>
		<div class="form-group col-sm-2">
		 	{{ Form::label('num_int', 'Número interno') }}
			{{ Form::text('num_int', $contacto->num_int, array('class'=>'form-control')) }}
		</div>
		<div class="form-group col-sm-4">
		  	{{ Form::label('colonia', '*Colonia') }}
			{{ Form::text('colonia', $contacto->colonia, array('class'=>'form-control')) }}
		</div>
	</div>  

	<div class="row">
		<div class="form-group col-sm-3">
			{{ Form::label('cp', 'Código postal') }}
			{{ Form::text('cp', $contacto->cp, array('class'=>'form-control')) }}
		</div>
		<div class="form-group col-sm-3">
			{{ Form::label('telefono', '*Teléfono (incluír lada)') }}
			{{ Form::text('telefono', $contacto->telefono, array('class'=>'form-control','maxlength'=>'10')) }}
		</div>
		<div class="form-group col-sm-3">
			{{ Form::label('movil', 'Celular') }}
			{{ Form::text('movil', $contacto->movil, array('class'=>'form-control')) }}
		</div>
		<div class="form-group col-sm-3">
			{{ Form::label('rfc', 'RFC') }}
			{{ Form::text('rfc', $contacto->rfc, array('class'=>'form-control')) }}
		</div>
	</div>

	<div class="row">
		{{ Form::label('facturacion', '¿Requiere Facturación?') }}
		@if ($alumno->facturacion == 1)
        	{{ Form::checkbox('facturacion','0',$alumno->facturacion) }}
    	@else
         	{{ Form::checkbox('facturacion','1',$alumno->facturacion) }}
   		@endif
	</div>

	<div class="row">
		<div class="form-group col-sm-12" id="center">
			{{ HTML::link('alumnos/facturacion','Cancelar', array('class'=>'btn btn-default')) }}
			{{ Form::submit('Actualizar informacion', array('class'=>'btn btn-primary','onclick'=>'thi.disabled=true;this.form.submit();')) }}

		</div>
	</div>
	{{ Form::close() }}


@stop