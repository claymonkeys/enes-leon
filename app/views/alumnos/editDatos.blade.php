@extends('layouts.alumnos')

@section('content')

    <div class="page-header">
    	<h1>Modificar Datos Generales</h1>
    </div>
	
	{{ Form::open(array('url'=>'alumnos/updatedatos','files'=>'true', 'method'=>'POST', 'role'=>'form')) }}
    	<div class="row">
			<div class="form-group col-sm-4">
				{{ Form::label('nombre', '*Nombre') }}
				{{ Form::text('nombre', $alumno->nombre, array('class'=>'form-control')) }}
			</div>
			<div class="form-group col-sm-4">
				{{ Form::label('a_paterno', '*Apellido Paterno') }}
				{{ Form::text('a_paterno', $alumno->a_paterno, array('class'=>'form-control')) }}
		    </div>
			<div class="form-group col-sm-4">
				{{ Form::label('a_materno', '*Apellido Materno') }}
				{{ Form::text('a_materno', $alumno->a_materno, array('class'=>'form-control')) }}
			</div>
		</div>   

		<div class="row">
		    <div class="form-group col-sm-4">
				{{ Form::label('sexo', '*Sexo') }}
				{{ Form::select('sexo',array(''=>'Seleccione Sexo','MASCULINO'=>'MASCULINO','FEMENINO'=>'FEMENINO'), $alumno->sexo, array('class'=>'form-control')) }}
			</div>
			<div class="form-group col-sm-4">
				{{ Form::label('nacionalidad', '*Nacionalidad') }}
				{{ Form::select('nacionalidad', array(), $contacto->nacionalidad, array('class'=>'form-control','id'=>'edit_nacionalidad')) }}
			</div>
			<div class="form-group col-sm-4">
				{{ Form::label('email', '*Correo electrónico') }}
				{{ Form::text('email', $contacto->email, array('class'=>'form-control')) }}
		    </div>
		</div>

		<div class="row">
			<div class="form-group col-sm-4">
				{{ Form::label('fecha_nac', '*Fecha de nacimiento') }}
				<div class="input-group date">
				    {{ Form::text('fecha_nac', $alumno->fecha_nac, array('class'=>'form-control')) }}
				    <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
				</div>
			</div>
			<div class="form-group col-sm-4">
				{{ Form::label('estado', '*Estado') }}
				{{ Form::select('estado', $estados, '', array('class'=>'form-control','id'=>'estado')) }}
		   </div>
			<div class="form-group col-sm-4">
				{{ Form::label('ciudad', '*Municipio o Delegación') }}
				{{ Form::select('ciudad', array(''=>'Seleccione una opción'),$contacto->ciudad,array('class'=>'form-control')) }}
		    </div>
	    </div>

		<div class="row">
			<div class="form-group col-sm-4">
	  			{{ Form::label('nivel_estudios', '*Último grado de estudios') }}
				{{ Form::select('nivel_estudios', array(''=>'Seleccione una opción', 'PRIMARIA'=>'PRIMARIA', 'SECUNDARIA'=>'SECUNDARIA', 'PREPARATORIA'=>'PREPARATORIA', 'LICENCIATURA'=>'LICENCIATURA', 'ESPECIALIDAD'=>'ESPECIALIDAD', 'MAESTRIA'=>'MAESTRIA', 'DOCTORADO'=>'DOCTORADO'), $alumno->nivel_estudios, array('class'=>'form-control')) }}
			</div>
			<div class="form-group col-sm-4">
			  	{{ Form::label('termino_estudios', '*Término de estudios') }}
				{{ Form::select('termino_estudios', array(''=>'Seleccione una opción', 'PASANTE'=>'PASANTE', 'TITULADO'=>'TITULADO'), $alumno->termino_estudios, array('class'=>'form-control')) }}
			</div>
			<div class="form-group col-sm-4">
			  	{{ Form::label('perfil_profesional', '*Perfil profesional') }}
				{{ Form::text('perfil_profesional', $alumno->perfil_profesional, array('class'=>'form-control')) }}
			</div>
		</div>
			  	
		<div class="row">
			<div class="form-group col-sm-12">
				{{ Form::label('institucion_estudios', '*Institución donde lo realizó') }}
				{{ Form::text('institucion_estudios', $alumno->institucion_estudios, array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="row">
			<div class="form-group col-sm-6">
				<ul>
					<li><b>*GRADO DE ESTUDIOS (TITULO PROFESIONAL</b>{{ Form::file('file',['accept' => 'application/pdf'],array('class'=>'btn btn-primary')) }}</li>
					<li><b>*INE (CREDENCIAL ANTES IFE)</b>{{ Form::file('file2',['accept' => 'application/pdf'],array('class'=>'btn btn-primary')) }}</li>
				</ul>
			</div>
			<div class="form-group col-sm-6">
				<ul>
					<li><b>*CEDULA (CEDULA PROFESIONAL)</b>{{ Form::file('file3',['accept' => 'application/pdf'],array('class'=>'btn btn-primary')) }}</li>
					<li><b>*CV((CURRICULUM VITAE)</b>{{ Form::file('file4',['accept' => 'application/pdf'],array('class'=>'btn btn-primary')) }}</li>
				</ul>
			</div>
		</div>
			  	
		<div class="row">
		  	<div class="form-group col-sm-5"></div>
		  	<div class="form-group col-sm-2" id="center">
		  		{{ Form::submit('Guardar informacion', array('class'=>'btn btn-primary','onclick'=>'this.disabled=true;this.form.submit();')) }}
			</div>
			<div class="form-group col-sm-5"></div>			
		</div>
	{{ Form::close() }}

@stop