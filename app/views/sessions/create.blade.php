@extends('layouts.logout')

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<h3>Ingreso</h3><hr>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="jumbotron">	
				{{ Form::open(array('route'=>'sessions.store', 'method'=>'POST', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="form-group">
					{{ Form::label('username', 'Usuario', array('class'=>'col-sm-3 control-label')) }}
					<div class="col-sm-9">
						{{ Form::text('username', '', array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					{{ Form::label('password', 'Contraseña', array('class'=>'col-sm-3 control-label')) }}
					<div class="col-sm-9">
						{{ Form::password('password', array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="restoreLink">
					{{HTML::link('recuperar','¿Has olvidado tu contraseña?')}}				
				</div>
				<br>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9" id='center'>
						{{ Form::submit('Entrar', array('class'=>'btn btn-primary acept-button','onclick'=>'this.disabled=true;this.form.submit();')) }}
					</div>
				</div>
				{{ Form::close() }}
			</div>
		</div>
			<div class="col-sm-6">
				<p>{{HTML::link('registro','Si desea inscribirse en un programa académico y aún no es alumno de Educación Continua haga click aquí.',array('class'=>'text-link'))}} {{ HTML::image('assets/img/hand-pointer-md.png') }}
</p>
			</div>
	</div>

@stop