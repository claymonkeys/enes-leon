@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Aspirantes</h3>
	  	</div>
	</div>
	
	{{-- <div class="container-fluid">
		<div class="row">
			@if (!empty($aspirantes))
				<table class="table table-striped" id="center">
					<th>Alumno</th>
					<th>Programa</th>
					<th>Unidad</th>
					<th>Estatus</th>
					<th>Acciones</th>
						
					@foreach($aspirantes as $lista)
						<tr>
							<td>{{ $lista->a_paterno ." ". $lista->a_materno ." ". $lista->nombre }}</td>
							<td>{{ $lista->programa }}</td>
							<td>{{ $lista->unidad }}</td>
							<td>{{ $lista->estatus }}</td>
							<td>
								{{HTML::link('aspirantes/acept/'.$lista->id,'Aceptar',array('class'=>'btn btn-primary btn-xs'))}}
								{{HTML::link('aspirantes/reject/'.$lista->id,'Rechazar',array('class'=>'btn btn-danger btn-xs'))}}
							</td>
						</tr>
					@endforeach
				</table>
			@endif			
		</div>
	</div> --}}
	
	<div class="row">
		<div class="col-sm-12">
			<table id="table-aspirantes" class="table table-striped table-condensed table-hover text-nowrap" cellspacing="0" width="100%">
				<thead>
					<th>Nombre</th>
					<th>Apellido Paterno</th>
					<th>Apellido Materno</th>
					<th>Programa</th>
					<th>Unidad</th>
					<th>Estatus</th>
					<th class="all">Acciones</th>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="modal-aspirantes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    	{{ Form::open(array('url'=>'aceptar', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'form-aspirantes', 'role'=>'form')) }}
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"></h4>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	      		{{ Form::label('grupo', 'Grupo', array('class'=>'col-sm-3 control-label')) }}
	      			<div class="col-sm-9">
								{{ Form::select('grupo', array(), '', array('class'=>'form-control')) }}
							</div>
						{{ Form::hidden('aspirante')}}
						{{ Form::hidden('alumno')}}
	      	</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary">Guardar cambios</button>
	      </div>
	      {{ Form::close() }}
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

@stop