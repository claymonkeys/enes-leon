@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Académicos</h3>
	  	</div>
	</div>
	
  	<hr>

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  		<div class="panel panel-default">
    		<div class="panel-heading" role="tab" id="headingOne">
      			<h4 class="panel-title">
        			<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          				 Editar Academico
        			</a>
      			</h4>
    		</div>

	    	<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	    		<div class="panel-body">

	    			<div class="row">
						{{ Form::open(array('url'=>'maestros/update/'.$academico->id, 'method'=>'POST', 'role'=>'form')) }}
					    	<div class="form-group col-sm-4">
					    		{{ Form::hidden('id_academico', $academico->id, array('id'=>'id_academico')) }}
					    		{{ Form::label('nombre', '*Nombre') }}
					    		{{ Form::text('nombre', $academico->nombre, array('class'=>'form-control')) }}
					    	</div>
					    	<div class="form-group col-sm-4">
					    		{{ Form::label('a_paterno', '*Apellido Paterno') }}
					    		{{ Form::text('a_paterno', $academico->a_paterno, array('class'=>'form-control')) }}
					    	</div>
					    	<div class="form-group col-sm-4">
					    		{{ Form::label('a_materno', '*Apellido Materno') }}
					    		{{ Form::text('a_materno', $academico->a_materno, array('class'=>'form-control')) }}
					    	</div>
					</div>   

					<div class="row">
					    <div class="form-group col-sm-4">
					    	{{ Form::label('sexo', '*Sexo') }}
					    	{{ Form::select('sexo',array(''=>'Seleccione Sexo','MASCULINO'=>'MASCULINO','FEMENINO'=>'FEMENINO'), $academico->sexo, array('class'=>'form-control')) }}
					    </div>
				  		<div class="form-group col-sm-4">
					    	{{ Form::label('fecha_nac', '*Fecha de nacimiento') }}
							<div class="input-group date">
					    		{{ Form::text('fecha_nac', $academico->fecha_nac, array('class'=>'form-control')) }}
					    		<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
					    	</div>
				  		</div>
				    	<div class="form-group col-sm-4">
				    		{{ Form::label('email', '*Correo electrónico') }}
				    		{{ Form::text('email', $contacto->email, array('class'=>'form-control')) }}
				    	</div>
				    </div>

				   	<div class="row">
					    <div class="form-group col-sm-4">
					    	{{ Form::label('nacionalidad', '*Nacionalidad') }}
					    	{{ Form::select('nacionalidad', array(), '', array('class'=>'form-control')) }}
					    </div>
					    <div class="form-group col-sm-4">
					    	{{ Form::label('estado', '*Estado') }}
					    	{{ Form::select('estado', array(''=>'Seleccione una opción'),'',array('class'=>'form-control','id'=>'edit_estado')) }}
					    </div>
					   	<div class="form-group col-sm-4">
					    	{{ Form::label('ciudad', '*Municipio o Delegación') }}
					    	{{ Form::select('ciudad', array(''=>'Seleccione una opción'),'',array('class'=>'form-control','id'=>'edit_ciudad')) }}
					    </div>
				    </div>

				    <div class="row">
					    <div class="form-group col-sm-8">
				  			{{ Form::label('titulo_profesional', '*Titulo profesional') }}
					    	{{ Form::text('titulo_profesional', $academico->titulo_profesional, array('class'=>'form-control')) }}
					    </div>
					   	<div class="form-group col-sm-4">
			  			{{ Form::label('unidad_academica', '*Unidad académica') }}
				    	{{ Form::select('unidad_academica', array(''=>'Unidad académica', '1'=>'ENES LEÒN', '2'=>'ENES SAN MIGUEL DE ALLENDE'), '', array('class'=>'form-control','id'=>'unidad_academica_edit')) }}
			  			</div>
				    </div>

					<div class="row">
				    	<div class="form-group col-sm-4">
				  			{{ Form::label('calle', '*Calle') }}
					    	{{ Form::text('calle', $contacto->calle , array('class'=>'form-control')) }}
				  		</div>
				  		<div class="form-group col-sm-2">
				  			{{ Form::label('num_ext', 'Número externo') }}
					    	{{ Form::text('num_ext',  $contacto->num_ext, array('class'=>'form-control')) }}
				  		</div>
				  		<div class="form-group col-sm-2">
			 			{{ Form::label('num_int', 'Número interno') }}
				    	{{ Form::text('num_int',  $contacto->num_int, array('class'=>'form-control')) }}
			  			</div>
				  		<div class="form-group col-sm-4">
				  			{{ Form::label('colonia', '*Colonia') }}
					    	{{ Form::text('colonia',  $contacto->colonia, array('class'=>'form-control')) }}
					  	</div>
					</div>

					<div class="row">
						<div class="form-group col-sm-3">
							{{ Form::label('cp', 'Código postal') }}
							{{ Form::text('cp', $contacto->cp, array('class'=>'form-control')) }}
				  		</div>
						<div class="form-group col-sm-3">
				  			{{ Form::label('telefono', '*Teléfono (incluír lada)') }}
						   	{{ Form::text('telefono',  $contacto->telefono, array('class'=>'form-control')) }}
				  		</div>
				 		<div class="form-group col-sm-3">
							{{ Form::label('movil', 'Celular') }}
					    	{{ Form::text('movil',  $contacto->movil, array('class'=>'form-control')) }}
				 		</div>
				  		<div class="form-group col-sm-3">
				  			{{ Form::label('rfc', 'RFC') }}
					    	{{ Form::text('rfc',  $contacto->rfc, array('class'=>'form-control')) }}
				  		</div>
					</div>

				  	<div class="row">
			  			<div class="form-group col-sm-12" id="center">
			  				{{ Form::submit('Actualizar', array('class'=>'btn btn-primary','onclick'=>'thitrue;this.form.submit();')) }}
			  				{{HTML::link('maestros/create','Cancelar',array('class'=>'btn btn-primary'))}}
						</div>
						{{ Form::close() }}
					</div>

				</div>
	    	</div>
    	</div>
	</div>

  	<hr>

	<div class="container-fluid">
		<div class="row">
			@if (!empty($academicos))
				<table class="table table-striped" id="center">
					<th>Nombre</th>
					<th>Titulo</th>
					<th>Unidad Academica</th>
					<th>Correo</th>
					<th>Telefono</th>
					<th>Acciones</th>
						
					@foreach($academicos as $lista)
						<tr>
							<td>{{ $lista->a_paterno ." ". $lista->a_materno ." ". $lista->nombre }}</td>
							<td>{{ $lista->titulo_profesional }}</td>
							<td>{{ $lista->unidad_academica }}</td>
							<td><a href="mailto:{{ $lista->email }}">{{ $lista->email }}</a></td>
							<td>{{ $lista->telefono }}</td>
							<td>
								{{HTML::link('maestros/ver/'.$lista->id,'Ver',array('class'=>'btn btn-success btn-xs'))}}
								{{HTML::link('maestros/edit/'.$lista->id,'Editar',array('class'=>'btn btn-primary btn-xs'))}}
								{{HTML::link('maestros/destroy/'.$lista->id,'Eliminar',array('class'=>'btn btn-danger btn-xs'))}}
							</td>
						</tr>
					@endforeach
				</table>
			@endif			
		</div>
	</div>
@stop