@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Académicos</h3>
	  	</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<table id="table-academicos" class="table table-striped table-condensed table-hover text-nowrap" cellspacing="0" width="100%">
				<thead>
					<th>Nombre(s)</th>
					<th>Apellido Paterno</th>
					<th>Apellido Materno</th>
					<th>Título</th>
					<th>Unidad Académica</th>
					<th>Correo</th>
					<th>Teléfono</th>
					<th class="all">Acciones</th>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>

<div class="modal fade" id="modal-academicos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      {{ Form::open(array('url'=>'', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'form-academicos', 'role'=>'form')) }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          {{ Form::label('nombre', '*Nombre', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('nombre', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('a_paterno', '*Apellido Paterno', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('a_paterno', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('a_materno', '*Apellido Materno', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('a_materno', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('sexo', '*Sexo', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::select('sexo', array(''=>'Seleccione Sexo','MASCULINO'=>'MASCULINO','FEMENINO'=>'FEMENINO'), '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('facha_nac', '*Fecha de Nacimiento', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              <div class="input-group date">
                {{ Form::text('fecha_nac', '', array('class'=>'form-control date')) }}
                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
              </div>
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('email', '*Correo electrónico', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::email('email', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('nacionalidad', '*Nacionalidad', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::select('nacionalidad', $nacionalidades, '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('estado', '*Estado', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::select('estado', $estados, '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('ciudad', '*Municipio o Delegación', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::select('ciudad', array(), '',array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('titulo_profesional', '*Titulo profesional', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('titulo_profesional', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('unidad_academica', '*Unidad académica', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::select('unidad_academica', $unidades, '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('calle', '*Calle', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('calle', '' , array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('num_ext', 'Número exterior', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('num_ext', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('num_int', 'Número interior', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('num_int', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('colonia', '*Colonia', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('colonia', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('cp', 'Código postal', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('cp', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('telefono', '*Teléfono (incluír lada)', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('telefono', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('movil', 'Celular', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('movil', '', array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
          {{ Form::label('rfc', 'RFC', array('class'=>'col-sm-4 control-label')) }}
            <div class="col-sm-8">
              {{ Form::text('rfc', '', array('class'=>'form-control')) }}
            </div>
        </div>
      </div><!-- /.modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
      {{ Form::close() }}
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop