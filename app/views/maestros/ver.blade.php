@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Académicos</h3>
	  	</div>
	</div>
	
  	<hr>

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  		<div class="panel panel-default">
    		<div class="panel-heading" role="tab" id="headingOne">
      			<h4 class="panel-title">
        			<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          				 Ver Academico
        			</a>
      			</h4>
    		</div>

	    	<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	    		<div class="panel-body">
					
					<div class="table-responsive">
						<table class="table table-striped " id="center">
		    				<tr>
			    				<th>Nombre</th>
			    				<th>Apellido Paterno</th>
			    				<th>Apellido Materno</th>
			    				<th>Sexo</th>
			    				<th>Fecha de Nacimiento</th>
		    				</tr>
		    				<tr>
		    					<td>{{$academico->nombre}}</td>
		    					<td>{{$academico->a_paterno}}</td>
		    					<td>{{$academico->a_materno}}</td>
		    					<td>{{$academico->sexo}}</td>
		    					<td>{{$academico->fecha_nac}}</td>
		    				</tr>
		    				<tr>
			    				<th>Correo</th>
			    				<th>Nacionalidad</th>
			    				<th>Estado</th>
			    				<th>Municipio</th>
			    				<th>Calle</th>
		    				</tr>
		    				<tr>
		    					<td>{{$contacto->email}}</td>
		    					<td>{{$nacionalidad->nacionalidad}}</td>
		    					<td>{{$estado->estado}}</td>
		    					<td>{{$municipio->municipio}}</td>
		    					<td>{{$contacto->calle}}</td>
		    				</tr>
		    				<tr>
			    				<th>Numero Ext</th>
			    				<th>Numero Int</th>
			    				<th>Colonia</th>
			    				<th>Codigo Postal</th>
			    				<th>Telefono</th>
		    				</tr>
		    				<tr>
		    					<td>{{$contacto->num_ext}}</td>
		    					<td>{{$contacto->num_int}}</td>
		    					<td>{{$contacto->colonia}}</td>
		    					<td>{{$contacto->cp}}</td>
		    					<td>{{$contacto->telefono}}</td>
		    				</tr>
		    				<tr>
			    				<th>Movil</th>
			    				<th>RFC</th>
			    				<th>Unidad Academica</th>
			    				<th colspan='2'>Titulo</th>
		    				</tr>
		    				<tr>
		    					<td>{{$contacto->movil}}</td>
		    					<td>{{$contacto->rfc}}</td>
		    					<td>{{$unidad->unidad}}</td>
		    					<td colspan='2'>{{$academico->titulo_profesional}}</td>
		    				</tr>
		    			</table>
					</div>
				</div>
			</div>
    	</div>
	</div>

	<hr>
	<div class="form-group col-sm-12" id="center">
  		{{HTML::link('maestros/create','Regresar',array('class'=>'btn btn-primary'))}}
  		{{HTML::link('maestros/edit/'.$academico->id,'Editar',array('class'=>'btn btn-primary'))}}
	</div>

@stop