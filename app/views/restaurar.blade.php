@extends('layouts.logout')

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<h3>¿Olvidaste tu contraseña?</h3><hr>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="jumbotron">	
				<br>
				<div class="row">
					<div class="col-sm-12" id='center-div'>
						<p>Por favor ingresa el correo electronico con el que te registraste en nuestro sistema y te generaremos una nueva clave que sera enviada a tu correo electronico. <br> Gracias!.</p>
					</div>
				</div>

				{{ Form::open(array('route'=>'recuperar', 'method'=>'POST', 'class'=>'form-horizontal', 'role'=>'form')) }}
				
				<div class="row">
					<div class="col-sm-4" id="center"></div>
					<div class="form-group" id="center">
						<div class="col-sm-4">
						{{ Form::label('correo', 'Correo', array('class'=>'control-label')) }}
						</div>
					</div>
					<div class="col-sm-4" id="center"></div>
				</div>

				<div class="row">
					<div class="col-sm-4" id="center"></div>
					<div class="form-group" id="center">
						<div class="col-sm-4">
							{{ Form::text('email', '', array('class'=>'form-control')) }}
						</div>					</div>
					<div class="col-sm-4" id="center"></div>
				</div>

				<div class="row">
					<div class="col-sm-5" id="center"></div>
					<div class="form-group" id="center">
						<div class="col-sm-2">
							{{ Form::submit('Enviar', array('class'=>'btn btn-primary','onclick'=>'this.disabled=true;this.form.submit();')) }}
						</div>					</div>
					<div class="col-sm-5" id="center"></div>
				</div>
				{{ Form::close() }}
			</div>

		</div>
	</div>

@stop