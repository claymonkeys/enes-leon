@extends('layouts.logout')

@section('content')

<div class="row">
	<div class="col-sm-12">
		<h3>Registro</h3>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-sm-10 col-sm-offset-1">
		{{ Form::open(array('url'=>'guardar', 'files'=>'true', 'method'=>'POST', 'id'=>'registro', 'role'=>'form')) }}
		<div id="rootwizard">
			<div class="navbar">
			  <div class="navbar-inner">
			    {{-- <div class="container"> --}}
					<ul>
					  <li><a href="#tab1" data-toggle="tab">1. Programa Académico</a></li>
						<li><a href="#tab2" data-toggle="tab">2. Datos Generales</a></li>
						<li><a href="#tab3" data-toggle="tab">3. Datos Escolares</a></li>
						<li><a href="#tab4" data-toggle="tab">4 ¿Requiere Factura?</a></li>
						<li><a href="#tab5" data-toggle="tab">5. Términos y Condiciones</a></li>
						<li><a href="#tab6" data-toggle="tab">6. Subir Documentacion</a></li>
					</ul>
			 		{{-- </div> --}}
			  </div>
			</div>
			<div class="tab-content">
			  <div class="tab-pane" id="tab1">
			  	<div class="row">
			  		<div class="col-sm-12" id="center-div">
			  		<h3>Programa Académico</h3>
			  		<p>Antes de comenzar la captura de datos recuerda que es necesario tener ya digitalizados y en formato PDF l
			  			os requisitos para inscripción, dependiendo el programa académico solicitado ya que estos pueden variar 
			  			(Título del último grado de estudios, cédula profesional, CV corto e identificación oficial) Gracias.</p>
			  		<span class="importante">Los datos con (*) son obligatorios para el registro</span>
			  		</div>
			  	</div>

			  	<hr>
			  	
			  	<div class="row">
			  		<div class="form-group col-sm-3">
			  			{{ Form::label('unidad_academica', '*Unidad académica') }}
				    	{{ Form::select('unidad_academica', $unidades, '', array('class'=>'form-control')) }}
			  		</div>
			  		<div class="form-group col-sm-3">
			  			{{ Form::label('tipo_programa', '*Tipo de programa') }}
			  			@if (!empty($tipos))
				    			{{ Form::select('tipo_programa', $tipos, 'Unidad Academica', array('class'=>'form-control')) }}
			  			@else
				    		{{ Form::select('tipo_programa', array(''=>'Tipo de programa'), '', array('class'=>'form-control')) }}
			  			@endif
			  		</div>
			  		<div class="form-group col-sm-3">
			  			{{ Form::label('programa_academico', '*Programa académico') }}
			  			{{ Form::select('programa_academico', array(), '', array('class'=>'form-control')) }}
			  		</div>
			  		<div class="form-group col-sm-3">
			  			{{ Form::label('como_se_entero', '*¿Cómo se enteró?') }}
				    	{{ Form::select('como_se_entero', array(''=>'Seleccione una opción', 'MEDIOS ESCRITOS'=>'MEDIOS ESCRITOS', 'RECOMENDACIÓN'=>'RECOMENDACIÓN', 'FACEBOOK'=>'FACEBOOK', 'TWITTER'=>'TWITTER', 'CORREO ELECTRÓNICO'=>'CORREO ELECTRÓNICO', 'PAGINA DE INTERNET'=>'PAGINA DE INTERNET'), '', array('class'=>'form-control')) }}
			  		</div>
			  	</div>

			  	<div style="float:right">
					<input type='button' class='btn button-next' name='next' value='Siguiente' />
				</div>
			  </div>
			  <div class="tab-pane" id="tab2">

			  	<div class="row">
			  		<div class="col-sm-12" id="center-div">
			  		<h3>Datos Generales</h3>
			  		<span class="importante">Los datos con (*) son obligatorios para el registro </span>
			  		</div>
			  	</div>

			  	<hr>

			    <div class="row">
				    <div class="form-group col-sm-4">
				    	{{ Form::label('nombre', '*Nombre') }}
				    	{{ Form::text('nombre', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
				    </div>
				    <div class="form-group col-sm-4">
				    	{{ Form::label('a_paterno', '*Apellido Paterno') }}
				    	{{ Form::text('a_paterno', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
				    </div>
				    <div class="form-group col-sm-4">
				    	{{ Form::label('a_materno', '*Apellido Materno') }}
				    	{{ Form::text('a_materno', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
				    </div>
			    </div>
			    <div class="row">
				    <div class="form-group col-sm-4">
				    	{{ Form::label('sexo', '*Sexo') }}
				    	{{ Form::select('sexo',array(''=>'Seleccione Sexo','MASCULINO'=>'MASCULINO','FEMENINO'=>'FEMENINO'), '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
				    </div>
				    <div class="form-group col-sm-4">
				    	{{ Form::label('nacionalidad', '*Nacionalidad') }}
				    	{{ Form::select('nacionalidad', $nacionalidades, '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
				    </div>
				    <div class="form-group col-sm-4">
				    	{{ Form::label('email', '*Correo electrónico') }}
				    	{{ Form::text('email', '', array('class'=>'form-control')) }}
				    </div>
			    </div>
			    <div class="row">
				    <div class="form-group col-sm-4">
				    	{{ Form::label('fecha_nac', '*Fecha de nacimiento') }}
							<div class="input-group date">
				    		{{ Form::text('fecha_nac', '', array('class'=>'form-control date','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
				    		<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
				    	</div>
				    </div>
				    <div class="form-group col-sm-4">
				    	{{ Form::label('estado', '*Estado') }}
				    	{{ Form::select('estado', $estados,'',array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
				    </div>
				   	<div class="form-group col-sm-4">
				    	{{ Form::label('ciudad', '*Municipio o Delegación') }}
				    	{{ Form::select('ciudad', array(), '',array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
				    </div>
			    </div>
			    <div style="float:right">
					<input type='button' class='btn button-next' name='next' value='Siguiente' />
				</div>
				<div style="float:left">
					<input type='button' class='btn button-previous' name='previous' value='Anterior' />
				</div>
			  </div>
				<div class="tab-pane" id="tab3">
				<div class="row">
			  		<div class="col-sm-12" id="center-div">
			  		<h3>Datos Escolares</h3>
			  		<span class="importante">Los datos con (*) son obligatorios para el registro </span>
			  		</div>
			  	</div>

			  	<hr>

					<div class="row">
			  		<div class="form-group col-sm-4">
			  			{{ Form::label('nivel_estudios', '*Último grado de estudios') }}
				    	{{ Form::select('nivel_estudios', array(''=>'Seleccione una opción', 'PRIMARIA'=>'PRIMARIA', 'SECUNDARIA'=>'SECUNDARIA', 'PREPARATORIA'=>'PREPARATORIA', 'LICENCIATURA'=>'LICENCIATURA', 'ESPECIALIDAD'=>'ESPECIALIDAD', 'MAESTRIA'=>'MAESTRIA', 'DOCTORADO'=>'DOCTORADO'), '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
			  		</div>
			  		<div class="form-group col-sm-4">
			  			{{ Form::label('termino_estudios', '*Término de estudios') }}
				    	{{ Form::select('termino_estudios', array(''=>'Seleccione una opción', 'PASANTE'=>'PASANTE', 'TITULADO'=>'TITULADO'), '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
			  		</div>
			  		<div class="form-group col-sm-4">
			  			{{ Form::label('perfil_profesional', '*Perfil profesional') }}
				    	{{ Form::text('perfil_profesional', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
			  		</div>
			  	</div>
			  	<div class="row">
			  		<div class="form-group col-sm-12">
			  			{{ Form::label('institucion_estudios', '*Institución donde lo realizó') }}
				    	{{ Form::text('institucion_estudios', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
			  		</div>
			  	</div>
				<div style="float:right">
					<input type='button' class='btn button-next' name='next' value='Siguiente' />
				</div>
				<div style="float:left">
					<input type='button' class='btn button-previous' name='previous' value='Anterior' />
				</div>
			  </div>

				<div class="tab-pane" id="tab4">

				<div class="row">
			  		<div class="col-sm-12" id="center-div">
			  		<h3>¿Requiere Facturación?</h3>
			  		<span class="importante">Los datos con (*) son obligatorios para el registro </span>
			  		</div>
			  	</div>

			  	<hr>

					<div class="row">
			  		<div class="form-group col-sm-4">
			  			{{ Form::label('calle', '*Calle') }}
				    	{{ Form::text('calle', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
			  		</div>
			  		<div class="form-group col-sm-2">
			  			{{ Form::label('num_ext', '*Número externo') }}
				    	{{ Form::number('num_ext', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
			  		</div>
			  		<div class="form-group col-sm-2">
			  			{{ Form::label('num_int', 'Número interno') }}
				    	{{ Form::number('num_int', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
			  		</div>
			  		<div class="form-group col-sm-4">
			  			{{ Form::label('colonia', '*Colonia') }}
				    	{{ Form::text('colonia', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();')) }}
			  		</div>
			  	</div>
			  	<div class="row">
			  		<div class="form-group col-sm-3">
			  			{{ Form::label('cp', '*Código postal') }}
				    	{{ Form::number('cp', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();',"maxlength"=>"5")) }}
			  		</div>
			  		<div class="form-group col-sm-3">
			  			{{ Form::label('telefono', '*Teléfono (incluír lada)') }}
				    	{{ Form::number('telefono', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();',"maxlength"=>"10")) }}
			  		</div>
			  		<div class="form-group col-sm-3">
			  			{{ Form::label('movil', 'Celular') }}
				    	{{ Form::number('movil', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();',"maxlength"=>"13")) }}
			  		</div>
			  		<div class="form-group col-sm-3">
			  			{{ Form::label('rfc', '*RFC') }}
				    	{{ Form::text('rfc', '', array('class'=>'form-control','onkeyup'=>'this.value=this.value.toUpperCase();',"maxlength"=>"13")) }}
			  		</div>
			  	</div>
			  	<div class="row">

			  		<div class="form-group col-sm-12">
								{{ Form::label('facturacion', '¿Requiere Facturación?') }}
								{{ Form::checkbox('facturacion') }}
			  		</div>

			  	</div>
			  	<div style="float:right">
					<input type='button' class='btn button-next' name='next' value='Siguiente' />
				</div>
				<div style="float:left">
					<input type='button' class='btn button-previous' name='previous' value='Anterior' />
				</div>

			  </div>
				<div class="tab-pane" id="tab5">

				<div class="row">
			  		<div class="col-sm-12" id="center-div">
			  		<h3>Términos y Condiciones </h3>
			  		<span class="importante">Los datos con (*) son obligatorios para el registro </span>
			  		</div>
			  	</div>

			  	<hr>

					<div class="row">
						<div class="col-sm-12">
							<textarea class="form-control" rows="5">
								La División de Educación Continua e Intercambio Académico tiene como objetivo:
								Comprender el marco conceptual de la Educación Continua y las formas de organización que favorecen el desarrollo de un centro educativo; diseñar planes de desarrollo institucional, a partir de la metodología de la planeación estratégica y prospectiva; presentar una propuesta de acto académico que integre a los protagonistas y factores que intervienen en el proceso educativo.
								La responsabilidad del participante es cumplir satisfactoriamente con las siguientes indicaciones:
								1. Realizar las actividades y prácticas requeridas en cada clase. En caso de no acreditar alguna actividad, no tendrá oportunidad de presentación posterior, en casos extraordinarios el Coordinador del programa académico es el único facultado para dar prorrogas, condiciones y fechas de entrega.
								2. Si el programa es presencial, todas las actividades se realizaran únicamente dentro del horario establecido para el mismo. En el caso de los programas en línea se ajustarán a las fechas de entrega establecidas en cada una de las actividades.
								3. Cumplir con las indicaciones de pago que le serán enviadas a través de correo electrónico.
								4. En caso de no pagar en las fechas estipuladas (los 5 primeros días del mes), se dará un plazo de 5 días más generando un recargo del 10 % sobre el monto a pagar, si el pago no se realiza en este plazo de 10 días y no se ha solicitado prórroga y autorización de la Jefatura de Educación Continua, nos reservamos el derecho de no permitir al alumno el ingreso al programa que este cursando y el participante será dado de baja sin que sean reembolsables los pagos anteriores.
								5. En caso que por alguna circunstancia tuviera que darse de baja del curso, el participante deberá comunicarlo por escrito a la División de Educación Continua, explicando las razones por las que lo hace. De no ser así continuará su compromiso de pago TOTAL del curso, aunque ya no asista.
								6. En programas presenciales el alumno deberá cuidar el equipo e instalaciones de la Universidad, en caso de uso inadecuado, tendrán que cubrir los gastos de reparación o desperfectos que se deriven del mismo.
								Resulta importante aclarar que en caso de que el participante no cumpla con estas responsabilidades, la División de Educación Continua deslinda las propias.
								Una vez leído el presente documento y estando de acuerdo con lo que se señala, asumo cada una de las condiciones que me corresponden.
							</textarea>
							<div class="checkbox">
								<label>
								  <input type="checkbox" value="" name="terminos">
								    *Aceptar términos
								</label>
							</div>
						</div>
					</div>
				<div style="float:right">
					<input type='button' class='btn button-next' name='next' value='Siguiente' />
				</div>
				<div style="float:left">
					<input type='button' class='btn button-previous' name='previous' value='Anterior' />
				</div>
			  </div>
				<div class="tab-pane" id="tab6">

				<div class="row">
			  		<div class="col-sm-12" id="center-div">
			  		<h3>Documentacion</h3>
			  		<span class="importante">Los datos con (*) son obligatorios para el registro </span>
			  		</div>
			  	</div>

			  	<hr>

					<div class="row">
						<div class="form-group col-sm-6">
							<p>Para terminar su registro por favor adjunte su documentación requerida en diferentes archivos .pdf para el programa académico al que se está registrando.</p>
							<p><b>A la derecha se enlista la documentación requerida.</b></p>

						</div>
						<div class="form-group col-sm-6">
							{{ Form::label('file', 'Subir archivos Requeridos') }}
							<br>
							<ul>
							<li>*Ultimo Grado de Estudios{{ Form::file('file',['accept'=>'application/pdf', 'class'=>'required']) }}</li>
							<li>*Identificación Oficial{{ Form::file('file2',['accept'=>'application/pdf', 'class'=>'required']) }}</li>
							<li>*Cedula Profesional{{ Form::file('file3',['accept'=>'application/pdf', 'class'=>'required']) }}</li>
							<li>*Curriculum Vitae Corto{{ Form::file('file4',['accept'=>'application/pdf', 'class'=>'required']) }}</li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-sm-12" id="center">
							{{ Form::submit('Enviar', array('class'=>'btn btn-primary')) }}
						</div>
					</div>
				<div style="float:left">
					<input type='button' class='btn button-previous' name='previous' value='Anterior' />
				</div>
			  </div>
			  <div class="row">
				<div class="form-group col-sm-12" id="center">
			  		{{HTML::link('/','Cancelar',array('class'=>'btn btn-primary'))}}
			  	</div>
			  </div>


			</div>	
		</div><!-- /#rootwizard -->
		{{ Form::close() }}

	</div>
</div>

@stop