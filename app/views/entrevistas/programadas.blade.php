@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Entrevistas Programadas</h3>
	  		<ul class="nav nav-pills">
  				<li role="presentation">{{HTML::link('entrevistas','Pendientes')}}</li>
  				<li role="presentation">{{HTML::link('entrevistas/programadas','Programadas')}}</li>
			</ul>
	  	</div>
	</div>
	<hr>
	
	<div class="container-fluid">
		<div class="row">
				<table class="table table-striped" id="center">
					<tr>
						<th>Nombre</th>
						<th>Apellido Paterno</th>
						<th>Apellido Materno</th>
						<th>DÍa</th>
						<th>Hora</th>
						<th>Estatus</th>
						<th>Acciones</th>
					</tr>
				@foreach($entrevistas as $lista)
					<tr>
						<td>{{$lista->nombre}}</td>
						<td>{{$lista->a_paterno}}</td>
						<td>{{$lista->a_materno}}</td>		
						<td>{{$lista->fecha}}</td>		
						<td>{{$lista->hora}}</td>
						<td>{{$lista->estatus_e}}</td>		
						<td>
							{{HTML::link('entrevistas/editar/'.$lista->id,'cambiar',array('class'=>'btn btn-primary btn-xs'))}}
						@if($lista->estatus_e == "PENDIENTE")
							{{HTML::link('entrevistas/revisar/'.$lista->id,'Realizado',array('class'=>'btn btn-success btn-xs'))}}
						@else
							{{HTML::link('entrevistas/revisar/'.$lista->id,'Posponer',array('class'=>'btn btn-danger btn-xs'))}}
						@endif
						</td>
					</tr>
					@endforeach
				</table>
		</div>
	</div>

@stop