@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Entrevistas Pendientes</h3>
	  		<ul class="nav nav-pills">
  				<li role="presentation">{{HTML::link('entrevistas','Pendientes')}}</li>
  				<li role="presentation">{{HTML::link('entrevistas/programadas','Programadas')}}</li>
			</ul>
	  	</div>
	</div>
	<hr>
	

	<div class="container-fluid">
		<div class="row">
			{{ Form::open(array('url' => 'entrevistas/actualizar/'.$entrevista->id, 'method'=>'POST', 'role'=>'form')) }}

				<div class="form-group col-sm-3" id="center">
					{{ Form::label('Nombre', '*Aspirante') }} <br>
					{{ Form::label('Aspirante', $entrevista->nombre . " " . $entrevista->a_paterno . " " . $entrevista->a_materno) }}
				</div>

				<div class="form-group col-sm-2" id="center">
					{{ Form::label('Fecha', '*Fecha de Entrevista') }}
					<input type="date" class="form-control" name="fecha" id="Fecha" value="<?php echo $entrevista->fecha; ?>">
				</div>

				<div class="form-group col-sm-2" id="center">
					{{ Form::label('Hora', '*Hora de Entrevista') }}
					<input type="time" class="form-control" name="hora" id="Hora" value="<?php echo $entrevista->hora; ?>">
				</div>


				<div class="form-group col-sm-2" id="center">
			  		{{ Form::submit('Actualizar', array('class'=>'btn btn-primary','onclick'=>'thitrue;this.form.submit();')) }}						
			  	</div>

			</div>

			{{ Form::close() }}

	</div>
	

@stop