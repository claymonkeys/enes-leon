@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Entrevistas Pendientes</h3>
	  		<ul class="nav nav-pills">
  				<li role="presentation">{{HTML::link('entrevistas','Pendientes')}}</li>
  				<li role="presentation">{{HTML::link('entrevistas/programadas','Programadas')}}</li>
			</ul>
	  	</div>
	</div>
	<hr>
	

	<div class="container-fluid">
		<div class="row">
			{{ Form::open(array('url' => 'entrevistas/guardar', 'method'=>'POST', 'role'=>'form')) }}

				{{ Form::hidden('id', $aspirante->id) }}

				<div class="form-group col-sm-3" id="center">
					{{ Form::label('Nombre', '*Aspirante') }} <br>
					{{ Form::label('Aspirante', $aspirante->nombre . " " . $aspirante->a_paterno) }}
				</div>

				<div class="form-group col-sm-2" id="center">
					{{ Form::label('Fecha', '*Fecha de Entrevista') }}
					<input type="date" class="form-control" name="fecha" id="Fecha">
				</div>

				<div class="form-group col-sm-2" id="center">
					{{ Form::label('Hora', '*Hora de Entrevista') }}
					<input type="time" class="form-control" name="hora" id="Hora">
				</div>


				<div class="form-group col-sm-2" id="center">
			  		{{ Form::submit('Agregar', array('class'=>'btn btn-primary','onclick'=>'this.disabled=true;this.form.submit();')) }}						
			  	</div>

			</div>

			{{ Form::close() }}

	</div>
	

@stop