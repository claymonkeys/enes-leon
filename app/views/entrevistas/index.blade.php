@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Entrevistas Pendientes</h3>
	  		<ul class="nav nav-pills">
  				<li role="presentation">{{HTML::link('entrevistas','Pendientes')}}</li>
  				<li role="presentation">{{HTML::link('entrevistas/programadas','Programadas')}}</li>
			</ul>
	  	</div>
	</div>
	<hr>
	

	<div class="container-fluid">
		<div class="row">
				<table class="table table-striped" id="center">
					<tr>
						<th>Nombre</th>
						<th>Apellido Paterno</th>
						<th>Apellido Materno</th>
						<th>Programa</th>
						<th>DÍa</th>
						<th>Hora</th>
						<th>Acciones</th>
					</tr>
				@foreach($aspirantes as $lista)
					<tr>
						<td>{{$lista->nombre}}</td>
						<td>{{$lista->a_paterno}}</td>
						<td>{{$lista->a_materno}}</td>		
						<td>{{$lista->programa }}</td>		
						<td>PENDIENTE</td>		
						<td>PRENDIENTE</td>
						<td>
							{{HTML::link('entrevistas/crear/'.$lista->id,'Programar Entrevista',array('class'=>'btn btn-primary btn-xs'))}}
						</td>
					</tr>
					@endforeach
				</table>
		</div>
	</div>
	

@stop