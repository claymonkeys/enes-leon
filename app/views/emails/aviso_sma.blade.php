<html>
<head></head>
<body>
	<h4>Asunto: Educación Continua ENES unidad San Miguel de Allende de la UNAM, <b>informe.</b></h4>

	<p>
		Educación Continua e Intercambio Académico de la Escuela Nacional de Estudios Superiores unidad San Miguel de Allende
		de la UNAM  agradece de antemano su interés y le informamos que por el momento no podemos atender la solicitud al 
		programa de su elección, esperando contar con su participación en futuros programas.
	</p>

	<p>
		Quedamos a sus órdenes para cualquier información y le enviamos un cordial saludo.
	</p>

	<p>
		{{ HTML::image('http://enes-leon.herokuapp.com/assets/img/enes-footer-email.png') }}
	</p>
</body>
</html>
