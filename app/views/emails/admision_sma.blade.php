<html>
<head></head>
<body>
	<h1>Aviso de Admisiòn al Programa ENES ENES - San Miguel de Allende</h1>
	<br>
	<h4>Asunto: Educación Continua ENES unidad San Miguel de Allende de la UNAM, <b>Aceptaciòn al programa.</b></h4>
	<br>
	<p>
		Educación Continua e Intercambio académico de la Escuela Nacional de Estudios Superiores unidad San Miguel de Allende de la UNAM 
		le da la más cordial bienvenida, usted ha sido aceptado al programa que eligió, para culminar su proceso de 
		inscripción ingrese al sistema por medio de la siguiente liga con su usuario y contraseña.
	</p>

	<p>
		<a href="http://enes-leon.herokuapp.com/login">Escuela Nacional de Estudios Superiores</a>
	</p>
	<p>
		Quedamos a sus órdenes para cualquier información y le enviamos un cordial saludo.
	</p>

	<p>
		{{ HTML::image('http://enes-leon.herokuapp.com/assets/img/enes-footer-email.png') }}
	</p>
</body>
</html>
