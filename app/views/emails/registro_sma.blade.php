<html>
<head></head>
<body>
	<h3>Educación Continua ENES unidad San Miguel de Allende de la UNAM, registro.</h3>

	<p>
		Educación Continua e Intercambio Académico de la Escuela Nacional de Estudios Superiores unidad San Miguel de Allende
		de la UNAM, agradece su interés en nuestra Institución, le informamos que su registro se ha realizado con éxito 
		y en breve recibirá respuesta a su solicitud.
	</p>

	<p>
		Datos de Acceso: <br>
		<b>Usuario:</b> {{$username}}<br>
		<b>Contraseña:</b> {{$password}} <br>
	</p>

	<p>
		Link de Acceso <a href="http://enes-leon.herokuapp.com/login">Escuela Nacional de Estudios Superiores</a>
	</p>

	<p>
		Seguimos a sus órdenes y le enviamos un cordial saludo.
	</p>

	<p>
		{{ HTML::image('http://enes-leon.herokuapp.com/assets/img/enes-footer-email.png') }}
	</p>
</body>
</html>
