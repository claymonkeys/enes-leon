@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Grupos</h3>
	  	</div>
	</div>
	
  	<hr>

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  		<div class="panel panel-default">
    		<div class="panel-heading" role="tab" id="headingOne">
      			<h4 class="panel-title">
        			<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          				Crear Grupo
        			</a>
      			</h4>
    		</div>

	    	<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
	    		<div class="panel-body">

	    			<div class="row">
						{{ Form::open(array('url'=>'grupos/store', 'method'=>'POST', 'role'=>'form')) }}
					    	<div class="form-group col-sm-3">
					    		{{ Form::label('grupo', '*Grupo') }}
					    		{{ Form::text('grupo', '', array('class'=>'form-control')) }}
					    	</div>
					    	<div class="form-group col-sm-3">
					    		{{ Form::label('maesto', '*Maestro') }}
						   		{{ Form::select('maestro', array(), '', array('class'=>'form-control','id'=>'maestro')) }}
					    	</div>
					    	<div class="form-group col-sm-3">
					    		{{ Form::label('programa', '*Programa') }}
						   		{{ Form::select('programa', array(), '', array('class'=>'form-control','id'=>'programaGrupo')) }}
					    	</div>
					    	<div class="form-group col-sm-3">
					    		{{ Form::label('cupo', '*Cupo') }}
						   		{{ Form::number('cupo', '', array('class'=>'form-control')) }}
					    	</div>
					</div>   

					<hr>

					<div class="row" id="center">
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Lunes', '*Lunes') }}
							{{ Form::checkbox('Lunes') }}
						</div>
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Martes', '*Martes') }}
							{{ Form::checkbox('Martes') }}
						</div>
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Miercoles', '*Miercoles') }}
							{{ Form::checkbox('Miercoles') }}
						</div>
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Jueves', '*Jueves') }}
							{{ Form::checkbox('Jueves') }}
						</div>
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Viernes', '*Viernes') }}
							{{ Form::checkbox('Viernes') }}
						</div>
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Sabado', '*Sabado') }}
							{{ Form::checkbox('Sabado') }}
						</div>
					</div>

					<div class="row">
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio', '*Inicio') }}
							<input type="time" class="form-control" name="inicio" id="inicio">
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio2', '*Inicio') }}
							<input type="time" class="form-control" name="inicio2" id="inicio2">
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio3', '*Inicio') }}
							<input type="time" class="form-control" name="inicio3" id="inicio3">
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio4', '*Inicio') }}
							<input type="time" class="form-control" name="inicio4" id="inicio4">
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio5', '*Inicio') }}
							<input type="time" class="form-control" name="inicio5" id="inicio5">
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio6', '*Inicio') }}
							<input type="time" class="form-control" name="inicio6" id="inicio6">
						</div>
					</div>

					<div class="row">
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin', '*Fin') }}
							<input type="time" class="form-control" name="fin" id="fin">
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin2', '*Fin') }}
							<input type="time" class="form-control" name="fin2" id="fin2">
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin3', '*Fin') }}
							<input type="time" class="form-control" name="fin3" id="fin3">
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin4', '*Fin') }}
							<input type="time" class="form-control" name="fin4" id="fin4">
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin5', '*Fin') }}
							<input type="time" class="form-control" name="fin5" id="fin5">
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin6', '*Fin') }}
							<input type="time" class="form-control" name="fin6" id="fin6">
						</div>
					</div>


				  	<div class="row">
			  			<div class="form-group col-sm-5"></div>
			  			<div class="form-group col-sm-2" id="center">
			  				{{ Form::submit('Agregar', array('class'=>'btn btn-primary','onclick'=>'thitrue;this.form.submit();')) }}						</div>
						<div class="form-group col-sm-5"></div>
						{{ Form::close() }}
					</div>

				</div>
	    	</div>
    	</div>
	</div>

  	<hr>

	<div class="container-fluid">
		<div class="row">
				<table class="table table-striped" id="center">
					<tr>
						<th>Grupo</th>
						<th>Academico</th>
						<th>Programa</th>
						<th>Dias</th>
						<th>Horarios</th>
						<th>Cupo</th>
						<th>Inicio</th>
						<th>Fin</th>
						<th>Acciones</th>
					</tr>
				@foreach($grupos as $lista)
					<tr>
						<td>{{HTML::link('grupos/ver/'.$lista->id,$lista->grupo)}}</td>
						<td>{{ $lista->a_paterno . " "  . $lista->a_materno . " " . $lista->nombre}}</td>
						<td>{{ $lista->programa }}</td>		
						<td>{{ substr($lista->dias,'2') }}</td>		
						<td>{{ substr($lista->horarios,'2') }}</td>		
						<td>{{ $lista->cupo }}</td>
						<td>{{ $lista->inicio }}</td>
						<td>{{ $lista->fin }}</td>
						<td>
							{{HTML::link('grupos/destroy/'.$lista->id,'Eliminar',array('class'=>'btn btn-danger btn-xs'))}}
							{{HTML::link('grupos/edit/'.$lista->id,'Editar',array('class'=>'btn btn-primary btn-xs'))}}
						</td>
					</tr>
					@endforeach
				</table>
		</div>
	</div>



<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i>Editar</h4>
      </div>
      <div class="modal-body">
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
	
@stop