@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Grupo: {{$grupo->grupo}}</h3>
	  		<h5>{{$curso->programa}}</h5>
	  		<h5>Dia(s): {{substr($grupo->dias,'2')}}</h5>
	  		<h5>Horario: {{substr($grupo->horarios,'2')}}</h5>
	  	</div>
	</div>
	
  	<hr>

  	@if($alumnos==null)
		<h4 id="center">No hay alumnos en este grupo</h4>
	@else
  	<h4 id="center">Lista de Alumno</h4>

	<div class="container-fluid">
		<div class="row">
				<table class="table table-striped" id="center">
					<tr>
						<th>#</th>
						<th>Nombre</th>
						<th>Apellido Paterno</th>
						<th>Apellido Materno</th>
						<th>Unidad Academica</th>
						<th>Acciones</th>
					</tr>
				<?php $contador=1 ?>
				@foreach($alumnos as $lista)
					<tr>
						<td>{{ $contador }}</td>
						<td>{{ $lista->nombre}}</td>
						<td>{{ $lista->a_paterno}}</td>
						<td>{{ $lista->a_materno }}</td>		
						<td>{{ $lista->unidad }}</td>		

						<td>
							{{HTML::link('grupos/delete/'.$lista->alumno_id,'Eliminar',array('class'=>'btn btn-danger btn-xs'))}}
						</td>
					</tr>
					<?php $contador++ ?>
					@endforeach
				</table>
				
		</div>
	</div>
	@endif


<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i>Editar</h4>
      </div>
      <div class="modal-body">
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
	
@stop