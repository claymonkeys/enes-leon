@extends('layouts.admin')

@section('content')

	<div class="row">
	  	<div class="col-sm-12" id="center-div">
	  		<h3>Grupos</h3>
	  	</div>
	</div>
	
  	<hr>

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  		<div class="panel panel-default">
    		<div class="panel-heading" role="tab" id="headingOne">
      			<h4 class="panel-title">
        			<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          				Editar Grupo
        			</a>
      			</h4>
    		</div>

	    	<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	    		<div class="panel-body">

	    			<div class="row">
						{{ Form::open(array('url'=>'grupos/update/'.$grupo->id, 'method'=>'POST', 'role'=>'form')) }}
					    	<div class="form-group col-sm-3">
					    		{{ Form::label('grupo', '*Grupo') }}
					    		{{ Form::text('grupo', $grupo->grupo, array('class'=>'form-control')) }}
					    	</div>
					    	<div class="form-group col-sm-3">
					    		{{ Form::label('maesto', '*Maestro') }}
						   		{{ Form::select('maestro', $maestros, $grupo->academico, array('class'=>'form-control','id'=>'maestro2')) }}
					    	</div>
					    	<div class="form-group col-sm-3">
					    		{{ Form::label('programa', '*Programa') }}
						   		{{ Form::select('programa', $programas, $grupo->programa, array('class'=>'form-control','id'=>'programaGrupo2')) }}
					    	</div>
					    	<div class="form-group col-sm-3">
					    		{{ Form::label('cupo', '*Cupo') }}
						   		{{ Form::number('cupo', $grupo->cupo, array('class'=>'form-control')) }}
					    	</div>
					</div>   

					<hr>
							<?php
								$z=0;
								$contador=0;
								$horarios=explode(", ", $grupo->horarios);
								$dias=explode(", ", $grupo->dias);
							?>
					<div class="row" id="center">
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Lunes', '*Lunes') }}
							<?php $z=0; ?>
							@foreach($dias as $lista)
								<?php $contador++; ?>
								@if($lista=="LUNES")
									<?php $z=1; break; ?>
								@endif
							@endforeach
								@if($z==1)
									{{ Form::checkbox('Lunes','1','Lunes') }}
								@else
									{{ Form::checkbox('Lunes','0') }}
								@endif
						</div>
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Martes', '*Martes') }}
							<?php $z=0; ?>
							@foreach($dias as $lista)
								<?php $contador++; ?>
								@if($lista=="MARTES")
									<?php $z=1; break; ?>
								@endif
							@endforeach
								@if($z==1)
									{{ Form::checkbox('Martes','1','Martes') }}
								@else
									{{ Form::checkbox('Martes','0') }}
								@endif
						</div>
						<div class="form-group col-sm-2" id="centrada">
							{{ Form::label('Miercoles', '*Miercoles') }}
							<?php $z=0; ?>
							@foreach($dias as $lista)
								<?php $contador++; ?>
								@if($lista=="MIERCOLES")
									<?php $z=1; break; ?>
								@endif
							@endforeach
								@if($z==1)
									{{ Form::checkbox('Miercoles','1','Miercoles') }}
								@else
									{{ Form::checkbox('Miercoles','0') }}
								@endif
						</div>
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Jueves', '*Jueves') }}
							<?php $z=0; ?>
							@foreach($dias as $lista)
								<?php $contador++; ?>
								@if($lista=="JUEVES")
									<?php $z=1; break; ?>
								@endif
							@endforeach
								@if($z==1)
									{{ Form::checkbox('Jueves','1','Jueves') }}
								@else
									{{ Form::checkbox('Jueves','0') }}
								@endif
						</div>
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Viernes', '*Viernes') }}
							<?php $z=0; ?>
							@foreach($dias as $lista)
								<?php $contador++; ?>
								@if($lista=="VIERNES")
									<?php $z=1; break; ?>
								@endif
							@endforeach
								@if($z==1)
									{{ Form::checkbox('Viernes','1','Viernes') }}
								@else
									{{ Form::checkbox('Viernes','0') }}
								@endif
						</div>
						<div class="form-group col-sm-2" id="centrada">
					    	{{ Form::label('Sabado', '*Sabado') }}
							<?php $z=0; ?>
							@foreach($dias as $lista)
								<?php $contador++; ?>
								@if($lista=="SABADO")
									<?php $z=1; break; ?>
								@endif
							@endforeach
								@if($z==1)
									{{ Form::checkbox('Sabado','1','Sabado') }}
								@else
									{{ Form::checkbox('Sabado','0') }}
								@endif
						</div>
					</div>

					<div class="row">
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio', '*Inicio') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="LUNES")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="inicio" value="{{$temporal[0]}}" id="inicio">
								@else
									<input type="time" class="form-control" name="inicio" id="inicio">
								@endif
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio2', '*Inicio') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="MARTES")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="inicio2" value="{{$temporal[0]}}" id="inicio2">
								@else
									<input type="time" class="form-control" name="inicio2" id="inicio2">
								@endif
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio3', '*Inicio') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="MIERCOLES")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="inicio3" value="{{$temporal[0]}}" id="inicio3">
								@else
									<input type="time" class="form-control" name="inicio3" id="inicio3">
								@endif
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio4', '*Inicio') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="JUEVES")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="inicio4" value="{{$temporal[0]}}" id="inicio4">
								@else
									<input type="time" class="form-control" name="inicio4" id="inicio4">
								@endif
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio5', '*Inicio') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="VIERNES")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="inicio5" value="{{$temporal[0]}}" id="inicio5">
								@else
									<input type="time" class="form-control" name="inicio5" id="inicio5">
								@endif
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('inicio6', '*Inicio') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="SABADO")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="inicio6" value="{{$temporal[0]}}" id="inicio6">
								@else
									<input type="time" class="form-control" name="inicio6" id="inicio6">
								@endif
						</div>
					</div>

					<div class="row">
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin', '*Fin') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="LUNES")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="fin" value="{{$temporal[1]}}" id="fin">
								@else
									<input type="time" class="form-control" name="fin" id="fin">
								@endif
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin2', '*Fin') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="MARTES")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="fin2" value="{{$temporal[1]}}" id="fin2">
								@else
									<input type="time" class="form-control" name="fin2" id="fin2">
								@endif
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin3', '*Fin') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="MIERCOLES")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="fin3" value="{{$temporal[1]}}" id="fin3">
								@else
									<input type="time" class="form-control" name="fin3" id="fin3">
								@endif
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin4', '*Fin') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="JUEVES")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="fin4" value="{{$temporal[1]}}" id="fin4">
								@else
									<input type="time" class="form-control" name="fin4" id="fin4">
								@endif
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin5', '*Fin') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="VIERNES")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="fin5" value="{{$temporal[1]}}" id="fin5">
								@else
									<input type="time" class="form-control" name="fin5" id="fin5">
								@endif
						</div>
						<div class="form-group col-sm-2" id="center">
							{{ Form::label('fin6', '*Fin') }}
							<?php $contador=0; $z=0;?>
							@foreach($dias as $lista)
								@if($lista=="SABADO")
									<?php $z=1; break; ?>
								@endif
								<?php $contador++; ?>
							@endforeach
								@if($z==1)
									<?php $temporal=explode('-',$horarios[$contador])?>
									<input type="time" class="form-control" name="fin6" value="{{$temporal[1]}}" id="fin6">
								@else
									<input type="time" class="form-control" name="fin6" id="fin6">
								@endif
						</div>
					</div>


				  	<div class="row">
			  			<div class="form-group col-sm-5"></div>
			  			<div class="form-group col-sm-2" id="center">
			  				{{ Form::submit('Actualizar', array('class'=>'btn btn-primary','onclick'=>'thitrue;this.form.submit();')) }}						</div>
						<div class="form-group col-sm-5"></div>
						{{ Form::close() }}
					</div>

				</div>
	    	</div>
    	</div>
	</div>


<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i>Editar</h4>
      </div>
      <div class="modal-body">
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
	
@stop