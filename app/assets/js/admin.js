/**
 *
 * ENES León
 *
 * admin.js
 * Scripts para backend(admin)
 *
 */

$(function(){

	/**
	 * Programas Datatable
	 *
   */
	var dt_programas = $('#table-programas').DataTable({
		"ajax": {
			"processing": true,
      "url": baseURL + "/programas/programas",
      "type": "GET",
		},
    "stateSave": true,
    "responsive": true,
		"columns": [
			{"data": "programa"},
      {"data": "tipo_programa"},
			{"data": "modalidad"},
			{"data": "admision"},
			{"data": "fecha_inicio"},
      {"data": "fecha_fin"},
			{
        // Agregar columna con etiquetas que indiquen el estatus
        "data": "estatus",
        "render": function ( data, type, full, meta ) {
          if (data == 'ACTIVO') {
            return '<span class="label label-success">ACTIVO</span>';
          } else if (data == 'INACTIVO') {
          return '<span class="label label-warning">INACTIVO</span>';
          }
        }
      },
			{
        // Agregar una ultima columna con botones de edición y eliminación
        "data": null,
        "searchable": false,
        "orderable": false,
        "defaultContent": '<div class="btn-group" role="group" aria-label="...">'
                      + '<a href="#" class="btn btn-primary btn-xs btn-edit" data-toggle="modal" data-target="#modal-programas"><i class="fa fa-fw fa-pencil"></i></a>'
                      + '<a href="#" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-fw fa-times"></i></a>'
                      + '</div>',
      },
		],
    // Agregar id recibido por JSON a cada fila creada
    "createdRow": function(row, data, dataIndex) {
      $(row).prop('id', data.id);
    },
    // Modificar DOM haciendo espacio para botón Nuevo
    "dom": "<'row'<'col-sm-1'<'#btn-new-programa'>><'col-sm-5'l><'col-sm-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-6'i><'col-sm-6'p>>",
		"language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
	});
	
	/**
   * Botones de Programas Datatable
   *
   */	
  // New
	$('#btn-new-programa').html('<a href="#" data-toggle="modal" data-target="#modal-programas"class="btn btn-primary btn-sm" id="table-btn-new"><i class="fa fa-fw fa-plus"></i>Nuevo</a>');
	$('#btn-new-programa').on('click', function() {
    // Atributos para que el modal sea de creación
    $('.modal-title').html('<i class="fa fa-plus fa-fw"></i>Nuevo Programa');
    // baseURL es creado en master layout con la funcion helper "URL::to('/')"
    var newAction = baseURL + '/programas/store';
    $('#form-programas').attr('action', newAction);

    // Reiniciar el formulario
    // Convertir objeto jQuery a objeto JS nativo para usar reset()
    $("#form-programas")[0].reset();
  });
  // Edit
	$('#table-programas').on('click', '.btn-edit', function(){
		// Atributos para que el modal sea de edición
    $('.modal-title').html('<i class="fa fa-pencil fa-fw"></i>Editar Programa');
    // El controlador espera el id
		var id = $(this).closest('tr').prop('id');
    // baseURL es creado en master layout con la funcion helper "URL::to('/')"
    var newAction = baseURL + '/programas/update/' + id;
    $('#form-programas').attr('action', newAction);
		
    var url = 'edit' + '/' + id;
		$.get(url, function(data){
			$('input[name=programa]').val(data.programa);
			$('select[name=tipo_programa]').val(data.tipo);
			$('select[name=modalidad]').val(data.modalidad);
			$('select[name=admision]').val(data.admision);
			$('input[name=fecha_inicio]').val(data.fecha_inicio);
      $('input[name=fecha_fin]').val(data.fecha_fin);
      
      var estatus = data.estatus;
      console.log(estatus);
      if (estatus == 'ACTIVO') {
        $('input[name=estatus]').prop('checked', true);
      } else if (estatus == 'INACTIVO') {
        $('input[name=estatus]').prop('checked', false);
      }
		});
	});
	
  // Eliminar
	$('#table-programas').on('click', '.btn-delete', function(){
		// El controlador espera el id
		var id = $(this).closest('tr').prop('id');
		var url = 'destroy/' + id;
		$.get(url);
		//dt_programas.ajax.reload();
	});

//--------------------------------------------------------------------------------

	/**
	 * Académicos Datatable
	 *
   */
	var dt_academicos = $('#table-academicos').DataTable({
		"ajax": {
      "processing": true,
      "url": baseURL + "/maestros/maestros",
      "type": "GET",
    },
    "stateSave": true,
    "responsive": true,
    "columns": [
      {"data": "nombre"},
      {"data": "a_paterno"},
      {"data": "a_materno"},
      {"data": "titulo_profesional"},
      {"data": "unidad"},
      {"data": "email"},
      {"data": "telefono"},
      {
        // Agregar una ultima columna con botones de edición y eliminación
        "data": null,
        "searchable": false,
        "orderable": false,
        "defaultContent": '<div class="btn-group" role="group" aria-label="...">'
                      + '<a href="#" class="btn btn-primary btn-xs btn-edit" data-toggle="modal" data-target="#modal-academicos"><i class="fa fa-fw fa-pencil"></i></a>'
                      + '<a href="#" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-fw fa-times"></i></a>'
                      + '</div>',
      },
    ],
    // Agregar id recibido por JSON a cada fila creada
    "createdRow": function(row, data, dataIndex) {
      $(row).prop('id', data.id);
    },
    // Modificar DOM haciendo espacio para botón Nuevo
    "dom": "<'row'<'col-sm-1'<'#btn-new-academico'>><'col-sm-5'l><'col-sm-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-6'i><'col-sm-6'p>>",
    "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
	});

  /**
   * Botones de Académicos Datatable
   *
   */
  // Nuevo
  $('#btn-new-academico').html('<a href="#" data-toggle="modal" data-target="#modal-academicos"class="btn btn-primary btn-sm" id="table-btn-new"><i class="fa fa-fw fa-plus"></i>Nuevo</a>');
  $('#btn-new-academico').on('click', function() {
    // Atributos para que el modal sea de creación
    $('.modal-title').html('<i class="fa fa-plus fa-fw"></i>Nuevo Académico');
    // baseURL es creado en master layout con la funcion helper "URL::to('/')"
    var newAction = baseURL + '/maestros/store';
    $('#form-academicos').attr('action', newAction);

    // Reiniciar el formulario
    // Convertir objeto jQuery a objeto JS nativo para usar reset()
    $("#form-academicos")[0].reset();
    // Vaciar el select dinamico de municipios
    $('select[name=ciudad]').empty();
  });
  
  // Editar
  $('#table-academicos').on('click', '.btn-edit', function(){
    // Atributos para que el modal sea de edición
    $('.modal-title').html('<i class="fa fa-pencil fa-fw"></i>Editar Académico');
    // El controlador espera el id
    var id = $(this).closest('tr').prop('id');
    // baseURL es creado en master layout con la funcion helper "URL::to('/')"
    var newAction = baseURL + '/maestros/update/' + id;
    $('#form-academicos').attr('action', newAction);

    var url = 'edit' + '/' + id;
    $.get(url, function(data){
      $('input[name=nombre]').val(data.nombre);
      $('input[name=a_paterno]').val(data.a_paterno);
      $('input[name=a_materno]').val(data.a_materno);
      $('select[name=sexo]').val(data.sexo);
      $('input[name=fecha_nac]').val(data.fecha_nac);
      $('input[name=email]').val(data.email);
      $('select[name=nacionalidad]').val(data.id_nacionalidad);
      $('select[name=estado]').val(data.id_estado);
      select_municipio(data.id_estado, data.id_municipio);
      $('input[name=titulo_profesional]').val(data.titulo_profesional);
      $('select[name=unidad_academica]').val(data.id_unidad);
      $('input[name=calle]').val(data.calle);
      $('input[name=num_ext]').val(data.num_ext);
      $('input[name=num_int]').val(data.num_int);
      $('input[name=colonia]').val(data.colonia);
      $('input[name=cp]').val(data.cp);
      $('input[name=telefono]').val(data.telefono);
      $('input[name=movil]').val(data.movil);
      $('input[name=rfc]').val(data.rfc);
    });
  });

  // Eliminar
  $('#table-academicos').on('click', '.btn-delete', function(){
    // El controlador espera el id
    var id = $(this).closest('tr').prop('id');
    var url = 'destroy/' + id;
    $.get(url);
    //dt_programas.ajax.reload();
  });

//--------------------------------------------------------------------------------

  /**
   * Aspirantes Datatable
   *
   */
  var dt_aspirantes = $('#table-aspirantes').DataTable({
    "ajax": {
      "processing": true,
      "url": baseURL + "/aspirantes/aspirantes",
      "type": "GET",
    },
    "stateSave": true,
    "responsive": true,
    "columns": [
      {"data": "nombre"},
      {"data": "a_paterno"},
      {"data": "a_materno"},
      {"data": "programa"},
      {"data": "unidad"},
      {"data": "estatus"},
      {
        // Agregar una ultima columna con botones de aceptar y rechazar
        "data": null,
        "searchable": false,
        "orderable": false,
        "defaultContent": '<div class="btn-group" role="group" aria-label="...">'
                      //+ '<a href="#" class="btn btn-primary btn-xs btn-accept">Aceptar</a>'
                      + '<a href="#" class="btn btn-primary btn-xs btn-accept" data-toggle="modal" data-target="#modal-aspirantes">Aceptar</a>'
                      + '<a href="#" class="btn btn-danger btn-xs btn-reject">Rechazar</a>'
                      + '</div>',
      },
    ],
    // Agregar id recibido por JSON a cada fila creada
    "createdRow": function(row, data, dataIndex) {
      $(row).prop('id', data.id);
    },
    "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
  });

  /**
   * Botones de Aspirantes Datatable
   *
   */
  // Aceptar
  $('#table-aspirantes').on('click', '.btn-accept', function(){
    // Atributos para que el modal sea de aceptación de aspirante
    $('.modal-title').html('<i class="fa fa-check fa-fw"></i>Aceptar aspirante');
    // El controlador espera el id del aspirante
    var id = $(this).closest('tr').prop('id');
    var url = baseURL +'/api/grupos/' + id;
    $.get(url, function(data){
      // Asegurarse que el select esté vacío
      $('select#grupo').empty();
      // Agregar opción por default
      $('select#grupo').append($('<option>').text('Seleccione una opción').attr('value', ''));
      // Agregar resultados de la API para el id del aspirante
      $.each(data, function(i, value) {
        $('select#grupo').append($('<option>').text(value.nombre_grupo).attr('value', value.id_grupo));
      });
      // Agregar id aspirante a campo oculto
      $('input[name=aspirante]').val(data[0].id_aspirante);
      // Agregar id alumno a campo oculto
      $('input[name=alumno]').val(data[0].id_alumno);
    });
  });

  // Rechazar
  $('#table-aspirantes').on('click', '.btn-reject', function(){
    // El controlador espera el id
    var id = $(this).closest('tr').prop('id');
    var url = baseURL +'/aspirantes/reject/' + id;
    $.get(url);
  });

}); // jQuery loaded