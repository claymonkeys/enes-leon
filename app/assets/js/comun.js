/**
 * ENES León
 * comun.js
 * Scripts en común para backend(admin) y frontend(alumnos)
 */


/**
 * Llenar select de municipio y opcionalmente selecciona uno
 *
 * @param  int  estado al que pertenece el municipio
 * @param  int  municipio a seleccionar
 */
function select_municipio(estado, municipio) {
  $.ajax({
    url: baseURL + '/api/municipios/' + estado,
    type: 'get',
    success: function (rta) {
      $('select[name=ciudad]').empty();
      //$('select#ciudad').append("<option value='' disabled selected style='display:none;'>Seleccione un Municipio</option>");
      $('select[name=ciudad]').append('<option value="">Seleccione un municipio</option>');
      $.each(rta, function (index, value) {
        $('select[name=ciudad]').append('<option value="' + value.id + '">' + value.municipio + '</option>');
      });
      if (municipio != null) {
        $('select[name=ciudad]').val(municipio);
      }
    }
  });
}


$(function(){
	
	/** 
	 * Cerrar alerts automaticamente
	 */
	$(".alert").fadeTo(3400, 600).slideUp(600, function(){
  	$(".alert").alert('close');
	});


	/** 
	 * Calendario
	 */
	$('.input-group.date').datepicker({
	    format: "yyyy/mm/dd",
	    startView: 2,
	    language: "es",
	    autoclose: true
	});

	//Llena los tipos de programas en el 'registro de alumnos' y el modulo 'programas'
	// $.ajax({
	// 	url: baseURL + '/api/tipos',
	// 	type: 'get',
	// 	success: function(output){
	// 		var options = '';
	// 		$('#tipo_programa').empty();
 //            $('#tipo_programa').append("<option value='' disabled selected style='display:none;'>Seleccione Tipo</option>");
	// 		for(i = 0; i < output.length; i++){
	// 			options += '<option value="' + output[i].id + '">' + output[i].tipo_programa + '</option>';
	// 		}
	// 		$('#tipo_programa').append(options);
	// 		$('#tipo_programa').trigger('change');
	// 	}
	// });


	//Llena la admision de los programas
	// $.ajax({
	// 	url: baseURL + '/api/admision',
	// 	type: 'get',
	// 	success: function(output){
	// 		var options = '';
	// 		$('#admision').empty();
 //            $('#admision').append("<option value='' disabled selected style='display:none;'>Seleccione Admision</option>");
	// 		for(i = 0; i < output.length; i++){
	// 			options += '<option value="' + output[i].id + '">' + output[i].admision + '</option>';
	// 		}
	// 		$('#admision').append(options);
	// 	}
	// });

	//Llena la modalidad de los programas
	// $.ajax({
	// 	url: baseURL + '/api/modalidades',
	// 	type: 'get',
	// 	success: function(output){
	// 		var options = '';
	// 		$('#modalidad').empty();
 //            $('#modalidad').append("<option value='' disabled selected style='display:none;'>Seleccione modalidad</option>");
	// 		for(i = 0; i < output.length; i++){
	// 			options += '<option value="' + output[i].id + '">' + output[i].modalidad + '</option>';
	// 		}
	// 		$('#modalidad').append(options);
	// 	}
	// });

	// Llena el select de los programas en el registro del alumno dependiendo del tipo de programa seleccionado
	 $("#tipo_programa").change(function () {
			$.ajax({
				url: baseURL + '/api/programas',
	            type: 'get',
	            dataType: 'json',
	            data: {"tipos": $("#tipo_programa").val()},
				success: function(output){
					var options = '';
					$('#programa_academico').empty();
                	$('#programa_academico').append("<option value='' disabled selected style='display:none;'>Seleccione un programa</option>");
					for(i = 0; i < output.length; i++){
						options += '<option value="' + output[i].id + '">' + output[i].programa + '</option>';
					}
					$('#programa_academico').append(options);
					$('#programa_academico').trigger('change');
				}
			});
	    });


	//Llena el select de Nacionalidades en el 'registro del alumno' y del 'Academico'
 // 	$.ajax({
	// 	url: baseURL + '/api/nacionalidades',
	// 	type: 'get',
	// 	success: function(output){
	// 		var options = '';
	// 		$('select#nacionalidad').empty();
 //            $('select#nacionalidad').append("<option value='' disabled selected style='display:none;'>Seleccione una Nacionalidad</option>");
	// 		for(i = 0; i < output.length; i++){
	// 			options += '<option value="' + output[i].id + '">' + output[i].nacionalidad + '</option>';
	// 		}
	// 		$('select#nacionalidad').append(options);
	// 	}
	// });


 	// Llena el select#ciudad con los municipios pertenecientes al estado seleccionado en el 'registro del alumno' y del 'academico'
 	// function populate_municipios(estado) {
	 //  $.ajax({
	 //    url: baseURL + '/api/municipios/' + estado,
	 //    type: 'get',
	 //    success: function (rta) {
	 //      $('select#ciudad').empty();
	 //      $('select#ciudad').append("<option value='' disabled selected style='display:none;'>Seleccione un Municipio</option>");
	 //      $.each(rta, function (index, value) {
	 //        $('select#ciudad').append('<option value="' + value.id + '">' + value.municipio + '</option>');
	 //      });
	 //    }
	 //  });
 	// }


 	//Llena el select de Unidades Academicas
 // 	$.ajax({
	// 	url: baseURL + '/api/unidades',
	// 	type: 'get',
	// 	success: function(output){
	// 		var options = '';
	// 		$('select#unidad_academica').empty();
 //            $('select#unidad_academica').append("<option value='' disabled selected style='display:none;' >Seleccione una Unidad</option>");
	// 		for(i = 0; i < output.length; i++){
	// 			options += '<option value="' + output[i].id + '">' + output[i].unidad + '</option>';
	// 		}
	// 		$('select#unidad_academica').append(options);
	// 	}
	// });


	// Cambia los municipios si se cambia de estado
	$('select[name=estado]').change(function(){
		var estado = $('select[name=estado]').val();
	 	select_municipio(estado, null);
	 });

	// Deshabilitar select de estado y municipio si la nacionalidad es diferente de mexicana
	$('select[name=nacionalidad]').change(function(){
		var nacionalidad = $('select[name=nacionalidad] option:selected').text();
	 	if (nacionalidad != 'MEXICANO(A)') {
	 		$('select[name=estado]').prop('disabled', true);
	 		$('select[name=ciudad]').prop('disabled', true);
	 	} else if (nacionalidad == 'MEXICANO(A)') {
	 		$('select[name=estado]').prop('disabled', false);
	 		$('select[name=ciudad]').prop('disabled', false);
	 	}
	 });


	// Llena los municipios del select
	// $.ajax({
	// 	url: baseURL + '/api/estados',
	// 	type: 'get',
	// 	success: function(output){
	// 		var options = '';
	// 		$('select#estado').empty();
 //      $('select#estado').append("<option value='' disabled selected style='display:none;'>Seleccione un Estado</option>");
	// 		for(i = 0; i < output.length; i++){
	// 			options += '<option value="' + output[i].id + '">' + output[i].estado + '</option>';
	// 		}
	// 		$('select#estado').append(options);
	// 	}
	// });

	// Llena los maestros del select
	$.ajax({
		url: baseURL + '/api/academicos',
		type: 'get',
		success: function(output){
			var options = '';
			$('select#maestro').empty();
      $('select#maestro').append("<option value='' disabled selected style='display:none;'>Seleccione maestro</option>");
			for(i = 0; i < output.length; i++){
				options += '<option value="' + output[i].id + '">' + output[i].a_paterno + " " + output[i].a_materno + " " + output[i].nombre + '</option>';
			}
			$('select#maestro').append(options);
		}
	});


	// Llena los maestros del select
	$.ajax({
		url: baseURL + '/api/programasGrupo',
		type: 'get',
		success: function(output){
			var options = '';
			$('select#programaGrupo').empty();
      $('select#programaGrupo').append("<option value='' disabled selected style='display:none;'>Seleccione programa</option>");
			for(i = 0; i < output.length; i++){
				options += '<option value="' + output[i].id + '">' + output[i].programa + '</option>';
			}
			$('select#programaGrupo').append(options);
		}
	});



//---------------------------------Script Editar Alumno------------------------------------------------

	//Carga la nacionalidad del alumno al consultar sus datos y editarlos
	function select_nacionalidad() {
		$.ajax({
			url: baseURL + '/alumnos/nacionalidad',
			type: 'get',
			success: function(output){
				$('select#edit_nacionalidad').val(output);
			}
		});
	}


	//Llena las nacionalidades en el select al editar los datos de un alumno
 // 	$.ajax({
	// 	url: baseURL + '/api/nacionalidades',
	// 	type: 'get',
	// 	success: function(output){
	// 		var options = '';
	// 		$('select#edit_nacionalidad').empty();
 //            $('select#edit_nacionalidad').append("<option value='' disabled selected style='display:none;' >Seleccione una Nacionalidad</option>");
	// 		for(i = 0; i < output.length; i++){
	// 			options += '<option value="' + output[i].id + '">' + output[i].nacionalidad + '</option>';
	// 		}
	// 		$('select#edit_nacionalidad').append(options);
	// 		select_nacionalidad();
	// 	}
	// });

	// Carga el Municipio correspondiente al alumno al editar sus datos
	function select_municipio_alumno() {
		$.ajax({
			url: baseURL + '/alumnos/municipio',
			type: 'get',
			success: function(output){
				$('select#ciudad').val(output);
			}
		});
	}


 	// Llena los municipios del select al editar los datos del alumno
 	function populate_municipios_alumno(estado) {
	  $.ajax({
	    url: baseURL + '/api/municipios/' + estado,
	    type: 'get',
	    success: function (rta) {
	      $('select#ciudad').empty();
	      $('select#ciudad').append("<option value='' disabled selected style='display:none;' >Seleccione un Municipio</option>");
	      $.each(rta, function (index, value) {
	        $('select#ciudad').append('<option value="' + value.id + '">' + value.municipio + '</option>');
	      });
	    	select_municipio_alumno(); 
	    }
	  });
 	}


  // Carga el Estado del alumno al editar sus datos
	// $.ajax({
	// 	url: baseURL + '/alumnos/estado',
	// 	type: 'get',
	// 	success: function(output){
	// 		$('select#estado').val(output);
	// 		populate_municipios_alumno(output);
	// 	}
	// });




//Script temporal de prueba que carga los datos para editar del maestro.--------------------------------------------------------


	// 	function select_nacionalidad_maestro() {
	// 	$.ajax({
	// 		url: baseURL + '/maestros/nacionalidad',
	// 		type: 'get',
	// 		data: {id: $("#id_academico").val()},
	// 		success: function(output){
	// 			$('select#nacionalidad').val(output);
	// 		}
	// 	});
	// }
	

	// Populate Nacionalidades Select
 // 	$.ajax({
	// 	url: baseURL + '/api/nacionalidades',
	// 	type: 'get',
	// 	success: function(output){
	// 		var options = '';
	// 		$('select#nacionalidad').empty();
 //            $('select#nacionalidad').append("<option value='' disabled selected style='display:none;' >Seleccione una Nacionalidad</option>");
	// 		for(i = 0; i < output.length; i++){
	// 			options += '<option value="' + output[i].id + '">' + output[i].nacionalidad + '</option>';
	// 		}
	// 		$('select#nacionalidad').append(options);
	// 		select_nacionalidad_maestro();
	// 	}
	// });





 	// Populate select#edit_ciudad with municipios of Maestro belonging to estado
 	function populate_municipios_maestro(estado) {
	  $.ajax({
	    url: baseURL + '/api/municipios/' + estado,
	    type: 'get',
	    success: function (rta) {
	      $('select#edit_ciudad').empty();
	      $('select#edit_ciudad').append("<option value='' disabled selected style='display:none;' >Seleccione un Municipio</option>");
	      $.each(rta, function (index, value) {
	        $('select#edit_ciudad').append('<option value="' + value.id + '">' + value.municipio + '</option>');
	      });
	    	select_municipio_maestro();
	    }
	  });
 	}


 	// Populate select#edit_municipio with municipios of Maestro belonging to estado
	function select_municipio_maestro() {
		$.ajax({
			url: baseURL + '/maestros/municipio',
			type: 'get',
			data: {id: $("#id_academico").val()},
			success: function(output){
				$('select#edit_ciudad').val(output);
			}
		});
	}


  // Load the estado corresponding with the DB for Maestros(called only once)
	function select_estado_maestro() {
		$.ajax({
			url: baseURL + '/maestros/estado',
			type: 'get',
			data: {id: $("#id_academico").val()},
			success: function(output){
				$('select#edit_estado').val(output);
				populate_municipios_maestro(output);
			}
		});
	}


 	// Populate select#edit_estados with estados
 //  	$.ajax({
	// 	url: baseURL + '/api/estados',
	// 	type: 'get',
	// 	success: function(output){
	// 		var options = '';
	// 		$('select#edit_estado').empty();
 //      		$('select#edit_estado').append("<option value='' disabled selected style='display:none;' >Seleccione un Estado</option>");
	// 		for(i = 0; i < output.length; i++){
	// 			options += '<option value="' + output[i].id + '">' + output[i].estado + '</option>';
	// 		}
	// 		$('select#edit_estado').append(options);
	// 		select_estado_maestro();
	// 	}
	// });


	//  $.ajax({
	// 	url: baseURL + '/api/unidades',
	// 	type: 'get',
	// 	success: function(output){
	// 		var options = '';
	// 		$('select#unidad_academica_edit').empty();
 //            $('select#unidad_academica_edit').append("<option value='' disabled selected style='display:none;' >Seleccione una Unidad</option>");
	// 		for(i = 0; i < output.length; i++){
	// 			options += '<option value="' + output[i].id + '">' + output[i].unidad + '</option>';
	// 		}
	// 		$('select#unidad_academica_edit').append(options);
	// 		select_unidad_maestro();
	// 	}
	// });



 	// Populate select#unidad_academica with unidad academica of Maestro
	function select_unidad_maestro(){
		$.ajax({
			url: baseURL + '/maestros/unidad',
			type: 'get',
			data: {id: $("#id_academico").val()},
			success: function(output){
				$('select#unidad_academica_edit').val(output);
			}
		});
	}


	$(document).ready(function() {
        $("#registro").bind("keypress", function(e) {
            if (e.keyCode == 13) {
                return false;
     	    }
        });
    });

}); //jQuery loaded
