/**
 * ENES León
 * alumnos.js
 * Scripts para frontend(alumnos)
 */

$(function(){

  /**
	 * Wizard registro alumnos
	 */
	$('#rootwizard').bootstrapWizard({
		'nextSelector': '.button-next',
		'previousSelector': '.button-previous',
		// Valida cada paso
		'onNext': function(tab, navigation, index){
			var $valid = $('form#registro').valid();
			if (!$valid) {
				$validator.focusInvalid();
				return false;
			}
		},
		// Deshabilitar clic en las tabs de los pasos
		'onTabClick': function(tab, navigation, index) {
			return false;
		}
	});

	function select_nacionalidad() {
		$.ajax({
			url: baseURL + '/alumnos/nacionalidad',
			type: 'get',
			success: function(output){
				$('select#edit_nacionalidad').val(output);
			}
		});
	}

	//Llena las nacionalidades en el select al editar los datos de un alumno
 	$.ajax({
		url: baseURL + '/api/nacionalidades',
	 	type: 'get',
	 	success: function(output){
			var options = '';
	 		$('select#edit_nacionalidad').empty();
            $('select#edit_nacionalidad').append("<option value='' disabled selected style='display:none;' >Seleccione una Nacionalidad</option>");
			for(i = 0; i < output.length; i++){
				options += '<option value="' + output[i].id + '">' + output[i].nacionalidad + '</option>';
 		}
	 		$('select#edit_nacionalidad').append(options);
	 		select_nacionalidad();
		}
	 });


 // Carga el Municipio correspondiente al alumno al editar sus datos
	function select_municipio_alumno() {
		$.ajax({
			url: baseURL + '/alumnos/municipio',
			type: 'get',
			success: function(output){
				$('select#ciudad').val(output);
			}
		});
	}


 	// Llena los municipios del select al editar los datos del alumno
 	function populate_municipios_alumno(estado) {
	  $.ajax({
	    url: baseURL + '/api/municipios/' + estado,
	    type: 'get',
	    success: function (rta) {
	      $('select#ciudad').empty();
	      $('select#ciudad').append("<option value='' disabled selected style='display:none;' >Seleccione un Municipio</option>");
	      $.each(rta, function (index, value) {
	        $('select#ciudad').append('<option value="' + value.id + '">' + value.municipio + '</option>');
	      });
	    	select_municipio_alumno(); 
	    }
	  });
 	}

  // Carga el Estado del alumno al editar sus datos
	 $.ajax({
	 	url: baseURL + '/alumnos/estado',
	 	type: 'get',
	 	success: function(output){
	 		$('select#estado').val(output);
	 		populate_municipios_alumno(output);
	 	}
	 });


  /**
   * Validación del formulario de registro de alumnos
   */
	$.validator.messages.required = 'Este campo es requerido';
	var $validator = $('form#registro').validate({
		errorElement: 'span',
		errorClass: 'error text-danger',
		submitHandler: function(form) {
    	// Deshabilitar boton de submit
    	$('form#registro input[type=submit]').prop('disabled', true);
    	// Cambiar texto de boton submit
    	$('form#registro input[type=submit]').val('Por favor espere...');
    	// Enviar formulario
    	form.submit();
    },
		rules: {
			unidad_academica: {
				required: true
			},
			tipo_programa: {
				required: true
			},
			programa_academico: {
			 	required: true
			},
			como_se_entero: {
				required: true
			},
			nombre: {
				required: true
			},
			a_paterno: {
				required: true
			},
			a_materno: {
				required: true
			},
			sexo: {
				required: true
			},
			nacionalidad: {
				required: true
			},
			email: {
				required: true,
				email: true,
				remote: baseURL + '/registro/check'
			},
			fecha_nac: {
				required: true
			},
			estado: {
				required: true
			},
			ciudad: {
				required: true
			},
			nivel_estudios: {
				required: true
			},
			termino_estudios: {
				required: true
			},
			perfil_profesional: {
				required: true
			},
			institucion_estudios: {
				required: true
			},
			calle: {
				required: true
			},
			num_ext: {
				required: true
			},
			colonia: {
				required: true
			},
			telefono: {
				required: true
			},
			terminos: {
				required: true
			},
			file: {
				required: true
			},
			file2: {
				required: true
			},
			file3: {
				required: true
			},
			file4: {
				required: true
			},
		},
		messages: {
			email: {
				required: 'El correo electrónico es requerido',
				email: 'No es un correo electrónico válido',
				remote: 'Este correo electrónico ya ha sido registrado'
			}
		}
	});



}); // jQuery loaded