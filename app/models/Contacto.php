<?php

class Contacto extends \Eloquent {
	protected $table = 'contacto';

	protected $primaryKey = 'id_contacto';
    protected $fillable = [];

	public function alumno()
    {
        return $this->belongsTo('Alumno');
    }

}