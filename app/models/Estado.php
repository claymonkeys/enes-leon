<?php

class Estado extends \Eloquent {
	protected $table = 'estados';

		public function municipio() {
        return $this->hasMany('Municipio');
    	}
}