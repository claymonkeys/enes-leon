<?php

class Aspirante extends Eloquent {
		
	protected $table = 'aspirantes';

		public function alumno() {
        return $this->belongsTo('Alumno');
    	}

 		public function programa() {
        return $this->belongsTo('Programa');
    	}
}