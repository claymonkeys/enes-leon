<?php

class Alumno extends Eloquent {
		
	protected $table = 'alumnos';
	protected $hidden = array('password');

		public function contacto() {
        return $this->belongsTo('Contacto');
    	}
}