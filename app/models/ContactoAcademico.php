<?php

class ContactoAcademico extends \Eloquent {
	protected $table = 'contacto_academicos';

	protected $primaryKey = 'id_contacto';
    protected $fillable = [];

	public function maestro()
    {
        return $this->belongsTo('Maestro');
    }

}