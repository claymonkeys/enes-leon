<?php

class Grupo extends \Eloquent {
	protected $table = 'grupos';

		public function academico() {
        return $this->belongsTo('Maestro');
    	}

    	public function programa() {
        return $this->belongsTo('Programa');
    	}
}