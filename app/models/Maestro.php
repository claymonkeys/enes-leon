<?php

class Maestro extends \Eloquent {
	protected $table = 'academicos';

	public function contacto() {
        return $this->belongsTo('ContactoAcademico');
   	}
}