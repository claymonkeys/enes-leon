<?php

class Programa extends \Eloquent {
	protected $table = 'programas';

	public function aspirante() {
        return $this->belongsTo('Aspirante');
    }
}