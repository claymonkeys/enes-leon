<?php

class GrupoAlumno extends Eloquent {
		
	protected $table = 'grupos-alumnos';

		public function alumno() {
        return $this->belongsTo('Alumno');
    	}

 		public function programa() {
        return $this->belongsTo('Grupo');
    	}
}