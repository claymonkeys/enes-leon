<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Ruta Home despues de login
Route::get('/', ['as'=>'home', function(){
	if (Auth::user()->privilegios == 2){
        return View::make('admin.home');
    }else{
        return View::make('alumnos.index');
    }
}])->before('auth');

//ruta de los alumnos despues de login 
Route::get('alumnos', ['as'=>'alumnos', function(){
	return View::make('alumnos.index');
}])->before('auth');

//Rutas Validas despues de login
Route::group(array('before'=>'auth'), function() {   
        Route::controller('programas', 'ProgramasController');
        Route::controller('maestros', 'MestrosController');
        Route::controller('index', 'AlumnosController');
        Route::controller('alumnos', 'AlumnosController');
        Route::controller('aspirantes', 'AspirantesController');
        Route::controller('grupos', 'GruposController');
        
        // Alias entrevistas
        Route::get('entrevistas/pendientes', "EntrevistaController@getIndex");
        Route::controller('entrevistas', 'EntrevistaController');
});

//Ruta de registro de alumnos
Route::controller('registro', 'RegistroController');

//Recuperar contraseña
Route::get('recuperar', ['as'=>'recuperar', function(){
	return View::make('restaurar');
}]);
Route::post('recuperar', 'AlumnosController@recuperar');

//Registrar Alumno
Route::post('guardar','AlumnosController@anyStore');

//Registrar Alumno en grupo
Route::post('aceptar', 'AlumnoGruposController@anyCreate');

//Methodos Validos para las sesiones
Route::get('login', 'SessionsController@create');
Route::get('logout', 'SessionsController@destroy');
Route::resource('sessions', 'SessionsController', ['only'=>['index', 'create', 'store', 'destroy']]);


//JSON API  URLs
Route::get('api/programas','ApiController@getProgramas');
Route::get('api/estados','ApiController@getEstados');
Route::get('api/municipios/{id_estado}','ApiController@getMunicipios');
Route::get('api/tipos','ApiController@getTipos');
Route::get('api/nacionalidades','ApiController@getNacionalidades');
Route::get('api/modalidades','ApiController@getModalidades');
Route::get('api/admision','ApiController@getAdmision');
Route::get('api/unidades','ApiController@getUnidades');
Route::get('api/unidad/{id_academico}','ApiController@getUnidad');
Route::get('api/academicos','ApiController@getMaestros');
Route::get('api/programasGrupo','ApiController@getProgramaGrupo');
Route::get('api/mailValidacion/{email}','ApiController@anyValidacion');
Route::get('api/grupos/{aspirante_id}','ApiController@anyGrupos');
