<?php

class RegistroController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /registro
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		// Arrays para llenar selects estáticos
		$tipos = DB::table('tipos')->join('programas','programas.tipo','=','tipos.id')
		->select('tipos.id as id','tipos.tipo_programa as tipo_programa')
		->where('programas.estatus','ACTIVO')
		->groupBy('tipos.tipo_programa')->orderBy('id', 'asc')->lists('tipo_programa', 'id');
		$modalidad = DB::table('modalidad')->orderBy('id', 'asc')->lists('modalidad', 'id');
		$admision = DB::table('admision')->orderBy('id', 'asc')->lists('admision', 'id');
		$unidades = DB::table('unidades')->orderBy('id', 'asc')->lists('unidad', 'id');
		$nacionalidades = DB::table('nacionalidades')->orderBy('id', 'asc')->lists('nacionalidad', 'id');
		$estados = DB::table('estados')->orderBy('id', 'asc')->lists('estado', 'id');
		$programas = DB::table('programas')->where('estatus','Activo')->orderBy('created_at', 'asc')->get();

		// Opción por default de los select
		$default = array(''=>'Seleccione una opción');
		
		// Concatenar opcion por default
		$tipos = $default + $tipos;
		$modalidad = $default + $modalidad;
		$admision = $default + $admision;
		$unidades = $default + $unidades;
		$nacionalidades = $default + $nacionalidades;
		$estados = $default + $estados;
		$programas = $default + $programas;
		
		return View::make('registro')->with(compact('tipos', 'modalidad', 'admision', 'unidades', 'estados', 'nacionalidades', 'programas'));

	}

	// Función llamada por jquery validator para el campo email
	public function getCheck() {
		// True, el email es valido
		$result = true;
		if (DB::table('users')->where('email',Input::get('email'))
            ->orderBy('created_at', 'desc')->first()) {
			// False, el email ya existe
			$result = false;
		}
	return Response::json($result);
	}

}