<?php

class AlumnosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /alumnos
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('alumnos.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /alumnos/create
	 *
	 * @return Response
	 */
	public function anyRegistro()
	{
		$contacto=DB::table('contacto')->where('email',Auth::user()->email)->first();

		$aspirante= new Aspirante;
		$aspirante->id_alumno=$contacto->id_alumno;
		$aspirante->id_programa=Input::get('programa_academico');

		$programa = Programa::find(Input::get('programa_academico'));

		$tipos = DB::table('tipos')->orderBy('id', 'asc')->lists('tipo_programa', 'id');
		$default = array(''=>'Seleccione una opción');

		$tipos = $default + $tipos;

		//Avisamos que se guardo
		if ($aspirante->save()) {
			Session::flash('message', "Se ha inscrito al programa ".$programa->programa." correctamente");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		return View::make('alumnos.cursos')->with(compact('tipos'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /alumnos
	 *
	 * @return Response
	 */
	public function anyStore()
	{
		//Agregamos el usuario
		$usuario=new User;

		$password=str_random(12);
		$hashedPassword=Hash::make($password);

		$usuario->username=Input::get('email');
		$usuario->email=Input::get('email');
		$usuario->password=$hashedPassword;
		$usuario->privilegios=1;

		if ($usuario->save()) {
        	$usuarioGuardado = true;
        	$this->email(Input::get('email'),$password,Input::get('nombre'),Input::get('unidad_academica'));
        }

        //Creamos al alumno
		$alumno=new Alumno;

		$alumno->nombre=strtoupper(Input::get('nombre'));
		$alumno->a_paterno=strtoupper(Input::get('a_paterno'));
		$alumno->a_materno=strtoupper(Input::get('a_materno'));
		$alumno->sexo=strtoupper(Input::get('sexo'));
		$alumno->fecha_nac=Input::get('fecha_nac');
		$alumno->unidad_academica=Input::get('unidad_academica');
		$alumno->nivel_estudios=Input::get('nivel_estudios');
		$alumno->perfil_profesional=strtoupper(Input::get('perfil_profesional'));
		$alumno->termino_estudios=Input::get('termino_estudios');
		$alumno->institucion_estudios=strtoupper(Input::get('institucion_estudios'));
		$alumno->facturacion=Input::get('facturacion');
		$alumno->como_se_entero=Input::get('como_se_entero');

		$nombre=Input::get('a_paterno').' '.Input::get('a_materno').' '.Input::get('nombre');
		$email=Input::get('email');

        if ($alumno->save()) {
        	$alumnoGuardado = true;
        }

        //Recuperamos el id del alumno
        $id_temp = DB::table('alumnos')->where('nombre',Input::get('nombre'))
        ->orderBy('created_at', 'desc')->first();

        //Subimos la documentacion del alumno
        $this->uploadFiles(Input::file('file'),Input::file('file2'),Input::file('file3'),Input::file('file4'),$id_temp->id); 
        	
    	//Creamos sus datos de contacto
    	$contacto=new Contacto;
		$contacto->id_alumno=$id_temp->id;
		$contacto->calle=strtoupper(Input::get('calle'));
		$contacto->num_int=Input::get('num_int');
		$contacto->num_ext=Input::get('num_ext');
		$contacto->colonia=strtoupper(Input::get('colonia'));
		$contacto->ciudad=strtoupper(Input::get('ciudad'));
		$contacto->estado=Input::get('estado');
		$contacto->cp=Input::get('cp');
		$contacto->nacionalidad=Input::get('nacionalidad');
		$contacto->rfc=strtoupper(Input::get('rfc'));
		$contacto->telefono=Input::get('telefono');
		$contacto->movil=Input::get('movil');
		$contacto->email=Input::get('email');


		if ($contacto->save()) {
			$contactoGuardado = true;
		}

		$aspirante= new Aspirante;
		$aspirante->id_alumno=$id_temp->id;
		$aspirante->id_programa=Input::get('programa_academico');
		//$aspirante->unidad_academica=Input::get('unidad_academica');

		$aspirante->save();

		//Avisamos que se guardo
		if ($usuarioGuardado and $alumnoGuardado and $contactoGuardado) {
			Session::flash('message', "Registro Completo, Se enviaran los datos de acceso a tu correo.");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		return Redirect::to('/login');
	}

	/**
	 * Display the specified resource.
	 * GET /alumnos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /alumnos/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyEditdatos()
	{
		$contacto=DB::table('contacto')->where('email',Auth::user()->email)
	        ->orderBy('created_at', 'desc')->first();
		$alumno = DB::table('alumnos')->where('id',$contacto->id_alumno)
	       ->orderBy('created_at', 'desc')->first();

	 	// Arrays para llenar selects estáticos
		$estados = DB::table('estados')->orderBy('id', 'asc')->lists('estado', 'id');

		// Opción por default de los select
		$default = array(''=>'Seleccione una opción');
		
		// Concatenar opcion por default
		$estados = $default + $estados;

		return View::make('alumnos.editDatos')->with(compact('alumno', 'contacto', 'estados'));
	}

	public function anyEditfacturacion()
	{
		$contacto=DB::table('contacto')->where('email',Auth::user()->email)
	    ->orderBy('created_at', 'desc')->first();

		$alumno = DB::table('alumnos')->where('id',$contacto->id_alumno)
	       ->orderBy('created_at', 'desc')->first();

	   	return View::make('alumnos.editFacturacion')->with(compact('alumno'))->with(compact('contacto'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /alumnos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyUpdatedatos()
	{
		$contactoTemp=DB::table('contacto')->where('email',Auth::user()->email)
	    	->orderBy('created_at', 'desc')->first();

		$alumno = DB::table('alumnos')->where('id',$contactoTemp->id_alumno)
	       ->orderBy('created_at', 'desc')->first();


	    //Subimos la documentacion del alumno
	        $this->uploadFiles(Input::file('file'),Input::file('file2'),Input::file('file3'),Input::file('file4'),$alumno->id); 
	        

	    $usuario= User::find(Auth::user()->id);

		$usuario->email=Input::get('email');

		if ($usuario->save()) {
    		$usuarioGuardado = true;
	    }

		$alumno=Alumno::find($alumno->id);

		$alumno->nombre=strtoupper(Input::get('nombre'));
		$alumno->a_paterno=strtoupper(Input::get('a_paterno'));
		$alumno->a_materno=strtoupper(Input::get('a_materno'));
		$alumno->sexo=strtoupper(Input::get('sexo'));
		$alumno->fecha_nac=strtoupper(Input::get('fecha_nac'));
		$alumno->nivel_estudios=strtoupper(Input::get('nivel_estudios'));
		$alumno->perfil_profesional=strtoupper(Input::get('perfil_profesional'));
		$alumno->termino_estudios=strtoupper(Input::get('termino_estudios'));
		$alumno->institucion_estudios=strtoupper(Input::get('institucion_estudios'));

	    if ($alumno->save()) {
	    	$alumnoGuardado = true;
	    }	
	        
	    $contacto = Contacto::find($contactoTemp->id_contacto);

		$contacto->ciudad=Input::get('ciudad');
		$contacto->estado=Input::get('estado');
		$contacto->nacionalidad=Input::get('nacionalidad');
		$contacto->email=Input::get('email');

		if ($contacto->save()) {
			$contactoGuardado = true;
		}

	    if ($alumnoGuardado and $contactoGuardado and $usuarioGuardado) {
			Session::flash('message', "Datos actualizados correctamente.");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		return Redirect::to('alumnos/datos');
	}

	
	public function anyUpdatefacturacion()
	{
		$contactoTemp=DB::table('contacto')->where('email',Auth::user()->email)
	        ->orderBy('created_at', 'desc')->first();

		$alumno = DB::table('alumnos')->where('id',$contactoTemp->id_alumno)
	       ->orderBy('created_at', 'desc')->first();

	    $alumno=Alumno::find($alumno->id);
	    if (Input::get('facturacion')==1) {
	    	$alumno->facturacion=1;
	    }else{
	    	$alumno->facturacion=0;
	    }
	    	

	    if ($alumno->save()) {
	       	$alumnoGuardado = true;
	    }
	        
	    $contacto = Contacto::find($contactoTemp->id_contacto);

		$contacto->calle=strtoupper(Input::get('calle'));
		$contacto->num_int=strtoupper(Input::get('num_int'));
		$contacto->num_ext=Input::get('num_ext');
		$contacto->colonia=Input::get('colonia');
		$contacto->cp=Input::get('cp');
		$contacto->rfc=strtoupper(Input::get('rfc'));
		$contacto->telefono=Input::get('telefono');
		$contacto->movil=Input::get('movil');

		if ($contacto->save()) {
			$contactoGuardado = true;
		}

	    if ($alumnoGuardado and $contactoGuardado) {
			Session::flash('message', "Datos actualizados correctamente.");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		return Redirect::to('alumnos/facturacion');
	}


	public function getDatos()
	{
		$contacto=DB::table('contacto')->where('email',Auth::user()->email)
	        ->orderBy('created_at', 'desc')->first();

		$estado = DB::table('estados')->where('id',$contacto->estado)->first();
		$municipio = DB::table('municipios')->where('id',$contacto->ciudad)->first();
		$nacionalidad = DB::table('nacionalidades')->where('id',$contacto->nacionalidad)->first();

		$alumno = DB::table('alumnos')->where('id',$contacto->id_alumno)
	       ->orderBy('created_at', 'desc')->first();

	    $unidad = DB::table('unidades')->where('id',$alumno->unidad_academica)->first();

		return View::make('alumnos.datos')->with(compact('alumno','contacto','estado','municipio','nacionalidad','unidad'));
	}

	
	public function getFacturacion()
	{
		$contacto=DB::table('contacto')->where('email',Auth::user()->email)
	    ->orderBy('created_at', 'desc')->first();

		$alumno = DB::table('alumnos')->where('id',$contacto->id_alumno)
	       ->orderBy('created_at', 'desc')->first();

		return View::make('alumnos.facturacion')->with(compact('alumno'))->with(compact('contacto'));
	}

	public function getCursos()
	{
		$tipos = DB::table('tipos')->orderBy('id', 'asc')->lists('tipo_programa', 'id');
		$default = array(''=>'Seleccione una opción');

		$tipos = $default + $tipos;

		return View::make('alumnos.cursos')->with(compact('tipos'));
	}

	public function getEventos()
	{
		return View::make('alumnos.eventos');
	}

	public function getPagos()
	{
		return View::make('alumnos.Pagos');
	}

	public function getResumen()
	{
		return View::make('alumnos.Resumen');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /alumnos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$alumno = Alumnos::find($id);

		if ($alumno->delete()) {
			Session::flash('message', "El alumno ha sido eliminado satisfactoriamente.");
			Session::flash('class', 'warning');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido.');
			Session::flash('class', 'danger');
		}

		return Redirect::to('/');
	}

	//Funcion de envio de correos
	public function email($correo,$password,$nombre,$unidad){

		//Decidimos de que host enviar el mail

		$sendName ="";

		//Host San Miguel
		if ($unidad==2) {
			$hostMail="smtp.mandrillapp.com";
			$sendMail="azael_dg@hotmail.com";
			$sendName="ENES - San Miguel";
			$sendPassword="SxFrf3XHZFIY9sLRlsY0Lg";
			$mailTemplate="emails.registro_sma";
		}
		
		//Host Leon
		if ($unidad==1) {
			$hostMail="smtp.mandrillapp.com";
			$sendMail="azael_dg@hotmail.com";
			$sendName="ENES - León";
			$sendPassword="SxFrf3XHZFIY9sLRlsY0Lg";
			$mailTemplate="emails.registro_leon";
		}


		//Cambiamos datos del mail host
		$transport = Mail::getSwiftMailer()->getTransport();
		$transport->setHost($hostMail);
		$transport->setEncryption('tls');
		Mail::alwaysFrom('educacion.continua@enes.unam.mx',$sendName);
		$transport->setEncryption('tls');
		$transport->setUsername($sendMail);
		$transport->setPassword($sendPassword);
		$swift = new Swift_Mailer($transport);

		Mail::setSwiftMailer($swift);


		//Tomamos los parametros para el envio
		$data=array(
			'username'=>$correo,
			'password'=>$password,
		);

		//Agarramos al destinatario
		$destinatario=$correo;


		//Enviamos el mail
		Mail::send($mailTemplate,$data,function($confirm) use ($destinatario,$nombre){
			$confirm->to($destinatario, $nombre)->subject('Educación Continua ENES de la UNAM, registro.');
		});

		return Redirect::to('/');
	}

	public function uploadFiles($file,$file2,$file3,$file4,$id){
		$path = public_path().'/assets/documentacion/'.$id;
		
		if(!File::exists($path)) {
    		File::makeDirectory($path, $mode = 0777, true, true);
		}

		if (isset($file)) {
			$file->move($path, "Titulo Profesional.pdf");
		}	

		if (isset($file2)) {
			$file2->move($path, "INE.pdf");
		}

		if (isset($file3)) {
			$file3->move($path, "Cedula.pdf");
		}	

		if (isset($file4)) {
			$file4->move($path, "CV.pdf");
		}		
	}

	public function recuperar(){

		$email=Input::get('email');

		if (DB::table('users')->where('username',$email)
        ->orderBy('created_at', 'desc')->first()) {

			$id_temp=DB::table('users')->where('username',$email)
        	->orderBy('created_at', 'desc')->first();

        	$usuario=User::find($id_temp->id);

        	$contacto_temp=DB::table('contacto')->where('email',$email)
        	->orderBy('created_at', 'desc')->first();

        	$alumno_temp=DB::table('alumnos')->where('id',$contacto_temp->id_alumno)
        	->orderBy('created_at', 'desc')->first();

        	$password=str_random(12);
			$hashedPassword=Hash::make($password);
			$usuario->password=$hashedPassword;


			if ($usuario->save()) {
	        	$this->email(Input::get('email'),$password,$alumno_temp->nombre,$alumno_temp->unidad_academica);
				Session::flash('message', 'Se ha enviado tu nueva contraseña a tu correo');
				Session::flash('class', 'success');
	        }
			return Redirect::back();


		}else{

			Session::flash('message', 'No existe un usuario con este correo');
			Session::flash('class', 'danger');
			return Redirect::back();
		}
	}

	public function getNacionalidad() {
		if(Auth::user()->privilegios==1){
			$contacto=DB::table('contacto')->where('email',Auth::user()->email)
		        ->orderBy('created_at', 'desc')->first();
		  	return Response::json($contacto->nacionalidad);
	  	}else{
	  		return Response::json(null);
	  	}
	}

	public function getEstado() {
		if(Auth::user()->privilegios==1){
			$contacto=DB::table('contacto')->where('email',Auth::user()->email)
		        ->orderBy('created_at', 'desc')->first();
		  	return Response::json($contacto->estado);
		}else{
	  		return Response::json(null);
	  	}
	}

	public function getMunicipio() {
		if(Auth::user()->privilegios==1){
			$contacto=DB::table('contacto')->where('email',Auth::user()->email)
		        ->orderBy('created_at', 'desc')->first();
		  	return Response::json($contacto->ciudad);
		}else{
	  		return Response::json(null);
	  	}
	}

}