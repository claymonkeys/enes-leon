<?php

class EntrevistaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /alumnos
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$aspirantes = DB::table('aspirantes')
		->join('alumnos', 'aspirantes.id_alumno', '=', 'alumnos.id')
		->join('programas', 'aspirantes.id_programa', '=', 'programas.id')
		->join('contacto', 'contacto.id_alumno', '=', 'alumnos.id')
		->join('unidades', 'alumnos.unidad_academica', '=', 'unidades.id')
		->where('aspirantes.estatus','PENDIENTE')
		->where('programas.admision','1')
		->whereNotIn('aspirantes.id', function($q){
    	$q->select('aspirante')->from('entrevistas');
		})
		->select('alumnos.nombre','alumnos.a_paterno','alumnos.a_materno','unidades.unidad as unidad', 'programas.programa','aspirantes.*','contacto.email')
		->orderBy('aspirantes.created_at', 'asc')->get();

		return View::make('entrevistas.index')->with(compact('aspirantes'));
	}

	public function getProgramadas()
	{
		$entrevistas = DB::table('entrevistas')
		->join('aspirantes', 'aspirantes.id', '=', 'entrevistas.aspirante')
		->join('alumnos', 'aspirantes.id_alumno', '=', 'alumnos.id')
		->select('alumnos.nombre','alumnos.a_paterno','alumnos.a_materno', 'entrevistas.estatus AS estatus_e','aspirantes.*','entrevistas.fecha','entrevistas.hora', 'entrevistas.id AS id')
		->orderBy('created_at', 'asc')
		->orderBy('estatus', 'asc')->get();
		return View::make('entrevistas.programadas')->with(compact('entrevistas'));
	}


	public function getCrear($id)
	{
		$aspirante = DB::table('aspirantes')
		->join('alumnos', 'aspirantes.id_alumno', '=', 'alumnos.id')->where('aspirantes.id',$id)->first();
		return View::make('entrevistas.crear')->with(compact('aspirante'));
	}

	public function getRevisar($id)
	{
		$entrevista=Entrevista::find($id);

		if ($entrevista->estatus=="REALIZADO") {
			$entrevista->estatus="PENDIENTE";
		}else{
			$entrevista->estatus="REALIZADO";
		}

		//Avisamos que se guardo
		if ($entrevista->save()) {
			Session::flash('message', "Se ha actualizado la entrevista correctamente");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		return Redirect::to('entrevistas/programadas');
	}


	public function anyActualizar($id)
	{
		$entrevista=Entrevista::find($id);
		$entrevista->fecha=Input::get('fecha');
		$entrevista->hora=Input::get('hora');

		//Avisamos que se guardo
		if ($entrevista->save()) {
			Session::flash('message', "Se ha actualizado la entrevista correctamente");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		return Redirect::to('entrevistas/programadas');
	}

	public function getEditar($id)
	{
		$entrevista = DB::table('entrevistas')
		->join('aspirantes', 'aspirantes.id', '=', 'entrevistas.aspirante')
		->join('alumnos', 'aspirantes.id_alumno', '=', 'alumnos.id')
		->select('alumnos.nombre','alumnos.a_paterno','alumnos.a_materno', 'entrevistas.estatus AS estatus_e','aspirantes.*','entrevistas.fecha','entrevistas.hora', 'entrevistas.id AS id')
		->where('entrevistas.id',$id)
		->first();

		return View::make('entrevistas.editar')->with(compact('entrevista'));
	}


	public function anyGuardar()
	{
		$entrevista= new Entrevista;
		$entrevista->aspirante=Input::get('id');
		$entrevista->fecha=Input::get('fecha');
		$entrevista->hora=Input::get('hora');

		//Avisamos que se guardo
		if ($entrevista->save()) {
			Session::flash('message', "Se ha programado la entrevista correctamente");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}
		return Redirect::to('entrevistas');
	}


}