<?php

class AspirantesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /aspirantes
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$aspirantes = DB::table('aspirantes')
		->join('alumnos', 'aspirantes.id_alumno', '=', 'alumnos.id')
		->join('programas', 'aspirantes.id_programa', '=', 'programas.id')
		->join('contacto', 'contacto.id_alumno', '=', 'alumnos.id')
		->join('unidades', 'alumnos.unidad_academica', '=', 'unidades.id')
		->where('aspirantes.estatus','PENDIENTE')
		->select('alumnos.nombre','alumnos.a_paterno','alumnos.a_materno','unidades.unidad as unidad', 'programas.programa','aspirantes.*','contacto.email')
		->orderBy('aspirantes.created_at', 'asc')->get();
		return View::make('aspirantes.ver')->with(compact('aspirantes'));
	}


	public function anyAccept($id)
	{
		$aspirante = Aspirante::find($id);
		$alumno = DB::table('alumnos')->where('id',$aspirante->id_alumno)->first();
		$programa = DB::table('programas')->where('id',$aspirante->id_programa)->first();
		$contacto = DB::table('contacto')->where('id_alumno',$alumno->id)->first();

		$aspirante->estatus="ACEPTADO";

		if ($aspirante->save()) {
			Session::flash('message', "Alumno aceptado al programa.");
			Session::flash('class', 'success');
			$this->email($contacto->email,$alumno->a_paterno." ".$alumno->a_materno." ".$alumno->nombre,$alumno->unidad_academica,$aspirante->estatus,$programa->programa);
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		$aspirantes = DB::table('aspirantes')
		->join('alumnos', 'aspirantes.id_alumno', '=', 'alumnos.id')
		->join('programas', 'aspirantes.id_programa', '=', 'programas.id')
		->join('contacto', 'contacto.id_alumno', '=', 'alumnos.id')
		->select('alumnos.nombre','alumnos.a_paterno','alumnos.a_materno','alumnos.unidad_academica as unidad', 'programas.programa','aspirantes.*','contacto.email')
		->orderBy('aspirantes.created_at', 'asc')->get();

		return Redirect::to('aspirantes')->with(compact('aspirantes'));
	}


	public function anyReject($id)
	{
		$aspirante = Aspirante::find($id);
		$alumno = DB::table('alumnos')->where('id',$aspirante->id_alumno)->first();
		$programa = DB::table('programas')->where('id',$aspirante->id_programa)->first();
		$contacto = DB::table('contacto')->where('id_alumno',$alumno->id)->first();

		$aspirante->estatus="RECHAZADO";

		if ($aspirante->save()) {
			Session::flash('message', "Alumno rechazado al programa.");
			Session::flash('class', 'success');
			$this->email($contacto->email,$alumno->a_paterno." ".$alumno->a_materno." ".$alumno->nombre,$alumno->unidad_academica,$aspirante->estatus,$programa->programa);
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		$aspirantes = DB::table('aspirantes')
		->join('alumnos', 'aspirantes.id_alumno', '=', 'alumnos.id')
		->join('programas', 'aspirantes.id_programa', '=', 'programas.id')
		->join('contacto', 'contacto.id_alumno', '=', 'alumnos.id')
		->select('alumnos.nombre','alumnos.a_paterno','alumnos.a_materno','alumnos.unidad_academica as unidad', 'programas.programa','aspirantes.*','contacto.email')
		->orderBy('aspirantes.created_at', 'asc')->get();

		return Redirect::to('aspirantes')->with(compact('aspirantes'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /aspirantes/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /aspirantes
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /aspirantes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /aspirantes/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /aspirantes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /aspirantes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	//Funcion de envio de correos
	public function email($correo,$nombre,$unidad,$respuesta,$programa){

		//Decidimos de que host enviar el mail

		//Host San Miguel
		if ($unidad==2) {
			$hostMail="smtp.mandrillapp.com";
			$sendMail="azael_dg@hotmail.com";
			$sendName="ENES - San Miguel";
			$sendPassword="SxFrf3XHZFIY9sLRlsY0Lg";
			if ($respuesta=="ACPTADO") {
				$mailTemplate="emails.admision_sma";
			}else{
				$mailTemplate="emails.rechazo_sma";
			}
		}
		
		//Host Leon
		if ($unidad==1) {
			$hostMail="smtp.mandrillapp.com";
			$sendMail="azael_dg@hotmail.com";
			$sendName="ENES - León";
			$sendPassword="SxFrf3XHZFIY9sLRlsY0Lg";
			if ($respuesta=="ACPTADO") {
				$mailTemplate="emails.admision_leon";
			}else{
				$mailTemplate="emails.rechazo_leon";
			}
		}


		//Cambiamos datos del mail host
		$transport = Mail::getSwiftMailer()->getTransport();
		$transport->setHost($hostMail);
		$transport->setEncryption('tls');
		Mail::alwaysFrom('educacion.continua@enes.unam.mx',$sendName);
		$transport->setEncryption('tls');
		$transport->setUsername($sendMail);
		$transport->setPassword($sendPassword);
		$swift = new Swift_Mailer($transport);

		Mail::setSwiftMailer($swift);

		//Tomamos los parametros para el envio
		$data=array(
			'respuesta'=>$respuesta,
			'unidad'=>$unidad,
			'nombre'=>$nombre,
			'programa'=>$programa,
		);

		//Agarramos al destinatario
		$destinatario=$correo;


		//Enviamos el mail
		Mail::send($mailTemplate,$data,function($confirm) use ($destinatario,$nombre){
			$confirm->to($destinatario, $nombre)->subject('Aviso admisión al programa ');
		});
		return Redirect::to('aspirantes');
	}

	/**
	 * Funcion llamada por Aspirantes Datatable
	 */
	public function getAspirantes() {
		// Es necesario utilizar un select explicito porque hay columnas con el mismo nombre en diferentes tablas
		$aspirantes = Aspirante::select('aspirantes.id',
																		'aspirantes.id_alumno',
																		'alumnos.a_paterno',
																		'alumnos.a_materno',
																		'alumnos.nombre',
																		'programas.programa',
																		'aspirantes.id_programa',
																		'unidades.unidad',
																		'alumnos.unidad_academica as id_unidad',
																		'aspirantes.estatus')
	    ->join('alumnos', 'aspirantes.id_alumno', '=', 'alumnos.id')
		  ->join('programas', 'aspirantes.id_programa', '=', 'programas.id')
		  ->join('unidades', 'alumnos.unidad_academica', '=', 'unidades.id')
		  ->where('aspirantes.estatus','PENDIENTE')
		  ->orderBy('aspirantes.created_at', 'asc')
		  ->get();

		// Datatables espera un arreglo de objetos JSON con el nombre 'data'
		return Response::json(array('data'=>$aspirantes));
	}

}