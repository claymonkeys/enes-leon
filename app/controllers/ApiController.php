<?php

class ApiController extends \BaseController {

	public function getEstados(){
		$estados = DB::table('estados')->orderBy('estado', 'asc')->get();
		return Response::json($estados);
	} 

	public function anyGrupos($aspirante_id){
		//$aspirante=DB::table('aspirantes')->where('id',$aspirante_id)->orderBy('id', 'asc')->first();
		//$grupos = DB::table('grupos')->where('programa',$aspirante->id_programa)->orderBy('id', 'asc')->get();
		//return Response::json($grupos);

		  $grupos = Aspirante::select('aspirantes.id as id_aspirante',
		  														'aspirantes.id_alumno',
		  														'grupos.id as id_grupo',
		  														'grupos.grupo as nombre_grupo')
		  ->join('programas', 'aspirantes.id_programa', '=', 'programas.id')
		  ->join('grupos', 'aspirantes.id_programa', '=', 'grupos.programa')
		  ->where('aspirantes.id', '=', $aspirante_id)
		  ->get();

		  return Response::json($grupos);
	} 

	public function getMunicipios($id_estado){
		$municipios = DB::table('municipios')->where('id_estado',$id_estado)->orderBy('municipio', 'asc')->get();
		return Response::json($municipios);
	} 

	public function getProgramas(){
		$programas = DB::table('programas')->where('tipo',$_GET['tipos'])->where('estatus','ACTIVO')->orderBy('created_at', 'asc')->get();
		return Response::json($programas);
	} 

	public function getTipos(){
		$tipos = DB::table('tipos')->orderBy('created_at', 'asc')->get();
		return Response::json($tipos);
	} 

	public function getNacionalidades(){
		$nacionalidades = DB::table('nacionalidades')->orderBy('created_at', 'asc')->get();
		return Response::json($nacionalidades);
	} 

	public function getModalidades(){
		$modalidades = DB::table('modalidad')->orderBy('created_at', 'asc')->get();
		return Response::json($modalidades);
	} 

	public function getAdmision(){
		$admision = DB::table('admision')->orderBy('created_at', 'asc')->get();
		return Response::json($admision);
	} 

	public function getUnidades(){
		$unidades = DB::table('unidades')->orderBy('created_at', 'asc')->get();
		return Response::json($unidades);
	} 

	public function getUnidad($id_academico){
		$academico= Maestro::find($id_academico);
		$unidad = DB::table('unidades')->where('id',$academico->unidad_academica)->orderBy('a_paterno', 'asc')->get();
		return Response::json($unidad);
	} 


	public function getMaestros(){
		$maestros = DB::table('academicos')->orderBy('created_at', 'asc')->get();
		return Response::json($maestros);
	} 

	public function getProgramaGrupo(){
		$programas = DB::table('programas')->where('estatus','ACTIVO')->orderBy('created_at', 'asc')->get();
		return Response::json($programas);
	} 

	public function anyValidacion($email){
		$validador;
		$programas = DB::table('users')->where('email',$email)->orderBy('created_at', 'asc')->get();
		if ($programas!=null) {
			$validador=1;
		}else{
			$validador=0;
		}
		return Response::json($validador);
	} 
}
