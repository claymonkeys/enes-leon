<?php

class ProgramasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('admin.home');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		// Arrays para llenar selects estáticos
		$tipos = DB::table('tipos')->orderBy('id', 'asc')->lists('tipo_programa', 'id');
		$modalidad = DB::table('modalidad')->orderBy('id', 'asc')->lists('modalidad', 'id');
		$admision = DB::table('admision')->orderBy('id', 'asc')->lists('admision', 'id');

		// Opción por default de los select
		$default = array(''=>'Seleccione una opción');
		
		// Concatenar opcion por default
		$tipos = $default + $tipos;
		$modalidad = $default + $modalidad;
		$admision = $default + $admision;

		return View::make('programas.crear')->with(compact('tipos', 'modalidad', 'admision'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function anyStore()
	{
		$programa=new Programa;
		$programa->programa=strtoupper(Input::get('programa'));
		$programa->tipo=strtoupper(Input::get('tipo_programa'));
		$programa->modalidad=strtoupper(Input::get('modalidad'));
		$programa->admision=strtoupper(Input::get('admision'));
		$programa->fecha_inicio=strtoupper(Input::get('fecha_inicio'));
		$programa->fecha_fin=strtoupper(Input::get('fecha_fin'));
		
		$estatus = Input::get('estatus');
		
		if (isset($estatus)) {
			$programa->estatus='ACTIVO';
		} else {
			$programa->estatus='INACTIVO';
		}

		if ($programa->save()) {
			Session::flash('message', "Programa Creado Correctamente.");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		$programas = DB::table('programas')->orderBy('created_at', 'asc')->get() ;
		return Redirect::to('programas/create')->with(compact('programas'));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		$programa = Programa::find($id);
		$programas = DB::table('programas')->where('id',$id)->orderBy('created_at', 'asc')->get();
		//return View::make('programas.actualizar')->with(compact('programas','programa'));
		return Response::json($programa);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	
	// public function anyActivar($id)
	// {
	// 	$programa = Programa::find($id);

	// 	$programa->estatus="ACTIVO";

	// 	if ($programa->save()) {
	// 		Session::flash('message', "Programa Habilitado.");
	// 		Session::flash('class', 'success');
	// 	} else {
	// 		Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
	// 		Session::flash('class', 'danger');
	// 	}

	// 	$programas = DB::table('programas')->orderBy('created_at', 'asc')->get() ;
	// 	//return Redirect::to('programas/create')->with(compact('programas'));
	// }


	
	// public function anyDesactivar($id)
	// {
	// 	$programa = Programa::find($id);

	// 	$programa->estatus="INACTIVO";

	// 	if ($programa->save()) {
	// 		Session::flash('message', "Programa Deshabilitado.");
	// 		Session::flash('class', 'success');
	// 	} else {
	// 		Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
	// 		Session::flash('class', 'danger');
	// 	}

	// 	$programas = DB::table('programas')->orderBy('created_at', 'asc')->get() ;
	// 	//return Redirect::to('programas/create')->with(compact('programas'));
	// }




	public function anyUpdate($id)
	{
		$programa = Programa::find($id);

		$programa->programa=strtoupper(Input::get('programa'));
		$programa->tipo=strtoupper(Input::get('tipo_programa'));
		$programa->modalidad=strtoupper(Input::get('modalidad'));
		$programa->admision=strtoupper(Input::get('admision'));
		$programa->fecha_inicio=strtoupper(Input::get('fecha_inicio'));
		$programa->fecha_fin=strtoupper(Input::get('fecha_fin'));

		$estatus = Input::get('estatus');
		
		if (isset($estatus)) {
			$programa->estatus='ACTIVO';
		} else {
			$programa->estatus='INACTIVO';
		}

		if ($programa->save()) {
			Session::flash('message', "Programa actualizado correctamente.");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		$programas = DB::table('programas')->orderBy('created_at', 'asc')->get() ;
		return Redirect::to('programas/create')->with(compact('programas'));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDestroy($id)
	{
		//busco al programa
		$programa = Programa::find($id);

		//elimino el programa
		if ($programa->delete()) {
			Session::flash('message', "Programa '$programa->programa' eliminado exitosamente");
			Session::flash('class', 'warning');
		} else {
			Session::flash('message', 'Ha ocurrido un error');
			Session::flash('class', 'danger');
		}
		
		$programas = DB::table('programas')->orderBy('created_at', 'asc')->get() ;
		return Redirect::to('programas/create')->with(compact('programas'));
	}

	/**
	 * Funcion llamada por Programas Datatable
	 */
	public function getProgramas() {
		// Es necesario utilizar un select explicito porque hay columnas con el mismo nombre en diferentes tablas
		$programas = Programa::select('programas.id',
																	'programas.programa',
																	'programas.fecha_inicio',
																	'programas.fecha_fin',
																	'programas.estatus',
																	'admision.admision',
																	'modalidad.modalidad',
																	'tipos.tipo_programa')
	    ->join('admision', 'programas.admision', '=', 'admision.id')
		  ->join('modalidad', 'programas.modalidad', '=', 'modalidad.id')
		  ->join('tipos', 'tipos.id', '=', 'programas.tipo')
		  ->orderBy('programas.created_at', 'asc')
		  ->get();

		// Datatables espera un arreglo de objetos JSON con el nombre 'data'
		return Response::json(array('data'=>$programas));
	} 

}
