<?php

class SessionsController extends \BaseController {

	/**
	 * Show the form for creating a new resource.
	 * GET /sessions/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('sessions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /sessions
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$attempt = Auth::attempt([
				'username' => $input['username'],
				'password' => $input['password']
			]);

		if ($attempt) {
			$privilegios = Auth::user()->privilegios;
			if ($privilegios == 2) {
				return Redirect::intended('/')->with('message', 'Ha ingresado al sistema')->with('class', 'success');
			}
			if ($privilegios == 1) {
				return Redirect::to('alumnos')->with('message', 'Ha ingresado al sistema')->with('class', 'success');
			}
		} else {
			return Redirect::back()->with('message', 'Datos de ingreso no válidos')->with('class', 'danger');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /sessions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
		return Redirect::home()->with('message', 'Ha salido del sistema')->with('class', 'info');
	}

}