<?php

class AlumnoGruposController extends \BaseController {


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function anyCreate()
	{
		$grupoAlumno = new GrupoAlumno;

		$grupoAlumno->grupo=Input::get('grupo');
		$grupoAlumno->alumno=Input::get('alumno');

		//dd($grupoAlumno->grupo);

		$aspirante = Aspirante::find(Input::get('aspirante'));
		// Cambiar estatus de aspirante de PENDIENTE a ACEPTADO
		$aspirante->estatus="ACEPTADO";

		//Avisamos que se guardo el alumno en un grupo
		if ($grupoAlumno->save()) {
			// Guardar también cambios de aspirante
			$aspirante->save();
			Session::flash('message', "Alumno asignado al grupo.");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}
		return Redirect::to('aspirantes');
	}


}
