<?php

class GruposController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$grupos = DB::table('grupos')
		->join('academicos','academicos.id','=','grupos.academico')
		->join('programas','programas.id','=','grupos.programa')
		->select('grupos.id AS id', 
		         'grupos.grupo AS grupo', 
		         'academicos.nombre AS nombre',
		         'grupos.dias AS dias',
		         'grupos.horarios AS horarios',
		         'grupos.cupo AS cupo',
		         'academicos.a_paterno AS a_paterno', 
		         'academicos.a_materno AS a_materno',
		         'programas.programa AS programa',
		         'programas.fecha_inicio AS inicio',
		         'programas.fecha_fin AS fin')
		->get();
		$maestros=DB::table('academicos')->get();

		return View::make('grupos.crear')->with(compact('grupos','maestros'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function anyStore()
	{
		$grupo= new Grupo;
		$grupo->academico=Input::get('maestro');
		$grupo->grupo=strtoupper(Input::get('grupo'));

		$Lunes=Input::get('Lunes');
		$Martes=Input::get('Martes');
		$Miercoles=Input::get('Miercoles');
		$Jueves=Input::get('Jueves');
		$Viernes=Input::get('Viernes');
		$Sabado=Input::get('Sabado');

		$inicio1=Input::get('inicio');
		$inicio2=Input::get('inicio2');
		$inicio3=Input::get('inicio3');
		$inicio4=Input::get('inicio4');
		$inicio5=Input::get('inicio5');
		$inicio6=Input::get('inicio6');

		$fin1=Input::get('fin');
		$fin2=Input::get('fin2');
		$fin3=Input::get('fin3');
		$fin4=Input::get('fin4');
		$fin5=Input::get('fin5');
		$fin6=Input::get('fin6');

		$horarios="";
		$dias="";

		if(isset($Lunes) and $inicio1!="" and $fin1!=""){
			$dias.=", LUNES";
			$horarios.=", ".$inicio1."-".$fin1;
		}
		if(isset($Martes) and $inicio2!="" and $fin2!=""){
			$dias.=", MARTES";
			$horarios.=", ".$inicio2."-".$fin2;
		}
		if(isset($Miercoles) and $inicio3!="" and $fin3!=""){
			$dias.=", MIERCOLES";
			$horarios.=", ".$inicio3."-".$fin3;
		}
		if(isset($Jueves) and $inicio4!="" and $fin4!=""){
			$dias.=", JUEVES";
			$horarios.=", ".$inicio4."-".$fin4;	
		}
		if(isset($Viernes) and $inicio5!="" and $fin5!=""){
			$dias.=", VIERNES";
			$horarios.=", ".$inicio5."-".$fin5;
		}
		if(isset($Sabado) and $inicio6!="" and $fin6!=""){
			$dias.=", SABADO";
				$horarios.=", ".$inicio6."-".$fin6;
		}
		
		$grupo->dias=$dias;
		$grupo->horarios=$horarios;
		$grupo->cupo=Input::get('cupo');
		$grupo->programa=Input::get('programa');

		$grupo->save();

		return Redirect::to('grupos/create');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyVer($id)
	{
		$alumnos = DB::table('grupos-alumnos')->
		select(
			'alumnos.nombre',
			'alumnos.id AS alumno_id',
			'alumnos.a_paterno',
			'alumnos.a_materno',
			'unidades.unidad',
			'grupos.grupo AS nombre_grupo',
			'grupos.id')
	    ->join('alumnos', 'grupos-alumnos.alumno', '=', 'alumnos.id')
		->join('grupos', 'grupos-alumnos.grupo', '=', 'grupos.id')
		->join('unidades', 'alumnos.unidad_academica', '=', 'unidades.id')
		->where('grupos-alumnos.grupo',$id)
		->orderBy('grupos-alumnos.created_at', 'asc')
		->get();

		$grupo=Grupo::find($id);

		$curso=DB::table('programas')->where('id',$grupo->programa)->first();

		return View::make('grupos.ver')->with(compact('alumnos','grupo','curso'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		$grupo=Grupo::find($id);
		$maestros=Maestro::select(DB::raw("CONCAT(a_paterno , ' ' , a_materno , ' ' , nombre) AS nombre,id"))->lists('nombre', 'id');
		$programas= DB::table("programas")->lists('programa', 'id');
		return View::make('grupos.editar')->with(compact('grupo','maestros','programas'));

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyUpdate($id)
	{
		$grupo=Grupo::find($id);
		$grupo->academico=Input::get('maestro');
		$grupo->grupo=strtoupper(Input::get('grupo'));

		$Lunes=Input::get('Lunes');
		$Martes=Input::get('Martes');
		$Miercoles=Input::get('Miercoles');
		$Jueves=Input::get('Jueves');
		$Viernes=Input::get('Viernes');
		$Sabado=Input::get('Sabado');

		$inicio1=Input::get('inicio');
		$inicio2=Input::get('inicio2');
		$inicio3=Input::get('inicio3');
		$inicio4=Input::get('inicio4');
		$inicio5=Input::get('inicio5');
		$inicio6=Input::get('inicio6');

		$fin1=Input::get('fin');
		$fin2=Input::get('fin2');
		$fin3=Input::get('fin3');
		$fin4=Input::get('fin4');
		$fin5=Input::get('fin5');
		$fin6=Input::get('fin6');

		$horarios="";
		$dias="";

		if(isset($Lunes) and $inicio1!="" and $fin1!=""){
			$dias.=", LUNES";
			$horarios.=", ".$inicio1."-".$fin1;
		}
		if(isset($Martes) and $inicio2!="" and $fin2!=""){
			$dias.=", MARTES";
			$horarios.=", ".$inicio2."-".$fin2;
		}
		if(isset($Miercoles) and $inicio3!="" and $fin3!=""){
			$dias.=", MIERCOLES";
			$horarios.=", ".$inicio3."-".$fin3;
		}
		if(isset($Jueves) and $inicio4!="" and $fin4!=""){
			$dias.=", JUEVES";
			$horarios.=", ".$inicio4."-".$fin4;	
		}
		if(isset($Viernes) and $inicio5!="" and $fin5!=""){
			$dias.=", VIERNES";
			$horarios.=", ".$inicio5."-".$fin5;
		}
		if(isset($Sabado) and $inicio6!="" and $fin6!=""){
			$dias.=", SABADO";
				$horarios.=", ".$inicio6."-".$fin6;
		}
		
		$grupo->dias=$dias;
		$grupo->horarios=$horarios;
		$grupo->cupo=Input::get('cupo');
		$grupo->programa=Input::get('programa');

		$grupo->save();

		return Redirect::to('grupos/create');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDestroy($id)
	{
		$grupo=Grupo::find($id);

		//elimino el grupo
		if ($grupo->delete()) {
			Session::flash('message', "Grupo eliminado exitosamente");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ha ocurrido un error');
			Session::flash('class', 'danger');
		}

		return Redirect::to('grupos/create')->with(compact('maestros'));
	}


	public function getDelete($id)
	{
		$grupoAlumno=DB::table('grupos-alumnos')->
		where('alumno',$id)->first();

		$target=GrupoAlumno::find($grupoAlumno->id);

		//elimino el grupo
		if ($target->delete()) {
			Session::flash('message', "Alumno eliminado del grupo exitosamente");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ha ocurrido un error');
			Session::flash('class', 'danger');
		}

		return Redirect::to('grupos/ver/'.$grupoAlumno->grupo);
	}




}
