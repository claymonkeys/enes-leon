<?php

class MestrosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		// Arrays para llenar selects estáticos
		$nacionalidades = DB::table('nacionalidades')->orderBy('id', 'asc')->lists('nacionalidad', 'id');
		$estados = DB::table('estados')->orderBy('id', 'asc')->lists('estado', 'id');
		$unidades = DB::table('unidades')->orderBy('id', 'asc')->lists('unidad', 'id');

		// Opción por default de los select
		$default = array(''=>'Seleccione una opción');
		
		// Concatenar opcion por default
		$nacionalidades = $default + $nacionalidades;
		$estados = $default + $estados;
		$unidades = $default + $unidades;

		return View::make('maestros.crear')->with(compact('nacionalidades', 'estados', 'unidades'));
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function anyStore()
	{
		//Creamos al maestro
		$academico=new Maestro;

		$academico->nombre=strtoupper(Input::get('nombre'));
		$academico->a_paterno=strtoupper(Input::get('a_paterno'));
		$academico->a_materno=strtoupper(Input::get('a_materno'));
		$academico->sexo=strtoupper(Input::get('sexo'));
		$academico->fecha_nac=Input::get('fecha_nac');
		$academico->unidad_academica=Input::get('unidad_academica');
		$academico->titulo_profesional=strtoupper(Input::get('titulo_profesional'));


		if ($academico->save()) {
	       	$academicoGuardado = true;
	    }

		//Recuperamos el id del maestro
	    $id_temp = DB::table('academicos')->where('nombre',Input::get('nombre'))
	    	->orderBy('created_at', 'desc')->first();

	    $contactoAcademico=new ContactoAcademico;
		$contactoAcademico->id_maestro=$id_temp->id;
		$contactoAcademico->calle=strtoupper(Input::get('calle'));
		$contactoAcademico->num_int=Input::get('num_int');
		$contactoAcademico->num_ext=Input::get('num_ext');
		$contactoAcademico->colonia=strtoupper(Input::get('colonia'));
		$contactoAcademico->ciudad=Input::get('ciudad');
		$contactoAcademico->estado=Input::get('estado');
		$contactoAcademico->nacionalidad=Input::get('nacionalidad');
		$contactoAcademico->cp=Input::get('cp');
		$contactoAcademico->rfc=strtoupper(Input::get('rfc'));
		$contactoAcademico->telefono=Input::get('telefono');
		$contactoAcademico->movil=Input::get('movil');
		$contactoAcademico->email=Input::get('email');

		if ($contactoAcademico->save()) {
			$contactoGuardado = true;
		}

		//Avisamos que se guardo
		if ($academicoGuardado and $contactoGuardado) {
			Session::flash('message', "Academico registrado correctamente.");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		return Redirect::to('maestros/create');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyShow($id)
	{
		$academico = Maestro::find($id);
		$contacto = DB::table('contacto_academicos')->where('id_maestro',$id)->first();
		$unidad = DB::table('unidades')->where('id',$academico->unidad_academica)->first();
		$estado = DB::table('estados')->where('id',$contacto->estado)->first();
		$municipio = DB::table('municipios')->where('id',$contacto->ciudad)->first();
		$nacionalidad = DB::table('nacionalidades')->where('id',$contacto->nacionalidad)->first();
		return View::make('maestros.ver')->with(compact('academico','contacto','estado','municipio','nacionalidad','unidad'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		$academico = Maestro::select('academicos.id',
																'academicos.nombre',
																'academicos.a_paterno',
																'academicos.a_materno',
																'academicos.sexo',
																'academicos.fecha_nac',
																'academicos.titulo_profesional',
																'contacto_academicos.telefono',
																'contacto_academicos.email',
																'estados.estado',
																'contacto_academicos.estado as id_estado',
																'municipios.municipio',
																'contacto_academicos.ciudad as id_municipio',
																'nacionalidades.nacionalidad',
																'contacto_academicos.nacionalidad as id_nacionalidad',
																'unidades.unidad',
																'academicos.unidad_academica as id_unidad',
																'contacto_academicos.calle',
																'contacto_academicos.num_ext',
																'contacto_academicos.num_int',
																'contacto_academicos.colonia',
																'contacto_academicos.cp',
																'contacto_academicos.telefono',
																'contacto_academicos.movil',
																'contacto_academicos.rfc')
	  	->join('contacto_academicos', 'academicos.id', '=', 'contacto_academicos.id_maestro')
	  	->join('municipios', 'contacto_academicos.ciudad', '=', 'municipios.id')
	  	->join('estados', 'contacto_academicos.estado', '=', 'estados.id')
	  	->join('nacionalidades', 'contacto_academicos.nacionalidad', '=', 'nacionalidades.id')
			->join('unidades', 'unidades.id', '=', 'academicos.unidad_academica')
			->find($id);
		//$contacto = DB::table('contacto_academicos')->where('id_maestro',$id)->first();
		//return View::make('maestros.actualizar')->with(compact('academico','contacto'));
		return Response::json($academico);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyUpdate($id)
	{
		$academico = Maestro::find($id);
		$contacto = DB::table('contacto_academicos')->where('id_maestro',$id)->first();

		$academico->nombre=strtoupper(Input::get('nombre'));
		$academico->a_paterno=strtoupper(Input::get('a_paterno'));
		$academico->a_materno=strtoupper(Input::get('a_materno'));
		$academico->sexo=strtoupper(Input::get('sexo'));
		$academico->fecha_nac=Input::get('fecha_nac');
		$academico->unidad_academica=Input::get('unidad_academica');
		$academico->titulo_profesional=strtoupper(Input::get('titulo_profesional'));


		if ($academico->save()) {
	       	$academicoGuardado = true;
	    }

		$contactoAcademico = ContactoAcademico::find($contacto->id_contacto);

		$contactoAcademico->calle=strtoupper(Input::get('calle'));
		$contactoAcademico->num_int=Input::get('num_int');
		$contactoAcademico->num_ext=Input::get('num_ext');
		$contactoAcademico->colonia=strtoupper(Input::get('colonia'));
		$contactoAcademico->ciudad=Input::get('ciudad');
		$contactoAcademico->estado=Input::get('estado');
		$contactoAcademico->nacionalidad=Input::get('nacionalidad');
		$contactoAcademico->cp=Input::get('cp');
		$contactoAcademico->rfc=strtoupper(Input::get('rfc'));
		$contactoAcademico->telefono=Input::get('telefono');
		$contactoAcademico->movil=Input::get('movil');
		$contactoAcademico->email=Input::get('email');


		if ($contactoAcademico->save()) {
			$contactoGuardado = true;
		}

		//Avisamos que se guardo
		if ($academicoGuardado and $contactoGuardado) {
			Session::flash('message', "Academico actualizado correctamente.");
			Session::flash('class', 'success');
		} else {
			Session::flash('message', 'Ups un error ha ocurrido, intentalo de nuevo');
			Session::flash('class', 'danger');
		}

		return Redirect::to('maestros/create');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function anyDestroy($id)
	{
		//busco al maestro
		$maestro = Maestro::find($id);

		//elimino el programa
		if ($maestro->delete()) {
			Session::flash('message', "Maestro '$maestro->nombre' eliminado exitosamente");
			Session::flash('class', 'warning');
		} else {
			Session::flash('message', 'Ha ocurrido un error');
			Session::flash('class', 'danger');
		}
		
		$maestros = DB::table('academicos')->orderBy('created_at', 'asc')->get() ;
		return Redirect::to('maestros/create')->with(compact('maestros'));
	}


	// public function anyNacionalidad() {
	// 	if(isset($_GET['id'])){
	// 		$academico=DB::table('academicos')->where('id',$_GET['id'])
	// 	        ->orderBy('created_at', 'desc')->first();

	// 		$contacto_academico=DB::table('contacto_academicos')->where('id_maestro',$academico->id)
	// 	        ->orderBy('created_at', 'desc')->first();
	// 	  	return Response::json($contacto_academico->nacionalidad);
	// 	}else{
	// 		return Response::json(null);
	// 	}
	// }

	// public function anyEstado() {
	// 	if(isset($_GET['id'])){
	// 		$academico=DB::table('academicos')->where('id',$_GET['id'])
	// 	        ->orderBy('created_at', 'desc')->first();

	// 		$contacto_academico=DB::table('contacto_academicos')->where('id_maestro',$academico->id)
	// 	        ->orderBy('created_at', 'desc')->first();
	// 	  	return Response::json($contacto_academico->estado);
	// 	}else{
	// 		return Response::json(null);
	// 	}
	// }

	// public function anyMunicipio() {
	// 	if(isset($_GET['id'])){
	// 		$academico=DB::table('academicos')->where('id',$_GET['id'])
	// 	        ->orderBy('created_at', 'desc')->first();

	// 		$contacto_academico=DB::table('contacto_academicos')->where('id_maestro',$academico->id)
	// 	        ->orderBy('created_at', 'desc')->first();
	// 	  	return Response::json($contacto_academico->ciudad);
	// 	}else{
	// 		return Response::json(null);
	// 	}
	// }

	// public function anyUnidad() {
	// 	if(isset($_GET['id'])){
	// 		$academico=DB::table('academicos')->where('id',$_GET['id'])
	//     	    ->orderBy('created_at', 'desc')->first();

	//   		return Response::json($academico->unidad_academica);
	//   	}else{
	//   		return Response::json(null);
	//   	}
	// }

	public function getMaestros() {
		// Es necesario utilizar un select explicito porque hay columnas con el mismo nombre en diferentes tablas
		$maestros = Maestro::select('academicos.id',
																'academicos.nombre',
																'academicos.a_paterno',
																'academicos.a_materno',
																'academicos.sexo',
																'academicos.fecha_nac',
																'academicos.titulo_profesional',
																'contacto_academicos.telefono',
																'contacto_academicos.email',
																'municipios.municipio',
																'estados.estado',
																'nacionalidades.nacionalidad',
																'unidades.unidad')
	  	->join('contacto_academicos', 'academicos.id', '=', 'contacto_academicos.id_maestro')
	  	->join('municipios', 'contacto_academicos.ciudad', '=', 'municipios.id')
	  	->join('estados', 'contacto_academicos.estado', '=', 'estados.id')
	  	->join('nacionalidades', 'contacto_academicos.nacionalidad', '=', 'nacionalidades.id')
			->join('unidades', 'unidades.id', '=', 'academicos.unidad_academica')
			->orderBy('academicos.created_at', 'asc')->get();

		// Datatables espera un arreglo de objetos JSON con el nombre 'data'
		return Response::json(array('data'=>$maestros));
	} 

}
