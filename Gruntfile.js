module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';',
        sourceMap: true
      },
      admin: {
        src: ['bower_components/jquery/dist/jquery.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/affix.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/alert.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/button.js',
              //'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/carousel.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/collapse.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/dropdown.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/modal.js',
              //'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/scrollspy.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tab.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/transition.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tooltip.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/popover.js',
              //'bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js',
              'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
              'bower_components/jquery-validation/dist/jquery.validate.js',
              'bower_components/datatables/media/js/jquery.dataTables.js',
              'bower_components/datatables-bootstrap3-plugin/media/js/datatables-bootstrap3.js',
              'bower_components/datatables-responsive/js/dataTables.responsive.js',
              //'bower_components/smartmenus/dist/jquery.smartmenus.js',
              'app/assets/js/comun.js',
              'app/assets/js/admin.js'],
        dest: 'public/assets/js/admin.min.js'
      },
      alumnos: {
        src: ['bower_components/jquery/dist/jquery.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/affix.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/alert.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/button.js',
              //'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/carousel.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/collapse.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/dropdown.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/modal.js',
              //'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/scrollspy.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tab.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/transition.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tooltip.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/popover.js',
              'bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js',
              'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
              'bower_components/jquery-validation/dist/jquery.validate.js',
              //'bower_components/datatables/media/js/jquery.dataTables.js',
              //'bower_components/datatables-bootstrap3-plugin/media/js/datatables-bootstrap3.js',
              //'bower_components/smartmenus/dist/jquery.smartmenus.js',
              'app/assets/js/comun.js',
              'app/assets/js/alumnos.js'],
        dest: 'public/assets/js/alumnos.min.js'
      }
    },
    uglify: {
      admin: {
        options: {
          mangle: false, // Keep the names of functions and variables unchanged
          sourceMap: true,
          //sourceMapIncludeSources : true,
          sourceMapIn: 'public/assets/js/admin.min.js.map'
        },
        files: {
          'public/assets/js/admin.min.js': 'public/assets/js/admin.min.js'
        }
      },
      alumnos: {
        options: {
          mangle: false, // Keep the names of functions and variables unchanged
          sourceMap: true,
          //sourceMapIncludeSources : true,
          sourceMapIn: 'public/assets/js/alumnos.min.js.map'
        },
        files: {
          'public/assets/js/alumnos.min.js': 'public/assets/js/alumnos.min.js'
        }
      },
    },
    sass: {
      css_dist: {
        options: {
          sourcemap: 'none',
          style: 'compressed'
        },
        files: {
          'public/assets/css/app.min.css': 'app/assets/scss/app.scss' 
        }
      }
    },
    copy: {
      font_awesome: {
        files: [
          {
            expand: true,
            filter: 'isFile',
            flatten: true,
            //cwd: 'bower_components/',
            src: 'bower_components/font-awesome/fonts/**',
            dest: 'public/assets/fonts'
          }
        ]
      }
    },
    watch: {
      javascript: {
        files: ['app/assets/js/comun.js', 'app/assets/js/admin.js', 'app/assets/js/alumnos.js'],
        tasks: ['concat:admin','uglify:admin', 'concat:alumnos', 'uglify:alumnos'],
        options: {
          livereload: true
        }
      },
      sass: {
        files: ['app/assets/scss/*.scss'],
        tasks: ['sass:css_dist'],
        options: {
          livereload: true
        }
      },
      views: {
        files: ['app/views/**/*.php'],
        options: {
          livereload: true
        }
      },
      grunt: {
        files: ['Gruntfile.js'],
        tasks: ['build'],
        options: {
          livereload: true
        }
      }
    }
  });

  // Load plugins
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Define tasks
  grunt.registerTask('build', ['concat','uglify','sass','copy']);
  grunt.registerTask('default', ['build','watch']);

};